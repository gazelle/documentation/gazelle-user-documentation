---
title:  Installation Manual
subtitle: UPI Sedex Simulator
author: Alexandre POCINHO
releasedate: 2022-02-23
toolversion: 1.0.0
function: Software Engineer
version: 1.00
status: Validated document
customer: IHE EUROPE
reference: KER1-MAN-IHE_EUROPE-UPI_SIMULATOR_USER-1_00
---
UPI Simulator is a mock for eCH-0213 and eCH-0214 standards of Swizterland ZAS for managing and querying SPID to/from UPI (Unique Person Identification).
- [General page for UPI](https://www.zas.admin.ch/zas/fr/home/partenaires-et-institutions-/unique-person-identification--upi-/identifiant-du-dossier-electronique-du-patient.html)

# Installation Guide

## Prequisites

- Installation of java 11 on your host : see wildfly18 installation guide below 
- Installation of Maven on your host : https://maven.apache.org/install.html
- Installation of Wildfly18 on your host : https://gazelle.ihe.net/gazelle-documentation/General/wildfly18.html

## Installation steps
The sources of the projects are available on [Inria's gitlab](https://gitlab.inria.fr/gazelle/public/simulation/upi-simulator.git).


1. Get the project from Nexus.
```
cd /tmp
wget https://gazelle.ihe.net/nexus/content/repositories/releases/net/ihe/gazelle/UPISimulator/1.0.0/UPISimulator-1.0.0.war
```

2. Move the WAR file on the deployment folder in Wildfly18.
```
mv UPISimulator-1.0.0.war /usr/local/wildfly18/standalone/deployments
```
3. Start the wildfly18 service.
```
sudo service wildfly18 start
```