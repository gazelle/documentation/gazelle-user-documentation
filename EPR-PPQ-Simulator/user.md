---
title:  User Manual
subtitle: PPQ Simulator
author: Youn Cadoret
function: Developer
date: 02/06/2019
toolversion: 1.3
version: 1.0
status: Approved Document
reference: KER1-MAN-IHE-PPQ_SIMULATOR_-0_01
customer: IHE-EUROPE
---


eHealthSuisse PPQ Repository MockUp
-----------------------------------

eHealthSuisse PPQ Repository MockUp simulates a PPQ Repository actor.

PPQ transactions are used by the policy source to add, update or delete policies stored in a policy repository  
and use policy consumer to retrieve policies in the repository.  
To do that, PPQ is build around of four different requests : **XACMLPolicyQuery, AddPolicyRequest, UpdatePolicyRequest** and **DeletePolicyRequest**

**XACMLPolicyQuery :**  
The Policy Consumer sends this message when it needs to retrieve existing XACML policies  
or policy sets of a patient stored in a Policy Repository (of the patients referenceCommunity).

**AddPolicyRequest** and **UpdatePolicyRequest :**  
The Policy Source sends these messages when it needs to add new or update existing  
policies and/or policy sets stored within the Policy Repository (of a patients referenceCommunity).

**DeletePolicyRequest :**  
The Policy Source sends these messages when it needs to delete existing patient-specific  
policy sets stored within the Policy Repository (of a patient?s referenceCommunity).



### Data Set

#### Healthcare Professional data


| subject-id     | subject-id-qualifier | IdP Simulator username |
|----------------|----------------------|------------------------|
| 7601000050717  |  urn:gs1:gln     |        magpar          |
| 7601002033572  |  urn:gs1:gln     |        rspieler        |
| 7601002469191  |  urn:gs1:gln     |        aandrews        |


#### Patient data

| extension-id | root-id | homeCommunityId | IdP Simulator username  |  PolicySetId      |
|--------------|---------|-----------------|-------------------------|--------|
|  761337610455909127            |  2.16.756.5.30.1.127.3.10.3       | urn:oid:1.3.6.1.4.1.21367.2017.2.6.2                |  aamrein                | x |
|  761337610436974489            |  2.16.756.5.30.1.127.3.10.3       | urn:oid:1.3.6.1.4.1.21367.2017.2.6.2                |  lavdic                 | x |    
|  761337610435209810            |  2.16.756.5.30.1.127.3.10.3       | urn:oid:1.3.6.1.4.1.21367.2017.2.6.2                |  bovie                  | urn:uuid:6c446cb5-721d-4843-be57-91defc788561  |   



### End Point

[https://ehealthsuisse.ihe-europe.net:10443/ppq-repository?wsdl](https://ehealthsuisse.ihe-europe.net:10443/ppq-repository?wsdl)

It requires TLS mutual authentication with testing certificate (from GSS PKI). [The wsdl can be browsed here](/ppq-repository?wsdl)



### XACMLPolicyQuery with Patient ID example

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xd="http://www.w3.org/2000/09/xmldsig#">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
   <wsa:Action>urn:e-health-suisse:2015:policy-administration:PolicyQuery</wsa:Action>
      <wsa:MessageID>807cf78a-36a0-4403-82d0-11f95d877188</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To>http://ehealthsuisse.ihe-europe.net/ppq-repository</wsa:To>
      <wss:Security xmlns:wss="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
     		<!-- Add an assertion here <saml2:Assertion.... -->
      </wss:Security>
   </soap:Header>
   <soap:Body>
      <xacml-samlp:XACMLPolicyQuery xmlns:xacml="urn:oasis:names:tc:xacml:2.0:policy:schema:os" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="cb8e5238-0a9f-4766-9179-b2e765957bbb" Version="2.0" IssueInstant="2019-02-05T14:58:39.274+01:00">
         <xacml-context:Request xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" xmlns:hl7="urn:hl7-org:v3">
            <xacml-context:Subject/>
            <xacml-context:Resource>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:epr-spid" DataType="urn:hl7-org:v3#II">
                  <xacml-context:AttributeValue>
                        <hl7:InstanceIdentifier xsi:type="hl7:II" root="2.16.756.5.30.1.127.3.10.3" extension="761337610455909127"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Resource>
            <xacml-context:Action/>
            <xacml-context:Environment/>
         </xacml-context:Request>
      </xacml-samlp:XACMLPolicyQuery>
   </soap:Body>
</soap:Envelope>
```


### XACMLPolicyQuery with PolicySetIdReference example

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xd="http://www.w3.org/2000/09/xmldsig#">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
   <wsa:Action>urn:e-health-suisse:2015:policy-administration:PolicyQuery</wsa:Action>
      <wsa:MessageID>807cf78a-36a0-4403-82d0-11f95d877188</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To>http://ehealthsuisse.ihe-europe.net/ppq-repository</wsa:To>
      <wss:Security xmlns:wss="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
     		<!-- Add an assertion here <saml2:Assertion.... -->
      </wss:Security>
   </soap:Header>
   <soap:Body>
      <xacml-samlp:XACMLPolicyQuery ID="cb8e5238-0a9f-4766-9179-b2e765957bbb" Version="2.0" IssueInstant="2019-02-12T14:08:21.963Z" xmlns:xacml="urn:oasis:names:tc:xacml:2.0:policy:schema:os" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <xacml:PolicySetIdReference>urn:uuid:451fb3f6-7b35-4344-bee1-ddc26018cab3</xacml:PolicySetIdReference>
      </xacml-samlp:XACMLPolicyQuery>
   </soap:Body>
</soap:Envelope>
```


### AddPolicyRequest example

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xd="http://www.w3.org/2000/09/xmldsig#" xmlns:xe="http://www.w3.org/2001/04/xmlenc#">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
      <wsa:Action>urn:e-health-suisse:2015:policy-administration:AddPolicy</wsa:Action>
      <wsa:MessageID>306de2f9-f272-4eff-bedc-45e8ea825575</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To>https://ehealthsuisse.ihe-europe.net/ppq-repository?wsdl</wsa:To>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
		<!-- Add an assertion here <saml2:Assertion.... -->
	</wsse:Security>
   </soap:Header>
   <soap:Body>
        <epr:AddPolicyRequest xmlns:hl7="urn:hl7-org:v3" xmlns:xacml="urn:oasis:names:tc:xacml:2.0:policy:schema:os"
                                  xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:epr="urn:e-health-suisse:2015:policy-administration"
                                  xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os"
                                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol"
                                  xmlns:xacml-saml="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:assertion">
             <saml:Assertion Version="2.0" ID="\_b882d240-45cd-4fa3-aa9f-65394115d0e0" IssueInstant="2019-03-06T14:28:35.249Z">
                 <saml:Issuer NameQualifier="urn:e-health-suisse:community-index">urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</saml:Issuer>
                <saml:Statement xsi:type="xacml-saml:XACMLPolicyStatementType">
                    <xacml:PolicySet PolicySetId="urn:uuid:451fb3f6-7b35-4344-bee1-ddc26018cab3" PolicyCombiningAlgId="urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:deny-overrides">
                      <xacml:Description>Patient specific PolicySet for EPD Setup 201 - granting full access to a patient at EPD setup</xacml:Description>
                      <xacml:Target>
                         <xacml:Subjects>
                            <xacml:Subject>
                               <xacml:SubjectMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                                   <xacml:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#anyURI">761337610435209810</xacml:AttributeValue>
                                  <xacml:SubjectAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id" DataType="http://www.w3.org/2001/XMLSchema#string"/>
                               </xacml:SubjectMatch>
                               <xacml:SubjectMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:anyURI-equal">
                                   <xacml:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#anyURI">urn:e-health-suisse:2015:epr-spid</xacml:AttributeValue>
                                  <xacml:SubjectAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier" DataType="http://www.w3.org/2001/XMLSchema#string"/>
                               </xacml:SubjectMatch>
                               <xacml:SubjectMatch MatchId="urn:hl7-org:v3:function:CV-equal">
                                  <xacml:AttributeValue DataType="urn:hl7-org:v3#CV">
                                      <hl7:CodedValue code="PAT" codeSystem="2.16.756.5.30.1.127.3.10.6"/>
                                  </xacml:AttributeValue>
                                  <xacml:SubjectAttributeDesignator DataType="urn:hl7-org:v3#CV" AttributeId="urn:oasis:names:tc:xacml:2.0:subject:role"/>
                               </xacml:SubjectMatch>
                            </xacml:Subject>
                         </xacml:Subjects>
                         <xacml:Resources>
                            <xacml:Resource>
                               <xacml:ResourceMatch MatchId="urn:hl7-org:v3:function:II-equal">
                                  <xacml:AttributeValue DataType="urn:hl7-org:v3#II">
                                      <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610435209810"/>
                                  </xacml:AttributeValue>
                                  <xacml:ResourceAttributeDesignator DataType="urn:hl7-org:v3#II" AttributeId="urn:e-health-suisse:2015:epr-spid"/>
                               </xacml:ResourceMatch>
                            </xacml:Resource>
                         </xacml:Resources>
                      </xacml:Target>
                      <xacml:PolicySetIdReference>urn:e-health-suisse:2015:policies:access-level:full</xacml:PolicySetIdReference>
                   </xacml:PolicySet>
                </saml:Statement>
             </saml:Assertion>
      </epr:AddPolicyRequest>
   </soap:Body>
</soap:Envelope>
```


### UpdatePolicyRequest example

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xd="http://www.w3.org/2000/09/xmldsig#" xmlns:xe="http://www.w3.org/2001/04/xmlenc#">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
      <wsa:Action>urn:e-health-suisse:2015:policy-administration:UpdatePolicy</wsa:Action>
      <wsa:MessageID>306de2f9-f272-4eff-bedc-45e8ea825575</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To>https://ehealthsuisse.ihe-europe.net/ppq-repository?wsdl</wsa:To>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
		<!-- Add an assertion here <saml2:Assertion.... -->
	</wsse:Security>
   </soap:Header>
   <soap:Body>
            <epr:UpdatePolicyRequest xmlns:hl7="urn:hl7-org:v3" xmlns:xacml="urn:oasis:names:tc:xacml:2.0:policy:schema:os"
                                  xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:epr="urn:e-health-suisse:2015:policy-administration"
                                  xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os"
                                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol"
                                  xmlns:xacml-saml="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:assertion">
             <saml:Assertion Version="2.0" ID="\_b882d240-45cd-4fa3-aa9f-65394115d0e0" IssueInstant="2019-03-06T14:28:35.249Z">
                 <saml:Issuer NameQualifier="urn:e-health-suisse:community-index">urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</saml:Issuer>
                     <saml:Statement xsi:type="xacml-saml:XACMLPolicyStatementType">
                    <xacml:PolicySet PolicySetId="urn:uuid:451fb3f6-7b35-4344-bee1-ddc26018cab3" PolicyCombiningAlgId="urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:deny-overrides">
                      <xacml:Description>Patient specific PolicySet for EPD Setup 201 - granting full access to a patient at EPD setup</xacml:Description>
                      <xacml:Target>
                         <xacml:Subjects>
                            <xacml:Subject>
                               <xacml:SubjectMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                                   <xacml:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#anyURI">761337610435209810</xacml:AttributeValue>
                                  <xacml:SubjectAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id" DataType="http://www.w3.org/2001/XMLSchema#string"/>
                               </xacml:SubjectMatch>
                               <xacml:SubjectMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:anyURI-equal">
                                   <xacml:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#anyURI">urn:e-health-suisse:2015:epr-spid</xacml:AttributeValue>
                                  <xacml:SubjectAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier" DataType="http://www.w3.org/2001/XMLSchema#string"/>
                               </xacml:SubjectMatch>
                               <xacml:SubjectMatch MatchId="urn:hl7-org:v3:function:CV-equal">
                                  <xacml:AttributeValue DataType="urn:hl7-org:v3#CV">
                                      <hl7:CodedValue code="PAT" codeSystem="2.16.756.5.30.1.127.3.10.6"/>
                                  </xacml:AttributeValue>
                                  <xacml:SubjectAttributeDesignator DataType="urn:hl7-org:v3#CV" AttributeId="urn:oasis:names:tc:xacml:2.0:subject:role"/>
                               </xacml:SubjectMatch>
                            </xacml:Subject>
                         </xacml:Subjects>
                         <xacml:Resources>
                            <xacml:Resource>
                               <xacml:ResourceMatch MatchId="urn:hl7-org:v3:function:II-equal">
                                  <xacml:AttributeValue DataType="urn:hl7-org:v3#II">
                                      <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610435209810"/>
                                  </xacml:AttributeValue>
                                  <xacml:ResourceAttributeDesignator DataType="urn:hl7-org:v3#II" AttributeId="urn:e-health-suisse:2015:epr-spid"/>
                               </xacml:ResourceMatch>
                            </xacml:Resource>
                         </xacml:Resources>
                      </xacml:Target>
                      <xacml:PolicySetIdReference>urn:e-health-suisse:2015:policies:access-level:full</xacml:PolicySetIdReference>
                 </xacml:PolicySet>
                 </saml:Statement>
             </saml:Assertion>
      </epr:UpdatePolicyRequest>
   </soap:Body>
</soap:Envelope>
```


### DeletePolicyRequest example

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xd="http://www.w3.org/2000/09/xmldsig#" xmlns:xe="http://www.w3.org/2001/04/xmlenc#">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
   <wsa:Action>urn:e-health-suisse:2015:policy-administration:DeletePolicy</wsa:Action>
      <wsa:MessageID>9db0ea75-e374-4718-a653-4614cafe4726</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To>http://ehealthsuisse.ihe-europe.net/ppq-repository</wsa:To>
      <wss:Security xmlns:wss="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
     <!-- Add an assertion here <saml2:Assertion.... -->
      </wss:Security>
   </soap:Header>
   <soap:Body>
      <epr:DeletePolicyRequest xmlns:epr="urn:e-health-suisse:2015:policy-administration"
                               xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                               xmlns:xacml="urn:oasis:names:tc:xacml:2.0:policy:schema:os" >
         <saml:Assertion Version="2.0" ID="\_9a71a18c-d7a7-4931-9168-150a8fbc5dc2" IssueInstant="">
            <saml:Issuer NameQualifier="urn:e-health-suisse:community-index">urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</saml:Issuer>
            <saml:Statement xsi:type="epr:XACMLPolicySetIdReferenceStatementType">
              <xacml:PolicySetIdReference>urn:uuid:53ec6db9-f79a-40ce-a8ab-7e5070210bf1</xacml:PolicySetIdReference>
            </saml:Statement>
         </saml:Assertion>
      </epr:DeletePolicyRequest>
   </soap:Body>
</soap:Envelope>
```
## Mock messages on GWT

Messages sent to the simulator can be found in Mock Messages feature of Gazelle Webservice Tester.
This feature is documented at : [https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html).

Messages exchanged with EPR-PPQ-Simulator can be found by filtering with the actor CH:POLICY_REPOSITORY.
