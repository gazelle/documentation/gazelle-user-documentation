---
title:  Jboss 5
subtitle: Installation
author: Anne-Gaëlle BERGE
date: 13/10/2016
toolversion:
function: Engineer
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-JBOSS_5_INSTALLATION-1_02
customer: IHE-EUROPE
---
# Purpose

This page describes the prerequisite to the installation of Gazelle
applications. All the tools developed in the context of the Gazelle
testbed project are developed for JBoss (5.0.1-GA or 7.2.0.final) and
use a postgreSQL database.

We recommand to install the Gazelle tools in a Debian-like environment,
it's the environment running on IHE Europe servers so we know that it is
correctly working. Moreover, most of the installation and configuration
procedures are described for such an environment.

# PostgreSQL

We are currenlty using posgreSQL 9.1 on most of our servers.

# Jboss 5.0.1-GA application server

## Install a JVM and Jboss AS

We usually use the Java virtual machine provided by Oracle. If you are
using Ubuntu, you can process as follows:

1.  Add this ppa to your sources list: deb
    http://ppa.launchpad.net/webupd8team/java/ubuntu precise main
2.  Install oracle-java6-installer and oracle-java6-set-default

``` bash
$> echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" | tee -a /etc/apt/sources.list
$> echo oracle-java6-installer shared/accepted-oracle-license-v1-1 boolean true | /usr/bin/debconf-set-selections
$> apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
$> apt-get update
$> apt-get install oracle-java6-installer oracle-java6-set-default
```

Then, install JBoss-5.0.1.GA, you will find a ZIP file
at http://freefr.dl.sourceforge.net/project/jboss/JBoss/JBoss-5.1.0.GA/jboss-5.1.0.GA-jdk6.zip;
unzip this file.

## Database driver

## Configure the server

Create a server to host Gazelle tools: copy
\${YOUR\_JBOSS}/server/default directory to
\${YOUR\_JBOSS}/server/gazelle. Make sure to correctly set the owner and
the rights of this new directory.

To secure your server, remove admin-console.war, jmx-console.war and
ROOT.war from your \${YOUR\_JBOSS}/server/gazelle/deploy directory.

## JBoss WS native

Our implementation of actors acting as SOAP clients uses the web service
framework of Jboss named JBoss WS native. By default, when you install
JBoss 5.1.0, Jboss WS native 3.1.2 is embedded. Unfortunately, this
version of the module contains some bugs and we have been forced to
update this framework to a more recent version: jbossws-native-3.4.0.GA.
This is the most recent version of this module which is compatible with
JBoss 5.1.0.GA. To upgrade JBoss WS native in your JBoss, please refer
to the documentation available on Jboss’s web
site:<https://community.jboss.org/wiki/JBossWS-Installation>
