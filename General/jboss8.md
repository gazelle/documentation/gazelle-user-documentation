---
title:  Jboss 8
subtitle: Installation
author: Benjamin CARRIOU
date: 23/08/2017
toolversion:
function: Engineer
version: 0.01
status: /!\ Not validated /!\
reference: KER1-MAN-IHE-JBOSS_8_INSTALLATION-0_01
customer: IHE-EUROPE
---
# Purpose

This page describes the prerequisite to the installation of Gazelle
applications. All the tools developed in the context of the Gazelle
testbed project are developed for JBoss (5.0.1-GA, 7.2.0.final or 8.0.0.Final) and
use a postgreSQL database.

We recommand to install the Gazelle tools in a Debian-like environment,
it's the environment running on IHE Europe servers so we know that it is
correctly working. Moreover, most of the installation and configuration
procedures are described for such an environment.

# PostgreSQL

We are currenlty using PostgreSQL 9.1 on most of our servers.

# Install a JVM

Most of our applications running on JBoss8 are using java 8. Consider
installing
[openJDK](http://openjdk.java.net/install/ "http://openjdk.java.net/install/").

``` bash
echo 'deb http://ftp.de.debian.org/debian jessie-backports main' >> /etc/apt/sources.list
sudo apt-get update && sudo apt-get install -t jessie-backports openjdk-8-jdk
```

# Install JBoss 8 application server

## Get package

from: [http://gazelle.ihe.net/jboss8/wildfly-8.0.0.Final.zip](http://gazelle.ihe.net/jboss8/wildfly-8.0.0.Final.zip "http://gazelle.ihe.net/jboss8/wildfly-8.0.0.Final.zip")

``` bash
wget -nv -O /tmp/wildfly-8.0.0.Final.zip http://gazelle.ihe.net/jboss8/wildfly-8.0.0.Final.zip
```

## Create init script

from: [http://gazelle.ihe.net/jboss8/init.d_jboss8](http://gazelle.ihe.net/jboss8/init.d_jboss8)

``` bash
wget -nv -O /tmp/init.d_jboss8 https://gazelle.ihe.net/jboss8/init.d_jboss8
```

## Install jboss in the /usr/local folder

``` bash
cd /usr/local
sudo mv /tmp/wildfly-8.0.0.Final.zip .
sudo unzip ./wildfly-8.0.0.Final.zip
sudo ln -s wildfly-8.0.0.Final jboss8
sudo rm -rf wildfly-8.0.0.Final.zip
sudo chown -R jboss:jboss-admin /usr/local/jboss8
sudo chmod -R 755 /usr/local/wildfly-8.0.0.Final
sudo mkdir /var/log/jboss8/
sudo chown -R jboss:jboss-admin /var/log/jboss8/
sudo chmod -R g+w /var/log/jboss8/
```

## Install the init script and make it start at system startup

``` bash
sudo mv /tmp/init.d_jboss8 /etc/init.d/jboss8
sudo chmod +x /etc/init.d/jboss8
sudo update-rc.d jboss8 defaults
```
