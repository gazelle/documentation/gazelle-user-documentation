---
title:  Wildfly 18
subtitle: Installation
author: Cédric Eoche-Duval
date: 20/01/2020
toolversion: 18.0.1.Final
releasedate: 2020-01-20
function: Lead developer
version: 1.01
status: To be reviewed
reference: KER1-MAN-IHE-WILDFLY_18_INSTALLATION-0_01
customer: IHE-EUROPE
---

# Purpose

This page describes the prerequisite to the installation of Gazelle
applications. All the tools developed in the context of the Gazelle
testbed project are developed for WildFly and JBoss (5.0.1-GA, 7.2.0.final, 8.0.0.Final, 10.0.0.Final, 18.0.1.Final) and
use a postgreSQL database.

We recommand to install the Gazelle tools in a Debian-like environment,
it's the environment running on IHE Europe servers so we know that it is
correctly working. Moreover, most of the installation and configuration
procedures are described for such an environment.

# PostgreSQL

We are currenlty using PostgreSQL 9.6 on most of our servers.

# Install a JVM

Our applications running on Wildfly18 are using java 11. Consider
installing
[openJDK](http://openjdk.java.net/install/ "http://openjdk.java.net/install/").

```bash
sudo apt-get update && sudo apt-get install openjdk-11-jre
```

# Install Wildfly 18 application server

## Get package and init script

Wildfly package can be downloaded from: [https://gazelle.ihe.net/wildfly18/wildfly-18.0.1.Final.zip](https://gazelle.ihe.net/wildfly18/wildfly-18.0.1.Final.zip "https://gazelle.ihe.net/wildfly18/wildfly-18.0.1.Final.zip")

```bash
wget -nv -O /tmp/wildfly-18.0.1.Final.zip https://gazelle.ihe.net/wildfly18/wildfly-18.0.1.Final.zip
```


init.d script can be downloaded from: [https://gazelle.ihe.net/wildfly18/init.d-wildfly18](https://gazelle.ihe.net/wildfly18/init.d-wildfly18)

```bash
wget -nv -O /tmp/init.d-wildfly18 https://gazelle.ihe.net/wildfly18/init.d-wildfly18
```

## Install jboss in the /usr/local folder

```bash
cd /usr/local
sudo mv /tmp/wildfly-18.0.1.Final.zip .
sudo unzip ./wildfly-18.0.1.Final.zip
sudo ln -s wildfly-18.0.1.Final wildfly18
sudo rm -rf wildfly-18.0.1.Final.zip
sudo chown -R jboss:jboss-admin /usr/local/wildfly-18.0.1.Final
sudo chmod -R 755 /usr/local/wildfly-18.0.1.Final
sudo mkdir /var/log/wildfly18/
sudo chown -R jboss:jboss-admin /var/log/wildfly18/
sudo chmod -R g+w /var/log/wildfly18/
```

## Install the init script and make it start at system startup

```bash
sudo mv /tmp/init.d-wildfly18 /etc/init.d/wildfly18
sudo chmod +x /etc/init.d/wildfly18
sudo chown root:root /etc/init.d/wildfly18
sudo update-rc.d wildfly18 defaults
```

Once the init.d script is setup, Wildfly server can be started, restarted, stopped or queried about its status using the commands:

```
sudo sudo systemctl start|restart|stop|status wildfly18.service
```

## Install or update PostgreSQL JDBC driver

1. Stop jboss and go to
```bash
sudo mkdir -p /usr/local/wildfly18/modules/org/postgresql/main
cd /usr/local/wildfly18/modules/org/postgresql/main
```

1. Download driver
```bash
sudo wget https://gazelle.ihe.net/wildfly18/postgresql-42.2.9.jar
sudo chown jboss:jboss-admin postgresql-42.2.9.jar
sudo chmod 775 postgresql-42.2.9.jar
```

1. Edit module.xml and update the version
```xml
    <module xmlns="urn:jboss:module:1.1" name="org.postgresql">
         <resources>
             <resource-root path="postgresql-42.2.9.jar"/>
         </resources>
         <dependencies>
             <module name="javax.api"/>
             <module name="javax.transaction.api"/>
         </dependencies>
    </module>
```

1. Add the driver to the standalone.xml file
```xml
    <datasources>
        ...
         <drivers>
            ...
            <driver name="postgresql" module="org.postgresql">
                <driver-class>org.postgresql.Driver</driver-class>
                <xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
            </driver>
        </drivers>
    </datasources>
```

1. Clean up your jboss
```bash
sudo rm -rf /usr/local/wildfly18/standalone/tmp/
sudo rm -rf /usr/local/wildfly18/standalone/data/
```

1. Restart Wildfly server and deploy your artifact


# Setup datasources for Gazelle applications

Most of Gazelle projects have extracted datasources. In order to deploy those projects, you will need to include some special 
instructions in your wildfly server configuration file :

Stop Wildfly and edit standalone.xml in /usr/local/YOUR_JBOSS_SERVER/standalone/configuration folder and update datasources :

```xml
    <datasources>
        ...
        <datasource pool-name="YOUR_TOOL_DATASOURCES" jndi-name="java:jboss/datasources/YOUR_TOOL_DATASOURCES"
                enabled="true" use-java-context="true">
            <connection-url>jdbc:postgresql://localhost:5432/YOUR_TOOL_DB</connection-url>
            <driver>postgresql</driver>
            <security>
                <user-name>YOUR_USER_NAME</user-name>
                <password>YOUR_PASSWORD</password>
            </security>
            <pool>
                <min-pool-size>MIN_POOL_SIZE</min-pool-size>
                <max-pool-size>MAX_POOL_SIZE</max-pool-size>
                <prefill>false</prefill>
                <use-strict-min>false</use-strict-min>
                <flush-strategy>FailingConnectionOnly</flush-strategy>
            </pool>
            <validation>
                <check-valid-connection-sql>select 1</check-valid-connection-sql>
                <validate-on-match>false</validate-on-match>
                <background-validation>false</background-validation>
                <use-fast-fail>false</use-fast-fail>
            </validation>
            <timeout>
                <idle-timeout-minutes>10</idle-timeout-minutes>
                <blocking-timeout-millis>30000</blocking-timeout-millis>
            </timeout>
            <statement>
                <prepared-statement-cache-size>30</prepared-statement-cache-size>
                <track-statements>false</track-statements>
            </statement>
        </datasource>
    </datasources>
```

**NOTE :** YOUR_TOOL_DATASOURCES can be found in TOOL-ds.xml file (/src/main/application/META-INF of your ear folder)
or in the application user manual.

Finally, restart your Wildfly to take into account your config.
