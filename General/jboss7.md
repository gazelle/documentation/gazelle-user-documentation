---
title:  Jboss 7
subtitle: Installation
author: Anne-Gaëlle BERGE
date: 13/10/2016
toolversion:
function: Engineer
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-JBOSS_7_INSTALLATION-1_02
customer: IHE-EUROPE
---
# Purpose

This page describes the prerequisite to the installation of Gazelle
applications. All the tools developed in the context of the Gazelle
testbed project are developed for JBoss (5.0.1-GA or 7.2.0.final) and
use a postgreSQL database.

We recommand to install the Gazelle tools in a Debian-like environment,
it's the environment running on IHE Europe servers so we know that it is
correctly working. Moreover, most of the installation and configuration
procedures are described for such an environment.

# PostgreSQL

We are currenlty using PostgreSQL 9.1 on most of our servers.

# Install a JVM

Most of our applications running on JBoss7 are using java 7. Consider
installing
[openJDK](http://openjdk.java.net/install/ "http://openjdk.java.net/install/").

``` bash
sudo apt-get install openjdk-7-jre
```

# Install JBoss 7 application server

## Get package

from: [http://gazelle.ihe.net/jboss7/jboss-as-7.2.0.Final.zip](https://gazelle.ihe.net/jboss7/jboss-as-7.2.0.Final.zip "https://gazelle.ihe.net/jboss7/jboss-as-7.2.0.Final.zip")

``` bash
wget -nv -O /tmp/jboss-as-7.2.0.Final.zip https://gazelle.ihe.net/jboss7/jboss-as-7.2.0.Final.zip
```

Be sure to use this packaged version, we provide the postgresql driver, and use different versions for modules hibernate and javassist.

## Get init script

from: [http://gazelle.ihe.net/jboss7/init.d_jboss7](http://gazelle.ihe.net/jboss7/init.d_jboss7)

``` bash
wget -nv -O /tmp/init.d_jboss7 https://gazelle.ihe.net/jboss7/init.d_jboss7
```

## Install jboss in the /usr/local folder

``` bash
cd /usr/local
sudo mv /tmp/jboss-as-7.2.0.Final.zip .
sudo unzip ./jboss-as-7.2.0.Final.zip
sudo ln -s jboss-as-7.2.0.Final jboss7
useradd jboss
groupadd jboss-admin
adduser jboss jboss-admin
sudo chown -R jboss:jboss-admin /usr/local/jboss7
sudo chmod -R 755 /usr/local/jboss-as-7.2.0.Final
sudo chown -R jboss:jboss-admin /var/log/jboss7/
sudo chmod -R g+w /var/log/jboss7/
```

## Install the init script and make it start at system startup

``` bash
sudo mv /tmp/init.d_jboss7 /etc/init.d/jboss7
sudo chmod +x /etc/init.d/jboss7
sudo update-rc.d jboss7 defaults
```

## Update JBoss 7 postgresql jdbc driver


1. Stop jboss and go to
```bash
cd /usr/local/jboss7/modules/system/layers/base/org/postgresql/main
```

2. Download driver
```bash
sudo wget https://gazelle.ihe.net/jboss7/postgresql-42.2.1.jre7.jar
sudo chown jboss:jboss-admin postgresql-42.2.1.jre7.jar
sudo chmod 775 postgresql-42.2.1.jre7.jar
```

3. Edit module.xml and update the version

```xml
<module xmlns="urn:jboss:module:1.1" name="org.postgresql">
     <resources>
         <resource-root path="postgresql-42.2.1.jre7.jar"/>
     </resources>
     <dependencies>
         <module name="javax.api"/>
         <module name="javax.transaction.api"/>
     </dependencies>
</module>
```

4. Clean up your jboss
```bash
sudo rm -rf /usr/local/jboss7/standalone/tmp/
sudo rm -rf /usr/local/jboss7/standalone/data/
```

5. Restart your jboss and deploy your ear


## Externalize datasources from tool projects

Some projects may have extracted datasources. In order to deploy those projects, you will need to include some special 
instructions in your jboss server configuration file :

Edit standalone.xml in /usr/local/YOUR_JBOSS_SERVER/standalone/configuration folder and update datasources :


```xml
<datasources>
    ...
    <datasource jndi-name="java:jboss/datasources/YOUR_TOOL_DATASOURCES" pool-name="YOUR_TOOL_DATASOURCES" enabled="true" use-java-context="true">
        <connection-url>jdbc:postgresql://localhost:5432/YOUR_TOOL_DB</connection-url>
        <driver>postgresql</driver>
        <security>
            <user-name>YOUR_USER_NAME</user-name>
            <password>YOUR_PASSWORD</password>
        </security>
    </datasource>
</datasources>
```
You may also need to add the driver to the standalone.xml file :

```xml
 <drivers>
    ...
    <driver name="postgresql" module="org.postgresql">
        <driver-class>org.postgresql.Driver</driver-class>
        <xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
    </driver>
    ...
</drivers>
```

**NOTE :** YOUR_TOOL_DATASOURCES can be found in TOOL-ds.xml file (/src/main/application/META-INF of your ear folder)
or in the tool user manual.

Finally, restart your JBoss to take into account your config.

## Externalize mail server configuration

The same way we extracted the datasource configuration, we have enable the configuration of the mail server in the `standalone.xml` file so you can configure the subsystem according to your configuration.

Here is the minimum configuration which is necessary:
```xml
<subsystem xmlns="urn:jboss:domain:mail:1.1">
<!-- here below is the mail resource definition -->
          <mail-session jndi-name="java:jboss/mail">
            <smtp-server ssl="TRUEORFALSE" outbound-socket-binding-ref="mail-smtp">
                <login name="USERNAME" password="PASSWORD"/>
            </smtp-server>
        </mail-session>
</subsystem> 
```
You will also need to modify the outbound socket configuration:
```xml
        <outbound-socket-binding name="mail-smtp">
          <remote-destination host="HERE-YOUR-HOST" port="HERE-THE-PORT"/>
        </outbound-socket-binding>
```

## Environment variables for applications

If Environment variables are required by applications, they can be defined by default in the file `/opt/gazelle/.env` this way :

```shell
GZL_EVSCLIENT_URL=http://localhost:8780/evs
GZL_SSO_ENABLED=false
# etc...
```

All those variables will be loaded by the jboss7 service.

The default file path can of the `.env` file can be modified in `/etc/init.d/jboss7` service file on those lines :

```shell
# Path to environment variable value to give to deployed applications.
GZL_ENV=/opt/gazelle/.env
```

See installation manual of specific tool for the list of required environment variables if so.
