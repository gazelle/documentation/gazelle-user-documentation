---
title:  Installation Manual
subtitle: Patient Registry
author: Franck Desaize, Alexandre POCINHO
releasedate: 2024-02-02
toolversion: 2.2.x
function: Developer
version: 0.01
status: Draft document
reference: KER1-MAN-IHE-PATIENT_REGISTRY_INSTALLATION-0_01
customer: IHE-EUROPE
---

# Patient Registry suite installation

## Patient Registry (PATREG)

The Patient Registry project is dedicated to the management of patient demographics and identifiers test data.
It will embed connectors such as FHIR PDQm, PIXm or PAM.

### Sources & binaries

The sources are accessible here: [https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/patient-registry](https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/patient-registry)

Bugs and issue tracking are accessible here : [https://gazelle.ihe.net/jira/projects/PATREG](https://gazelle.ihe.net/jira/projects/PATREG). The name of the latest release, can be obtained in the "Releases" section.

To get official artifact (binaries), search for `patient-registry` in IHE Nexus : [https://gazelle.ihe.net/nexus](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22patient-registry%22)
and select the **patient-registry.jar** artifact for download.

### Installation

#### Patient Manager dependency

Patient Registry depends currently on the database of Patient Manager. Only patients created/managed in Patient Manager will be accessible in the Patient Registry Search API.  
Please follow [Patient Manager installation process](/gazelle-documentation/Patient-Manager/installation.html).

Patient Registry needs at least Patient Manager version 9.11.6.

#### Application server

Patient registry need to be deployed on Widlfly-18.  
Please read first the documentation on how to install and configure this server for Gazelle
applications : [general considerations for WildFly 18](https://gazelle.ihe.net/gazelle-documentation/General/wildfly18.html)

#### Database configuration

Patient registry is using Patient Manager's database. It only needs a datasource connection declared in wildfly 18.  
See [Setup datasources for gazelle applications](https://gazelle.ihe.net/gazelle-documentation/General/wildfly18.html#setup-datasources-for-gazelle-applications)

**Datasource name** : `patientRegistryDS`

**Database name** : `pam-simulator`

#### Deployment

Copy the **jar** artifact app.patient-registry-service-X.X.X.jar in the deployment folder of the wildfly installation under the name
**patient-registry.jar**. This is important for the path on which Patient Registry's web services will be exposed.

```bash
cp app.patient-registry-service-X.X.X.jar /usr/local/wildfly18/standalone/deployments/patient-registry.jar
```

Start wildfly. The API can be accessed at (depending on your configured host and port)

For Patient processing Service :

* GITB Processing Service : GET <http://localhost:8380/patient-registry/PatientProcessingService/patient-processing-service?wsdl>

For CrossReference Processing Service :

* GITB Processing Service : GET <http://localhost:8380/patient-registry/CrossReferenceService/xref-processing-service?wsdl>

## PDQm Connector

PDQm Connector is a connector for Patient Registry. It is dedicated to interface FHIR requests defined in PDQm standard to and the
Patient Registry API for patient demographics and identifiers test data management.

### Sources & binaries (PDQm)

The sources are accessible here: [https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/pdqmsimulator](https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/pdqmsimulator)

Bugs and issue tracking are accessible in the same project as the Patient Registry :
[https://gazelle.ihe.net/jira/projects/PATREG](https://gazelle.ihe.net/jira/projects/PATREG).
The name of the latest release, can be obtained in the "Releases" section.

To get official artifact (binaries), search for `pdqm-connector-service` in IHE Nexus : [https://gazelle.ihe.net/nexus](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22pdqm-connector-service%22)
and select the **pdqm-connector.war** artifact for download.

### Installation (PDQm)

#### Patient Manager dependency (PDQm)

Same as [Patient Manager Dependency for PATREG](#patient-manager-dependency)

**AND** PDQm also needs Patient Registry to be installed with connector version >1.0.x, you will need Patient Registry version >1.0.x.

#### Operational Preferences (PDQm)

PDQm connector defines mandatory Operational Preferences that you will have to define in your Application server.
To know how to do that, see [General considerations for WildFly 18](/gazelle-documentation/General/wildfly18.html)

Define a resource in your server with name *java:/app/gazelle/pdqm-connector/operational-preferences*.

This resource should refer to a deployment.properties file. This file shall contain the following properties :

| Property Name       | Description                                                               | Example value                                             |
|--------------|-----------|------------|
| patientregistry.url | URL of the Patient Registry Processing Service used to manage patients.   | <https://example.com/patient-registry/patient-registry/PatientProcessingService/patient-processing-service?wsdl>|

#### Application server (PDQm)

PDQm need to be deployed on Wildfly-18. Please read first the documentation on how to install and configure this server for Gazelle applications : [General considerations for WildFly 18](/gazelle-documentation/General/wildfly18.html)

##### Configure WildFly 18 f(PDQm)

Install factories module

1. Stop wildfly and go to:

    ```bash
    sudo mkdir -p /usr/local/wildfly18/modules/system/layers/base/net/ihe/gazelle/factories/main
    cd /usr/local/wildfly18/modules/system/layers/base/net/ihe/gazelle/factories/main
    ```

2. Download module `factories.jar`:

    ```bash
    sudo wget https://gazelle.ihe.net/wildfly18/factories.jar
    ```

3. Create `module.xml` file:

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <module xmlns="urn:jboss:module:1.1" name="net.ihe.gazelle.factories">
        <resources>
            <resource-root path="factories.jar"/>
        </resources>
        <dependencies>
            <module name="javax.api"/>
            <module name="javax.transaction.api"/>
            <module name="javax.servlet.api" optional="true"/>
        </dependencies>
    </module>
    ```

4. Add `jboss:jboss-admin` rights:

    ```bash
    sudo chown -R jboss:jboss-admin .
    sudo chmod -R 775 .
    ```

5. Stop Wildfly and edit standalone.xml in `/usr/local/wildfly18/standalone/configuration` ; add the factories binding in the naming subsystem :

Replace this property `${DEPLOYMENT_PROPERTIES}` with your own path to the deployment property, which is stored in the pdqm-connector project : `pdqm-connector/pdqm-connector-service/src/main/resources/deployment.properties`

```xml
    <subsystem xmlns="urn:jboss:domain:naming:2.0">
       <bindings>
          <object-factory name="java:/app/gazelle/pdqm-connector/operational-preferences" module="net.ihe.gazelle.factories" class="net.ihe.gazelle.factories.PropertiesFactory">
             <environment>
                <property name="path" value="${DEPLOYMENT_PROPERTIES}"/>
             </environment>
          </object-factory>
       </bindings>
       <remote-naming/>
    </subsystem>
```

#### Database configuration (PDQm)

Same as [Database for PATREG](#database-configuration) **EXCEPT FOR** :

**Datasource name** : `pdqmConnectorDS`  
**Database name** : `pam-simulator`

#### Deployment (PDQm)

Copy the **war** artifact `pdqm-connector-X.X.X.war` in the deployment folder of the wildfly installation under the name `pdqm-connector.war`  
This is important for the path on which connector's web services will be exposed.

```bash
cp pdqm-connector-X.X.X.war /usr/local/wildfly18/standalone/deployments/pdqm-connector.war
```

Start wildfly. The API can be accessed at (depending on your configured host and port)

* Retrieve : GET <https://www.example.com/pdqm-connector/Patient/{id}>
* Search : POST <https://www.example.com/pdqm-connector/Patient>

## PIXm Connector

PIXm Connector is a connector for Patient Registry. It is dedicated to interface FHIR requests defined in PIXm standard to and the
Patient Registry API for patient demographics and identifiers test data management.

### Sources & binaries (PIXm)

The sources are accessible here: <https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/pixm-connector>.

Bugs and issue tracking are accessible in the same project as the Patient Registry :
<https://gazelle.ihe.net/jira/projects/PATREG>.
The name of the latest release, can be obtained in the "Releases" section.

To get official artifact (binaries), search for `pixm-connector-service` in IHE Nexus : [https://gazelle.ihe.net/nexus](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22pixm-connector-service%22)
and select the **pixm-connector.war** artifact for download.

### Installation (PIXm)

#### Patient Manager dependency (PIXm)

Same as [Patient Manager dependency for PATREG](#patient-manager-dependency)

PIXm also needs Patient Registry to be installed:

|PIXm version|PATREG version|
|:---:|:---:|
|`1.0.0`|`2.0.0`|
|`2.0.0`|`2.1.0`|
|`3.0.0`|`2.2.0`|

#### Matchbox and Http-Validator dependencies (PIXm)

Since `3.0.0` PIXm needs two deployed applications for validation:

* HTTP-validator with the install documentation [here](https://gitlab.inria.fr/gazelle/public/validation/http-validator/-/blob/06997c194efc0f775d9af1c2aa7ae49e13d4d06d/documentation/installation.md).
* Matchbox with the install documentation [here](https://github.com/ahdis/matchbox/blob/29da3b9dfec2334ceb031346ef12d0e08a626122/README.md)

#### Operational Preferences

PIXm connector defines mandatory Operational Preferences that you will have to define in your Application server.
To know how to do that, see [General considerations for WildFly 18](/gazelle-documentation/General/wildfly18.html)

/!\ Since `3.0.0` PIXm connector needs to be installed in Wildfly version that supports `JDK 17`. But the tutorial remains the same as Wildfly 18.

|PIXm version|JDK version|Wildfly version|
|:---:|:---:|:---:|
|`1.0.0`|11|18.0.1.Final|
|`2.0.0`|11|18.0.1.Final|
|`3.0.0`|17|30.0.1.Final|

Define a resource in your server with name *java:/app/gazelle/pixm-connector/operational-preferences*.

```xml
<subsystem xmlns="urn:jboss:domain:naming:2.0">
<bindings>
<object-factory name="java:/app/gazelle/pixm-connector/operational-preferences" module="net.ihe.gazelle.factories" class="net.ihe.gazelle.factories.PropertiesFactory">
<environment>
<property name="path" value="${DEPLOYMENT_PROPERTIES}"/>
</environment>
</object-factory>
</bindings>
<remote-naming/>
</subsystem>
```

Replace this property `${DEPLOYMENT_PROPERTIES}` with the path to the `deployment.properties` file (like `/opt/pixm-connector/deployment.properties`)

This file shall contain the following properties :

|Property Name| Description | Example value |
|:---:|:---:|:---:|
| patientregistry.url | URL of the Patient Registry Processing Service used to manage patients.| <https://example.com/patient-registry/PatientProcessingService/patient-processing-service?wsdl>|
| xrefpatientregistry.url | URL of the CrossReference Processing Service used to XReference.   | <https://example.com/patient-registry/CrossReferenceService/xref-processing-service?wsdl> |

Since version `3.0.0` PIXm needs:

* endpoints for matchbox and http-validator
* profiles id (URL + headers validation with http-validator)
* profile canonical url (body validation with Matchbox FHIR)

|Property Name| Description | Example value |
|:---:|:---:|:---:|
|APP_HTTP_VALIDATOR_SERVER|Endpoint of HTTP-Validator|<https://www.example.com/http-validator>|
|PROFILE_ID_CREATE_UPDATE_DELETE_ITI_104|Profile ID for ITI-104|IHE_ITI-104-PatientFeed_Query|
|PROFILE_ID_POST_ITI_83|Profile ID for POST ITI-83|IHE_ITI-83_POST_PIXm_Query|
|PROFILE_ID_GET_ITI_83|Profile ID for GET ITI-83|IHE_ITI-83_GET_PIXm_Query|
|APP_IG_FHIR_SERVER|Endpoint for Matchbox|<https://www.example.com/matchboxv3/fhir>|
|PIXM_PATIENT_PROFILE|Canonical URL for PIXm Patient Profile|<https://profiles.ihe.net/ITI/PIXm/StructureDefinition/IHE.PIXm.Patient>|
|PIXM_PARAMETERS_PROFILE|Canonical URL for PIXm Paramaters Profile|<https://profiles.ihe.net/ITI/PIXm/StructureDefinition/IHE.PIXm.Query.Parameters.In>|

#### Application server (PIXm)

Same as [Application Server (PDQm)](#application-server-pdqm) for both Wildfly 18 and Wildfly 30.

#### Deployment (PIXm)

Copy the **war** artifact `pixm-connector-X.X.X.war` (< 3.0.0) or `pixm-connector-service-X.X.X.war` (>= 3.0.0) in the deployment folder of the wildfly installation under the name
`pixm-connector.war`.  
This is important for the path on which connector's web services will be exposed.

Before `3.0.0`:

```bash
cp pixm-connector-X.X.X.war /usr/local/wildfly18/standalone/deployments/pixm-connector.war
```

Since `3.0.0`:

```bash
cp pixm-connector-service-X.X.X.war /usr/local/wildfly30/standalone/deployments/pixm-connector.war
```

Start wildfly.

The API can be accessed at (depending on your configured host and port):

* ITI-104 : PUT/DELETE <https://example.com/pixm-connector/fhir/Patient?identifier=patient.system%7Cpatient.id>
* ITI-83 : GET <https://example.com/pixm-connector/fhir/Patient/$ihe-pix?sourceIdentifier=patient.system%7Cpatient.id&targetSystem=targetSystem>
* Capability statement  : <https://example.com/pixm-connector/fhir/metadata>
