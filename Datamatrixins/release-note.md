---
title: Release note
subtitle: Datamatrixins
toolversion: 1.0.1
releasedate: 2023-11-14
author: Clément LAGORCE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-DATAMATRIXINS
---

# 1.0.1
_Release date: 2023-11-14_

__Bug__
* \[[HTTP-16](https://gazelle.ihe.net/jira/browse/HTTP-16)\] Release/Doc
* \[[HTTP-43](https://gazelle.ihe.net/jira/browse/HTTP-43)\] Use new Validation API and Reports