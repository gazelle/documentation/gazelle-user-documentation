---
title: Release note
subtitle: Gazelle Transformation Service
toolversion: 2.2.1
releasedate: 2024-10-18
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_TRANSFORMATION_SERVICE
---
# 2.2.1
_Release date: 2024-10-18_

__Task__
* \[[TRANSFOSRV-76](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-76)\] Integrate a json to xml not in a fhir context

# 2.2.0
_Release date: 2024-02-06_

__Task__
* \[[TRANSFOSRV-74](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-74)\] Integrate new sso-client-v7

# 2.1.2
_Release date: 2022-04-25_

__Story__
* \[[TRANSFOSRV-73](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-73)\] Add Json to Xml Transformation

# 2.1.1
_Release date: 2021-01-26_

__Remarks__

__Bug__
* \[[TRANSFOSRV-71](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-71)\] Cas service invalid
* \[[TRANSFOSRV-72](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-72)\] Unable to use CAS login in docker context

# 2.1.0
_Release date: 2019-03-21_

__Remarks__

Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.


__Improvement__

* \[[TRANSFOSRV-67](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-67)\] Extract datasource configuration
* \[[TRANSFOSRV-68](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-68)\] Update SQL scripts archive

# 2.0.0
_Release date: 2018-02-27_

__Remarks__

A new client module named gazelle-transformation-ws-client can be used to access transformation services from this version.
The PostgreSQL driver has been externalized and must then be deployed as a JBoss module.

__Sub-task__

* \[[TRANSFOSRV-55](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-55)\] Clean up code - rename to gazelle-transformation
* \[[TRANSFOSRV-56](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-56)\] Create ws client module

__Bug__

* \[[TRANSFOSRV-46](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-46)\] Update documentation link
* \[[TRANSFOSRV-54](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-54)\] GazelleTransformation is modifying data files submitted for transformation
* \[[TRANSFOSRV-57](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-57)\] Tool version is no more accessible by rest service
* \[[TRANSFOSRV-59](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-59)\] Update postgresql driver to version 42.2.1.jre7

# 1.0.9
_Release date: 2018-01-08_

__Bug__

* \[[TRANSFOSRV-40](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-40)\] "Delete" icon is not reachable in "Manage DFDL Files" page
* \[[TRANSFOSRV-42](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-42)\] Missing translations and wrong text
* \[[TRANSFOSRV-48](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-48)\] Last blank characters of analyzed file are removed.

__Improvement__

* \[[TRANSFOSRV-41](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-41)\] Action button ">" is not relevant for XSD files
* \[[TRANSFOSRV-44](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-44)\] Add webservice to get tool version
* \[[TRANSFOSRV-45](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-45)\] Add prod build profile
* \[[TRANSFOSRV-47](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-47)\] Upgrade to Daffodil 2.0.0 that was recently released
* \[[TRANSFOSRV-51](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-51)\] Really CAS apereo Logout

# 1.0.4
_Release date: 2017-07-12_


__Technical task__

* \[[TRANSFOSRV-33](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-33)\] Performance test

# 1.0.3
_Release date: 2017-06-12_


__Story__

* \[[TRANSFOSRV-30](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-30)\] Optimisation of compilation time
* \[[TRANSFOSRV-31](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-31)\] Compile DFDL File from the GUI

# 1.0.2
_Release date: 2017-05-29_

__Technical task__

* \[[TRANSFOSRV-13](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-13)\] XML transformation methods

__Bug__

* \[[TRANSFOSRV-23](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-23)\] Error message when doing action login->logout->login
* \[[TRANSFOSRV-25](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-25)\] Error of duplication : javax.persistence.EntityExistsException
* \[[TRANSFOSRV-26](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-26)\] java.lang.NoClassDefFoundError when doing the XML to Data transformation (transformation inverse)

__Story__

* \[[TRANSFOSRV-6](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-6)\] Deploy DAFFODIL transformation service on Wildfy

# 1.0.1
_Release date: 2017-05-23_


__Bug__

* \[[TRANSFOSRV-18](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-18)\] org.jboss.weld.context.NonexistentConversationException when accessing pages via the buttons 
* \[[TRANSFOSRV-19](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-19)\] Search by criteria doesn't work in manageDfdlFiles
* \[[TRANSFOSRV-20](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-20)\] 404 not found  when accessing administration/configure page
* \[[TRANSFOSRV-21](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-21)\] editing home title and main content in the home page doesn't work
* \[[TRANSFOSRV-22](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-22)\] Missing field on view pages 
* \[[TRANSFOSRV-24](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-24)\] Transaction issue with the GUI and the Webservice. They don't work together

# 1.0.0
_Release date: 2017-05-05_


__Technical task__

* \[[TRANSFOSRV-11](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-11)\] Create Daffodil transformation Class
* \[[TRANSFOSRV-12](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-12)\] Data transformation methods
* \[[TRANSFOSRV-14](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-14)\] DFDL File Management methods

__Story__

* \[[TRANSFOSRV-6](https://gazelle.ihe.net/jira/browse/TRANSFOSRV-6)\] Deploy DAFFODIL transformation service on Wildfy
