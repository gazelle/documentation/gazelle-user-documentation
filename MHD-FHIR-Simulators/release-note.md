#---
#title: Release note
#subtitle: MHD FHIR Simulator 
#toolversion: 1.0.0
#releasedate: 2024-06-27
#authors: Romuald DUBOURG
#function: Software Engineer
#customer: eHealthsuisse
#reference:
#---

# 1.0.0
_Release date: 2024-06-27_

__Story__
* \[[CHMHD-3](https://gazelle.ihe.net/jira/browse/CHMHD-3)\] Create gitlab project with quarkus / java 17 and create a hapi fhir servlet	Medium - Medium priority, must be plan in one of the next cycles.
* \[[CHMHD-4](https://gazelle.ihe.net/jira/browse/CHMHD-4)\] [CH ITI-65] Provide Document Bundle
* \[[CHMHD-5](https://gazelle.ihe.net/jira/browse/CHMHD-5)\] [CH ITI-66] Find Document Lists
* \[[CHMHD-6](https://gazelle.ihe.net/jira/browse/CHMHD-6)\] [CH ITI-67] Find Document References
* \[[CHMHD-7](https://gazelle.ihe.net/jira/browse/CHMHD-7)\] [CH ITI-68] Retrieve Document
* \[[CHMHD-8](https://gazelle.ihe.net/jira/browse/CHMHD-8)\] Create client for Nist Tool kit
* \[[CHMHD-11](https://gazelle.ihe.net/jira/browse/CHMHD-11)\] HTTP Validator Profile
* \[[CHMHD-12](https://gazelle.ihe.net/jira/browse/CHMHD-12)\] Interceptor configuration/instanciation

