---
title: User Manual
subtitle: MHD Fhir Simulator
author: Romuald DUBOURG
releasedate: 2024-06-27
toolversion: 1.0.0
function: Engineer
version: 1.0.0
status: draft
---

# MHD Fhir Simulator

## Description

The MHD FHIR Simulator acts like a server to return queried resources fetch from Nist FHIR Toolkit (asbestos) .
It had a validation process to check if the Header + Body + URL are compliant regarding the IHE specification.
And a  validation process to check if sent resource is compliant regarding the IHE specification.
Only Documents, Document References and Lists are fetched from the Patient Registry.


## [IHE ITI-65] Provide Document Bundle

- [IHE Specifications](https://profiles.ihe.net/ITI/MHD/ITI-65.html)

## Overview

Here is a quick overview of the available functionality from MHD Fhir Simulator

| Operation                   | HTTP Methods | URL to call                                       | Entry parameter                        | Returned value       | Remarks|
|-----------------------------|--------------|---------------------------------------------------|----------------------------------------|----------------------|---|
| Provide Document Bundle  | POST         | ```https://example.com/mhd-fhir-simulator/fhir``` | Document bundle int the request's body | Transaction response |/|


## [CH ITI-66] Find Document Lists

- [IHE Specifications](https://profiles.ihe.net/ITI/MHD/ITI-66.html)

## Overview

Here is a quick overview of the available functionality from MHD Fhir Simulator

| Operation                   | HTTP Methods | URL to call                                                         | Entry parameter                                                   | Returned value                                                                          | Remarks|
|-----------------------------|--------------|---------------------------------------------------------------------|-------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---|
| Find Document Lists | GET          | ```https://example.com/mhd-fhir-simulator/fhir/List?{parameters}``` | Parameters to search for specific ITI-66 MHD Fhir resource ITI-66 | A bundle containing 0 to many ITI-66 MHD Fhir Specific corresponding to search criteria |/|

## [CH ITI-67] Find Document References

- [IHE Specifications](https://profiles.ihe.net/ITI/MHD/ITI-67.html)

## Overview

Here is a quick overview of the available functionality from MHD Fhir Simulator

| Operation               | HTTP Methods | URL to call                                                                      | Entry parameter                                                   | Returned value                                                                          | Remarks|
|-------------------------|--------------|----------------------------------------------------------------------------------|-------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---|
| Find Document Reference | GET          | ```https://example.com/mhd-fhir-simulator/fhir/DocumentReference?{parameters}``` | Parameters to search for specific ITI-67 MHD Fhir resource ITI-67 | A bundle containing 0 to many ITI-67 MHD Fhir Specific corresponding to search criteria |/|

## [CH ITI-68] Retrieve Document

- [IHE Specifications](https://profiles.ihe.net/ITI/MHD/ITI-68.html)

## Overview

Here is a quick overview of the available functionality from MHD Fhir Simulator

| Operation               | HTTP Methods | URL to call                                                     | Entry parameter                            | Returned value              | Remarks|
|-------------------------|--------------|-----------------------------------------------------------------|--------------------------------------------|-----------------------------|---|
| Find Document Reference | GET          | ```https://example.com/mhd-fhir-simulator/fhir/Document/{id}``` | ID for a specific ITI-68 MHD Fhir resource | the desired ITI-68 Resource |/|



Capability statement of the application can be found with : <https://example.com/mhd-fhir-simulator/fhir/metadata>

As described in [HAPI FHIR resources](https://hapifhir.io/hapi-fhir/docs/server_plain/rest_operations_operations.html), some strings are automatically escaped when the FHIR server parses URLs:

|Given String|Parsed as|
| :---: | :---: |
|\||%7C|


## Validation process

Each operation implies a validation of requests for `ITI-65`, `ITI-66`, `ITI-67` and `ITI-68`
Validation is done by calling:

- [HTTP Validator](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/http-validator) for URL and Headers for `ITI-65`, `ITI-66`, `ITI-67` and `ITI-68`.
- [FHIR Validator](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/fhir-validator) or Matchbox for the body of the request for `ITI-65`.

The validation is done by default but can be switched with options:  

- `evs.endpoint=https://${FQDN}/evs/rest/validations`

- `http.validation.name=HTTP Message Validator`
- `http.validation.schematron.name-iti-65=CH_ITI-65-ProvideDocumentBundle_POST_MHD_Request`
- `http.validation.schematron.name-iti-66=CH_ITI-66-FindDocumentLists-GET_MHD_Request`
- `http.validation.schematron.name-iti-67=CH_ITI-67-FindDocumentReferences-GET_MHD_Request`
- `http.validation.schematron.name-iti-68=CH_ITI-68-RetrieveDocument-GET_MHD_Request`

- `fhir.validation.name=Gazelle FHIR Validator R4`
- `fhir.validation.schematron.name-iti-65=CH:MHD [ITI-65] Provide Document Bundle Comprehensive Request`


Resources are fetched from the NIST FHIR Toolkit (https://gitlab.inria.fr/gazelle/public/processing/patient-registry).

You can choose which NIST FHIR Toolkit target by setting the `nist.fhir.toolkit.endpoint` parameter



