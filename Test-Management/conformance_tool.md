---
title:  Launching Conformance Tool of XDS Tools from Gazelle Test Management
subtitle: Gazelle Test Management
author: Malo TOUDIC
function: Developer
date: 2025-01-27
version: 0.01
status: Draft
reference: KER1-MAN-IHE-TEST_MANAGEMENT_GWT_XDSTOOLS-0_01
customer: IHE-EUROPE
---

# Introduction

The goal is to launch the Conformance Tool of XDS Tools (from version 6) directly from a test instance in Gazelle Test Management.

You can find the XDS Tools documentation at this location : [https://github.com/usnistgov/iheos-toolkit2/wiki/Launching-Conformance-Tool-from-Gazelle](https://github.com/usnistgov/iheos-toolkit2/wiki/Launching-Conformance-Tool-from-Gazelle)

# Configuration

## Pre-requisites

To perform this workflow, the following tools shall be available in your environment:

* NIST XDS Tools (version 6 or higher)
* Gazelle Test Management (5.8.0 or higher)
* Gazelle Webservice Tester (1.2.0 or higher)

## Gazelle Test Management

Go to Administration > Application Preferences, set the 'Gazelle Webservice Tester URL' preference and verify that 'Application OID' is already set and correct.

Then, for each test where you want the user to be able to launch the Conformance Tool, edit the description of a test and add this exact link via the code view :

```
<a href="https://$$GWT$$/rest/xdstools?testInstanceId=$$testInstanceId$$&callingToolOid=$$callingToolOid$$" target="_blank">Link to XDSTools</a>
```
Only the test instance created after the edition of the test will have the link to XDS Tools.

## Gazelle Webservice Tester

Go to Administration > Manage calling tools and add a new calling tool: set a name, the OID and the URL of Gazelle Test Management. The URL must only be the base URL of the test environment (eg. https://validation.sequoiaproject.org/).

Go to Administration > Configuration and add the full xdstools_url.

Then, if not already done, run the gazelle-webservice-tester-ear/src/main/sql/initConformanceTool.sql script.

Then you need to :

* map the actor from XDS Tools to the role in test keyword from Test Management in Administration > Conformance Tool > Actor Type.
* map the integration profile from XDS Tools to integration profile from Test Management in Administration > Conformance Tool > Profile.
* map the option from XDS Tools to the option from Test Management in Administration > Conformance Tool > Option.


## XDSTools

Go to Toolkit configuration and set the Gazelle Config URL which has this format :

```
http://sake.irisa.fr/EU-CAT/systemConfigurations.seam
```

# Execution
