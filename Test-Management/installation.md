---
title:  Installation manual
subtitle: Gazelle Test Management
author: Nicolas BAILLIET
function: Software Engineer
releasedate: 27/01/2025
toolversion: 10.X.X
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-TEST_MANAGEMENT_INSTALLATION-1_02
customer: IHE-EUROPE
---

# Gazelle Test Management - Installation & Configuration

# Purpose

Thanks for having chosen Gazelle ! Test Management is an open-source project under Apache2 licence.

Here is a guide to help you with installing Test Management.

# Pre-requisites

Gazelle Test Management runs under Jboss AS 7.2.0 with a PostgreSQL 9.6 or higher database. For general considerations regarding the installation of the
application server, read [Jboss7 installation guide](../General/jboss7.html).

# Binary

You can either fetch the public EAR (released version) in our [Nexus repository](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~gazelle-tm-ear*~~~) or compile the tool by your own.

## Build from sources

The sources are available on the INRIA's Gitlab:
```bash
git clone https://gitlab.inria.fr/gazelle/public/core/test-management.git
```

It requires jdk8 and maven to build, but jdk7 to run.

```bash
cd test-management
mvn clean install
```

The archive (EAR) and the distribution file are created and placed into gazelle-tm/gazelle-tm-ear/target directory.

# Database setup

## Database creation

You can choose whatever database name you want, you will need to report it in the datasource section of the Jboss configuration.

```bash
su postgresql
psql
postgres=# CREATE USER gazelle;
postgres=# CREATE DATABASE "your\_database" OWNER gazelle ENCODING UTF-8;
postgres=# ALTER USER gazelle WITH ENCRYPTED PASSWORD 'password';
postgres=# \q
exit
```

## Database initialization for TM version 7.0.0 and newer

Database initialization is automatically handled by the EAR itself.

## Database initialization for TM version prior to 7.0.0

Download the files containing all the data required by the application to properly work (schema-X.X.X.sql and init-X.X.X.sql, X.X.X being the latest version available) on Nexus in the sql.zip file : [Nexus repository](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~gazelle-tm-ear*~~~) and import it into the newly created database as shown below.

```bash
psql -U gazelle your_database < schema-X.X.X.sql
psql -U gazelle your_database < init-X.X.X.sql
psql -U gazelle your_database < create_functions.sql
```

# Graphviz

Gazelle Test Management need the package graphviz to display graph (Display relations between actors and transactions for a given integration profile)
```bash
sudo apt install graphviz
```

# Single Sign On

With the release 5.15.0, Gazelle Test Management  needs a file to interact with the SSO application.
This application read the configuration of the SSO in a file located in `/opt/gazelle/cas`. You need
to create this folder and the configuration file `gazelle-tm.properties`.

```
 mkdir /opt/gazelle/cas
 touch /opt/gazelle/cas/gazelle-tm.properties
 chown -R jboss:jboss-admin /opt/gazelle/cas
 chmod -R g+w /opt/gazelle/cas
```
The file **gazelle-tm.properties** shall be created and save in **/opt/gazelle/cas**.

### SSO properties with gazelle-user-management and Keycloak SSO (Test-Management version 7.0.0 and newer)

Integrated with Keycloak's Gazelle, the file `gazelle-tm.properties` must contain:

```properties
casServerUrlPrefix=https://yourUrl.com/auth/realms/gazelle/protocol/cas
casServerLoginUrl=https://yourUrl.com/auth/realms/gazelle/protocol/cas/login
casLogoutUrl=https://yourUrl.com/auth/realms/gazelle/protocol/cas/logout
service = https://yourUrl.com/gazelle
```

### SSO properties with Apereo CAS (Test-Management version prior to 7.0.0)

Integrated with Apereo-CAS' Gazelle, the file `gazelle-tm.properties` must contain:

```properties
casServerUrlPrefix = https://yourUrl.com/sso
casServerLoginUrl = https://yourUrl.com/sso/login
casLogoutUrl = https://yourUrl.com/sso/logout
service = https://yourUrl.com/gazelle
```

# Deployment

Test Management requires JBoss 7.2.0 to have some additional libraries (see pre-requisites section).

Since version 5.10.0, Before you start your Jboss server, you shall configure the datasource at `[YOUR_JBOSS_INSTALL]/standalone/configuration/standalone.xml`.
Read details in the "Externalize datasources from tool projects" section of [JBoss 7 installation page](../General/jboss7.html).

The datasource element to import in the standalone.xml file is available in the [project](https://gazelle.ihe.net/jenkins/view/Test%20Management/job/gazelle-tm-public/ws/gazelle-tm-ear/src/main/datasource/TestManagement-ds.xml).

Once you have copied the content of this file in your Jboss configuration:

* Update the ${jdbc.connection.url} variable to the location of your database. If postgres is running locally, should be something like jdbc:postgresql:\[DB_NAME\].
* Update the ${seamName} variable to:
    * 'TestManagement' if the EAR has been built with the __public__, __CAT__, __eucat__, or __na__ profile (that is the case if you download it from Nexus)
    * 'TestManagementDEV' if the EAR has been built with the __dev__ profile (it is typically the case if you compiled the tool without specifying a profile)
    * 'GMM' if the EAR has been built with the __gmm__ profile (it is the case for the instance running at https://gazelle.ihe.net/GMM)
    * 'ProductRegistry' if the EAR has been built with the __pr__ profile (it is the case for the instance running at https://product-registry.ihe.net/PR)
* Update ${jdbc.user} and ${jdbc.password} to match your database user and password
* Update ${min.pool.size} and ${max.pool.size} with consistent data (be careful, those values are also set in persistence.xml). Use:
    * min.pool.size = 5 and max.pool.size = 150 for the instance running during EU-CAT or NA connectathons
    * min.pool.size = 1 and max.pool.size = 70 for the instance running at EU-CAT outside of connectathon periods and for intances using the __public__ profile
    * min.pool.size = 1 and max.pool.size = 30 for the Gazelle Master Model
    * min.pool.size = 1 and max.pool.size = 10 for the Product Registry and other instances

You can retrieve those values in [gazelle-tm's pom.xml file](https://gazelle.ihe.net/jenkins/view/Test%20Management/job/gazelle-tm-public/ws/pom.xml) for each profile.

Since version 5.11.0, you need to configure the mail session that is needed by the tool in the jboss configuration file `standalone.xml`. Read details in the "Externalize mail server configuration" section of [JBoss 7 installation page](../General/jboss7.html) for further information.

Copy the gazelle-tm.ear into the " standalone/deployments" directory of your JBoss 7 server. Finally, start your server. When the application is deployed, open a browser and go to **http://yourserver/gazelle**. If the deployment and the database initialization are successful you should see the home page.

# First start

This instance of Test Management you have just deployed is free of organization, user and testing 
session. Consequently, the next step will be to create your organization, your account (as an 
administrator of the application) and the testing session. A testing session is used to hold one 
event, for example a Connectathon, a showcase or whatever requiring the use of Test Management. If 
the first part of the installation is successful, you should see the Gazelle home page (see file 
entitled Home page before installation).

![](./media/TM_first_install.png)

By hitting the "Start installation" button, you will reach a page dedicated to the different steps of the installation, for each step, a screen capture has been attached (see at the bottom of the page).

1.  Create your Organization: In Test Management, each user is linked to an organization. Before creating your personal account, you have to register your Organization by providing its name. You are also asking for a keyword, the one will be used in the application to find out your organization and to build the keyword of the various systems you will register.
![](./media/install_step_1.png)

2.  Create your Account: Provide the application with your first name, last name, email and password. This account will have the administration rights (admin\_role).
![](./media/install_step_2.png)

3.  Create the testing session: The testing session is mandatory for using Test Management. You can run several sessions in parallel. For each session you have to provide a long list of informations...
![](./media/install_step_3.png)

4.  Configure the application: This part of the installation, is dedicated to the preference settings.
  *  The application URL is required to build permanent link, the admin name and admin email are used when the application sends emails to the users.
  * The application home path is the path to the directory where all files required or created by Test Management will be stored. If this folder does not exist, the application creates it (if it has sufficient rights).
  * The photos directory is the path to store users' photos; this directory should be accessible through the internet by the application.
![](./media/install_step_4.png)

5.  End: Once you have performed those 4 steps, you can hit the Start button.
You will be redirect to the Home page, if everythink worked fine during the configuration, you should see the testing session you have just created. Now, you can log in using your newly created account. The application can be published on-line and users will be able to create their own organization and their own account. Then, they should be able to register systems for the domains you have linked to the testing session until the registration deadline you have set.
![](./media/install_step_5.png)


# Customize the Home page UI

## Version 6.3.0 and newer

### General

The home page support multiple sections, each section contains HTML content, and pre-defined css classes.

For the current version, since **(6.3.0)**, the application doesn't provide a graphical interface to manage these sections,
therefore, an administrator should insert manually his HTML content in the table "tm_sections".

### Default behavior

By default, two sections (Documentation and Announcement) are displayed only in the home page, the other two sections
(Tool index and Credits) are visible through all the application pages.

### Update section priors to 7.0.1

`update tm_section update content='<YOUR_HTML_CONTENT>' where id=<SECTION_ID>`


![](./media/homepage_demo.png)

### Update section since version 7.0.1

The administrator of the current session have access to the Home Page Manager via Administration menu.
By default, the announcement section is selected. It can be changed via the drop-down menu.
The selected section can be edited via this page. The `Save and Continue` button will update the section. 
Restore button restore the previous version of the selected section.

### Update section since 9.5.0

It is now possible to run automated test steps.
To activate it, 2 environnements variables are needed :
- maestro_url=http://localhost:8080/gateway
- automated_step_enabled=true
Note that maestro_url is temporarily pointing to a gateway micro service that redirect requests to ITB Test Bed.
It will be integrated in maestro in the future so the /gateway might change.

![](./media/home_page_manager.png)

## Version priors to 6.3.0

The home page is built of two blocks the administrators can customize when they are logged in.

* The **main panel** is the one which is always displayed and with a header, by default, set to "Main Panel".

* The **secondary panel** is displayed only if it is not empty. It can be located above or below the main panel. To create it, hit the "create a second panel" button located at the bottom of the main panel.

Each of those two blocks depends on the selected language. That means that, you have to edit those panels in all languages supported by the application. For each panel, you can edit its header title.
![](./media/install_end.png)
