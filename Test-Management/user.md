---
title:  User manual
subtitle: Gazelle Test Management
author: Anne-Gaëlle Bergé
function: Engineer
releasedate: 2025-01-27
toolversion: 10.X.X
version: 2.01
status: Validated
reference: KER1-MAN-IHE-TEST_MANAGEMENT_USER-2_01
customer: IHE-EUROPE
---

# Introduction

## Purpose

This guide introduce the usability of Test Management platform.

## Gazelle Test Management Feature List

The following table summarizes the list of features in the Gazelle Test Management Tool.

* Registration process management
    * [Users management](#users-management)  
    * [Contacts management](#contacts-management)
    * [Organizations management](#organizations-management)
    * [Contracts management](#contract-management)
    * [Fees management](#fees-management)
    * [Systems management](#systems-management)
    * Attendees management
* Model and test reporsitory management
    * Model management
    * [Tests management](#tests-management) 
    * Binding to Gazelle Master Model  
* Test session management  
    * [Network configuration management](#configuration-management)
    * [Proxy management](#proxy-management) 
    * [Monitor management](#monitor-management) 
    * [Notifications](#test-update-notification)
* Test Execution
    * Test plan and execution
    * Communication between peers

# Basic Terminology

## Test Types

## No Peer

*  Test run by your self. There is no need of a test partner to run this type of test
*  Used for conformance testing of artefacts (document, messages) that the SUT can produce.
*  No peer tests are usually pre-requisite to Peer to Peer tests.

## Peer to Peer

*  Test run with one or two partners SUT. 
*  Test scheduled by SUT manager.

## Group Test

*  Test run with a larger group of SUT 
*  Test scheduled by management

## Testing depth

See more information [*there*](https://gazelle.ihe.net/content/supportivethorough-testing). The testing depth is defined at registration time.

## Thorough Testing

*  For those who do not qualify for Supportive Testing. The default testing mode.

## Supportive Testing

*  For profiles in Final Text
*  For Actor / Integration Profile / Option that have been tested by the vendor in a past connectathon
*  There must be a published integration statement
*  Used for participants who would like to help partners

## Test case

A definition of a test involving one or more IHE actors in one or more profile.

## Test run

Formely known as test instance. Details about the execution of a test case (with specific test partners when peer-to-peer test)

## Test Management Components

### Testing Session Management

Gazelle Test Management can handle multiple testing sessions. The Multiple Test session can be run in parallel.  Monitor and Systems register for specific testing session. One can select the coverage of the testing session in terms of Domain and/or Profile. One can also specify the type of testing to be performed in a testing session : Pre-Connectathon, Connectathon, Certification.

### Users Management

Module for the management of gazelle users and their roles. Handles the users account management: registration, lost password, user information update. Handles the management of the roles of the users.

Users are associated to an organization.
[*Read more...*](#users-and-contacts)


### Contacts Management

Management of the contacts for a participating company. Allows the participating companies to enter contacts within Gazelle. Contacts are mainly used for administrative purposes. Used to identify the financial contact, the contact for marketing questions and the contact for technical questions.

Contacts are NOT required to have a login.

### Organizations Management

Management of all organization relative information. This information is mainly for the purpose of the Connectathon management. Users, Contacts and Systems belongs to an organization. The module allows the management of the information about the organization : Address, Contacts, VAT information,... [*Read more...*](#organization)

### Contract Management

Component that manages the creation of the contract for testing session participants. Summarize the registration of the participants and create a pdf document based on a template that can be customized for each testing session. This makes use of the JasperReport library. The link to the JasperReport template for the contract definition in a testing session, is specified within the properties of the testing session. One has the choice to enable or disable it for a given testing session. [*Read more...*](https://gazelle.ihe.net/content/generation-contract)

### Fees Management

Component for the management of invoice (generation of the invoice, based on a template) and estimation of the amount of the fees to be gathered for an event based on participation. Helpful for connectathon management. Invoice generation is dependent of the testing session and as for the contract based on a template that can be specific to a session.  Can be disabled for a given testing session. [*Read more...*](./admin.html#administrative-and-financial-page)

### Systems Under Test Management

The system under test is a component or a piece of hardware on which test cases are performed. It is operated by a vendor. Module for the management of Systems Under Tests (SUT). Manages the registration of the SUT for a testing session. What are the actors, integration profiles and options supported by each SUT. Allow participants to copy system characteristics from one testing session to the other.

Contains a rules engine that checks if the system correctly implements the IHE requirements of dependencies among the actors and profiles. [Read more...](#registration-process)

### Proxy Management

Interactions with the Proxy component. Control of the proxy, instruct the proxy of the interfaces to open and close for testing purposes. Allow participants to link test instance steps to corresponding messages captured by the proxy.


### Network Configuration Management

Management of the network interfaces of the systems under test and testing tools for a given test session. Knowing that most of the time spent during testing is lost during exchange of connectivity parameters, it is essential that Gazelle allows the test participants to correctly and quickly share network interfaces informations. That component allows the user to provide the information relative to their own systems. It also allows the test participants to quickly find the relevant information about their peers in a test.

### Monitor Management

Management of the monitors. Monitors are the independent and neutral individuals that check and validate the test performed during a testing session. The component allows the Technical Manager to assign tests to specific monitors. It allows the manager to split the load of test verification among the monitors. Test can be assigned to monitors either test by test, or monitor by monitor

### Samples Sharing 

Module for sharing samples. This component allows registered system to share and find samples. Some system need to provide samples, some need to render them. The component allows the participants to easily find the samples relevant for their systems. The sample sharing module is linked to External Validation Services for checking the conformity of the shared samples. The component makes also use of internal validation services

When relevant, the component module allows sample creators to provide a screen capture of the rendering of their samples. It also allows sample readers to provide a screen capture of the rendering of the sample on their system.

A sample and a test can be linked together. This is useful in the case of scrutiny test. [*Read more...*](https://gazelle.ihe.net/content/sample-sharing)

### Test Management

Modules for the definition of the tests. Test requirements per Actor/Profile/Options. The test case contains a summary, a description of the test workflow, the list of participants and, the instructions for the participants.

The test management feature allows the test designers to define the test cases:

* What is the purpose of the test ? (assertion tested)
* Which actors/profile/options are concerned by the test ? (test participants including the cardinality for each participants)
* The scenario of the test (sequence diagram of the interactions between the participants to the test), including the definition of what need to be checked at each step.
* How many executions of the test are required for a SUT to be declared as successful to the test.

Within the test repository, participants can find out which tests need to be performed by their systems.

On the test execution page, participants can execute a test, select the participants (peer system under test or simulators), and report evidences.

### Evaluation

Module to allow the evaluation of the capabilities of the systems under test accepted to the testing session. It allows the project managers to review the tests performed and thus determine whether the SUT successfully demonstrate the correct implementation of a specific actor / integration profile / option combination.

### Single Sign On

When enable component of Gazelle Test Management allows user to use a CAS server for their identification. Allows harmonization of the logins among the different applications. All Gazelle simulators also use the CAS login component. JIRA bug tracking system makes also use of it. [*Read more...*](https://gazelle.ihe.net/content/gazelle-single-sign-authentication-users)

### Notifications on test execution

The purpose of the Test Update Notification is to reduce the load on the server that is hosting the Gazelle application. It allows participants to be informed of updates on the test instances that are of concern for them. Instead of refreshing the Test Execution page all the times the participants are informed of new tests instances creation, updates, comments and grading.



# Home page

From version 6.4.0, users with role vendor or vendor_admin, sees the the "test session process" section when they are logged in. This section reflects the fact that a test session is basically made of four phases. A breadcrumb indicates to the user which phase is ongoing (in blue):

1. Registration: Displayed until the organisation has at least one system accepted to the session;
2. Preparation: Displayed when the organisation has at least one system accepted to the session and until the test session starts (test session starts date);
3. Testing: Displayed from the test session start date to the test session end date; 
4. Evaluation: Displayed after the test session end date.

Completed phases turn into green. Organisation with no system under test accepted to the session are "stuck" in the Registration phase. When the registration deadline is in the past, the breadcrumb turns into Orange to warn the user that he has missed the deadline.

Note that this section gives you information about your ongoing tasks, it does not mean that you are no more able to access the other features of the tool.

# Registration Process

During the Registration process, the Gazelle Test Management Application gathers information needed in order for an organization to participate in an upcoming testing session.

To complete registration for a testing session, an organization uses Gazelle to enter:

- User account(s) in Gazelle
- Organization details
- Contact persons in your orgainzation
- System information - the profiles/actors/options that you will test
- Participant information - people who will attend the testing session

When this information is complete, may be asked to generate a contract in Gazelle for the testing session.

This "How to" details for these tasks follow in the pages below.

## Registration concepts
## Organization
In Gazelle Test Management, an organization is the entity that is presenting test systems at the Connectathon.

IHE publishes Connectathon results per organization, thus, the organization name, address, and finanical contact you enter in gazelle is important.

## Users and Contacts
A “User” has a login and password to access the applications of the Gazelle Test Bed.
Gazelle Test Management is managing the users. Other applications like simulators, validators, EVS Client do not have user management but allow user authentication throught a CAS (Single Sign On) application.

A “Contact” is a person in your organization who will interact with us in preparation for a testing session :

 - Financial Contact (only 1)
 - Marketing Contact (1 or more)
 - Technical Contact (1 or more)

A user may, or may not, be a contact

Users and Contacts belong to an Organization

## Users Roles
There are two levels of users in Gazelle Test Management :

 - Vendor admin role
 	- Approves new user accounts for your organization
 	- Can edit user and contact information
 	- Can edit organization info (eg address)
 	- Can edit all test system & testing details
- Vendor role
	- Can edit all test system & testing details

## System
In Gazelle, a ‘system’

 - provides a set of IHE profiles, actors and options an organization wants to test.
 - participates in the testing session as the SUT


## Testing Session
Gazelle Test Management can manages multiple testing session. A testing sessions may represent:

- Connectathons
- Internet Testing
- Projectathons

When a user logs in to Gazelle, the user is viewing information for a single testing session.  Gazelle always "remembers" the last testing session a user was working in.  A user may switch between testing sessions.  How-to instructions are here.

## Registering and managing users for an organization

A "user" is person with a login and password to access the Gazelle Test Management tool.   A user in Gazelle is always linked to an organization.  A user can only view/modify information for the organization he/she belongs to.

**Creating a new user account**

To create a new user, go to the home page of Gazelle Test Management and click on the link "Create an account"
![Creating an account](./media/notloggedinmenu.png)

New user then needs to fill out the form with valid information.

If the organization is already known in gazelle, select it from the "Organization Name" dropdown list; otherwise select "New company" as the "Organization Name" in order to create a new organization in Gazelle.
![Registration of a user](./media/register_a_user.png)

Upon form submission, an email is then sent to the user in order to confirm the registration.

When entering a new organization in that form the user that is created automatically gets the administration rights for the organization.

When selecting an existing organization in that form. The creation of the account is conditionned to the acceptance of one of the users with the admin role in that organization.

In that case the new user account is not activated until a user with the role vendor_admin in your organization activates the account.

For more information on user rights see the next section on user priviledges.  


**User priviledges in Gazelle**

A user with 'vendor' privileges in Gazelle (the default), can execute the tests and edit the configuration of network interfaces for the organisation's systems under test.

A user with 'vendor_admin' priviledges in Gazelle, is able in addition to the roles of 'vendor' to:

- manage users related to his/her organization (activate/de-activate accounts)
- manage contacts to his/her organization
- manage the testing session attendees
- download the contract for the event
- manage the systems under test

**Managing organization's user account as a "vendor_admin" user**

Users with the vendor_admin role can manage the users in his/her organization from the menu : *Gazelle menu Administration -> Manage users*

You can use this form to activate or de-activate user accounts, or update email or password for a user.

![Edition of a user as a user with vendor_admin role](./media/edit_user_as_vendor_admin.png)


## Register attendees to Testing Session

This page describe how to register the participants to a Testing Session. Participants to the testing session are the person who will get a badge an will be able to enter the floor where the testing in taking place. This feature might not be enabled for all the testing session.

Only the users with the role "vendor\_admin" can register attendees to the testing session.

The list of attendees is available under the Registration menu, in the Event Attendees section. To edit the list of attendees, click on the "Manage attendees" button.

There are 3 means to register a participant to a testing session :

* Import from users
* Import from contacts
* Add participant: the use of this feature is discouraged since it means that the user will not be linked to a Gazelle account

## Import from Users

By selecting the button  **import from users,** one can select the participants to add in the list of registered users for the organization.

## Import from Contacts

By selecting the button **import from contacts,** one can select the participants to add in the list of contacts already declared in Gazelle Test Management tool. Contacts do not need to have a login.


## Registering and managing system for an organization

The system registration page is accessed through the menu : **Registration**

In that page you can : 

* **Add a system** : This will create a new system in the gazelle tool.
* **Add a system from an existing testing session** : This will attach a system that is already registered to another testing session to the session currently selected. The feature is interesting to  register systems both for a connectathon testing session and internet testing session at the same time. Note that when attaching a system from another session to the current session you will not be able to remove actors / profiles registered. If you want to add a system to the current session that is based on a system in a previous session but that does not implement the same list of actors and profiles, then the feature you want to use is the **Import systems from another testing session.**
* **Copy systems from another testing session :** A click on that button will create a new system, initialising it with the information of the selected system. The name and the keyword of the system are appended with the string "\_COPY" in order to distinguish the new system from the old one. 

## Generation of the contract

The Gazelle software system is used to generate a contract for IHE Connectathons that includes the systems to be tested and several contacts within your organization. If any of these are missing, the software will not let you generate a contract.

## First Level Registration Errors

To generate a contract, the administrator for your organization (the person who created the Gazelle account) will select **Registration** from the web interface. Next figure shows common error messages that occur when generating a contract. The error messages in red are generated by Gazelle. The numbers in ( ) are pasted onto the screen capture and refer to notes below the figure.

1.  The mailing address is missing for the company 

    1.  This is for general correspondence with your company. 

    2.  Select **Registration -&gt; Manage company information**

    3.  Under Mailing address, add an address for your company. This is on the right half of the form. Select **Click here to add a new address**. If you already have entered an address but you were presented with the error message, you need the next step that associates the address as your company mailing address. This first step adds an address to our database, but does not make it your mailing address.

    4.  The address that you entered is presented in a table with the rightmost column an action column. If you have added a second address, you will see a table with two lines. To associate an address as your mailing address, click on the **Select** link in the appropriate row.

    5.  You will now see the address copied from the right part of the form to the left side of the form, including the country flag. Scroll down and select Save. That should get rid of error (1).

2.  Missing address for Financial Contacts

    1.  This address is for the person in your organization responsible for processing our requests for payment.

    2.  Select **Registration -&gt; Manage company information** 

    3.  Scroll down to the Billing address section. Add an address on the right side of the form; Select **Click here to add a new address.** 

        1.  You might already have an address for your company that is the same for thisperson. Great; skip down to iii.

        2.  If you don’t have an address registered for this financial contact or the address of the financial contact differs from the main address, add it now.

        3.  In the table of addresses on the right side of the form, under the Billing address section, click on the Select link in the appropriate row.

        4.  You will now see the address copied from the right part of the form to the left side of the form.

    4.   Scroll down and select the Save button.

3.  Missing Technical Contacts

    1.  We need to verify that there are one or more staff members in your organization we can contact for testing and other technical details.

    2.  Select **Administration -&gt; Manage contacts **

    3.  You will see a table of contact points in your organization. That table might be empty or already have members. You can add a contact person with the Add a contact button in the upper right corner. For this error (Missing Technical Contacts), make sure you select the checkbox for Technical.

    4.  If the Technical Contact was already listed in the table but is not indicated as a Technical Contact in the Professional functions column, edit the row for that person (right most column of the table, edit button). Select the checkbox for Technical and do not forget to hit Save. 

    5.  We require at least one contact entered in the system for the generation of the contract. You are welcome to enter more technical contacts. This will be useful for organizations that have multiple systems, each with a separate technical contact. 

    6.  If you are bringing one system and three engineers, please enter one candidate as your technical contact. That person would be able to answer most technical questions or delegate to someone else. Please do not enter a person who is merely going to contact your engineers and pass along the questions.

4.  Missing Marketing Contacts 

    1.  We need to verify that there are one or more staff members we can contact if we have marketing questions (is there a logo we can use in documentation? What is the exact name to use for your company?). 

    2.  Refer to (3) above. Rather than selecting the checkbox for Technical, select the checkbox for Marketing/Commercial. 

    3.  The same person can fill the roles for Billing, Technical and Marketing/Commercial. We split them out for those organizations that have separate duties. 

5.  Missing System Information 

    1.  If there is missing system information, you have registered zero systems for testing. The purpose of the contract is to explicitly list what you will test and include a computation for fees. You will need to register the system or systems you wish to test and all profile/actor pairs to be tested. 

    2.  Select Registration 

    3.  Enter one or more systems. 

        1.  Make sure you save the systems. 

        2.  Make sure you select the Profile/Actors tab and enter what you want to test. Check for missing dependencies (ATNA requires CT). 

        3.  Fill in information if you plan to participate in a Showcase/Demonstration 

6.  Missing Domain Information

    1.  This means you have either entered zero systems, or you have entered systems but did not add any capabilities (Profile/actor tab).

    2.  Refer back to (5) for details.

## Second Level Errors: Dependencies

Many IHE Integration Profiles have dependencies defined in the documentation. One example is that an ATNA Secure Node is always required to implement the Time Client actor in the Consistent Time profile. When you enter registration information, there is a button available to you labeled Check for Missing Dependencies. Rather than automatically register you, the system gives you this button to let you check when you feel it is appropriate. A box will pop up, list the missing dependencies, and give you the option to click a link to add them on the spot.

When you return to the financial page, one section will list all of your registered systems. The grid will give the system name, domains tested and fee information. One column will also indicate if all dependencies have been resolved. A green icon indicates sufficient registration; a red icon means that a dependency needs to be satisfied.

You cannot generate a contract if there are dependencies to be resolved, so you need to return to the page for managing systems.

* The rules defining dependencies are written in the Technical Frameworks. We transcribe those rules into Gazelle and might make mistakes. Please check carefully. We may have added a dependency that is not a requirement, or left one out. If we have omitted a dependency in our automated rules and you do not register, you are still responsible for those tests during the Connectathon.

* The rules defining dependencies do not identify all combinations that you will find important. For example, the XDS.b profile requires the Document Source and Document Consumer to obtain a consistent patient identifier for an Affinity Domain but does not specify the mechanism. We support that with the PIX and PDQ profiles for Connectathon testing, but do not require that. Participants fail to register for PIX or PDQ and are then surprised to realize they do not have a mechanism to obtain the patient identifier in the Affinity Domain.

## User Preferences

User preferences are mainly used by Test Management application to customize some views according to the user's wishes. The main preferences you may want to update are

* The number of results (rows) in table

* Whether or not you want the diagram sequence of a test to be displayed on the Test Instance page. If not, you may save some space in the page, the one will be lighter to load.

User preferences can also be used to communicate some useful pieces of information to monitors and other connectathon participants, such as your photo, the languages you speak...

To configure your own preferences, you have first to be logged in. Then, on the top right corner of Test Management, hit the link with your user name and preferences (shown below).

This link leads you to your preferences management page. If you never change anything, the page may look something like this.

![](./media/usr_pref.png)

As you can see in the screen capture above, this page also enables you to change your password. Some fields have been recently added as

* **Skype account** During the connectathon week, it can be useful to communicate using Skype. Such a field already exists for the system but a monitor for example, who does not own systems may want to "publish" his/her Skype name to speak with other monitors or participants.

* **Table Label** During the connectathon week, you will sit at a table, the one will be localized by everybody thanks to a table label, typically A1, J10... When you sit the first day of the connectathon, fill this value so that other attendees will find you more easily.

* **Spoken languages** The languages you are able to speak.

Hit the Edit button to update those fields. The following panel will be displayed.

![](./media/usr_pref_edited.png)

When you hit the "plus" icon, a menu is displayed and you can pick up your language(s). If need, hit the "red cross" button in front of a language to remove it from the list. When finish, hit the "finish" button.

![](./media/spoken_language.png)

Do not forget to save your changes before leaving this page.

# Testing Session

## Concept of the Testing Session

Gazelle Test Management can handle multiple testing session. There are testing sessions for multiple purposes. 

* Connectathon and Projectathon
* Online testing events
* Conformity assessment

Testing sessions are created by Gazelle Test Management administrators and can be configured for the purposes listed above.

## Selecting a Testing Session

In order to change testing session, you need to click on "Join another session" in the menu bar. If you are logged in the session set as "default" by the tool administrator, the button is blue, otherwise, the button is orange: 

![](./media/switch_session_in_menu.png)

Then select the session of your choice and you will be redirected to the home page.

![](./media/select_a_session.png)

Make sure that once you have completed that task, the top of the screen displays the name of the testing session that you have selected.

Note that since Gazelle Test Management version 3.5 we can select different color scheme to differentiate the sessions.

# Sample Sharing

This section is about sharing samples in the Gazelle Test Management application. Samples can be shared for testing purpose. Connectathon and Pre-connectathon scrutiny tests requires participants to share sample. Gazelle allows the participants to share the sample, find the samples shared by partners and perform validation of samples using External Validation Services access.

## Concepts of sample sharing

Gazelle has a feature that allows to share samples among the participants.

We are calling "sample" any document or object that an application creates and is used by another application. Typically samples are : 

* sample DICOM object
* sample ISO image of DICOM CD-ROM or DVD
* sample CDA documents
* sample XDW documents...

Gazelle Test Management provides the users with the list of possible sample to share or consume based on the registration of actor/profiles pair. 

## Sharing samples

Samples are accessed from the Testing > Sample exchange menu entry.

Example of the screen shown to the participants with the list of possible samples that can be shared for the systems selected.

![](./media/sample_to_share.png)

Exemple of the screen shown to the participants with the list of samples that partners have provided for sharing and are of concerns to the selected systems. 

![](./media/samples_to_render.png)

# Configuration of Network interfaces

Your systems under test acting as a responder expose network interfaces to the other participants. To help you identify the service you need to expose, Gazelle Test Management generates a default network interface configuration for each service your system is expected to expose. Once the Testing Session manager has generated the default configurations, participants can edit them and approve them.

This section describe how to edit and approve the network interfaces in Gazelle Test Management

The configurations are accessed through the menu "Preparation > SUT's network interfaces" or "Preparation > ORG_KEYWORD: Network interfaces .

If your system acts as an initiator, you might also be asked to share a few details like the AETitle (DICOM), or the sending facility/application names (HL7).

## HL7v2

This page present the form to edit the HL7v2 configurations :

![](./media/add_a_config.png)

## Extract Configs from Gazelle

This page explains how to export the configuration information from Gazelle in a format the SUT can use to configure themselves.

There are 2 methods to get the configurations from test partners :

* Using parametric URL webservices
* Using SOAP webservices

For the moment the only export format is CSV (Comma Separated Values) files generation. 

## Export Peers configuration parameters from the GUI

When searching for peers configurations in Gazelle (menu Preparation-&gt; SUT's network interfaces), click on the link "URL for downloading configurations as CSV" 

![](./media/all_conf_new_menu.png)

This URL is accessing the parametric service for downloading configurations.

**testingSessionId,** **configurationType** and **systemKeyword** are parameters that can be set by accessing the URL directly :

Europe : [*https://gazelle.ihe.net/TM/systemConfigurations.seam*](https://gazelle.ihe.net/TM/systemConfigurations.seam)

North America : [*https://gazelle.iheusa.org/gazelle-na/systemConfigurations.seam*](https://gazelle.iheusa.org/gazelle-na/systemConfigurations.seam)

System keyword is given if you use the GUI.

You can build the url that matches your need and have periodic query to the tool in order to feed your SUT with the most up to date information from the database.

Here are examples on how to use it : 

* [*https://gazelle.ihe.net/EU-CAT/systemConfigurations.seam?**testingSessionId**=25&**configurationType**=HL7V2InitiatorConfiguration*](https://gazelle.ihe.net/EU-CAT/systemConfigurations.seam?testingSessionId=25&configurationType=HL7V2InitiatorConfiguration)
* [*https://gazelle.ihe.net/EU-CAT/systemConfigurations.seam?**testingSessionId**=25&**systemKeyword**=ADT\_AGFA\_ORBIS\_8.4*](https://gazelle.ihe.net/EU-CAT/systemConfigurations.seam?testingSessionId=25&systemKeyword=ADT_AGFA_ORBIS_8.4)

The response is a CSV file like this one : 
```bash
"Configuration Type", "Company",  "System" , "Host" , "Actor" , "is secured", "is approved" , "comment" , "aeTitle", "sopClass","transferRole","port" ,"port proxy" ,"port secured"

"DICOM SCU","AGFA","WS\_AGFA\_0","agfa13","IMG\_DOC\_CONSUMER","false","false","For optional DICOM Q/R","WS\_AGFA\_0","QR","SCU","","",""

"DICOM SCU","AKGUN","PACS\_AKGUN\_2012","akgun10","PPSM","false","false","","PACS\_AKGUN\_2012","MPPS","SCU","","",""
```

## Export peers configuration parameter using SOAP webservices

The wsdl of the webservice to access the peers configuration parameter is located there :

* For Europe :
  * [*https://gazelle.ihe.net/gazelle-tm-gazelle-tm-ejb/ConfigurationsWS?wsdl*](https://gazelle.ihe.net/gazelle-tm-gazelle-tm-ejb/ConfigurationsWS?wsdl)
* For NA :
  * [*https://gazelle.iheusa.org/gazelle-tm-gazelle-tm-ejb/ConfigurationsWS?wsdl*](https://gazelle.iheusa.org/gazelle-tm-gazelle-tm-ejb/ConfigurationsWS?wsdl)

The function of the SOAP webservice are richer than the REST as the it allows filtering on the actor concerned by the configuration. If this functionality is need/requested it will be added to the REST service as well. Note that we do not provide a sample client for that service.

## OID Management

This page explains how to access the OID values assigned to the systems participating to a testing session.

There are 3 methods for that purpose :

* Export as an Excel file from the GUI
* Using REST webservice
* Using SOAP webservice

## Export OIDs from GUI

You can get the list of OIDs from the GUI : Preparation --&gt; OID registry. On this page, you can do a search for a specific OID by filtering on the institution, the systemKeyword, the testing session, the integration profile, the actor, the option, and the label of the OID (homeCommunityId, organization OID, etc).

You can then use the link "Export as Excel file" to get an xls file containing all OIDs looking for.

## Export OIDs using Rest webservices

You can generate directly a CSV file containing the oid, the label and the system keyword, by using REST webservice. The URL of the webservice is https://gazelle.ihe.net/TM/oidSystems.seam?systemKeyword=XXX&testingSessionId=YYY&requirement=ZZZ

where arguments used are :

| **Argument**     | **Opt** | **Type** | **List of Possible Values**             |
|------------------|---------|----------|-----------------------------------------|
| systemKeyword    | O       | String   |                                         |
| testingSessionId | R       | Integer  |                                         |
| requirement      | O       | String   |- sourceID OID<br/>  - sender device id.root OID<br/> - repositoryUniqueID OID<br/>  - receiver device id.root OID<br/>  - patient ID assigning authority OID<br/>  - homeCommunityID OID<br/>  - organization OID      |


## Export OIDs using SOAP webservices

The wsdl of the webservice to access the OIDs of systems is located there :

[*https://gazelle.ihe.net/gazelle-tm-gazelle-tm-ejb/ConfigurationsWS?wsdl*](https://gazelle.ihe.net/gazelle-tm-gazelle-tm-ejb/ConfigurationsWS?wsdl)

The concerned methods are :

*  getListOIDBySystemByRequirementAsCSV : return a csv string which contains all OIDs searched by systemKeyword, testingSessionId and oidRequirementLabel
*  getOIDBySystemByRequirement : return exactly the first OID searched by systemKeyword, testingSessionId and oidRequirementLabel

![](./media/soap_ui_envelop.png)

Note that here again the testingSessionId is a required argument, then you need to specify either the systemKeyword or the requirement, or the both of them.

## Network Configuration

The page Network Configuration provides the users with the necessary information for the current testing session.

The information provided in that page is of 2 kinds. 

* A blob where the administrator of the current testing session can provide to the participants a list of usefull an relevant information. This can be the networking parameters: 
    * Default gateway
    * DNS
    * Netmask 

But it may also contain the information for the printer that is on site.

The page also provides a button to download a hosts file for the configuration of the systems that do not support DNS. 

The preferred method is for the participants to the testing session to use DNS in order to resolv hostnames. However we have encountered some systems in some past session that could not use DNS. So we have provided a mean to give the participants the access to a hosts file that can be installed on their SUT for name resolution. 

Note that hosts file is NOT dynamic and the hosts file will have to be reloaded and reconfigured by the participants who have chosen not to use DNS after each test partner IP or name change. 

Once the hosts file is downloaded it can be used to configure the SUT. Please refer to the operating system of the SUT for the set up of that file. Below is a list of pointer for 3 OSes

-   [*Configuration of host file on windows system*](http://helpdeskgeek.com/windows-7/windows-7-hosts-file/)
-   [*Configuration of host file on MacOsX system*](https://discussions.apple.com/thread/2493759)
-   [*Configuration of host file on Linux system*](http://community.linuxmint.com/tutorial/view/159)

# Preparatory Testing

_Note to test designers: from version 6.6.0, Pre-Connectathon feature has been merged into the Test Execution feature. If you have Pre-Connectathon tests defined, change their type to "Preparatory"_.

Testing before testing event is often a requirement and is achieved using testing tools.

An index to all available tools for testing IHE profiles is provided at the following URL: [*http://wiki.ihe.net/index.php?title=IHE\_Test\_Tool\_Information*](http://wiki.ihe.net/index.php?title=IHE_Test_Tool_Information)

Gazelle helps testing session participants managing their preparatory tests: 

*  Based on the capabilities declared for each SUT, Gazelle will identify the list of test that needs to be executed.
*  Gazelle provides information about the location of the documentation of the tests
*  Gazelle provides a mechanism for a participant to return the test logs and mark the test as completed.

The preparatory tests are accessible from Preparation > Preparatory tests menu.

# Test execution

Gazelle Test Management users access the Test execution page through the menu "Testing -&gt; Test execution " as shown on the following screen capture.

The dashboard offer advanced filtering capabilities and allows the user to have an overview of the testing progress from different angles.

![Extract of the test execution page](./media/test_execution.png)

The test execution page can be filtered according to several criteria. When you select more than one criteria, the AND clause applies.

For each SUT capability (represented by a panel), the vendor has access to:
* The testing depth approved for that capability (Thorough vs Supportive);
* Until the evaluation is performed, the progress for the capability, in terms of test runs. That is to say:
    * Total is the sum of the targets for the required test cases.
    * Number of completed is the sum of verified (or self-verified) test runs, in the limit of the target for each test. 
* The result of the evaluation (status), when made available by the management team
* The list of test cases to be executed for this capability

For each test case:
* Its name, short summary (hover the i icon), and acces to the test case description (click the i icon)
* Its type (Preparatory, Connectathon, Conformity assessment or whatever test type defined by the test designers)
* Whether it is optional or required
* The number of successful runs you need to pass the capability (Target)
* The list of possible partners including the list of partners you have already tested against (click on the numbers)
* The list of runs along with their status

Focus on the target:
The target is the number of test runs you need to successfully execute for the management team to mark your SUT capability as passed.

Target for required test cases is displayed in orange, it turns into green when the target is reached (or over reached). The badge shows how many runs are taken into account for the evaluation (NB VERIFIED / NB EXPECTED).

Target for optional test cases is displayed in grey, it turns into green when the target is reached.

## Quick filters

During the preparation and testing phases, when you execute the tests, you might want to display only a subset of the SUT capabilities registered by your organisation. You can do so using the filters. Clicking on the floppy disk in the search criteria panel will create a quick filter. You are asked to give it a name, and it will be added on the top of the page. Clicking on it at any moment will automatically set the filters as they were when you created the quick filter.

You can make a quick filter your favorite Test Execution page by clicking on the star icon. In that case, each time you access the Test Execution page, the favorite quick filter will be applied. Use the "clear filter" button if you need to display back all the available entries.

From a technical point of view, quick filters are cookies, it means that:
* They expire after 6 weeks;
* They will vanish if you delete the cookies for the Gazelle site, use the Incognito mode of your Internet browser, or have configured your browser to delete all cookies when you close the tab or window.

## Monitor Workflow

## Test validation workflow

*  Find test in the monitor worklist
*  Claim test (release it if necessary)
*  Review test requirements
*  Visit participants, look for evidence
*  Grade the test :
  *  Verified -&gt; Done
  *  Failed -&gt; Enter comments, done
  *  Partially Verified -&gt; Enter comments, done

## Test Instance Workflow

## Test Instances Statuses

A test instance can have the following statuses :

*  **Running** : this is the initial state of a test instance, once the user press on the "Start" button.

*  **Paused** : A test instance can be paused, and the restarted by the user.

*  **Aborted** : If the test instance was started per mistake, or is a duplicate of an existing test instance. Then one can abort it. Aborted test instances are not checked by monitors. 

*  **To be verified** : Once a test instance is completed, the user can set it status to "to be verified". Monitors work list contains test instances with that status.

*  **Failed** : Based on the evidences and the observation of the logs or the actual run of the test by vendors, a monitor may fail a test. 

*  **Verified** : A monitor once convinced that the test is successful can mark it as verified

*  **Partially verified** : If a monitor thinks that a test is incomplete but that their is a chance that the vendor may fix the problem during the test session, then he/she can mark the test as partially verified. 

*  **Critical** : Sometime toward the end of the connectathon, the project manager activates the "critical" status mode in Gazelle. Monitors verify then the Test instances with the status critical first. 

## Test Instances Transitions states

### From the Vendor perspective

![](./media/vendor_perspective.png)

### From the Monitor perspective

Monitors work on test instances that have one of  the 3 following status:

*  To be verified
*  Critical
*  Partially Verified

The output status are : 

*  Failed
*  Verified
*  Partially Verified

![](./media/monitor_perspective.png)

### Complete state diagram for Test Instances

![](./media/TI_diag.png)

## Validation of test steps logs within test instance

The aim of this page is to explain how the validation result displayed on a test instance page are retrieved from the EVSClient tool.

When a test instance is created, the page looks like:

![](./media/test_steps.png)

There are different possibilities, like the next sequence diagram shows :

![](./media/seq_diag.png)

If we choose the first case, which is to add the permanent link from the proxy in a step message :

![](./media/steps_add_url.png)

When it 'is added :

![](./media/steps_url_added.png)

The file is to be validated in Proxy to EVSClient, the test instance page looks like (use the refresh button is not) :

![](./media/steps_url_validated.png)

We can see in data column  the last validation status from EVSClient.

We can see in Actions column :

*  A color button (green=passed, blue=unknown or not performed, red=failed)
*  The refresh button
*  A map button which is an hyperlink to go to the posted link.

If the user click on color button, then he is redirected towards the last result of validation.

If the user click on refresh button, then TM reloads the last validation status.

The button's color evolves according to the result and the date is update.



# Internet Testing

The purpose of this chapter is to explain how to use gazelle test management in order to perform testing over the internet (aka Online test sessions).

## Specificity of Internet testing

Internet testing using Gazelle Test Management is very similar to testing during the connectathon. The major difference between connectathon testing and Internet testing is an increased difficulty to achieve the communication between the different test partners.

*  Test Partners are not located in the same room. 
*  Communication between the SUT needs to go through corporate firewalls 

## Registration

Registration to an Gazelle Internet testing session is easy. There is no need to create a specific system for the internet testing session. One can import a system that is already registered within another testing session. 

# Administration

## Administer users

*  Users with the role admin can edit users from all companies registered into the system.
*  Users with the role vendor\_admin can edit the users linked to their own company.

As a user with the role ***vendor\_admin*** visit the page **Registration** -&gt; **Manage Users**

## Import/export of Model elements

Administrater user are able to import or export model elements from the instance of Gazelle Test Management they are working on. 

## Import/Export of TF Rules

### Import

TF Rules can be extracted from the Gazelle Test Management instance into an XML file. This file can be later on used to import those TF Rules in any other
instance of the tool.
To do so go to the TF/Edit TF Rules menu :

![](./media/tf_menu.png)

Once you accessed the TF Rules page, you will have the list of all TF Rules for your instance displayed. Those rules can be filter base on Actor, Integration Profile
or option. To export them simply click the __Export Filtered TF Rules__ button. This will create the XML file corresponding to the filtered TF Rules and
automatically downloads it.

![](./media/tf_rules_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<aipoRules>
    <aipoRule>
        <cause xsi:type="aipoSingle" actorKeyword="KWActor1" integrationProfileKeyword="KWIP1" optionKeyword="KWOption1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
        <consequence xsi:type="aipoList" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <or>false</or>
            <aipoCriterions>
                <aipoSingle actorKeyword="KWActor3" integrationProfileKeyword="KWIP3" optionKeyword="KWOption3"/>
                <aipoSingle actorKeyword="KWActor4" integrationProfileKeyword="KWIP4" optionKeyword="KWOption4"/>
                <aipoSingle actorKeyword="KWActor5" integrationProfileKeyword="KWIP5" optionKeyword="KWOption5"/>
            </aipoCriterions>
        </consequence>
        <comment>This is the first Rule of our list</comment>
    </aipoRule>
    <aipoRule>
        <cause xsi:type="aipoList" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <or>false</or>
            <aipoCriterions>
                <aipoSingle actorKeyword="KWActor9" integrationProfileKeyword="KWIP9" optionKeyword="KWOption9"/>
                <aipoList>
                    <or>true</or>
                    <aipoCriterions>
                        <aipoSingle actorKeyword="KWActor11" integrationProfileKeyword="KWIP11" optionKeyword="KWOption11"/>
                        <aipoSingle actorKeyword="KWActor12" integrationProfileKeyword="KWIP12" optionKeyword="KWOption12"/>
                        <aipoSingle actorKeyword="KWActor13" integrationProfileKeyword="KWIP13" optionKeyword="KWOption13"/>
                    </aipoCriterions>
                </aipoList>
            </aipoCriterions>
        </cause>
        <consequence xsi:type="aipoSingle" actorKeyword="KWActor7" integrationProfileKeyword="KWIP7" optionKeyword="KWOption7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
        <comment>This is the Second Rule of our list</comment>
    </aipoRule>
<aipoRules>
```

Now with this file, you can import TF Rules into any instance of the tool. 

### Export

On the same page, at the bottom you will find a panel titled __Import TF Rules__.

![](./media/tf_rules_import_panel.png)

This panel explains the basic rules that are used for TF Rules import : 

* Actor/Integration Profile/Option detection is based on keywords. If one does not appear in the database the criterion will be ignored
* If a "and" or "or" list of criterion gets down to one element, it will be kept as a single criterion
* If a Rule miss either a cause or a consequence criterion, it will be ignored by the import

You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the TF Rules or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what elements were found missing, what rules have been ignored, what rules will be imported and what rules have been found duplicated 
and won't be imported. 

![](./media/tf_rules_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import TF Rules__ button on the bottom of the report to validate the changes
and import them into the Gazelle Test Management instance. If something is not ok with the uploaded document, just click the __Reset__ button. 
Changes will not be made on the tools data and you will be able to upload another document.

## Import/Export of Standards

### Import

Standards can also be extracted into an XML file. The principle is very similar to what is done with other model elements.
To export Standards, go to the Specifications > Standards menu :

Once you accessed the Standards page, you will have the list of all Standards for your instance displayed. Those standards can be filter base on Keyword,
Name or Network Communication Type. To export them simply click the __Export Filtered Standards__ button. This will create the XML file corresponding to 
the filtered Standards and automatically downloads it.

![](./media/tf_standards_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<standards>
    <standard>
        <keyword>StandardKW1</keyword>
        <name>StandardName1</name>
        <version>v1</version>
        <networkCommunicationType>DICOM</networkCommunicationType>
        <url>https://www.test1.com</url>
        <transactions>
            <transactionKeyword>TransactionKW2</transactionKeyword>
        </transactions>
    </standard>
    <standard>
        <keyword>StandardKW3</keyword>
        <name>StandardName3</name>
        <version>v3</version>
        <networkCommunicationType>HTTP</networkCommunicationType>
        <url>https://www.test3.com</url>
        <transactions>
            <transactionKeyword>TransactionKW4</transactionKeyword>
            <transactionKeyword>TransactionKW5</transactionKeyword>
            <transactionKeyword>TransactionKW6</transactionKeyword>
        </transactions>
    </standard>
</standards>

```

Now with this file, you can import those Standards into any instance of the tool.     

### Export

On the same page, at the bottom you will find a panel titled __Import Standards__.

![](./media/tf_standards_import_panel.png)

This panel explains the basic rules that are used for Standard import : 

* Transaction detection is based on keywords. If one does not appear in the database the transaction link with the Standard will be ignored
* The imported Standard will be linked to all transaction found in the XML that are also in the database

You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the Standards or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what Transactions were found missing, what Standards will be imported and which one have been found duplicated and won't be imported. 

![](./media/tf_standards_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import Standards__ button on the bottom of the report to validate the changes
and import them into the Gazelle Test Management instance. If something is not ok with the document, click the __Reset__ button. Changes will not be made on
the tools data and you will be able to upload another document.


## Import/Export of SUT network interface's templates

### Import

Network interfaces templates based on interoperability model can also be extracted into an XML file. The principle is very similar to what is done with other model elements.
To export Network interfaces templates, go to the Specification > SUT network interface's templates :

Once you accessed the Network interfaces templates page, you will have the list of all templates for your instance displayed. Those templates can be filter based 
on Domain, Integration Profile and Actor. To export them simply click the __Export Filtered Configurations__ button. This will create the XML file
corresponding to the filtered configurations and automatically downloads it.

![](./media/tf_configurations_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<configurationTypeMappedWithAIPOList>
    <configurationTypeMappedWithAipo>
        <actorIntegrationProfileOption>
            <actorIntegrationProfile>
                <actor>
                    <keyword>KWActor3</keyword>
                    <name>NameActor3</name>
                </actor>
                <integrationProfile>
                    <keyword>KWIP3</keyword>
                    <name>NameIP3</name>
                </integrationProfile>
            </actorIntegrationProfile>
            <integrationProfileOption>
                <keyword>KWOption2</keyword>
                <name>NameOption2</name>
                <description>Unused Description2</description>
                <toDisplay>true</toDisplay>
            </integrationProfileOption>
            <maybeSupportive>true</maybeSupportive>
        </actorIntegrationProfileOption>
        <configurationTypeWithPortsWSTypeAndSopClassList>
            <configurationTypeWithPortsWSTypeAndSopClass>
                <configurationType name="Type4" category="Category4" usedForProxy="false"/>
                <webServiceType>
                    <description>Description4</description>
                    <needsOID>false</needsOID>
                    <integrationProfile>
                        <keyword>KWIP4</keyword>
                        <name>NameIP4</name>
                    </integrationProfile>
                </webServiceType>
                <wsTransactionUsage>
                    <usage>Usage4</usage>
                    <transaction>
                        <keyword>TRKW4</keyword>
                        <name>Name4</name>
                        <description>Desc4</description>
                    </transaction>
                </wsTransactionUsage>
                <transportLayer keyword="TLKW4" name="TLName4"/>
            </configurationTypeWithPortsWSTypeAndSopClass>
        </configurationTypeWithPortsWSTypeAndSopClassList>
    </configurationTypeMappedWithAipo>
    <configurationTypeMappedWithAipo>
        <actorIntegrationProfileOption>
            <actorIntegrationProfile>
                <actor>
                    <keyword>KWActor7</keyword>
                    <name>NameActor7</name>
                </actor>
                <integrationProfile>
                    <keyword>KWIP7</keyword>
                    <name>NameIP7</name>
                </integrationProfile>
            </actorIntegrationProfile>
            <integrationProfileOption>
                <keyword>KWOption6</keyword>
                <name>NameOption6</name>
                <description>Unused Description6</description>
                <toDisplay>true</toDisplay>
            </integrationProfileOption>
            <maybeSupportive>true</maybeSupportive>
        </actorIntegrationProfileOption>
        <configurationTypeWithPortsWSTypeAndSopClassList>
            <configurationTypeWithPortsWSTypeAndSopClass>
                <configurationType name="Type8" category="Category8" usedForProxy="false"/>
                <webServiceType>
                    <description>Description8</description>
                    <needsOID>false</needsOID>
                    <integrationProfile>
                        <keyword>KWIP8</keyword>
                        <name>NameIP8</name>
                    </integrationProfile>
                </webServiceType>
                <wsTransactionUsage>
                    <usage>Usage8</usage>
                    <transaction>
                        <keyword>TRKW8</keyword>
                        <name>Name8</name>
                        <description>Desc8</description>
                    </transaction>
                </wsTransactionUsage>
                <transportLayer keyword="TLKW8" name="TLName8"/>
            </configurationTypeWithPortsWSTypeAndSopClass>
        </configurationTypeWithPortsWSTypeAndSopClassList>
    </configurationTypeMappedWithAipo>
</configurationTypeMappedWithAIPOList>
```

Now with this file, you can import those Configurations into any instance of the tool.     

### Export

On the same page, at the bottom you will find a panel titled __Import Configurations__.

![](./media/tf_configurations_import_panel.png)

This panel explains the basic rules that are used for Configurations import : 

* If the Actor/IntegrationProfile/Option linked to the configuration does not exist in database, the configuration will be ignored.
* If a Transaction linked to a configuration does not exist, the configuration will be ignored.
* Model Elements detection is based on keywords

You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the Configurations or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what Model Elements were found missing, what Configurations will be ignored, what Configurations will be imported and which one have
been found duplicated and won't be imported. 

![](./media/tf_configurations_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import Configurations__ button on the bottom of the report to validate the changes
and import them into the Gazelle Test Management instance. If something is not ok with the document, click the __Reset__ button. Changes will not be made on
the tools data and you will be able to upload another document.


## Import/Export of Audit Messages

### Import

Audit Messages can also be extracted into an XML file. The principle is very similar to what is done with other model elements.
To export Audit Messages, go to the Specifications > Audit Message menu :

Once you accessed the Audit Messages page, you will have the list of all Audit Messages for your instance displayed. Those Audit Messages can be filter base 
on Event, Transaction, OID, Actor or Document Section. To export them simply click the __Export Filtered Audit Messages__ button. This will create the XML file
corresponding to the filtered Audit Messages and automatically downloads it.

![](./media/tf_auditmessages_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<auditMessages>
    <auditMessage>
        <id>0</id>
        <auditedEvent>PATIENT_RECORD_EVENT</auditedEvent>
        <oid>OID0</oid>
        <auditedTransaction>
            <keyword>TKW0</keyword>
            <name>TName0</name>
            <description>TDesc0</description>
        </auditedTransaction>
        <issuingActor>
            <keyword>AKW0</keyword>
            <name>AName0</name>
        </issuingActor>
        <documentSection>
            <section>Section1</section>
            <document>
                <document_md5_hash_code>Hash2</document_md5_hash_code>
            </document>
            <type>TITLE</type>
        </documentSection>
        <comment>Comment0</comment>
        <eventCodeType>ECT0</eventCodeType>
    </auditMessage>
    <auditMessage>
        <id>3</id>
        <auditedEvent>PATIENT_RECORD_EVENT</auditedEvent>
        <oid>OID3</oid>
        <auditedTransaction>
            <keyword>TKW3</keyword>
            <name>TName3</name>
            <description>TDesc3</description>
        </auditedTransaction>
        <issuingActor>
            <keyword>AKW3</keyword>
            <name>AName3</name>
        </issuingActor>
        <documentSection>
            <section>Section4</section>
            <document>
                <document_md5_hash_code>Hash5</document_md5_hash_code>
            </document>
            <type>TITLE</type>
        </documentSection>
        <comment>Comment3</comment>
        <eventCodeType>ECT3</eventCodeType>
    </auditMessage>
</auditMessages>
```

Now with this file, you can import those Audit Messages into any instance of the tool.     

### Export

On the same page, at the bottom you will find a panel titled __Import Audit Messages__.

![](./media/tf_auditmessages_import_panel.png)

This panel explains the basic rules that are used for Audit Messages import : 

* Actor/Transactions detection is based on keywords. If one does not appear in the database the Audit Message will be ignored
* If a Document Section does not exist on this instance, the Audit Message will be ignored as well

You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the Audit Messages or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what Model Elements were found missing, what Audit Messages will be ignored, what Audit Messages will be imported and which one have
been found duplicated and won't be imported. 

![](./media/tf_auditmessages_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import Audit Messages__ button on the bottom of the report to validate the changes
and import them into the Gazelle Test Management instance. If something is not ok with the document, click the __Reset__ button. Changes will not be made on
the tools data and you will be able to upload another document.


## Import/Export of Documents

### Import

Documents can also be extracted into an XML file. The principle is very similar to what is done with other model elements.
To export Documents, go to the Specifications > Documents :

Once you accessed the Documents page, you will have the list of all Documents for your instance displayed. Those Documents can be filter base 
on Domain, type, volume, title, revision, date of publication, life-cycle status and name. To export them simply click the __Export Filtered Documents__ 
button. This will create the XML file corresponding to the filtered Documents and automatically downloads it.

![](./media/tf_documents_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<documents>
    <Document>
        <name>IHE_ITI_TF_Vol2a</name>
        <title>IHE ITI Technical Framework Volume 2a (ITI-TF-2a)</title>
        <revision>11.0</revision>
        <type>TECHNICAL_FRAMEWORK</type>
        <url>http://ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol2a.pdf</url>
        <volume>2a</volume>
        <domain>
            <keyword>ITI</keyword>
            <name>IT-Infrastructure</name>
            <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
            <id>2</id>
        </domain>
        <linkStatus>0</linkStatus>
        <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
        <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
        <document_md5_hash_code>bf130d69e3a4bcf8f30cfdded2f0504c</document_md5_hash_code>
        <dateOfpublication>2014-09-23T00:00:00+02:00</dateOfpublication>
    </Document>

    <Document>
        <name>IHE_ITI_TF_Vol2b</name>
        <title>IHE ITI Technical Framework Volume 2b (ITI TF-2b)</title>
        <revision>11.0</revision>
        <type>TECHNICAL_FRAMEWORK</type>
        <url>http://ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol2b.pdf</url>
        <volume>2b</volume>
        <domain>
            <keyword>TOTO</keyword>
            <name>IT-Infrastructure</name>
            <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
            <id>2</id>
        </domain>
        <linkStatus>0</linkStatus>
        <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
        <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
        <document_md5_hash_code>5e8e3def16e02c51e47c31260d1ce599</document_md5_hash_code>
        <dateOfpublication>2014-09-23T00:00:00+02:00</dateOfpublication>
    </Document>

    <DocumentSection>
        <section>Tartiflette1</section>
        <document>
	    <name>Tartiflette</name>
	    <title>Tartiflette Volume 1 (ITI TF-1)</title>
	    <revision>69.0</revision>
	    <type>TECHNICAL_FRAMEWORK</type>
	    <url>http://ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol1.pdf</url>
	    <volume>1</volume>
	    <domain>
	        <keyword>ITI</keyword>
	        <name>IT-Infrastructure</name>
	        <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
	        <id>2</id>
	    </domain>
	    <linkStatus>0</linkStatus>
	    <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
	    <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
	    <document_md5_hash_code>TaRTIFLETTE</document_md5_hash_code>
	    <dateOfpublication>1995-09-14T00:00:00+02:00</dateOfpublication>
	</document>
        <type>TITLE</type>
    </DocumentSection>
    
    <DocumentSection>
        <section>TestNewSection</section>
        <document>
            <name>IHE_ITI_TF_Vol2a</name>
            <title>IHE ITI Technical Framework Volume 2a (ITI-TF-2a)</title>
            <revision>11.0</revision>
            <type>TECHNICAL_FRAMEWORK</type>
            <url>http://ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol2a.pdf</url>
            <volume>2a</volume>
            <domain>
                <keyword>ITI</keyword>
                <name>IT-Infrastructure</name>
                <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
                <id>2</id>
            </domain>
            <linkStatus>0</linkStatus>
            <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
            <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
            <document_md5_hash_code>bf130d69e3a4bcf8f30cfdded2f0504c</document_md5_hash_code>
            <dateOfpublication>2014-09-23T00:00:00+02:00</dateOfpublication>
        </document>
        <type>TABLE</type>
    </DocumentSection>
    <DocumentSection>
        <section>Table_3_21_6__RSP_Segment_Patte</section>
        <document>
            <name>IHE_ITI_TF_Vol2a</name>
            <title>IHE ITI Technical Framework Volume 2a (ITI-TF-2a)</title>
            <revision>11.0</revision>
            <type>TECHNICAL_FRAMEWORK</type>
            <url>http://ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol2a.pdf</url>
            <volume>2a</volume>
            <domain>
                <keyword>ITI</keyword>
                <name>IT-Infrastructure</name>
                <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
                <id>2</id>
            </domain>
            <linkStatus>0</linkStatus>
            <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
            <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
            <document_md5_hash_code>bf130d69e3a4bcf8f30cfdded2f0504c</document_md5_hash_code>
            <dateOfpublication>2014-09-23T00:00:00+02:00</dateOfpublication>
        </document>
        <type>TABLE</type>
    </DocumentSection>
    
    <DocumentSection>
        <section>1_1_Overview_of_the_Technical_F</section>
        <document>
            <name>IHE_ITI_TF_Vol2b</name>
            <title>IHE ITI Technical Framework Volume 2b (ITI TF-2b)</title>
            <revision>11.0</revision>
            <type>TECHNICAL_FRAMEWORK</type>
            <url>http://ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol2b.pdf</url>
            <volume>2b</volume>
            <domain>
                <keyword>ITI</keyword>
                <name>IT-Infrastructure</name>
                <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
                <id>2</id>
            </domain>
            <linkStatus>0</linkStatus>
            <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
            <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
            <document_md5_hash_code>5e8e3def16e02c51e47c31260d1ce599</document_md5_hash_code>
            <dateOfpublication>2014-09-23T00:00:00+02:00</dateOfpublication>
        </document>
        <type>TITLE</type>
    </DocumentSection>
</documents>
```

Now with this file, you can import those Documents into any instance of the tool.     

### Export

On the same page, at the bottom you will find a panel titled __Import Documents__.

![](./media/tf_documents_import_panel.png)

This panel explains the basic rules that are used for Documents import : 

* Domain detection is based on keywords. If one does not appear in the database the Document and its Sections will be ignored 
* Sections that are not already registered will be added to the database if they reference a valid document.

You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the Documents or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what Domains were found missing, what Documents will be ignored, what Documents will be imported and which one have
been found duplicated and won't be imported. 

![](./media/tf_documents_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import Documents__ button on the bottom of the report to validate the changes
and import them into the Gazelle Test Management instance. If something is not ok with the document, click the __Reset__ button. Changes will not be made on
the tools data and you will be able to upload another document.


## Import/Export of HL7 Message Profiles

### Import

HL7 Message Profiles can also be extracted into an XML file. The principle is very similar to what is done with other model elements.
To export HL7 Message Profiles, go to the Specifications > HL7v2 message profiles:

Once you accessed the HL7 Message Profiles page, you will have the list of all Profiles for your instance displayed. Those Profiles can be filter base 
on Domain, actor, transaction, affinity domain, hl7 version or message types. To export them simply click the __Export Filtered Documents__ 
button. This will create the XML file corresponding to the filtered Documents and automatically downloads it.

![](./media/tf_hl7messageprofiles_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<hl7MessageProfiles>
    <HL7MessageProfile>
        <Oid>1.3.6.1.4.12559.11.1.1.147</Oid>
        <Hl7Version>2.3.1</Hl7Version>
        <Domain>
            <keyword>ITI</keyword>
            <name>IT-Infrastructure</name>
            <description>The IT Infrastructure Domain supplies infrastructure for sharing healthcare information. An infrastructure interoperability component represents a common IT function that is used as a building block for a variety of use cases... a necessary ingredient, but rarely visible to the end user!! These components may be embedded in an application, but are often deployed as a shared resource within a RHIO or Health Information Exchange.</description>
            <id>2</id>
        </Domain>
        <Actor>
            <keyword>PAT_ID_X_REF_MGR</keyword>
            <name>Patient Identity Cross-reference Manager</name>
            <description>Serves a well-defined set of Patient Identification Domains. Based on information provided by in each Patient Identification Domain by a Patient Identification Source Actor, it manages the cross-referencing of patient identifiers across Patient Identification Domains.</description>
            <id>27</id>
        </Actor>
        <Transaction>
            <keyword>ITI-8</keyword>
            <name>Patient Identity Feed</name>
            <description>Allows a Patient Identity Source Actor to notify a Patient Identity Cross-Referencing Actor of all events related to patient identification (creation, update, merge, etc.).</description>
            <id>50</id>
            <transactionStatusType>
                <keyword>FT</keyword>
                <name>Final Text</name>
                <description></description>
            </transactionStatusType>
            <transactionLinks>
                <fromActor>
                    <keyword>PAT_ID_SRC</keyword>
                    <name>Patient Identity Source</name>
                    <description>Each Patient Identification Domain requires this Actor to assign patient identities and to notify a Patient Identity Cross-reference Manager Actor of all events related to patient identification (creation, update, merge, etc.). For example, the Registration (ADT) Actor defined by the radiology Scheduled Workflow Profile would likely be a Patient Identity Source.</description>
                    <id>45</id>
                </fromActor>
                <toActor>
                    <keyword>DOC_REGISTRY</keyword>
                    <name>Document Registry</name>
                    <description>The Document Registry Actor maintains metadata about each registered document in a document entry. This includes a link to the Document in the Repository where it is stored. The Document Registry responds to queries from Document Consumer actors about documents meeting specific criteria. It also enforces some healthcare specific technical policies at the time of document registration.</description>
                    <id>43</id>
                </toActor>
            </transactionLinks>
            <transactionLinks>
                <fromActor>
                    <keyword>PAT_ID_SRC</keyword>
                    <name>Patient Identity Source</name>
                    <description>Each Patient Identification Domain requires this Actor to assign patient identities and to notify a Patient Identity Cross-reference Manager Actor of all events related to patient identification (creation, update, merge, etc.). For example, the Registration (ADT) Actor defined by the radiology Scheduled Workflow Profile would likely be a Patient Identity Source.</description>
                    <id>45</id>
                </fromActor>
                <toActor>
                    <keyword>PAT_ID_X_REF_MGR</keyword>
                    <name>Patient Identity Cross-reference Manager</name>
                    <description>Serves a well-defined set of Patient Identification Domains. Based on information provided by in each Patient Identification Domain by a Patient Identification Source Actor, it manages the cross-referencing of patient identifiers across Patient Identification Domains.</description>
                    <id>27</id>
                </toActor>
            </transactionLinks>
        </Transaction>
        <TriggerEvent>ACK^A01^ACK</TriggerEvent>
        <MessageOrderControlCode></MessageOrderControlCode>
        <AffinityDomains>
            <keyword>IHE</keyword>
            <labelToDisplay>IHE</labelToDisplay>
            <description>IHE</description>
        </AffinityDomains>
    </HL7MessageProfile>
    <HL7MessageProfile>
        <Oid>1.1.1.1.1.1.1.1.1.1.1</Oid>
        <Hl7Version>2.69.3</Hl7Version>
        <Domain>
            <keyword>ITI</keyword>
        </Domain>
        <Actor>
            <keyword>PAT_ID_X_REF_MGR</keyword>
        </Actor>
        <Transaction>
            <keyword>ITI-8</keyword>
        </Transaction>
        <TriggerEvent>ACK^A04^ACK</TriggerEvent>
        <MessageOrderControlCode></MessageOrderControlCode>
        <AffinityDomains>
            <keyword>TOTO</keyword>
            <labelToDisplay>TOTO</labelToDisplay>
            <description>This is a test</description>
        </AffinityDomains>
    </HL7MessageProfile>
</hl7MessageProfiles>
```

Now with this file, you can import those Profiles into any instance of the tool.     

### Export

On the same page, at the bottom you will find a panel titled __Import HL7 Message Profiles__.

![](./media/tf_hl7messageprofiles_import_panel.png)

This panel explains the basic rules that are used for Hl7 Message Profiles import : 

* Actor/Domain/Transaction detection is based on keywords. If one does not appear in the database the Message Profile will be ignored.
* Duplicate Message Profile detection is base on the Profile OID.
* Affinity Domain detection is based on keyword. If one does not exist, it will be imported along with the profile.


You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the Profiles or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what Domains, Actors or Transactions were found missing, what Hl7 Message Profiles will be ignored, what Profiles will be imported 
and which one have been found duplicated and won't be imported. 

![](./media/tf_hl7messageprofiles_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import HL7 Message Profiles__ button on the bottom of the report to validate 
the changes and import them into the Gazelle Test Management instance. If something is not ok with the document, click the __Reset__ button. Changes will 
not be made on the tools data and you will be able to upload another document.

## Import/Export of OID Root Definition

### Import

OID Root Definition can be extracted from the Gazelle Test Management instance into an XML file. This file can be later on used to import those OID Root 
Definitions in any other instance of the tool.
To do so go to the Preparation >  : Manage OIDs

Once you accessed the OID Management page, when accessing the OID Roots tab, you will have the list of all OID Roots for your instance displayed. 
To export them simply click the __Export OID Roots__ button. This will create the XML file corresponding to the OID Roots and
automatically downloads it.

![](./media/oid_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<oidRootDefinitions>
    <oidRootDefinition>
        <rootOID>1.3.6.1.4.1.21367.2017.2.3</rootOID>
        <lastValue>29</lastValue>
        <label>
            <label>repositoryUniqueID OID</label>
        </label>
        <oidRequirements>
            <label>repositoryUniqueID OID</label>
            <actorIntegrationProfileOptionList>
                <actorIntegrationProfile>
                    <actor>
                        <keyword>IMG_DOC_SOURCE</keyword>
                        <name>Imaging Document Source</name>
                        <description>Publishes imaging data and makes it available for retrieval.</description>
                        <id>62</id>
                    </actor>
                    <integrationProfile>
                        <keyword>XCA-I</keyword>
                        <name>Cross-Community Access for Imaging</name>
                        <description>The Cross-Community Access for Imaging (XCA-I) Integration Profile specifies actors and transactions to query and retrieve patient-relevant medical imaging data being held by other communities. </description>
                        <id>253</id>
                        <integrationProfileStatusType>
                            <keyword>FT</keyword>
                            <name>Final Text</name>
                            <description></description>
                        </integrationProfileStatusType>
                        <domainsForDP>
                            <keyword>RAD</keyword>
                            <name>Radiology</name>
                            <description>IHE Radiology addresses information sharing, workflow and patient care in radiology, including mammography and nuclear medicine.</description>
                            <id>1</id>
                        </domainsForDP>
                    </integrationProfile>
                    <profileLinks>
                        <transaction>
                            <keyword>RAD-69</keyword>
                            <name>Retrieve Imaging Doc Set</name>
                            <description>Retrieve Imaging Doc Set</description>
                            <id>179</id>
                            <transactionStatusType>
                                <keyword>FT</keyword>
                                <name>Final Text</name>
                                <description></description>
                            </transactionStatusType>
                            <transactionLinks>
                                <fromActor>
                                    <keyword>RESP_IMG_GATEWAY</keyword>
                                    <name>Responding Imaging Gateway</name>
                                    <description>The responding Imaging Gateway proxies Cross Gateway Retrieve Imaging Document Set requests from an Initiating Imaging Gateway to an Imaging Document Source with an Image Document Set Retrieve request.</description>
                                    <id>1192</id>
                                </fromActor>
                                <toActor>
                                    <keyword>IMG_DOC_SOURCE</keyword>
                                    <name>Imaging Document Source</name>
                                    <description>Publishes imaging data and makes it available for retrieval.</description>
                                    <id>62</id>
                                </toActor>
                            </transactionLinks>
                        </transaction>
                        <transactionOptionType>
                            <keyword>R</keyword>
                            <name>Required</name>
                            <description></description>
                        </transactionOptionType>
                    </profileLinks>
                </actorIntegrationProfile>
                <integrationProfileOption>
                    <keyword>NONE</keyword>
                    <name>None</name>
                    <description>None</description>
                    <reference></reference>
                    <toDisplay>true</toDisplay>
                </integrationProfileOption>
                <maybeSupportive>true</maybeSupportive>
            </actorIntegrationProfileOptionList>
            <prefix></prefix>
        </oidRequirements>
    </oidRootDefinition>
</oidRootDefinitions>
```

Now with this file, you can import OID Roots into any instance of the tool. 

### Export

On the same page, at the bottom you will find a panel titled __Import OID Roots__.

![](./media/oid_import_panel.png)

This panel explains the basic rules that are used for OID Roots import : 

* Actor/Integration Profile/Option detection is based on keywords. If one does not appear in the database but is referenced by a requirement, the requirement will be ignored.
* If an OID Root references an unknown label (or no label) it will be ignored
* If an OID Root is already defined for a label, it will not be imported. Additional requirements from the imported file will be added to the already existing requirements.

You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the OID Roots or cancel the action. If the option
is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made in the database.    
The reports sums up what elements were found missing, what Root have been ignored, what Root will be imported and what Root have been found duplicated 
and won't be imported. 

![](./media/oid_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import OID Roots__ button on the bottom of the report to validate the changes
and import them into the Gazelle Test Management instance. If something is not ok with the uploaded document, just click the __Reset__ button. 
Changes will not be made on the tools data and you will be able to upload another document.


## Import/Export of Samples Type

### Import

Samples Type can also be extracted into an XML file. The principle is very similar to what is done with other model elements.
To export Samples Type, go to the Testing > Manage Samples menu :

Once you accessed the Samples Administration page, you will have the list of all Samples Type for your instance displayed. Those Samples Type can be 
filtered base on their status. To export them simply click the __Export Filtered Samples' Type__ 
button. This will create the XML file corresponding to the filtered Samples' Type and automatically downloads it.

![](./media/samples_export_button.png)

Here is an example of what kind of XML file will be generated :

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<objectTypes>
    <objectType>
        <keyword>GSPS</keyword>
        <description>Grey Scale Presentation State</description>
        <default_desc>&lt;p&gt;Patient ID:&lt;/p&gt;
&lt;p&gt;Patient Name:&lt;/p&gt;
&lt;p&gt;Your table:&lt;/p&gt;
&lt;p&gt;Your name:&lt;/p&gt;</default_desc>
        <instructions>&lt;p&gt;&lt;span style="border-collapse: collapse; line-height: 12px;"&gt;Upload your sample GSPS objects and associated images. See add'l instructions in ...&lt;/span&gt;&lt;/p&gt;
&lt;p&gt;&lt;span style="border-collapse: collapse; line-height: 12px;"&gt;&lt;strong&gt;Pre-Connectathon test:&lt;/strong&gt; MESA test 282 &lt;/span&gt;&lt;/p&gt;
&lt;p&gt;&lt;span style="border-collapse: collapse; line-height: 12px;"&gt;&lt;strong&gt;Connectathon test&lt;/strong&gt; CPI_Store Sample.&lt;/span&gt;&lt;/p&gt;
&lt;p&gt;&lt;span style="border-collapse: collapse; line-height: 12px;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/p&gt;</instructions>
        <objectTypeStatus>
            <description>The object type can be used</description>
            <keyword>ready</keyword>
            <labelToDisplay>ready</labelToDisplay>
        </objectTypeStatus>
        <objectAttributes>
            <keyword>Bitmap Display Shutter</keyword>
            <description>Dicom Part 3 C7.6.15</description>
        </objectAttributes>
        <objectCreators>
            <description></description>
            <AIPO>
                <actorIntegrationProfile>
                    <actor>
                        <keyword>EC</keyword>
                        <name>Evidence Creator</name>
                        <description>A system that creates additional evidence objects such as images, presentation states, Key Image Notes, and/or Evidence Documents and transmits them to an Image Archive. It also makes requests for storage commitment to the Image Manager for the data previously transmitted. It may also retrieve worklist entries for post-processing steps from the Post-Processing Manager and provide notification of completion of the step, allowing the enterprise to track the status of post-processing work.</description>
                        <id>18</id>
                    </actor>
                    <integrationProfile>
                        <keyword>CPI</keyword>
                        <name>Consistent Presentation of Images</name>
                        <description>The Consistent Presentation of Images Integration Profile specifies a number of transactions that maintain the consistency of presentation for grayscale images and their presentation state information (including user annotations, shutters, flip/rotate, display area, and zoom). It also defines a standard contrast curve, the Grayscale Standard Display Function, against which different types of display and hardcopy output devices can be calibrated. It thus supports hardcopy, softcopy and mixed environments.</description>
                        <id>17</id>
                        <integrationProfileTypes>
                            <keyword>EHR_ACCESS</keyword>
                            <name>EHR Access</name>
                            <description></description>
                        </integrationProfileTypes>
                        <integrationProfileStatusType>
                            <keyword>FT</keyword>
                            <name>Final Text</name>
                            <description></description>
                        </integrationProfileStatusType>
                        <domainsForDP>
                            <keyword>RAD</keyword>
                            <name>Radiology</name>
                            <description>IHE Radiology addresses information sharing, workflow and patient care in radiology, including mammography and nuclear medicine.</description>
                            <id>1</id>
                        </domainsForDP>
                        <documentSection>
                            <section>5_Consistent_Presentation_of_Im</section>
                            <document>
                                <name>IHE_RAD_TF_Vol1</name>
                                <title>IHE Radiology Technical Framework Volume 1</title>
                                <revision>13.0</revision>
                                <type>TECHNICAL_FRAMEWORK</type>
                                <url>http://ihe.net/uploadedFiles/Documents/Radiology/IHE_RAD_TF_Vol1.pdf</url>
                                <volume>1</volume>
                                <domain>
                                    <keyword>RAD</keyword>
                                    <name>Radiology</name>
                                    <description>IHE Radiology addresses information sharing, workflow and patient care in radiology, including mammography and nuclear medicine.</description>
                                    <id>1</id>
                                </domain>
                                <linkStatus>0</linkStatus>
                                <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
                                <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
                                <document_md5_hash_code>f7f2125b95d3562447399300aa5aa4a1</document_md5_hash_code>
                                <dateOfpublication>2014-07-30T00:00:00+02:00</dateOfpublication>
                            </document>
                            <type>TITLE</type>
                        </documentSection>
                    </integrationProfile>
                    <profileLinks>
                        <transaction>
                            <keyword>RAD-14</keyword>
                            <name>Query Images</name>
                            <description>An Image Display queries the Image Archive for a list of entries representing images by patient, study, series, or instance.</description>
                            <id>15</id>
                            <transactionStatusType>
                                <keyword>FT</keyword>
                                <name>Final Text</name>
                                <description></description>
                            </transactionStatusType>
                            <transactionLinks>
                                <fromActor>
                                    <keyword>ID</keyword>
                                    <name>Image Display</name>
                                    <description>A part of a system that can access imaging evidence objects (images, Presentation States, Key Image Notes, Evidence Documents) through network query/retrieve or reading interchange media and allow the user to view these objects.</description>
                                    <id>32</id>
                                </fromActor>
                                <toActor>
                                    <keyword>IM</keyword>
                                    <name>Image Manager/Archive</name>
                                    <description>A system that provides functions related to safe data storage and image data handling. It supplies image availability information to the Department System Scheduler. It is always grouped with an Image Archive to provide long term storage of images, presentation states, Key Image Notes, and Evidence Documents.</description>
                                    <id>17</id>
                                </toActor>
                            </transactionLinks>
                        </transaction>
                        <transactionOptionType>
                            <keyword>O</keyword>
                            <name>Optional</name>
                            <description></description>
                        </transactionOptionType>
                    </profileLinks>
                </actorIntegrationProfile>
                <integrationProfileOption>
                    <keyword>NONE</keyword>
                    <name>None</name>
                    <description>None</description>
                    <reference></reference>
                    <toDisplay>true</toDisplay>
                </integrationProfileOption>
                <maybeSupportive>true</maybeSupportive>
            </AIPO>
        </objectCreators>
        <objectFiles>
            <description>Upload a screen capture of the document of your peer as rendered by your system. Note that depending on the options supported by your system, the screen capture may also include a capture of the screen showing imported data in your system
</description>
            <min>1</min>
            <max>1</max>
            <type>
                <keyword>SNAPSHOT</keyword>
                <description>Snapshot of object</description>
                <extensions>jpg,png,gif,jpeg</extensions>
                <writable>false</writable>
                <validate>false</validate>
            </type>
            <uploader>reader</uploader>
        </objectFiles>
        <objectReaders>
            <description></description>
            <AIPO>
                <actorIntegrationProfile>
                    <actor>
                        <keyword>ID</keyword>
                        <name>Image Display</name>
                        <description>A part of a system that can access imaging evidence objects (images, Presentation States, Key Image Notes, Evidence Documents) through network query/retrieve or reading interchange media and allow the user to view these objects.</description>
                        <id>32</id>
                    </actor>
                    <integrationProfile>
                        <keyword>CPI</keyword>
                        <name>Consistent Presentation of Images</name>
                        <description>The Consistent Presentation of Images Integration Profile specifies a number of transactions that maintain the consistency of presentation for grayscale images and their presentation state information (including user annotations, shutters, flip/rotate, display area, and zoom). It also defines a standard contrast curve, the Grayscale Standard Display Function, against which different types of display and hardcopy output devices can be calibrated. It thus supports hardcopy, softcopy and mixed environments.</description>
                        <id>17</id>
                        <integrationProfileTypes>
                            <keyword>EHR_ACCESS</keyword>
                            <name>EHR Access</name>
                            <description></description>
                        </integrationProfileTypes>
                        <integrationProfileStatusType>
                            <keyword>FT</keyword>
                            <name>Final Text</name>
                            <description></description>
                        </integrationProfileStatusType>
                        <domainsForDP>
                            <keyword>RAD</keyword>
                            <name>Radiology</name>
                            <description>IHE Radiology addresses information sharing, workflow and patient care in radiology, including mammography and nuclear medicine.</description>
                            <id>1</id>
                        </domainsForDP>
                        <documentSection>
                            <section>5_Consistent_Presentation_of_Im</section>
                            <document>
                                <name>IHE_RAD_TF_Vol1</name>
                                <title>IHE Radiology Technical Framework Volume 1</title>
                                <revision>13.0</revision>
                                <type>TECHNICAL_FRAMEWORK</type>
                                <url>http://ihe.net/uploadedFiles/Documents/Radiology/IHE_RAD_TF_Vol1.pdf</url>
                                <volume>1</volume>
                                <domain>
                                    <keyword>RAD</keyword>
                                    <name>Radiology</name>
                                    <description>IHE Radiology addresses information sharing, workflow and patient care in radiology, including mammography and nuclear medicine.</description>
                                    <id>1</id>
                                </domain>
                                <linkStatus>0</linkStatus>
                                <linkStatusDescription>This url is pointing to a PDF</linkStatusDescription>
                                <lifecyclestatus>FINAL_TEXT</lifecyclestatus>
                                <document_md5_hash_code>f7f2125b95d3562447399300aa5aa4a1</document_md5_hash_code>
                                <dateOfpublication>2014-07-30T00:00:00+02:00</dateOfpublication>
                            </document>
                            <type>TITLE</type>
                        </documentSection>
                    </integrationProfile>
                    <profileLinks>
                        <transaction>
                            <keyword>RAD-14</keyword>
                            <name>Query Images</name>
                            <description>An Image Display queries the Image Archive for a list of entries representing images by patient, study, series, or instance.</description>
                            <id>15</id>
                            <transactionStatusType>
                                <keyword>FT</keyword>
                                <name>Final Text</name>
                                <description></description>
                            </transactionStatusType>
                            <transactionLinks>
                                <fromActor>
                                    <keyword>ID</keyword>
                                    <name>Image Display</name>
                                    <description>A part of a system that can access imaging evidence objects (images, Presentation States, Key Image Notes, Evidence Documents) through network query/retrieve or reading interchange media and allow the user to view these objects.</description>
                                    <id>32</id>
                                </fromActor>
                                <toActor>
                                    <keyword>IM</keyword>
                                    <name>Image Manager/Archive</name>
                                    <description>A system that provides functions related to safe data storage and image data handling. It supplies image availability information to the Department System Scheduler. It is always grouped with an Image Archive to provide long term storage of images, presentation states, Key Image Notes, and Evidence Documents.</description>
                                    <id>17</id>
                                </toActor>
                            </transactionLinks>
                        </transaction>
                        <transactionOptionType>
                            <keyword>R</keyword>
                            <name>Required</name>
                            <description></description>
                        </transactionOptionType>
                    </profileLinks>
                </actorIntegrationProfile>
                <integrationProfileOption>
                    <keyword>NONE</keyword>
                    <name>None</name>
                    <description>None</description>
                    <reference></reference>
                    <toDisplay>true</toDisplay>
                </integrationProfileOption>
                <maybeSupportive>true</maybeSupportive>
            </AIPO>
        </objectReaders>
    </objectType>
</objectTypes>
```

Now with this file, you can import those Samples' Type into any instance of the tool.     

### Export

On the same page, at the bottom you will find a panel titled __Import Samples' Type__.

![](./media/samples_import_panel.png)

This panel explains the basic rules that are used for Samples' Type import : 

* Actor/Integration Profile/Option detection is based on keywords. If one does not appear in the database the Samples' Type will be ignored
* If a Samples' Type references an unknown status (based on keyword) it will be ignored
* If a Samples' Type references a file with unknown typ, the Samples' Type will be ignored


You can also see a checkbox for __Review Changes before saving__. This option is activated by default. When activated, a report about the files content
will be displayed as information to the user. The user will then be able to accept the changes and import the Samples' Type or cancel the action. If 
the option is not activated, the changes will be imported directly in the base. The report will still be displayed but changes have already been made 
in the database.    
The reports sums up what TF Model elements were found missing, what Samples' Type will be ignored, what Samples' Type will be imported and which one have
been found duplicated and won't be imported. 

![](./media/samples_import_report.png)

If the __Review Changes before saving__ option is activated, simply click the __Import Samples' Type__ button on the bottom of the report to validate 
the changes and import them into the Gazelle Test Management instance. If something is not ok with the document, click the __Reset__ button. 
Changes will not be made on the tools data and you will be able to upload another document.


# External Testing Tool

In the context of the Continual Testing Tool, we have developed a web service to upload result from an external tool.

On a Test Definition, the preference "Is External Tool", allow to generate a token on a testInstance of the related test.
A token is unique for **a given user** and **a given testInstance**. It has an 8-hours validity period. Also the token value is a unique key value randomly generated.

In the section generate Authentication Token, it allows user to generate a token for this testInstance. If there is an existing valid token, it will be filled by this value.
If user regenerates a new Token, it will overwrite the value and renew the validity for 8 hours.
![](./media/TokenProvider.png)

There are two webservices to upload result :
- *"/rest/testinstances/files/logs"*  give the ability to upload test-logs (a zip file) using the generated Token on the related testInstance. (**Warning** this web service can consume only *application/octet-stream*). This archive file shall contain all details of the executed external test suite.
- *"/rest/testinstances/files/report"* give the ability to upload test-report (an XML file) using the generated Token on the related testInstance. (**Warning** this web service can consume only *application/octet-stream* and *application/xml*). The test-report shall contain a summary of the executed external test suite. It has to pass an XSD validation to be uploaded on the test instance.

Uploaded files are available in the section *Files*. A valid upload file is saved using the type (test-logs or test-report) and the token value.

A user can upload those file manually. He has to upload a file that match the required pattern. 2 patterns exist in the case of External Test :
- "test-report-<tokenValue>.xml"
- "test-logs-<tokenValue>.zip"

If file doesn't mach the pattern it will not be uploaded as result for the testInstance.

Once the test-report upload is done, it is parsed on page loading. A green or red circle shall be displayed following the overall status with a short summary in the label.

Also, the status of the test instance has been updated and set to "To Be Verified". It's not allow uploading on a testInstance which the status is "Verified","Failed" or "Aborted", or the testingSession is closed.

# Systems Management

## Permanent link to system

For each system, there are a permanent link from gazelle to go directly to the summary of the system on the specified session. The link contains the description of the system, implemented IHE actors/profiles, system information summary, and the list of demonstrations that the system is registered for.

This permanent link has this form :

[*https://gazelle.ihe.net/EU-CAT/systemInSession.seam?system=XXXX&testingSessionId=XX*](https://gazelle.ihe.net/EU-CAT/systemInSession.seam?system=XXXX&testingSessionId=XX)

*  system : the keyword of the system
*  testingSessionId : the id of the testing session (15 for pisa, 8 for Bordeaux, etc).


# TM - Release notes

Gazelle Test Management release notes can be found on the JIRA pages of the project at the following URL :

[*https://gazelle.ihe.net/jira/browse/GZL\#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel*](https://gazelle.ihe.net/jira/browse/GZL#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel)

