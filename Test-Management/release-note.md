---
title: Release note
subtitle: Test Management
toolversion: 10.0.1
releasedate: 2025-01-27
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-TEST_MANAGEMENT
---

# 10.0.1

_Release date: 2025-01-27_

__Bug__
* \[[GZL-5235](https://gazelle.ihe.net/jira/browse/GZL-5235)\] When creating a testing session, the tool does not check if the start date, end date and registration date are in the appropriate order
* \[[GZL-5243](https://gazelle.ihe.net/jira/browse/GZL-5243)\] Review the permissions for the creation of new entries in the list of SUT's network interfaces
* \[[GZL-5279](https://gazelle.ihe.net/jira/browse/GZL-5279)\] [GZ TM] All assertions not shown in test cases.
* \[[GZL-5445](https://gazelle.ihe.net/jira/browse/GZL-5445)\] Invite team member button must redirect to user management
* \[[GZL-5446](https://gazelle.ihe.net/jira/browse/GZL-5446)\] GTM cannot access tests/testdefinitions/documentation
* \[[GZL-5464](https://gazelle.ihe.net/jira/browse/GZL-5464)\] Increase test's name and keyword size when editing test case

__Improvement__
* \[[GZL-5472](https://gazelle.ihe.net/jira/browse/GZL-5472)\] Rename RAW Channel for proxy into Raw TCP

# 10.0.0

_Release date: 2024-11-29_

Context : GUM Step 4 renovation

__Bug__
* \[[GZL-5399](https://gazelle.ihe.net/jira/browse/GZL-5399)\] Missing some preferences in repeatable migrations 
* \[[GZL-5400](https://gazelle.ihe.net/jira/browse/GZL-5400)\] Update user registration url to match the new one
* \[[GZL-5403](https://gazelle.ihe.net/jira/browse/GZL-5403)\] Monitor management page broken after user deletion
* \[[GZL-5434](https://gazelle.ihe.net/jira/browse/GZL-5434)\] Unable to delete a test session. Even an empty test session.

__Story__
* \[[GZL-5369](https://gazelle.ihe.net/jira/browse/GZL-5369)\] Account management - Remove user preferences pages
* \[[GZL-5370](https://gazelle.ihe.net/jira/browse/GZL-5370)\] Account management - Remove user list + edit pages

__Task__
* \[[GZL-5402](https://gazelle.ihe.net/jira/browse/GZL-5402)\] Update translations in Crowdin

__Improvement__
* \[[GZL-5441](https://gazelle.ihe.net/jira/browse/GZL-5441)\] Upgrade sso-client-v7 library to 5.0.0

# 9.5.4

_Release date: 2024-11-29_

__Bug__
* \[[GZL-5416](https://gazelle.ihe.net/jira/browse/GZL-5416)\] Can't show tests from metatests

# 9.5.3

_Release date: 2024-11-25_

__Bogue__
* \[[GZL-5415](https://gazelle.ihe.net/jira/browse/GZL-5415)\] s:token error org.jboss.seam.ui.UnauthorizedCommandException No client identifier provided

# 9.5.2

_Release date: 2024-11-12_

__Bogue__
* \[[GZL-5411](https://gazelle.ihe.net/jira/browse/GZL-5411)\] We should not allow edits information of a closed test session in Registration menu
* \[[GZL-5413](https://gazelle.ihe.net/jira/browse/GZL-5413)\] Proxy filters with dates are not working

__Amélioration__
* \[[GZL-5412](https://gazelle.ihe.net/jira/browse/GZL-5412)\] Remove button to old proxy


# 9.5.1

_Release date: 2024-11-04_

__Bogue__
* \[[GZL-5173](https://gazelle.ihe.net/jira/browse/GZL-5173)\] [QUAL] special caracters/accent are not accepted in test session page
* \[[GZL-5266](https://gazelle.ihe.net/jira/browse/GZL-5266)\] Metatest not properly displayed on Evaluation page
* \[[GZL-5267](https://gazelle.ihe.net/jira/browse/GZL-5267)\] Grading entries vanish


# 9.5.0

_Release date: 2024-10-21_

__Task__
* \[[GZL-5376](https://gazelle.ihe.net/jira/browse/GZL-5376)\] Update test repository view with automated test step
* \[[GZL-5377](https://gazelle.ihe.net/jira/browse/GZL-5377)\] Create Automated test step input entity
* \[[GZL-5378](https://gazelle.ihe.net/jira/browse/GZL-5378)\] Update AutomatedTestStep model
* \[[GZL-5379](https://gazelle.ihe.net/jira/browse/GZL-5379)\] Add fileSystem storage for script in test step
* \[[GZL-5380](https://gazelle.ihe.net/jira/browse/GZL-5380)\] Complete edit UI for automated-test step
* \[[GZL-5382](https://gazelle.ihe.net/jira/browse/GZL-5382)\] DAO can query automated test step
* \[[GZL-5383](https://gazelle.ihe.net/jira/browse/GZL-5383)\] Create AutomatedTestStep Entity
* \[[GZL-5384](https://gazelle.ihe.net/jira/browse/GZL-5384)\] Add new SQL table for automated test step
* \[[GZL-5385](https://gazelle.ihe.net/jira/browse/GZL-5385)\] Update test view
* \[[GZL-5386](https://gazelle.ihe.net/jira/browse/GZL-5386)\] Add automated test step to the test instance model
* \[[GZL-5387](https://gazelle.ihe.net/jira/browse/GZL-5387)\] Update test instance UI to display automated test step
* \[[GZL-5388](https://gazelle.ihe.net/jira/browse/GZL-5388)\] Resolve script to display enough upload file button in UI
* \[[GZL-5389](https://gazelle.ihe.net/jira/browse/GZL-5389)\] Run step in TestInstanceManager
* \[[GZL-5390](https://gazelle.ihe.net/jira/browse/GZL-5390)\] Client to query GITB Gateway for runTest
* \[[GZL-5391](https://gazelle.ihe.net/jira/browse/GZL-5391)\] REST API for receiving report notification
* \[[GZL-5392](https://gazelle.ihe.net/jira/browse/GZL-5392)\] Update UI with report status display
* \[[GZL-5404](https://gazelle.ihe.net/jira/browse/GZL-5404)\] Store ITB PDF report
* \[[GZL-5406](https://gazelle.ihe.net/jira/browse/GZL-5406)\] Add new Automated test step type

# 9.4.0

_Release date: 2024-09-24_

__Bug__
* \[[GZL-5393](https://gazelle.ihe.net/jira/browse/GZL-5393)\] Too much DB requests created when  clicking on "Generate configs for selected session" button

__Improvement__
* \[[GZL-5397](https://gazelle.ihe.net/jira/browse/GZL-5397)\] Filter tests by peer type

# 9.3.2

_Release date: 2024-09-02_

__Bug__
* \[[GZL-5375](https://gazelle.ihe.net/jira/browse/GZL-5375)\] Datahouse capture button is displayed even when Datahouse is not deployed
* \[[GZL-5394](https://gazelle.ihe.net/jira/browse/GZL-5394)\] TM - Disable message capture if session is terminated

__Task__
* \[[GZL-5393](https://gazelle.ihe.net/jira/browse/GZL-5393)\] OOTS platform down after clicking on "Generate configs for selected session" button
* \[[GZL-5398](https://gazelle.ihe.net/jira/browse/GZL-5398)\] Rename HL7 to HL7v2 in Proxy WS


# 9.3.1

_Release date: 2024-06-25_

__Improvement__
* \[[GZL-5372](https://gazelle.ihe.net/jira/browse/GZL-5372)\] Upgrade sso-client-v7 library to 4.1.1

# 9.3.0

_Release date: 2024-06-17_

__Improvement__
* \[[GZL-5339](https://gazelle.ihe.net/jira/browse/GZL-5339)\] [PROXY 02][29] Add a link to a Proxy message in test steps
* \[[GZL-5341](https://gazelle.ihe.net/jira/browse/GZL-5341)\] [PROXY 02][39] User adds a comment to a test step
* \[[GZL-5342](https://gazelle.ihe.net/jira/browse/GZL-5342)\] [PROXY 02][40] User uploads an evidence to a test step
* \[[GZL-5343](https://gazelle.ihe.net/jira/browse/GZL-5343)\] [PROXY 02][41] SUT operator changes the execution status of a test step
* \[[GZL-5344](https://gazelle.ihe.net/jira/browse/GZL-5344)\] [PROXY 02][42] Monitor changes the grading status of a test step

__IT Help__
* \[[GZL-5367](https://gazelle.ihe.net/jira/browse/GZL-5367)\] TI - technical id present in Resume


# 9.2.0

_Release date: 2024-05-31_

__Task__
* \[[GZL-5352](https://gazelle.ihe.net/jira/browse/GZL-5352)\] Improve TestStepsInstance with start and end dates
* \[[GZL-5353](https://gazelle.ihe.net/jira/browse/GZL-5353)\] Implement start/stop capturing messages
* \[[GZL-5354](https://gazelle.ihe.net/jira/browse/GZL-5354)\] Redirect to filtered messages in proxy based on step

__Improvement__
* \[[GZL-5337](https://gazelle.ihe.net/jira/browse/GZL-5337)\] [PROXY 02][27] User starts/stops the capture of the messages for a given test step
* \[[GZL-5338](https://gazelle.ihe.net/jira/browse/GZL-5338)\] [PROXY 02][28] User accesses a pre-filtered list of messages in the Proxy


# 9.1.2

_Release date: 2024-05-30_

__Bug__
* \[[GZL-5329](https://gazelle.ihe.net/jira/browse/GZL-5329)\] Problem in Test instance - Meta data : "Testing Session"
* \[[GZL-5356](https://gazelle.ihe.net/jira/browse/GZL-5356)\] Test instance claim conflict
* \[[GZL-5359](https://gazelle.ihe.net/jira/browse/GZL-5359)\] Some buttons in GMM are not accessible due to footer
* \[[GZL-5360](https://gazelle.ihe.net/jira/browse/GZL-5360)\] Wrong testing session when creating the ATNA questionnaire
* \[[GZL-5362](https://gazelle.ihe.net/jira/browse/GZL-5362)\] Allow filtering the evaluation page based on testing depth

__Incident__
* \[[GZL-5361](https://gazelle.ihe.net/jira/browse/GZL-5361)\] Errors on Registration page are not true.

# 9.1.1

_Release date: 2024-05-17_

__Bug__
* \[[GZL-5355](https://gazelle.ihe.net/jira/browse/GZL-5355)\] Monitor failed to update Test Instance Status and error page is dispalyed

# 9.1.0
_Release date: 2024-05-14_

__Bug__
* \[[GZL-5347](https://gazelle.ihe.net/jira/browse/GZL-5347)\] [QA IAM] User Preference Organization Cache Issue
* \[[GZL-5348](https://gazelle.ihe.net/jira/browse/GZL-5348)\] [QA IAM] Possible to disable an delegated user 
* \[[GZL-5349](https://gazelle.ihe.net/jira/browse/GZL-5349)\] [QA IAM] Reset password button should be hidden from 

# 9.0.0
_Release date: 2024-04-04_

__Story__
* \[[GZL-5314](https://gazelle.ihe.net/jira/browse/GZL-5314)\] [DELEG] Display warning when editing delegated-user
* \[[GZL-5315](https://gazelle.ihe.net/jira/browse/GZL-5315)\] [DELEG] Edition of delegated organization

__Improvement__
* \[[GZL-5318](https://gazelle.ihe.net/jira/browse/GZL-5318)\] [GUM POST STEP2] Add getOrganizationById to organization webservice

# 8.0.1
_Release date: 2024-03-06_

__Bug__
* \[[GZL-5328](https://gazelle.ihe.net/jira/browse/GZL-5328)] Making organization's keyword Read Only
* \[[GZL-5330](https://gazelle.ihe.net/jira/browse/GZL-5330)] Menu entry to access the list of test is duplicated
__Remarks__
* include 
  * Gazelle-Model 6.0.1 
  * SSO-Client 3.0.1

# 8.0.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Story__
* \[[GZL-5206](https://gazelle.ihe.net/jira/browse/GZL-5206)\] [GUM] Remove account creation in TM
* \[[GZL-5207](https://gazelle.ihe.net/jira/browse/GZL-5207)\] [GUM] Remove username display
* \[[GZL-5261](https://gazelle.ihe.net/jira/browse/GZL-5261)\] [GUM] Use new user client in TM to perform actions on users 

__Task__
* \[[GZL-5205](https://gazelle.ihe.net/jira/browse/GZL-5205)\] [GUM] Create webservice to manage organizations
* \[[GZL-5270](https://gazelle.ihe.net/jira/browse/GZL-5270)\] [GUM] Setup cache for user lookups


# 7.1.7
_Release date: 2024-04-12_

__Bug__
* \[[GZL-5336](https://gazelle.ihe.net/jira/browse/GZL-5336)\] Problem encountered while generating the invoice in Gazelle TM

# 7.1.6
_Release date: 2024-01-04_

__Bug__
* \[[GZL-5306](https://gazelle.ihe.net/jira/browse/GZL-5306)] Display test menu entry as not logged in" is not functionnal
* \[[GZL-5307](https://gazelle.ihe.net/jira/browse/GZL-5307)] Anonymous users can download the IHE mode

# 7.1.5
_Release date: 2023-11-06_

__Remarks__ 

Update gazelle-model, release 5.0.2 

Update gazelle-tm-tools, release 4.1.7

__Tâche__

* \[[GZL-5247](https://gazelle.ihe.net/jira/browse/GZL-5247)\] Make testing session uniquely identified

__Bug__
* \[[GZL-5271](https://gazelle.ihe.net/jira/browse/GZL-5271)\] Impossible to create a new testing session in TM.
* \[[GZL-5273](https://gazelle.ihe.net/jira/browse/GZL-5273)\] Some unit tests don't pass anymore
* \[[GZL-5273](https://gazelle.ihe.net/jira/browse/GZL-5277)\] A modification of a testing session name is not well reported

# 7.1.4
_Release date: 2023-10-13_

__Bug__
* \[[GZL-5265](https://gazelle.ihe.net/jira/browse/GZL-5265)\] Test run are no more accessible if EVS link is wrong.

# 7.1.3
_Release date: 2023-10-09_

__Tâche__
* \[[GZL-5268](https://gazelle.ihe.net/jira/browse/GZL-5268)\] Invalid Dashboard value in Evaluation for capabilities
* \[[GZL-5269](https://gazelle.ihe.net/jira/browse/GZL-5269)\] TM-Remove report button for vendors

# 7.1.2
_Release date: 2023-09-20_

__Bug__
* \[[GZL-5252](https://gazelle.ihe.net/jira/browse/GZL-5252)\] Metatests not properly displayed on Test Execution page

# 7.1.1
_Release date: 2023-09-14_

__Bug__
* \[[GZL-5260](https://gazelle.ihe.net/jira/browse/GZL-5260)\] Admin cannot access the list of registered SUT and manage them

# 7.1.0
_Release date: 2023-09-11_

__Bug__
* \[[GZL-5164](https://gazelle.ihe.net/jira/browse/GZL-5164)\] Test session creation - setting dates for time-boxed test sessions
* \[[GZL-5220](https://gazelle.ihe.net/jira/browse/GZL-5220)\] Fix GMM and PR home page
* \[[GZL-5251](https://gazelle.ihe.net/jira/browse/GZL-5251)\] In GMM, test editors and administrators shall have access to objects/listObjectType.seam
* \[[GZL-5252](https://gazelle.ihe.net/jira/browse/GZL-5252)\] Metatests not properly displayed on Test Execution page
* \[[GZL-5258](https://gazelle.ihe.net/jira/browse/GZL-5258)\] Wrong validation permanent links from EVS v6 when providing evidence

__Improvement__
* \[[GZL-5213](https://gazelle.ihe.net/jira/browse/GZL-5213)\] Allow the adminstrator of the tool to edit the sections of the home page
* \[[GZL-5214](https://gazelle.ihe.net/jira/browse/GZL-5214)\] Inform the users about the testability status of the profiles they have signed up for
* \[[GZL-5215](https://gazelle.ihe.net/jira/browse/GZL-5215)\] Allow the testing session manager to automatically update the evaluation for entries related to non-testable profiles

# 6.3.2-SEGUR (SUPPORT)
_Release date: 2023-09-11_

__Bug__
* \[[GZL-5258](https://gazelle.ihe.net/jira/browse/GZL-5258)\] Wrong validation permanent links from EVS v6 when providing evidence

Remarks: This version is a SUPPORT release to correct a blocking bug in 6.3.1-SEGUR version.

# 6.10.5 (SUPPORT)
_Release date: 2023-09-11_

__Bug__
* \[[GZL-5258](https://gazelle.ihe.net/jira/browse/GZL-5258)\] Wrong validation permanent links from EVS v6 when providing evidence  

Remarks: This version is a SUPPORT release to correct a blocking bug in 6.10.4 version.  

# 7.0.1
_Release date: 2023-09-06_

__Bug__
* \[[GZL-5257](https://gazelle.ihe.net/jira/browse/GZL-5257)\] Missing gazelle_home_path preference for migrations

# 7.0.0

_Release date: 2023-07-12_

Step 1 of Gazelle User Management renovation. 

__Bug__
* \[[GZL-5210](https://gazelle.ihe.net/jira/browse/GZL-5210)\] [GUM] Problem of httpclient dependency versions
* \[[GZL-5221](https://gazelle.ihe.net/jira/browse/GZL-5221)\] Activation email to vendor-admin has out-dated instructions.
* \[[GZL-5224](https://gazelle.ihe.net/jira/browse/GZL-5224)\] "Delete user" button should be "Deactivate user"
* \[[GZL-5228](https://gazelle.ihe.net/jira/browse/GZL-5228)\] Email client application may activate user without admin consent.
* \[[GZL-5230](https://gazelle.ihe.net/jira/browse/GZL-5230)\] Proxy message link is not displayed in test instance
* \[[GZL-5231](https://gazelle.ihe.net/jira/browse/GZL-5231)\] Monitor cannot access notes of a system
* \[[GZL-5244](https://gazelle.ihe.net/jira/browse/GZL-5244)\] Home page show error when there is no preparatory tests during preparation phase

__Task__
* \[[GZL-5190](https://gazelle.ihe.net/jira/browse/GZL-5190)\] [GUM] Setup Flyway for TM DB
* \[[GZL-5191](https://gazelle.ihe.net/jira/browse/GZL-5191)\] [GUM] Remove native authentication
* \[[GZL-5199](https://gazelle.ihe.net/jira/browse/GZL-5199)\] [GUM] Remove reset password from TM
* \[[GZL-5200](https://gazelle.ihe.net/jira/browse/GZL-5200)\] [GUM] Only uses Identity to access user attributes
* \[[GZL-5201](https://gazelle.ihe.net/jira/browse/GZL-5201)\] [GUM] Remove block account possibility in TM
* \[[GZL-5209](https://gazelle.ihe.net/jira/browse/GZL-5209)\] [GUM] Remove log as feature from TM

See also [GUM release note](https://doc-ihe.kereval.cloud/gazelle-applications/v/gazelle-user-management/release-note).

# 6.10.4
_Release date: 2023-06-23_

__Bug__
* \[[GZL-5212](https://gazelle.ihe.net/jira/browse/GZL-5212)\] Inconsistant Prepatory Test Status from the View of Admin
* \[[GZL-5217](https://gazelle.ihe.net/jira/browse/GZL-5217)\] DIGIT_Unable to edit Monitor test assignment

# 6.10.3

_Release date: 2023-06-01_

__Sub-task__
* \[[GZL-5126](https://gazelle.ihe.net/jira/browse/GZL-5126)\] Show statistics about SUT network interfaces for testing session managers and administrators

__Bug__
* \[[GZL-5186](https://gazelle.ihe.net/jira/browse/GZL-5186)\] TM: Conditions for the user to generate his contract have to been revised
* \[[GZL-5203](https://gazelle.ihe.net/jira/browse/GZL-5203)\] Test Cases are listed not in the expected order
* \[[GZL-5211](https://gazelle.ihe.net/jira/browse/GZL-5211)\] monitor worklist unexpected error

# 6.10.2
_Release date: 2023-04-18_

__Bug__
* \[[GZL-5111](https://gazelle.ihe.net/jira/browse/GZL-5111)\] Allow vendors to use "complete with error" and "verified by vendor" for preparatory test cases
* \[[GZL-5198](https://gazelle.ihe.net/jira/browse/GZL-5198)\] Allow anonymous vendors to access the testing session scope

# 6.3.1-SEGUR
_Release date: 2023-04-18_

__Bug__
* \[[GZL-5208](https://gazelle.ihe.net/jira/browse/GZL-5208)\] Fix TOS popup SEGUR

# 6.10.1
_Release date: 2023-04-18_

__Bug__
* \[[GZL-5183](https://gazelle.ihe.net/jira/browse/GZL-5183)\] Cannot generate XSL file from the "List of invoices" page
* \[[GZL-5187](https://gazelle.ihe.net/jira/browse/GZL-5187)\] Fix TOS popup

__Story__
* \[[GZL-5072](https://gazelle.ihe.net/jira/browse/GZL-5072)\] Export of financial reports fail
* \[[GZL-5153](https://gazelle.ihe.net/jira/browse/GZL-5153)\] Review the layout and content of the all test runs page

# 6.10.0
_Release date: 2022-12-23_

__Sub-task__
* \[[GZL-5113](https://gazelle.ihe.net/jira/browse/GZL-5113)\] Show statistics about testing session scope to the TSM and admin users
* \[[GZL-5114](https://gazelle.ihe.net/jira/browse/GZL-5114)\] Show how many participants are registered to the TSM and admin users during the registration phase
* \[[GZL-5127](https://gazelle.ihe.net/jira/browse/GZL-5127)\] Show statistics about preparatory tests for testing session managers and administrator

__Bug__
* \[[GZL-5140](https://gazelle.ihe.net/jira/browse/GZL-5140)\] Review the evaluation dashboard for testing session managers and administrator
* \[[GZL-5145](https://gazelle.ihe.net/jira/browse/GZL-5145)\] Fix the filtering criteria on the evaluation page
* \[[GZL-5151](https://gazelle.ihe.net/jira/browse/GZL-5151)\] Test execution page: quick filter shall store the test type when selected
* \[[GZL-5154](https://gazelle.ihe.net/jira/browse/GZL-5154)\] During the registration phase, monitors should not see the test session process section of the home page
* \[[GZL-5155](https://gazelle.ihe.net/jira/browse/GZL-5155)\] Actions in Gazelle Test Management take several seconds to resolve
* \[[GZL-5158](https://gazelle.ihe.net/jira/browse/GZL-5158)\] Do not put minimum size on progress bars
* \[[GZL-5159](https://gazelle.ihe.net/jira/browse/GZL-5159)\] "Participating SUTs" statistics shall not count dropped SUT
* \[[GZL-5162](https://gazelle.ihe.net/jira/browse/GZL-5162)\] Registration phase / Participating SUTs: The number of different organisations who registered a SUT is not accurate
* \[[GZL-5163](https://gazelle.ihe.net/jira/browse/GZL-5163)\] Testing session scope page is empty for monitors
* \[[GZL-5165](https://gazelle.ihe.net/jira/browse/GZL-5165)\] Testing session scope: list of profiles is not saved in the relevant test session
* \[[GZL-5166](https://gazelle.ihe.net/jira/browse/GZL-5166)\] Testing session scope: updating data shall not cause the panels to collapse
* \[[GZL-5171](https://gazelle.ihe.net/jira/browse/GZL-5171)\] [QUAL] version in footer is not the version of TM
* \[[GZL-5174](https://gazelle.ihe.net/jira/browse/GZL-5174)\] [QUAL] In registration phase as vendor - no information or button
* \[[GZL-5175](https://gazelle.ihe.net/jira/browse/GZL-5175)\] [QUAL] In Preparation phase as vendor - no information or button
* \[[GZL-5178](https://gazelle.ihe.net/jira/browse/GZL-5178)\] Layout of the Testing session scope page is broken

__Story__
* \[[GZL-4980](https://gazelle.ihe.net/jira/browse/GZL-4980)\] GMM:  Cannot add Referenced Standard to a Transaction
* \[[GZL-5109](https://gazelle.ihe.net/jira/browse/GZL-5109)\] Display Registration details to the testing session manager and administrator
* \[[GZL-5115](https://gazelle.ihe.net/jira/browse/GZL-5115)\] Manage the testing session scope in the tool
* \[[GZL-5116](https://gazelle.ihe.net/jira/browse/GZL-5116)\] Preparation dashboard for testing session managers and administrator

__Remarks__

# 6.3.0-SEGUR
_Release date: 2022-10-18_

__Story__
* \[[GZL-5073](https://gazelle.ihe.net/jira/browse/GZL-5073)\] Create API for test report retrieving

__Remarks__

This version is forked from 6.2.3 (before the new UI portal) and is specifically developed for SEGUR client platform. 

# 6.9.0
_Release date: 2022-09-20_

__Bug__
* \[[GZL-5130](https://gazelle.ihe.net/jira/browse/GZL-5130)\] Wrong ending date in TestingSession management

__Story__
* \[[GZL-5073](https://gazelle.ihe.net/jira/browse/GZL-5073)\] Create API for test report retrieving

# 6.8.0
_Release date: 2022-09-08_

__Sub-task__
* \[[GZL-5122](https://gazelle.ihe.net/jira/browse/GZL-5122)\] Show statistics for total test runs for testing session managers and administrators
* \[[GZL-5123](https://gazelle.ihe.net/jira/browse/GZL-5123)\] Show statistics about remaining amount of work for testing session manager and administrator
* \[[GZL-5124](https://gazelle.ihe.net/jira/browse/GZL-5124)\] Show statistics about "Left over" test runs for testing session managers and administrators

__Bug__
* \[[GZL-5128](https://gazelle.ihe.net/jira/browse/GZL-5128)\] Include Proxy port for secured services in the CSV export
* \[[GZL-5135](https://gazelle.ihe.net/jira/browse/GZL-5135)\] TSM shall be able to filter the evaluation page not to display all test types

__Story__
* \[[GZL-5119](https://gazelle.ihe.net/jira/browse/GZL-5119)\] Testing dashboard for testing session managers and administrator
* \[[GZL-5120](https://gazelle.ihe.net/jira/browse/GZL-5120)\] Evaluation dashboard for testing session managers and administrator
* \[[GZL-5121](https://gazelle.ihe.net/jira/browse/GZL-5121)\] Testing session process section for monitors


# 6.7.1
_Release date: 2022-09-02_

__Bug__
* \[[GZL-5117](https://gazelle.ihe.net/jira/browse/GZL-5117)\] Gazelle TM registers to many users in rocket.chat

__Improvement__
* \[[GZL-5133](https://gazelle.ihe.net/jira/browse/GZL-5133)\] Disable profile channel creation during Communication tool synchronization

# 6.7.0
_Release date: 2022-08-17_

__Bug__
* \[[GZL-5107](https://gazelle.ihe.net/jira/browse/GZL-5107)\] Test run shall be reported each time it relates to a SUT Capability included in the tested role
* \[[GZL-5108](https://gazelle.ihe.net/jira/browse/GZL-5108)\] GazelleTM: All registered AIPOs must appear on Test Execution page

__Story__
* \[[GZL-5097](https://gazelle.ihe.net/jira/browse/GZL-5097)\] Show statistics about test runs to the vendor during the testing phase
* \[[GZL-5098](https://gazelle.ihe.net/jira/browse/GZL-5098)\] show statistics about capability statuses to the vendor during the evaluation phase

__Improvement__
* \[[GZL-5104](https://gazelle.ihe.net/jira/browse/GZL-5104)\] Display the test type on the evaluation page
* \[[GZL-5105](https://gazelle.ihe.net/jira/browse/GZL-5105)\] Not all test runs are not shown on the "All test runs" page
* \[[GZL-5106](https://gazelle.ihe.net/jira/browse/GZL-5106)\] Add a tooltip on test run link

# 6.6.0
_Release date: 2022-08-03_

__Bug__
* \[[GZL-5100](https://gazelle.ihe.net/jira/browse/GZL-5100)\] Progress bar shows non accurate details
* \[[GZL-5101](https://gazelle.ihe.net/jira/browse/GZL-5101)\] Change the link of the "Test execution" shortcut
* \[[GZL-5102](https://gazelle.ihe.net/jira/browse/GZL-5102)\] Adapt the layout of the Network configuration card when no network interface is generated

__Story__
* \[[GZL-5076](https://gazelle.ihe.net/jira/browse/GZL-5076)\] New layout for the test execution page - Phase 1
* \[[GZL-5095](https://gazelle.ihe.net/jira/browse/GZL-5095)\] Show network interface statistics to the vendor during the preparation phase
* \[[GZL-5096](https://gazelle.ihe.net/jira/browse/GZL-5096)\] Change the link for the "Preparatory tests" menu entry

__Improvement__
* \[[GZL-5099](https://gazelle.ihe.net/jira/browse/GZL-5099)\] User shall not be able to start a test when the session is closed

# 6.5.0
_Release date: 2022-07-22_

__Bug__
* \[[GZL-5079](https://gazelle.ihe.net/jira/browse/GZL-5079)\] [Gazelle-Model] Links between transaction and standards have vanished
* \[[GZL-5089](https://gazelle.ihe.net/jira/browse/GZL-5089)\] transaction issues

__Story__
* \[[GZL-5080](https://gazelle.ihe.net/jira/browse/GZL-5080)\] Join test parterns in channnel
* \[[GZL-5081](https://gazelle.ihe.net/jira/browse/GZL-5081)\] Switch for enable disable RC
* \[[GZL-5082](https://gazelle.ihe.net/jira/browse/GZL-5082)\] Create channel at test run start
* \[[GZL-5083](https://gazelle.ihe.net/jira/browse/GZL-5083)\] Archive Channel when status change
* \[[GZL-5084](https://gazelle.ihe.net/jira/browse/GZL-5084)\] Send chatroom message to the communication tool
* \[[GZL-5085](https://gazelle.ihe.net/jira/browse/GZL-5085)\] ability to join testrun channel
* \[[GZL-5086](https://gazelle.ihe.net/jira/browse/GZL-5086)\] Communication Tool OnBoarding for Gazelle User & Organization
* \[[GZL-5087](https://gazelle.ihe.net/jira/browse/GZL-5087)\] UnArchive Channel when status change

# 6.4.2
_Release date: 2022-06-23_

__Bug__
* \[[GZL-5077](https://gazelle.ihe.net/jira/browse/GZL-5077)\] Vendor sees the "invite team members" button whereas he has no permission to access the page
* \[[GZL-5078](https://gazelle.ihe.net/jira/browse/GZL-5078)\] user with late_registration role shall not see the error message saying that the registration is closed

__Story__
* \[[GZL-5012](https://gazelle.ihe.net/jira/browse/GZL-5012)\] Operator edits SUT
* \[[GZL-5013](https://gazelle.ihe.net/jira/browse/GZL-5013)\] Operator adds a SUT
* \[[GZL-5014](https://gazelle.ihe.net/jira/browse/GZL-5014)\] Operator imports a SUT from another test session

__Improvement__
* \[[GZL-4910](https://gazelle.ihe.net/jira/browse/GZL-4910)\] Complete the SUT Management page for administrator/testing session managers
* \[[GZL-5074](https://gazelle.ihe.net/jira/browse/GZL-5074)\] Allow the user to filter the test execution page based on filter type

# 6.4.1
_Release date: 2022-04-28_

__Bug__
* \[[GZL-5068](https://gazelle.ihe.net/jira/browse/GZL-5068)\] Cannot create PDF "Profile coverage"

# 6.4.0
_Release date: 2022-04-13_

__Bug__
* \[[GZL-5059](https://gazelle.ihe.net/jira/browse/GZL-5059 )\] Update notification message on registration page
* \[[GZL-5060](https://gazelle.ihe.net/jira/browse/GZL-5060 )\] Cannot update results at system AIPO level
* \[[GZL-5061](https://gazelle.ihe.net/jira/browse/GZL-5061 )\] Session details on the "change session pop-up" are not accurate
* \[[GZL-5062](https://gazelle.ihe.net/jira/browse/GZL-5062 )\] Progress Bar "preparation" entry is grey instead of green
* \[[GZL-5064](https://gazelle.ihe.net/jira/browse/GZL-5064 )\] Wording on SUT Summary + Counting SUT with unresolved dependencies Incorrect
* \[[GZL-5066](https://gazelle.ihe.net/jira/browse/GZL-5066 )\] Request to access again the Test requirement page
* \[[GZL-5067](https://gazelle.ihe.net/jira/browse/GZL-5067 )\] As an administrator of the tool, I shall be able to edit information about any organisation

__Story__
* \[[GZL-5008](https://gazelle.ihe.net/jira/browse/GZL-5008 )\] SUT summary during registration
* \[[GZL-5009](https://gazelle.ihe.net/jira/browse/GZL-5009 )\] Inform the user which phase is ongoing
* \[[GZL-5010](https://gazelle.ihe.net/jira/browse/GZL-5010 )\] Add option to the testing session to display relevant information to the user.
* \[[GZL-5044](https://gazelle.ihe.net/jira/browse/GZL-5044 )\] Improve the display of the participating SUTs table on registration page
* \[[GZL-5052](https://gazelle.ihe.net/jira/browse/GZL-5052 )\] Session process section for vendors during the preparatory phase v1
* \[[GZL-5055](https://gazelle.ihe.net/jira/browse/GZL-5055 )\] Make wording more meaningful for the supportive/thorough feature
* \[[GZL-5056](https://gazelle.ihe.net/jira/browse/GZL-5056 )\] Display the test type to the user
* \[[GZL-5063](https://gazelle.ihe.net/jira/browse/GZL-5063 )\] Team & Contract Summary during Registration

__Remarks__

This version contains the second version of the new Portal UI


# 6.3.1
_Release date: 2022-03-23_

__Bug__
* \[[GZL-5054](https://gazelle.ihe.net/jira/browse/GZL-5054) \] Login using CAS in Test Management redirects to the error.seam page

# 6.3.0
_Release date: 2022-03-21_

__Remarks__

This version contains the first version of the new Portal UI

__Bug__
* \[[GZL-4812](https://gazelle.ihe.net/jira/browse/GZL-4812 )\] Review user rights on financial/financialSummary.xhtml page

__Story__
* \[[GZL-4999](https://gazelle.ihe.net/jira/browse/GZL-4999 )\] Reorganize the home page - first version
* \[[GZL-5001](https://gazelle.ihe.net/jira/browse/GZL-5001 )\] Reorganize the footer section of the template
* \[[GZL-5006](https://gazelle.ihe.net/jira/browse/GZL-5006 )\] Reorganise menu bar
* \[[GZL-5007](https://gazelle.ihe.net/jira/browse/GZL-5007 )\] Single page for registration V1
* \[[GZL-5022](https://gazelle.ihe.net/jira/browse/GZL-5022 )\] Participating SUTs section on registration page (read-only)
* \[[GZL-5023](https://gazelle.ihe.net/jira/browse/GZL-5023 )\] Display fees informations on the new registration page
* \[[GZL-5024](https://gazelle.ihe.net/jira/browse/GZL-5024 )\] Display information about the contract on the registration page
* \[[GZL-5025](https://gazelle.ihe.net/jira/browse/GZL-5025 )\] List attendees on the registration page (read-only)

__Task__
* \[[GZL-5002](https://gazelle.ihe.net/jira/browse/GZL-5002 )\] User is invited to join the default session
* \[[GZL-5003](https://gazelle.ihe.net/jira/browse/GZL-5003 )\] Home entry in the menu bar
* \[[GZL-5004](https://gazelle.ihe.net/jira/browse/GZL-5004 )\] Relevant specifications menu entry
* \[[GZL-5027](https://gazelle.ihe.net/jira/browse/GZL-5027 )\] Bar Menu new design
* \[[GZL-5036](https://gazelle.ihe.net/jira/browse/GZL-5036 )\] Bar Menu crowdin update
* \[[GZL-5037](https://gazelle.ihe.net/jira/browse/GZL-5037 )\] Merging and resolving dependencies issues


# 6.2.3
_Release date: 2022-02-02_

__Bug__
* \[[GZL-4994](https://gazelle.ihe.net/jira/browse/GZL-4994 )\] Add search parameter for organisation name and keyword

# 6.2.2
_Release date: 2022-01-22_

__Story__
* \[[GZL-4983](https://gazelle.ihe.net/jira/browse/GZL-4983 )\] German participants receive notification messages in French

# 6.2.1
_Release date: 2022-01-19_

__Story__
* \[[GZL-4984](https://gazelle.ihe.net/jira/browse/GZL-4984 )\] GDPR Refacto banner in standalone library

# 6.2.0
_Release date: 2021-12-15_

__Story__
* \[[GZL-4981](https://gazelle.ihe.net/jira/browse/GZL-4981)\] Add a search feature on the list of samples page
* \[[GZL-4982](https://gazelle.ihe.net/jira/browse/GZL-4982)\] RGPD Validate

__Dependencies__

* Update of gazelle-assets
* Update of gazelle-tm-tools

# 6.1.1
_Release date: 2021-07-23_

__Bug__
* \[[GZL-4940](https://gazelle.ihe.net/jira/browse/GZL-4940)\] Create XML stream to export test report data

# 6.1.0
_Release date: 2021-07-20_

__Remarks__
This version of Gazelle Test Management is only compatible with EVSClient 5.13.4 and forward because of a change in gazelle-evsclient-connector.

__Improvement__
* \[[GZL-4977](https://gazelle.ihe.net/jira/browse/GZL-4977)\] New build profile in maven for Sharazone to increase the upload file max size

# 6.0.0
_Release date: 2021-06-01_

__Story__
* \[[GZL-4971](https://gazelle.ihe.net/jira/browse/GZL-4971)\] - Request Proxy for secured proxy port V1 
* \[[GZL-4972](https://gazelle.ihe.net/jira/browse/GZL-4972)\] - Open Proxy secured channel V1 

# 5.16.0
_Release date: 2021-05-18_

__Story__
* \[[GZL-4956](https://gazelle.ihe.net/jira/browse/GZL-4956)\] - External test report publication in test instance
* \[[GZL-4957](https://gazelle.ihe.net/jira/browse/GZL-4957)\] - Token for uploading files must have a validity period
* \[[GZL-4958](https://gazelle.ihe.net/jira/browse/GZL-4958)\] - Prevent upload of logs and report in "finished" test instance
* \[[GZL-4960](https://gazelle.ihe.net/jira/browse/GZL-4960)\] - Show a green or red icon next to a test-report in a test instance.

__Improvement__
* \[[GZL-4959](https://gazelle.ihe.net/jira/browse/GZL-4959)\] - Add comment in chat room at token generation and any upload (Audit).


# 5.15.0
_Release date: 2021-05-18_

__Warning__ : this release 

__Story__
* \[[GZL-4953](https://gazelle.ihe.net/jira/browse/GZL-4953)\] - Sending notifications by emails
* \[[GZL-4954](https://gazelle.ihe.net/jira/browse/GZL-4954)\] - Vendors may be able to set any status in a test instance


__Improvement__
* \[[GZL-4952](https://gazelle.ihe.net/jira/browse/GZL-4952)\] - Privacy of the sample section in Gazelle
* \[[GZL-4955](https://gazelle.ihe.net/jira/browse/GZL-4955)\] - Update cas configuration to use a dedicated property file




## Gazelle model 4.2.7

__Story__
* \[[GZL-4953](https://gazelle.ihe.net/jira/browse/GZL-4953)\] - Sending notifications by emails

__Bug__

* \[[GZL-4967](https://gazelle.ihe.net/jira/browse/GZL-4967)\] "Comment" tab of a system shall not be disclosed to users that are not part of the company owning the system

# 5.14.3
_Release date: 2021-05-06_

__Bug__
* \[[GZL-4779](https://gazelle.ihe.net/jira/browse/GZL-4779)\] Monitor worklist -- sort by "oldest" on top
* \[[GZL-4967](https://gazelle.ihe.net/jira/browse/GZL-4967)\] "Comment" tab of a system shall not be disclosed to users that are not part of the company owning the system

__Improvement__
* \[[GZL-4968](https://gazelle.ihe.net/jira/browse/GZL-4968)\] Admin and account manager shall be able to manual set the VAT amount

# 5.14.2
_Release date: 2021-03-25_

__Remarks__
Update gazelle-model, release 4.2.6

__Bug__
* \[[GZL-4962](https://gazelle.ihe.net/jira/browse/GZL-4962)\] Sample page is not updated when you switch to a new session
* \[[GZL-4965](https://gazelle.ihe.net/jira/browse/GZL-4965)\] Cannot link a standard to a transaction


# 5.14.1
_Release date: 2020-11-30_

__Remarks__
Update gazelle-model, release 4.2.4

__Bug__
* \[[GZL-4951](https://gazelle.ihe.net/jira/browse/GZL-4951)\] The webmethod 'getAllMessageProfiles' returns a bad response

# 5.14.0
_Release date: 2020-10-30_

__Improvement__
* \[[GZL-4947](https://gazelle.ihe.net/jira/browse/GZL-4947)\] CTT Integration


# 5.13.0
_Release date: 2020-10-19_

__Bug__
* \[[GZL-4909](https://gazelle.ihe.net/jira/browse/GZL-4909)\] Unexpected error on re-login

__Improvement__
* \[[GZL-4851](https://gazelle.ihe.net/jira/browse/GZL-4851)\] Import/Export Samples definitions
* \[[GZL-4852](https://gazelle.ihe.net/jira/browse/GZL-4852)\] Import/export OID root definitions

# 5.12.1
_Release date: 2019-09-26_

__Improvement__

* \[[GZL-4891](https://gazelle.ihe.net/jira/browse/GZL-4891)\] Add Completed With Errors and Self Verified tests status in KPI for preCAT tests
* \[[GZL-4892](https://gazelle.ihe.net/jira/browse/GZL-4892)\] Add a filter on Test Peer type in test instances page

# 5.12.0
_Release date: 2019-09-23_

__Story__

* \[[GZL-4872](https://gazelle.ihe.net/jira/browse/GZL-4872)\] Add possibility for participants to start group tests

# 5.11.1
_Release date: 2019-09-09_

__Bug__

* \[[GZL-4885](https://gazelle.ihe.net/jira/browse/GZL-4885)\] Export link not redirecting to an upload dialog box

# 5.11.0
_Release date: 2019-09-06_

__Bug__

* \[[GZL-4820](https://gazelle.ihe.net/jira/browse/GZL-4820)\] Acceptance of new systems already set to true
* \[[GZL-4827](https://gazelle.ihe.net/jira/browse/GZL-4827)\] [System creation] Exceptions are not reported to the user
* \[[GZL-4830](https://gazelle.ihe.net/jira/browse/GZL-4830)\] Registration overview gives inconsistent results
* \[[GZL-4835](https://gazelle.ihe.net/jira/browse/GZL-4835)\] '&amp;' in test description become '&' in test instance
* \[[GZL-4869](https://gazelle.ihe.net/jira/browse/GZL-4869)\] System type not added in database with init scripts
* \[[GZL-4870](https://gazelle.ihe.net/jira/browse/GZL-4870)\] Problem importing TF configurations : error at import

__Story__

* \[[GZL-4845](https://gazelle.ihe.net/jira/browse/GZL-4845)\] Administrators need to export/import Configuration definitions

__Task__

* \[[GZL-4828](https://gazelle.ihe.net/jira/browse/GZL-4828)\] Add the company address in the excel export of the testing session participants

__Improvement__

* \[[GZL-4838](https://gazelle.ihe.net/jira/browse/GZL-4838)\] Externalize the configuration of the mail server
* \[[GZL-4840](https://gazelle.ihe.net/jira/browse/GZL-4840)\] Administrators need to export/import TF rules
* \[[GZL-4849](https://gazelle.ihe.net/jira/browse/GZL-4849)\] Import/Export HL7 message profiles
* \[[GZL-4850](https://gazelle.ihe.net/jira/browse/GZL-4850)\] Import/Export audit messages definitions
* \[[GZL-4861](https://gazelle.ihe.net/jira/browse/GZL-4861)\] Import/Export TF Documents
* \[[GZL-4862](https://gazelle.ihe.net/jira/browse/GZL-4862)\] Import/Export Standards
* \[[GZL-4879](https://gazelle.ihe.net/jira/browse/GZL-4879)\] Three more attributes are needed in the CSV export of the test cases

# 5.10.0
_Release date: 2019-03-21_

__Remarks__
Management of the datasource has changed. Refer to the installation manual to configure your Jboss when updating to this version.

__Bug__

* \[[GZL-4821](https://gazelle.ihe.net/jira/browse/GZL-4821)\] Hosts bound to dropped systems are visible to participants and shall not

__Improvement__

* \[[GZL-4791](https://gazelle.ihe.net/jira/browse/GZL-4791)\] Extract datasource configuration
* \[[GZL-4792](https://gazelle.ihe.net/jira/browse/GZL-4792)\] Update SQL scripts archive
* \[[GZL-4822](https://gazelle.ihe.net/jira/browse/GZL-4822)\] Since non approved configurations are not used by the proxy, hide the proxy port until they are approved

# 5.9.6
_Release date: 2019-03-13_

__Bug__
* \[[GZL-4804](https://gazelle.ihe.net/jira/browse/GZL-4804)\] /Test Management/System Testing/5 - System Management/As Vendor admin/TM-312:Vendor admin generates Integration Statemen
* \[[GZL-4819](https://gazelle.ihe.net/jira/browse/GZL-4819)\] Adding a HL7 conformance profile is failing when the selected transaction is linked to a document
* \[[GZL-4813](https://gazelle.ihe.net/jira/browse/GZL-4813)\] Can't update connectathon results

# 5.9.5
_Release date: 2018-12-20_

__Bug__
* \[[GZL-4775](https://gazelle.ihe.net/jira/browse/GZL-4775)\] In Test summary view "is Validated ?" and " Test validated ?" is the same info
* \[[GZL-4777](https://gazelle.ihe.net/jira/browse/GZL-4777)\] Test status, type and peer-type not displayed if not admin
* \[[GZL-4789](https://gazelle.ihe.net/jira/browse/GZL-4789)\] Adding a host broken for organizations with underscore character

__Task__
* \[[GZL-4786](https://gazelle.ihe.net/jira/browse/GZL-4786)\] Fix unit test on gazelle-model

__Improvement__
* \[[GZL-4776](https://gazelle.ihe.net/jira/browse/GZL-4776)\] Test definition -> test roles are not orderable

# 5.9.4
_Release date: 2018-12-05_

__Bug__
* \[[GZL-4733](https://gazelle.ihe.net/jira/browse/GZL-4733)\] [CAS] configFileLocation is the same for PR/GMM/TM
* \[[GZL-4768](https://gazelle.ihe.net/jira/browse/GZL-4768)\] Sequence Diagram is not display by default
* \[[GZL-4770](https://gazelle.ihe.net/jira/browse/GZL-4770)\] Can not create a new test with Gazelle TM
* \[[GZL-4773](https://gazelle.ihe.net/jira/browse/GZL-4773)\] Remove all the references to QR codes and monitor app 
* \[[GZL-4774](https://gazelle.ihe.net/jira/browse/GZL-4774)\] Remove any reference to patient generation&sharing feature
* \[[GZL-4780](https://gazelle.ihe.net/jira/browse/GZL-4780)\] Layout of setup password page is broken

__Improvement__
* \[[GZL-4767](https://gazelle.ihe.net/jira/browse/GZL-4767)\] Missing check/warning on Transaction standard

# 5.9.3
_Release date: 2018-08-16_

__Bug__
* \[[GZL-4765](https://gazelle.ihe.net/jira/browse/GZL-4765)\] HQL left outer join trouble

# 5.9.2
_Release date: 2018-08-16_

__Bug__
* \[[GZL-4757](https://gazelle.ihe.net/jira/browse/GZL-4757)\] Bug when editing two systems at the same time
* \[[GZL-4758](https://gazelle.ihe.net/jira/browse/GZL-4758)\] 'Report an issue' button shall not appear on test instance page when the feature is not configured
* \[[GZL-4759](https://gazelle.ihe.net/jira/browse/GZL-4759)\] The link to xdstools doesn't work when there is an &amp; instead of a &

__Improvement__
* \[[GZL-4761](https://gazelle.ihe.net/jira/browse/GZL-4761)\] Emails spam all admin

# 5.9.1
_Release date: 2018-07-11_

__Bug__
* \[[GZL-4742](https://gazelle.ihe.net/jira/browse/GZL-4742)\] Bug in generation of ldif
* \[[GZL-4745](https://gazelle.ihe.net/jira/browse/GZL-4745)\] Removing from a system an AIPO that is subject in a test instance crash CAT and results page
* \[[GZL-4748](https://gazelle.ihe.net/jira/browse/GZL-4748)\] Each time a system registration is modified (Add or remove AIPO) the CAT page cannot be reached
* \[[GZL-4749](https://gazelle.ihe.net/jira/browse/GZL-4749)\] [DGSANTE] Bug in the generation of report of TS

__Improvement__
* \[[GZL-4739](https://gazelle.ihe.net/jira/browse/GZL-4739)\] Add support for RAW configuration type

# 5.9.0
_Release date: 2018-04-11_

__Bug__
* \[[GZL-4736](https://gazelle.ihe.net/jira/browse/GZL-4736)\] Cannot delete integration Profile

__Story__
* \[[GZL-4740](https://gazelle.ihe.net/jira/browse/GZL-4740)\] Add export configuration in ldif structure

# 5.8.0
_Release date: 2018-03-07_

__Sub-task__
* \[[GZL-4682](https://gazelle.ihe.net/jira/browse/GZL-4682)\] Create a check about the port proxy in the current testing session
* \[[GZL-4683](https://gazelle.ihe.net/jira/browse/GZL-4683)\] Create a check about the ip generated by Gazelle for the current testing session

__Bug__
* \[[GZL-4210](https://gazelle.ihe.net/jira/browse/GZL-4210)\] in Application configuration, need to handle the case when tls url is empty
* \[[GZL-4675](https://gazelle.ihe.net/jira/browse/GZL-4675)\] Cannot edit hosts without configurations
* \[[GZL-4678](https://gazelle.ihe.net/jira/browse/GZL-4678)\] Fix VAT explanation in organization management
* \[[GZL-4679](https://gazelle.ihe.net/jira/browse/GZL-4679)\] When user add aipo to a system the table display only 5 results
* \[[GZL-4681](https://gazelle.ihe.net/jira/browse/GZL-4681)\] Testing session overview is providing an error page
* \[[GZL-4690](https://gazelle.ihe.net/jira/browse/GZL-4690)\] DICOM sample validation trouble
* \[[GZL-4695](https://gazelle.ihe.net/jira/browse/GZL-4695)\] Error in Testing Session Participants
* \[[GZL-4708](https://gazelle.ihe.net/jira/browse/GZL-4708)\] The refresh button of the result of validation is refreshing a part of the table
* \[[GZL-4709](https://gazelle.ihe.net/jira/browse/GZL-4709)\] Download connectathon result report is not well populated
* \[[GZL-4710](https://gazelle.ihe.net/jira/browse/GZL-4710)\] We lost join table domain/profile
* \[[GZL-4713](https://gazelle.ihe.net/jira/browse/GZL-4713)\] test instance notification messages status is not well populated
* \[[GZL-4726](https://gazelle.ihe.net/jira/browse/GZL-4726)\] /Test Management/System Testing/9 - Configuration Management/As Gazelle Admin/TM-282:Manage hosts configuration -  Exécu
* \[[GZL-4727](https://gazelle.ihe.net/jira/browse/GZL-4727)\] Vendor Admin user can edit information of contact from different organization
* \[[GZL-4728](https://gazelle.ihe.net/jira/browse/GZL-4728)\] /Test Management/System Testing/5 - System Management/As Vendor/TM-286:Vendor Generates Integration Statement. -  Execut

__Story__
* \[[GZL-4696](https://gazelle.ihe.net/jira/browse/GZL-4696)\] The Patient Generation & Sharing feature is no more needed in TM

__Improvement__
* \[[GZL-4602](https://gazelle.ihe.net/jira/browse/GZL-4602)\] When we fail a test, can we have the focus cursor directly in the text area?
* \[[GZL-4644](https://gazelle.ihe.net/jira/browse/GZL-4644)\] Use new CAS SSO
* \[[GZL-4680](https://gazelle.ihe.net/jira/browse/GZL-4680)\] We need a page to check the configuration for the testing session
* \[[GZL-4699](https://gazelle.ihe.net/jira/browse/GZL-4699)\] OID Management : Need to synchronise comment between EU-CAT and gazelle-na
* \[[GZL-4700](https://gazelle.ihe.net/jira/browse/GZL-4700)\] User want to see TF and Test List menu entry
* \[[GZL-4701](https://gazelle.ihe.net/jira/browse/GZL-4701)\] Create webservices between TM, GWT and XdsTools4
* \[[GZL-4714](https://gazelle.ihe.net/jira/browse/GZL-4714)\] Update posgresql driver

# 5.7.3
_Release date: 2018-01-11_

__Bug__
* \[[GZL-4135](https://gazelle.ihe.net/jira/browse/GZL-4135)\] If you edit/show a user, you save, you try to edit/show a new user, you need to click twice
* \[[GZL-4137](https://gazelle.ihe.net/jira/browse/GZL-4137)\] When you try to delete a user, you have a popupPanel with tables on it, they are not sortable
* \[[GZL-4140](https://gazelle.ihe.net/jira/browse/GZL-4140)\] sort of import a contact/user is not working
* \[[GZL-4142](https://gazelle.ihe.net/jira/browse/GZL-4142)\] Sort in the financial summary is not working
* \[[GZL-4490](https://gazelle.ihe.net/jira/browse/GZL-4490)\] In "Host configuration" admin page, add a comment to say that the hosts configuration shown are only for completed and accepted systems.
* \[[GZL-4498](https://gazelle.ihe.net/jira/browse/GZL-4498)\] The layout of OID management has a regression
* \[[GZL-4500](https://gazelle.ihe.net/jira/browse/GZL-4500)\] The color of the button skip is not user friendly (black on blue)
* \[[GZL-4594](https://gazelle.ihe.net/jira/browse/GZL-4594)\] In web service transaction usage configuration, we are able to select an actor which is not related to that profile
* \[[GZL-4669](https://gazelle.ihe.net/jira/browse/GZL-4669)\] Improve the Style for the change password panel
* \[[GZL-4670](https://gazelle.ihe.net/jira/browse/GZL-4670)\] In NA2018 session, assign monitors to test page shows test instances
* \[[GZL-4674](https://gazelle.ihe.net/jira/browse/GZL-4674)\] Host management loose his filter

__Improvement__
* \[[GZL-4312](https://gazelle.ihe.net/jira/browse/GZL-4312)\] System owner change: display identity instead of username
* \[[GZL-4406](https://gazelle.ihe.net/jira/browse/GZL-4406)\] I should be able to edit a URL in a test instance
* \[[GZL-4567](https://gazelle.ihe.net/jira/browse/GZL-4567)\] Please add edit feature in GMM for Actor/Transaction pairs in a profile.
* \[[GZL-4659](https://gazelle.ihe.net/jira/browse/GZL-4659)\] Use local set by user to select the language for test exportation

# 5.7.2
_Release date: 2017-12-21_

__Bug__
* \[[GZL-4461](https://gazelle.ihe.net/jira/browse/GZL-4461)\] Monitor assignement, Excel export : Assigned test number column present but empty.
* \[[GZL-4481](https://gazelle.ihe.net/jira/browse/GZL-4481)\] When you edit a user, the help images are not aligned with the checkbox and roles
* \[[GZL-4623](https://gazelle.ihe.net/jira/browse/GZL-4623)\] When you import a system from an old testing session, it is automatically accepted
* \[[GZL-4633](https://gazelle.ihe.net/jira/browse/GZL-4633)\] Need to fix invoice and financial summary generation
* \[[GZL-4635](https://gazelle.ihe.net/jira/browse/GZL-4635)\] GMM:  Delete a role from a test, and all associated test steps are deleted w/o warning!
* \[[GZL-4636](https://gazelle.ihe.net/jira/browse/GZL-4636)\] Impossible to validate Integration Statement
* \[[GZL-4638](https://gazelle.ihe.net/jira/browse/GZL-4638)\] Editing a system in gazelle shows wrong keyword
* \[[GZL-4639](https://gazelle.ihe.net/jira/browse/GZL-4639)\] Pre-CAT test results page: Unexpected behavior about a description of test instance.
* \[[GZL-4641](https://gazelle.ihe.net/jira/browse/GZL-4641)\] Impossible to delete a system
* \[[GZL-4642](https://gazelle.ihe.net/jira/browse/GZL-4642)\] NPE in gazelle-na log
* \[[GZL-4645](https://gazelle.ihe.net/jira/browse/GZL-4645)\] Network configuration: Host editing is broken
* \[[GZL-4649](https://gazelle.ihe.net/jira/browse/GZL-4649)\] Test Instance Page: Gazelle does not cache comments before posting
* \[[GZL-4650](https://gazelle.ihe.net/jira/browse/GZL-4650)\] Unable to display admin-manage-system page if a simulator has been created
* \[[GZL-4651](https://gazelle.ihe.net/jira/browse/GZL-4651)\] Can't copy system and it would be nice to have a sort in the list of session per year in the "import system from other session" option
* \[[GZL-4653](https://gazelle.ihe.net/jira/browse/GZL-4653)\] GMM:  I was allowed to delete a RoleInTest that was used in a test.
* \[[GZL-4654](https://gazelle.ihe.net/jira/browse/GZL-4654)\] Download test doesn't work when description is only in french
* \[[GZL-4658](https://gazelle.ihe.net/jira/browse/GZL-4658)\] Can't update user when email is wrong

__Story__
* \[[GZL-4610](https://gazelle.ihe.net/jira/browse/GZL-4610)\] Connectathon results report use IHE logo per default. Use Testing Session logo instead

__Improvement__
* \[[GZL-3691](https://gazelle.ihe.net/jira/browse/GZL-3691)\] Improve test lifecycle
* \[[GZL-4435](https://gazelle.ihe.net/jira/browse/GZL-4435)\] It is good to have a notification email when someone change the password of the user.
* \[[GZL-4561](https://gazelle.ihe.net/jira/browse/GZL-4561)\] Update system automated tests
* \[[GZL-4624](https://gazelle.ihe.net/jira/browse/GZL-4624)\] When you access to the "system acceptance page", and you want to "not accept" already accepted system, it does not work
* \[[GZL-4646](https://gazelle.ihe.net/jira/browse/GZL-4646)\] Need to improve invoice and financial summary
* \[[GZL-4652](https://gazelle.ihe.net/jira/browse/GZL-4652)\] Consider updating GMM to show step count for each role
* \[[GZL-4655](https://gazelle.ihe.net/jira/browse/GZL-4655)\] Consider adding a password reset button for managers
* \[[GZL-4660](https://gazelle.ihe.net/jira/browse/GZL-4660)\] add missing translation to crowdin and translate them

# 5.7.1
_Release date: 2017-11-15_

__Bug__
* \[[GZL-4601](https://gazelle.ihe.net/jira/browse/GZL-4601)\] The buttons for monitor related to Validate all the status (or not) contains a workflow issue
* \[[GZL-4606](https://gazelle.ihe.net/jira/browse/GZL-4606)\] I am a monitor and administrator, when I set a test to "to be verified", it is automatically claimed by me
* \[[GZL-4616](https://gazelle.ihe.net/jira/browse/GZL-4616)\] Missing translation
* \[[GZL-4640](https://gazelle.ihe.net/jira/browse/GZL-4640)\] NA2018:  Exception when adding 2 monitor user account -- account not added

__Improvement__
* \[[GZL-4507](https://gazelle.ihe.net/jira/browse/GZL-4507)\] When an administrator marks a test Critical, that admin should not also claim the test
* \[[GZL-4564](https://gazelle.ihe.net/jira/browse/GZL-4564)\] NA2018:  Do we need another configuration type?
* \[[GZL-4603](https://gazelle.ihe.net/jira/browse/GZL-4603)\] We don't have the email of the users in the excel export from the admin user page, is there a reason for this?
* \[[GZL-4615](https://gazelle.ihe.net/jira/browse/GZL-4615)\] Block claim/unclaim TI when status is verified or failed
* \[[GZL-4632](https://gazelle.ihe.net/jira/browse/GZL-4632)\] Compilation of jasper reports need to be improved.
* \[[GZL-4648](https://gazelle.ihe.net/jira/browse/GZL-4648)\] Monitor Worklist Page: Could you change the default sort order for the usability.

# 5.7.0
_Release date: 2017-10-04_

__Bug__

* \[[GZL-3347](https://gazelle.ihe.net/jira/browse/GZL-3347)\] NA2018:  System summary report has no content
* \[[GZL-4155](https://gazelle.ihe.net/jira/browse/GZL-4155)\] /administration/listMonitors.seam: faces messages are displayed below the input fields
* \[[GZL-4308](https://gazelle.ihe.net/jira/browse/GZL-4308)\] In home page edition you can add a picture but it never displayed in view mode
* \[[GZL-4319](https://gazelle.ihe.net/jira/browse/GZL-4319)\] /Test Management/System Testing/7 - Pre-Connectathon Workflow/Test execution/TM-216:View list of Pre-CAT test for a syst
* \[[GZL-4336](https://gazelle.ihe.net/jira/browse/GZL-4336)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-255:View participants' configurations
* \[[GZL-4420](https://gazelle.ihe.net/jira/browse/GZL-4420)\] Matching of email addresses should be case insensitive
* \[[GZL-4476](https://gazelle.ihe.net/jira/browse/GZL-4476)\] OID Root Management: be able to filter by comment
* \[[GZL-4484](https://gazelle.ihe.net/jira/browse/GZL-4484)\] Session scope side effect
* \[[GZL-4486](https://gazelle.ihe.net/jira/browse/GZL-4486)\] A user created an account with a username containing a whitespace
* \[[GZL-4504](https://gazelle.ihe.net/jira/browse/GZL-4504)\] When you select "Show only claimed tests for all monitors ", it does not do what it should do
* \[[GZL-4508](https://gazelle.ihe.net/jira/browse/GZL-4508)\] Problem when the test step is failed, the fail button not well displayed
* \[[GZL-4517](https://gazelle.ihe.net/jira/browse/GZL-4517)\] The filters in the page to edit the TF Rules, are based on profiles in the currently selected testing session;
* \[[GZL-4523](https://gazelle.ihe.net/jira/browse/GZL-4523)\] https://gazelle.ehdsi.ihe-europe.net/gazelle/testing/testsDefinition/testsList.seam?testType=2&testStatus=4
* \[[GZL-4526](https://gazelle.ihe.net/jira/browse/GZL-4526)\] Crash in application when creating a test in a vm where only Conformity tests were existing.
* \[[GZL-4538](https://gazelle.ihe.net/jira/browse/GZL-4538)\] When you click on the TI link in KPI page for monitor, you get an empty modalpanel
* \[[GZL-4539](https://gazelle.ihe.net/jira/browse/GZL-4539)\] Security issue in the page of graph of tests instances
* \[[GZL-4541](https://gazelle.ihe.net/jira/browse/GZL-4541)\] When you click on the file link, we don't go to the file section of the testinstance
* \[[GZL-4544](https://gazelle.ihe.net/jira/browse/GZL-4544)\] YANPE - NullPointer exeception in ttestDefinitionManager.getTestStepsList()
* \[[GZL-4590](https://gazelle.ihe.net/jira/browse/GZL-4590)\] Impossible to create new Assigning Authority in Gazelle Test Management
* \[[GZL-4591](https://gazelle.ihe.net/jira/browse/GZL-4591)\] Missing translations
* \[[GZL-4596](https://gazelle.ihe.net/jira/browse/GZL-4596)\] NPE when user try to swith Testing session
* \[[GZL-4599](https://gazelle.ihe.net/jira/browse/GZL-4599)\] KPI for test instances is not translated in KPI menu
* \[[GZL-4604](https://gazelle.ihe.net/jira/browse/GZL-4604)\] Cannot modify an existing host
* \[[GZL-4605](https://gazelle.ihe.net/jira/browse/GZL-4605)\] Image in test description does not display in view, but in edit mode

__Improvement__

* \[[GZL-4426](https://gazelle.ihe.net/jira/browse/GZL-4426)\] Configuration : hostname
* \[[GZL-4457](https://gazelle.ihe.net/jira/browse/GZL-4457)\] Improve configuration creation page
* \[[GZL-4483](https://gazelle.ihe.net/jira/browse/GZL-4483)\] In the page for the sample, validation results are hidden
* \[[GZL-4496](https://gazelle.ihe.net/jira/browse/GZL-4496)\] A monitor shall not be able to set a test to aborted
* \[[GZL-4502](https://gazelle.ihe.net/jira/browse/GZL-4502)\] When we click on failing a test, a check panel is shown to add a comment, the button to confirm is clickable event if there are no comment
* \[[GZL-4503](https://gazelle.ihe.net/jira/browse/GZL-4503)\] Request to add table number to System config page
* \[[GZL-4530](https://gazelle.ihe.net/jira/browse/GZL-4530)\] Use the right form type for the sending of email from admin application preference
* \[[GZL-4568](https://gazelle.ihe.net/jira/browse/GZL-4568)\] Rendering of  description for option description could be improved
* \[[GZL-4587](https://gazelle.ihe.net/jira/browse/GZL-4587)\] When the testing session is "internet testing", we still verify that the hostname is less than 24 character
* \[[GZL-4597](https://gazelle.ihe.net/jira/browse/GZL-4597)\] Allow 2 systems from the same company in one test session to share a host
* \[[GZL-4607](https://gazelle.ihe.net/jira/browse/GZL-4607)\] Add "log in as" in user search result for admin

# 5.6.1
_Release date: 2017-09-04_

__Bug__

* \[[GZL-4460](https://gazelle.ihe.net/jira/browse/GZL-4460)\] Monitor Assignement, test number is not refreshed after assignement deletion.
* \[[GZL-4509](https://gazelle.ihe.net/jira/browse/GZL-4509)\] As admin, when you download a report for an organization, then you select another organization, the filtering does not work anymore
* \[[GZL-4550](https://gazelle.ihe.net/jira/browse/GZL-4550)\] NPE in Test instances overview
* \[[GZL-4553](https://gazelle.ihe.net/jira/browse/GZL-4553)\] When you click on the link to the result of validation in the sample, it is opened in the same window
* \[[GZL-4554](https://gazelle.ihe.net/jira/browse/GZL-4554)\] The export of Test as XML shall not export the jira related to the test
* \[[GZL-4556](https://gazelle.ihe.net/jira/browse/GZL-4556)\] Jasper Reports in subdirectories are not compiled. 
* \[[GZL-4563](https://gazelle.ihe.net/jira/browse/GZL-4563)\] [EHEALTHBEL] Gazelle TM - Tests List
* \[[GZL-4570](https://gazelle.ihe.net/jira/browse/GZL-4570)\] GMM Diagram of transactions in a profile is weird for FP profile
* \[[GZL-4582](https://gazelle.ihe.net/jira/browse/GZL-4582)\] In the page editDocument check url refuses https url !

__Improvement__

* \[[GZL-4469](https://gazelle.ihe.net/jira/browse/GZL-4469)\] Patient display: use dl-horizontal to shorten the page
* \[[GZL-4559](https://gazelle.ihe.net/jira/browse/GZL-4559)\] Add possibility to filter in h:selectOneMenu for systemInSessionCreation
* \[[GZL-4586](https://gazelle.ihe.net/jira/browse/GZL-4586)\] Sample name should not be unique within the application

# 5.6.0
_Release date: 2017-08-30_

__Epic__
* \[[GZL-4555](https://gazelle.ihe.net/jira/browse/GZL-4555)\] Need to rewrite systemInSession.xhtml and associated bean SystemManager

__Bug__
* \[[GZL-4521](https://gazelle.ihe.net/jira/browse/GZL-4521)\] Error in the description of an Internet Testing Session
* \[[GZL-4531](https://gazelle.ihe.net/jira/browse/GZL-4531)\] We need to have two emails: one to send emails, and one to contact administrator
* \[[GZL-4532](https://gazelle.ihe.net/jira/browse/GZL-4532)\] The icon for EU-CAT has disapeared
* \[[GZL-4534](https://gazelle.ihe.net/jira/browse/GZL-4534)\] When querying for a profile using the SOAP interface, profile keyword is capitalized before search and thus it fails.
* \[[GZL-4536](https://gazelle.ihe.net/jira/browse/GZL-4536)\] User from company A can add a system to company B
* \[[GZL-4545](https://gazelle.ihe.net/jira/browse/GZL-4545)\] Problem in the page /users/loginCAS/login.seam
* \[[GZL-4565](https://gazelle.ihe.net/jira/browse/GZL-4565)\] Multiple display issue on connectathon page
* \[[GZL-4578](https://gazelle.ihe.net/jira/browse/GZL-4578)\] Issue when accessing user from the search
* \[[GZL-4579](https://gazelle.ihe.net/jira/browse/GZL-4579)\] Error in this application if last_modifier_id of tf_actor is null

__Story__
* \[[GZL-4552](https://gazelle.ihe.net/jira/browse/GZL-4552)\] Update Gazelle TM with new version of Patient Generation

__Improvement__
* \[[GZL-4533](https://gazelle.ihe.net/jira/browse/GZL-4533)\] When application is TM and GMM, we should not display the GMM home page
* \[[GZL-4557](https://gazelle.ihe.net/jira/browse/GZL-4557)\] Rewrite system creation
* \[[GZL-4558](https://gazelle.ihe.net/jira/browse/GZL-4558)\] Rewrite system edition
* \[[GZL-4560](https://gazelle.ihe.net/jira/browse/GZL-4560)\] Rewrite system viewer
* \[[GZL-4572](https://gazelle.ihe.net/jira/browse/GZL-4572)\] Rewrite system list
* \[[GZL-4573](https://gazelle.ihe.net/jira/browse/GZL-4573)\] Rewrite system copy/import

# 5.5.7
_Release date: 2017-06-01_

__Bug__
* \[[GZL-4522](https://gazelle.ihe.net/jira/browse/GZL-4522)\] When editing a rule if you click on the button to view the rule a pop up appears, if you close that pop up you lose the control. Greyed screen

# 5.5.6
_Release date: 2017-05-31_

__Bug__
* \[[GZL-4495](https://gazelle.ihe.net/jira/browse/GZL-4495)\] List of tests instance in monitors kpi is not working
* \[[GZL-4520](https://gazelle.ihe.net/jira/browse/GZL-4520)\] GMM:  Cannot add new default config
* \[[GZL-4528](https://gazelle.ihe.net/jira/browse/GZL-4528)\] Gazelle TM configured as PR cannot access PDF file using https when SNI is needed

__Improvement__
* \[[GZL-4529](https://gazelle.ihe.net/jira/browse/GZL-4529)\] Improve error messages when sequence diagram generation fails.

# 5.5.5
_Release date: 2017-05-15_

__Bug__
* \[[GZL-4511](https://gazelle.ihe.net/jira/browse/GZL-4511)\] Warn on grading doesn't work
* \[[GZL-4516](https://gazelle.ihe.net/jira/browse/GZL-4516)\] When editing a system the list of system types shows all types. It does not use the visibility tag

__Improvement__
* \[[GZL-4524](https://gazelle.ihe.net/jira/browse/GZL-4524)\] Allow Testing Session Manager to Override Same company testing in a testing session

# 5.5.4
_Release date: 2017-03-31_

__Bug__
* \[[GZL-4492](https://gazelle.ihe.net/jira/browse/GZL-4492)\] When the name of the sample type is too long, it is trancated and we don't have anymore the button plus to upload new samples

# 5.5.3
_Release date: 2017-03-20_

__Bug__
* \[[GZL-4416](https://gazelle.ihe.net/jira/browse/GZL-4416)\] Make sure access to partner statistics will never be blocked
* \[[GZL-4436](https://gazelle.ihe.net/jira/browse/GZL-4436)\] Status Change Messages in Gazelle are in the wrong order
* \[[GZL-4447](https://gazelle.ihe.net/jira/browse/GZL-4447)\] When you edit a user, the checkbox are not aligned with the associated roles
* \[[GZL-4449](https://gazelle.ihe.net/jira/browse/GZL-4449)\] As vendor, we can't access to some systems configurations
* \[[GZL-4450](https://gazelle.ihe.net/jira/browse/GZL-4450)\] Imported monitors in Participant page do not have the correct default attributes set
* \[[GZL-4453](https://gazelle.ihe.net/jira/browse/GZL-4453)\] Allow user to start test instance with tools from same company
* \[[GZL-4454](https://gazelle.ihe.net/jira/browse/GZL-4454)\] system config auditMessages : unhandled rest response if system's keyword exists several times in DB
* \[[GZL-4455](https://gazelle.ihe.net/jira/browse/GZL-4455)\] in KPI for Tests popup on column M doesn't work
* \[[GZL-4459](https://gazelle.ihe.net/jira/browse/GZL-4459)\] TM-195:View readers' files: empty panel at the bottom of the page
* \[[GZL-4462](https://gazelle.ihe.net/jira/browse/GZL-4462)\] Sample Management as Monitor : comment sample button does not appear for a monitor
* \[[GZL-4464](https://gazelle.ihe.net/jira/browse/GZL-4464)\] System configuration: Add a tooltip on the padlock
* \[[GZL-4466](https://gazelle.ihe.net/jira/browse/GZL-4466)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-410:Edit configuration -  Exec
* \[[GZL-4467](https://gazelle.ihe.net/jira/browse/GZL-4467)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-672:Vendor_admin must be able
* \[[GZL-4470](https://gazelle.ihe.net/jira/browse/GZL-4470)\] Admin hosts management : admin should be able to create host for any company
* \[[GZL-4472](https://gazelle.ihe.net/jira/browse/GZL-4472)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-466:Download Excel file summarizing the list of ava
* \[[GZL-4473](https://gazelle.ihe.net/jira/browse/GZL-4473)\] Link to validation result bound to a proxy URL does not lead to a valid page
* \[[GZL-4474](https://gazelle.ihe.net/jira/browse/GZL-4474)\] As Admin, we can't access to systems configurations
* \[[GZL-4477](https://gazelle.ihe.net/jira/browse/GZL-4477)\] KPI tables : need title description
* \[[GZL-4479](https://gazelle.ihe.net/jira/browse/GZL-4479)\] WS to start channels, allow to open channels without responder IP

__Improvement__
* \[[GZL-3573](https://gazelle.ihe.net/jira/browse/GZL-3573)\] Need the list of test in a sessions that are not assigned to any monitor
* \[[GZL-3623](https://gazelle.ihe.net/jira/browse/GZL-3623)\] Filter initialization issue in KPI for tests page
* \[[GZL-4388](https://gazelle.ihe.net/jira/browse/GZL-4388)\] On assigning authorities page, display systems in session from the current testing session only
* \[[GZL-4412](https://gazelle.ihe.net/jira/browse/GZL-4412)\] Allow TM admin to customize the time between two runs of background tasks (for results, partners especially)
* \[[GZL-4418](https://gazelle.ihe.net/jira/browse/GZL-4418)\] We need a simple way to find tests with no monitors assigned
* \[[GZL-4448](https://gazelle.ihe.net/jira/browse/GZL-4448)\] Support GSS syslog view url  in test instance step
* \[[GZL-4482](https://gazelle.ihe.net/jira/browse/GZL-4482)\] Add a confirmation popup for approve/unapprove all configuration

# 5.5.2
_Release date: 2017-02-24_

__Bug__
* \[[GZL-4413](https://gazelle.ihe.net/jira/browse/GZL-4413)\] Issue when browsing sample tree for readers
* \[[GZL-4445](https://gazelle.ihe.net/jira/browse/GZL-4445)\] remove error in log

__Improvement__
* \[[GZL-4411](https://gazelle.ihe.net/jira/browse/GZL-4411)\] Document purpose of "Test Result Refresh Interval (s)" on application preferences page

# 5.5.1
_Release date: 2017-02-22_

__Bug__
* \[[GZL-4442](https://gazelle.ihe.net/jira/browse/GZL-4442)\] Can't access sample page

# 5.5.0
_Release date: 2017-02-22_

__Bug__
* \[[GZL-4175](https://gazelle.ihe.net/jira/browse/GZL-4175)\] NA2017: when you claim a test, the order of the tests change which is really disturbing
* \[[GZL-4306](https://gazelle.ihe.net/jira/browse/GZL-4306)\] Last update date in CAT results is never updated
* \[[GZL-4376](https://gazelle.ihe.net/jira/browse/GZL-4376)\] GMM:  Error creating Meta Test
* \[[GZL-4400](https://gazelle.ihe.net/jira/browse/GZL-4400)\] user w/ vendor_role gets "you don't have permission..." error when trying to view roles
* \[[GZL-4401](https://gazelle.ihe.net/jira/browse/GZL-4401)\] Request to sort configurations by port
* \[[GZL-4414](https://gazelle.ihe.net/jira/browse/GZL-4414)\] CAT Result : trouble with the warn on AIPO
* \[[GZL-4422](https://gazelle.ihe.net/jira/browse/GZL-4422)\] Sample validation results have troubles
* \[[GZL-4423](https://gazelle.ihe.net/jira/browse/GZL-4423)\] In Test Instance page we need to split long Actor name
* \[[GZL-4424](https://gazelle.ihe.net/jira/browse/GZL-4424)\] System configuration can be edited by vendor from another company
* \[[GZL-4428](https://gazelle.ihe.net/jira/browse/GZL-4428)\] Review application preference
* \[[GZL-4434](https://gazelle.ihe.net/jira/browse/GZL-4434)\] When creating a new test definition, Peer type is not saved
* \[[GZL-4437](https://gazelle.ihe.net/jira/browse/GZL-4437)\] Sometimes the firstUserRegistration link provided in email doesn't work

__Improvement__
* \[[GZL-3239](https://gazelle.ihe.net/jira/browse/GZL-3239)\] When a new AIPO is added by an admin, the value of testing type is not always set
* \[[GZL-4405](https://gazelle.ihe.net/jira/browse/GZL-4405)\] KPI for systems mixes pre-connectathon and connectathon tests
* \[[GZL-4427](https://gazelle.ihe.net/jira/browse/GZL-4427)\] Update monitor worklist

# 5.4.6
_Release date: 2017-01-16_


__Bug__

* \[[GZL-4381](https://gazelle.ihe.net/jira/browse/GZL-4381)\] In session dashboard, the filtering from the filter panel, or from the table is not working
* \[[GZL-4382](https://gazelle.ihe.net/jira/browse/GZL-4382)\] mysterious disappearances in NA2017 testing session
* \[[GZL-4383](https://gazelle.ihe.net/jira/browse/GZL-4383)\] 2 problems with assigning authority in Patient Gen & Sharing
* \[[GZL-4396](https://gazelle.ihe.net/jira/browse/GZL-4396)\] Test instance page is not working anymore

# 5.4.5
_Release date: 2016-12-30_


__Bug__

* \[[GZL-2357](https://gazelle.ihe.net/jira/browse/GZL-2357)\] Long description for profile option blows up GMM
* \[[GZL-4214](https://gazelle.ihe.net/jira/browse/GZL-4214)\] In GMM when I do a search on EVS_RAD-55 test, I get an error
* \[[GZL-4219](https://gazelle.ihe.net/jira/browse/GZL-4219)\] GMM : Cannot save modification in audit messages
* \[[GZL-4266](https://gazelle.ihe.net/jira/browse/GZL-4266)\] In the pages test instance, if you add a comment to a test step there is no cancel button
* \[[GZL-4276](https://gazelle.ihe.net/jira/browse/GZL-4276)\] No logical order to profile columns on system summary excel
* \[[GZL-4279](https://gazelle.ihe.net/jira/browse/GZL-4279)\] NA2017:  System under test should not be listed as a valid test partner
* \[[GZL-4280](https://gazelle.ihe.net/jira/browse/GZL-4280)\] If a system tests with all available partners, isn't that 100%?
* \[[GZL-4290](https://gazelle.ihe.net/jira/browse/GZL-4290)\] Add information about duplicate system in same test instance
* \[[GZL-4320](https://gazelle.ihe.net/jira/browse/GZL-4320)\] /Test Management/System Testing/7 - Pre-Connectathon Workflow/Test grading/TM-234:List pre-connectathon test instances -
* \[[GZL-4330](https://gazelle.ihe.net/jira/browse/GZL-4330)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-352:Display the samples worklist for a system. -  E
* \[[GZL-4332](https://gazelle.ihe.net/jira/browse/GZL-4332)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-194:Comment a sample -  Executed ON (ISO FORMAT): 2
* \[[GZL-4335](https://gazelle.ihe.net/jira/browse/GZL-4335)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-255:View participants' configurations 
* \[[GZL-4338](https://gazelle.ihe.net/jira/browse/GZL-4338)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-602:Users should be able to upload fil
* \[[GZL-4360](https://gazelle.ihe.net/jira/browse/GZL-4360)\] NA2017:  No logical order to presentation of configs, and can't sort by Details 1 
* \[[GZL-4362](https://gazelle.ihe.net/jira/browse/GZL-4362)\] In Manage tests requirements no select in filters
* \[[GZL-4366](https://gazelle.ihe.net/jira/browse/GZL-4366)\] GMM:  checking "is tested" box deleted all of the participants in my Role!
* \[[GZL-4369](https://gazelle.ihe.net/jira/browse/GZL-4369)\] ear deployement trouble
* \[[GZL-4392](https://gazelle.ihe.net/jira/browse/GZL-4392)\] New test does not appear correctly on Connectathon Results page
* \[[GZL-4394](https://gazelle.ihe.net/jira/browse/GZL-4394)\] Cannot 'uncheck' a test step (monitor).

__Story__

* \[[GZL-3726](https://gazelle.ihe.net/jira/browse/GZL-3726)\] Patient generation, trouble with pattern

__Improvement__

* \[[GZL-4220](https://gazelle.ihe.net/jira/browse/GZL-4220)\] GMM : Audit-messages, I would like to be able to filter by specification oid.
* \[[GZL-4273](https://gazelle.ihe.net/jira/browse/GZL-4273)\] too many arrangements of domain/profile/actor/option dropdown lists
* \[[GZL-4307](https://gazelle.ihe.net/jira/browse/GZL-4307)\] It could be useful to have an "activate user" button
* \[[GZL-4370](https://gazelle.ihe.net/jira/browse/GZL-4370)\] Export to PDF of connectathon test should not be proposed when test type is not connectathon
* \[[GZL-4372](https://gazelle.ihe.net/jira/browse/GZL-4372)\] Update password have not a pretty form

# 5.4.4
_Release date: 2016-11-30_


__Bug__

* \[[GZL-4358](https://gazelle.ihe.net/jira/browse/GZL-4358)\] Can't edit system summary

__Improvement__

* \[[GZL-4305](https://gazelle.ihe.net/jira/browse/GZL-4305)\] Connectathon results Automatic Grading

# 5.4.3
_Release date: 2016-11-28_


__Improvement__

* \[[GZL-4356](https://gazelle.ihe.net/jira/browse/GZL-4356)\] Add new soap method to get the list of TI in a testing session

# 5.4.2
_Release date: 2016-11-24_

__Bug__
* \[[GZL-3430](https://gazelle.ihe.net/jira/browse/GZL-3430)\] When you create a new testing session, and you enable the test to critical, an error occure
* \[[GZL-3920](https://gazelle.ihe.net/jira/browse/GZL-3920)\] filters selection are lost after a long time without using the page, but session still active
* \[[GZL-4039](https://gazelle.ihe.net/jira/browse/GZL-4039)\] Pre-CAT results : table goes out of the screen
* \[[GZL-4157](https://gazelle.ihe.net/jira/browse/GZL-4157)\] /administration/listMonitors.seam: some tests are displayed and should not
* \[[GZL-4176](https://gazelle.ihe.net/jira/browse/GZL-4176)\] Monitor worklist: monitor list in filter is not appropriate
* \[[GZL-4178](https://gazelle.ihe.net/jira/browse/GZL-4178)\] download ZIP of samples does not have extension
* \[[GZL-4181](https://gazelle.ihe.net/jira/browse/GZL-4181)\] Gazelle shows the system itself as a possible partner
* \[[GZL-4201](https://gazelle.ihe.net/jira/browse/GZL-4201)\] I would expect that a session "hidden from list" is not only hidden from list on the manage session page
* \[[GZL-4213](https://gazelle.ihe.net/jira/browse/GZL-4213)\] Only  testing session manager and admin roles should access to the complete list of CAT results
* \[[GZL-4265](https://gazelle.ihe.net/jira/browse/GZL-4265)\] In a gazelle test instance, after uploading a file, if you delete it, you can not add one anymore.
* \[[GZL-4267](https://gazelle.ihe.net/jira/browse/GZL-4267)\] Comment on pre-cat test are removed
* \[[GZL-4269](https://gazelle.ihe.net/jira/browse/GZL-4269)\] Action email can contain '//' in the URL !
* \[[GZL-4271](https://gazelle.ihe.net/jira/browse/GZL-4271)\] Static analysis
* \[[GZL-4274](https://gazelle.ihe.net/jira/browse/GZL-4274)\] field number_of_tests_to_realize in table tm_test does not exist
* \[[GZL-4275](https://gazelle.ihe.net/jira/browse/GZL-4275)\] Wrong total on financial summary page
* \[[GZL-4284](https://gazelle.ihe.net/jira/browse/GZL-4284)\] In the page /gazelle/administration/preferences.seam messages are displayed multiples times
* \[[GZL-4288](https://gazelle.ihe.net/jira/browse/GZL-4288)\]  "Add note" button on Connectathon Results page does not work
* \[[GZL-4293](https://gazelle.ihe.net/jira/browse/GZL-4293)\] present lists as they are actually sorted
* \[[GZL-4299](https://gazelle.ihe.net/jira/browse/GZL-4299)\] Unable to delete a system on Manage system page, but 2 delete buttons on Find Systems
* \[[GZL-4300](https://gazelle.ihe.net/jira/browse/GZL-4300)\] GMM: When you change a test status, GMM erases the short description
* \[[GZL-4306](https://gazelle.ihe.net/jira/browse/GZL-4306)\] Last update date in CAT results is never updated
* \[[GZL-4309](https://gazelle.ihe.net/jira/browse/GZL-4309)\] /Test Management/System Testing/2 - User Account Management/Any user/TM-278:Upload a picture in preferences -  Executed
* \[[GZL-4310](https://gazelle.ihe.net/jira/browse/GZL-4310)\] List contacts -  Missing strings
* \[[GZL-4311](https://gazelle.ihe.net/jira/browse/GZL-4311)\] The continue buttton on the switch session page is useless
* \[[GZL-4313](https://gazelle.ihe.net/jira/browse/GZL-4313)\] /Test Management/System Testing/5 - System Management/TM-428:Access conformance statements through URL -  Executed ON (I
* \[[GZL-4317](https://gazelle.ihe.net/jira/browse/GZL-4317)\] Addition of DICOM/HL7 integration statement requires to save twice
* \[[GZL-4318](https://gazelle.ihe.net/jira/browse/GZL-4318)\] /Test Management/System Testing/5 - System Management/As Vendor/TM-286:Vendor Generates Integration Statement. -  Execut
* \[[GZL-4321](https://gazelle.ihe.net/jira/browse/GZL-4321)\] Broken layout if no System comments
* \[[GZL-4322](https://gazelle.ihe.net/jira/browse/GZL-4322)\] From pre-cat page, link to system leads to an empty page
* \[[GZL-4326](https://gazelle.ihe.net/jira/browse/GZL-4326)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-283:Export configurations in a
* \[[GZL-4328](https://gazelle.ihe.net/jira/browse/GZL-4328)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-195:View readers' files -  Executed ON (ISO FORMAT)
* \[[GZL-4329](https://gazelle.ihe.net/jira/browse/GZL-4329)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-195:View readers' files -  Executed ON (ISO FORMAT)
* \[[GZL-4332](https://gazelle.ihe.net/jira/browse/GZL-4332)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-194:Comment a sample -  Executed ON (ISO FORMAT): 2
* \[[GZL-4333](https://gazelle.ihe.net/jira/browse/GZL-4333)\] /Test Management/System Testing/10 - Samples Management/As Monitor and Admin/TM-193:Validate sample -  Executed ON (ISO
* \[[GZL-4334](https://gazelle.ihe.net/jira/browse/GZL-4334)\] /Test Management/System Testing/5 - System Management/TM-428:Access conformance statements through URL -  Executed ON (I
* \[[GZL-4341](https://gazelle.ihe.net/jira/browse/GZL-4341)\] When you are a monitor and you move a test from verified to "to be verified" without claiming the test, the chat logs in not in the good order
* \[[GZL-4343](https://gazelle.ihe.net/jira/browse/GZL-4343)\] /Test Management/System Testing/13 - Connectathon workflow/As monitor/TM-676:Monitor must be asked to justify the reason
* \[[GZL-4344](https://gazelle.ihe.net/jira/browse/GZL-4344)\] In some case, it's not possible to save a system in session with an HL7 conformance statement attached
* \[[GZL-4345](https://gazelle.ihe.net/jira/browse/GZL-4345)\] When editing an invoice, the number of system invoiced or participant invoiced is not updated.
* \[[GZL-4346](https://gazelle.ihe.net/jira/browse/GZL-4346)\] When you validate all test steps using the button below the table, the progress bar is not updated
* \[[GZL-4348](https://gazelle.ihe.net/jira/browse/GZL-4348)\] Test definition link on main connectathon page is broken
* \[[GZL-4350](https://gazelle.ihe.net/jira/browse/GZL-4350)\] Users with testing session manager role can edit sessions that they do not manage

__Story__
* \[[GZL-4353](https://gazelle.ihe.net/jira/browse/GZL-4353)\] Review fixed issues to be closed

__Improvement__
* \[[GZL-3164](https://gazelle.ihe.net/jira/browse/GZL-3164)\] request to see partners when hovering over test instance number
* \[[GZL-4113](https://gazelle.ihe.net/jira/browse/GZL-4113)\] When you save a preset, all the page is rendered and you loose the current filter you have done
* \[[GZL-4278](https://gazelle.ihe.net/jira/browse/GZL-4278)\] NA2017: When a monitor or user clicks a check on a test step, you cannot uncheck it
* \[[GZL-4282](https://gazelle.ihe.net/jira/browse/GZL-4282)\] 3 mouse movements needed to open Main Connecthathon page
* \[[GZL-4283](https://gazelle.ihe.net/jira/browse/GZL-4283)\] removed default value of "None" for option dropdown on Connectathon Results page
* \[[GZL-4287](https://gazelle.ihe.net/jira/browse/GZL-4287)\] On Connectathon Results export, replace database value with GUI value and add Organization column

# 5.4.1
_Release date: 2016-10-03_


__Bug__

* \[[GZL-3530](https://gazelle.ihe.net/jira/browse/GZL-3530)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-244:List sharing logs -  Executed ON (ISO FORMAT): 2015-01-06 11:47:59
* \[[GZL-4004](https://gazelle.ihe.net/jira/browse/GZL-4004)\] /testing/test/cat.xhtml: java.lang.NullPointerException
* \[[GZL-4005](https://gazelle.ihe.net/jira/browse/GZL-4005)\] /configuration/configurationList.xhtml, rendered="#{!configurationsOverview.editRights}":
* \[[GZL-4044](https://gazelle.ihe.net/jira/browse/GZL-4044)\] The same test can be added several times to the monitor assignement
* \[[GZL-4078](https://gazelle.ihe.net/jira/browse/GZL-4078)\] /Test Management/System Testing/17 - Connectathon execution follow-up/TM-445:Admin shall be able to access information a
* \[[GZL-4081](https://gazelle.ihe.net/jira/browse/GZL-4081)\] We have a white line between the different participants list when you select participants for a new test
* \[[GZL-4085](https://gazelle.ihe.net/jira/browse/GZL-4085)\] errors in the log of CAT_SERVER
* \[[GZL-4141](https://gazelle.ihe.net/jira/browse/GZL-4141)\] Errors in Quartz logs
* \[[GZL-4174](https://gazelle.ihe.net/jira/browse/GZL-4174)\] As monitor you cannot claim a test when the name of the option is long
* \[[GZL-4185](https://gazelle.ihe.net/jira/browse/GZL-4185)\] List of partners does not match completed test instances
* \[[GZL-4191](https://gazelle.ihe.net/jira/browse/GZL-4191)\] The user message number is not updated after refreshing message
* \[[GZL-4198](https://gazelle.ihe.net/jira/browse/GZL-4198)\] List of tests assigned to a monitor is emptied if you add twice the same test
* \[[GZL-4256](https://gazelle.ihe.net/jira/browse/GZL-4256)\] gazelle-na reports error when vendors update default configs
* \[[GZL-4257](https://gazelle.ihe.net/jira/browse/GZL-4257)\] NA Gazelle, weird error when you update a system
* \[[GZL-4259](https://gazelle.ihe.net/jira/browse/GZL-4259)\] Default SOP class is not shown in edit mode for DICOM configs
* \[[GZL-4260](https://gazelle.ihe.net/jira/browse/GZL-4260)\] dropped system should not appear on dropdown list of systems on Configuration page

__Improvement__

* \[[GZL-3696](https://gazelle.ihe.net/jira/browse/GZL-3696)\] NA2016: Make global assigning authorities visible in all testing sessions
* \[[GZL-3840](https://gazelle.ihe.net/jira/browse/GZL-3840)\] TM-187:add AIPO to a simulator -  Executed ON (ISO FORMAT): 2015-11-24 16:14:03
* \[[GZL-4035](https://gazelle.ihe.net/jira/browse/GZL-4035)\] When user download the host file, send the file as attachment instead of inline
* \[[GZL-4156](https://gazelle.ihe.net/jira/browse/GZL-4156)\] /administration/listMonitors.seam
* \[[GZL-4221](https://gazelle.ihe.net/jira/browse/GZL-4221)\] confusing sequence when adding a new test system 

# 5.4.0
_Release date: 2016-09-20_


__Bug__

* \[[GZL-4031](https://gazelle.ihe.net/jira/browse/GZL-4031)\] Gazelle is not able to read the list of assertions received from AssertionManager
* \[[GZL-4133](https://gazelle.ihe.net/jira/browse/GZL-4133)\] We are not able anymore to sort or to filter in System Management page for the admin
* \[[GZL-4208](https://gazelle.ihe.net/jira/browse/GZL-4208)\] GMM:  Cannot leave ports blank when adding default config for DICOM SCU
* \[[GZL-4224](https://gazelle.ihe.net/jira/browse/GZL-4224)\] Cannot add profiles to testing session
* \[[GZL-4231](https://gazelle.ihe.net/jira/browse/GZL-4231)\] GZL Registration shows all profile/actors, not just those chosen for a session
* \[[GZL-4243](https://gazelle.ihe.net/jira/browse/GZL-4243)\] wrong session administrator shown on system registration page
* \[[GZL-4246](https://gazelle.ihe.net/jira/browse/GZL-4246)\] Cannot generate default configs for Eye Care test session
* \[[GZL-4249](https://gazelle.ihe.net/jira/browse/GZL-4249)\] Trigger event "Actor-Start-Stop" should be renamed into Application Activity
* \[[GZL-4254](https://gazelle.ihe.net/jira/browse/GZL-4254)\] Can't acces list of systems
* \[[GZL-4255](https://gazelle.ihe.net/jira/browse/GZL-4255)\] gazelle-model : Sonar analysis
* \[[GZL-4258](https://gazelle.ihe.net/jira/browse/GZL-4258)\] Export of Connectathon tests to PDF does not contain text in the Test Descr section
* \[[GZL-4262](https://gazelle.ihe.net/jira/browse/GZL-4262)\] Green triangle to launch EVSClient from test instance does not work on gazelle-na

__Improvement__

* \[[GZL-4241](https://gazelle.ihe.net/jira/browse/GZL-4241)\] add a button in administration to clean filecache
* \[[GZL-4244](https://gazelle.ihe.net/jira/browse/GZL-4244)\] display relevant AIPO in partners window on main connectathon page.
* \[[GZL-4245](https://gazelle.ihe.net/jira/browse/GZL-4245)\] Profile/actor tab - create logical order for dropdown lists

# 5.3.1
_Release date: 2016-09-05_


__Bug__

* \[[GZL-3723](https://gazelle.ihe.net/jira/browse/GZL-3723)\] In KPI for tests, when user click on Timeline or Graph, a blank page is displayed
* \[[GZL-4189](https://gazelle.ihe.net/jira/browse/GZL-4189)\] test instance notification messages status are reversed
* \[[GZL-4208](https://gazelle.ihe.net/jira/browse/GZL-4208)\] GMM:  Cannot leave ports blank when adding default config for DICOM SCU
* \[[GZL-4222](https://gazelle.ihe.net/jira/browse/GZL-4222)\] When switching session, the Profiles offered by registration is from the previously selected session
* \[[GZL-4223](https://gazelle.ihe.net/jira/browse/GZL-4223)\] Adding profiles/actors -- sort entries for profiles / actors / options 
* \[[GZL-4228](https://gazelle.ihe.net/jira/browse/GZL-4228)\] problem with system registration
* \[[GZL-4229](https://gazelle.ihe.net/jira/browse/GZL-4229)\] Missing IntegrationProfileOption Web service
* \[[GZL-4230](https://gazelle.ihe.net/jira/browse/GZL-4230)\] gazelle-na:  kpiMonitor spreadsheet is empty
* \[[GZL-4235](https://gazelle.ihe.net/jira/browse/GZL-4235)\] NA2017: Contract extension is not PDF
* \[[GZL-4238](https://gazelle.ihe.net/jira/browse/GZL-4238)\] GMM: Cannot cancel from actor definition unless you enter keyword and name
* \[[GZL-4242](https://gazelle.ihe.net/jira/browse/GZL-4242)\] actor keyword length check when typing

__Story__

* \[[GZL-2252](https://gazelle.ihe.net/jira/browse/GZL-2252)\] When I create a simulator the system's keyword contains HIMSS !
* \[[GZL-4209](https://gazelle.ihe.net/jira/browse/GZL-4209)\] Optimize PR

__Improvement__

* \[[GZL-4227](https://gazelle.ihe.net/jira/browse/GZL-4227)\] GMM: Update the default template for Test Description 

# 5.3.0
_Release date: 2016-05-20_


__Bug__

* \[[GZL-2083](https://gazelle.ihe.net/jira/browse/GZL-2083)\] Tests Definition's PDF does not output correctly if there are no English Test Description
* \[[GZL-2176](https://gazelle.ihe.net/jira/browse/GZL-2176)\] Difference of the saving control
* \[[GZL-2271](https://gazelle.ihe.net/jira/browse/GZL-2271)\] Column filter doesn't execute in case of paste via right-click menu.
* \[[GZL-2425](https://gazelle.ihe.net/jira/browse/GZL-2425)\] problem on the TVA calculated if there are Fees discount
* \[[GZL-2632](https://gazelle.ihe.net/jira/browse/GZL-2632)\] If a large amount of data is in the database, response is very slow
* \[[GZL-2667](https://gazelle.ihe.net/jira/browse/GZL-2667)\] Cannot enter double-byte character to Rich-Text area with Google Chrome
* \[[GZL-2803](https://gazelle.ihe.net/jira/browse/GZL-2803)\] Cannot add a contact to participant lists if his/her address is already used by another participant
* \[[GZL-2821](https://gazelle.ihe.net/jira/browse/GZL-2821)\] Missing entry in GMM
* \[[GZL-2862](https://gazelle.ihe.net/jira/browse/GZL-2862)\] Ability for user to select 'Supportive' is not initially shown for AIPO enabled as supportive in GMM
* \[[GZL-3031](https://gazelle.ihe.net/jira/browse/GZL-3031)\] There are a duplication of the testParticipant for the RoleInTest, I guess that a DISTINCT was missing in some JPQL requests
* \[[GZL-3048](https://gazelle.ihe.net/jira/browse/GZL-3048)\] When you create a new actor, and a new profile. If you add the actor to the profile from the profile edition page, the actor is linked to the profile, but we can not see it in the table
* \[[GZL-3056](https://gazelle.ihe.net/jira/browse/GZL-3056)\] the testParticipant are deleted, I do not how !! I am trying to work even with this, but is is realy blocker as issue.
* \[[GZL-3141](https://gazelle.ihe.net/jira/browse/GZL-3141)\] Redirect to error page while adding a comment to file uploaded to a test step
* \[[GZL-3277](https://gazelle.ihe.net/jira/browse/GZL-3277)\] The Management of Role in gazelle still does not work !
* \[[GZL-3319](https://gazelle.ihe.net/jira/browse/GZL-3319)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-263:Check message validation status -  Executed ON (ISO FORMAT): 2014-07-22 15:02:50
* \[[GZL-3328](https://gazelle.ihe.net/jira/browse/GZL-3328)\] /Test Management/System Testing/8 - Monitor Management/As Admin/TM-351:Print Monitor tests list -  Executed ON (ISO FORMAT): 2014-07-22 13:58:12
* \[[GZL-3423](https://gazelle.ihe.net/jira/browse/GZL-3423)\] Error loading tooltips when no testing session is selected
* \[[GZL-3459](https://gazelle.ihe.net/jira/browse/GZL-3459)\] System Acceptance: Missing a system
* \[[GZL-3463](https://gazelle.ihe.net/jira/browse/GZL-3463)\] Adding a second profile/actor pair to a role removes first
* \[[GZL-3467](https://gazelle.ihe.net/jira/browse/GZL-3467)\] Cannot add a second AIPO to a test role
* \[[GZL-3483](https://gazelle.ihe.net/jira/browse/GZL-3483)\] In GMM, when you flip a test status, the color coding is lost until manual refresh
* \[[GZL-3489](https://gazelle.ihe.net/jira/browse/GZL-3489)\] Clean this warning: No component found to process as 'ajaxSingle' for clientId j_id599:systemDecorate:SystemListBox
* \[[GZL-3552](https://gazelle.ihe.net/jira/browse/GZL-3552)\] Some generated patients are missing addresses
* \[[GZL-3566](https://gazelle.ihe.net/jira/browse/GZL-3566)\] In GMM, creating roles throws random exceptions
* \[[GZL-3570](https://gazelle.ihe.net/jira/browse/GZL-3570)\] GMM: Cannot edit test description if that description was previously empty
* \[[GZL-3604](https://gazelle.ihe.net/jira/browse/GZL-3604)\] Test status filter seems not to be saved in the URL
* \[[GZL-3609](https://gazelle.ihe.net/jira/browse/GZL-3609)\] "Edit" button missing from Profile/Actor tabs unless you request "View"
* \[[GZL-3631](https://gazelle.ihe.net/jira/browse/GZL-3631)\] TM crashes when generating an ATNA questionnaire for IASI's system
* \[[GZL-3640](https://gazelle.ihe.net/jira/browse/GZL-3640)\] link to a deleted issue still appears
* \[[GZL-3671](https://gazelle.ihe.net/jira/browse/GZL-3671)\] On Monitor worklist page, as an admin, the filter on monitors shows inactive monitors
* \[[GZL-3677](https://gazelle.ihe.net/jira/browse/GZL-3677)\] all users roles assigned to users are deleted
* \[[GZL-3697](https://gazelle.ihe.net/jira/browse/GZL-3697)\] GMM:  Adding Participant to a Role removes a different Participant instead
* \[[GZL-3701](https://gazelle.ihe.net/jira/browse/GZL-3701)\] GMM:  Browse transaction export to excel includes only current page
* \[[GZL-3717](https://gazelle.ihe.net/jira/browse/GZL-3717)\] EYE2015: Cannot select Responder for EYECARE-20 in test step
* \[[GZL-3936](https://gazelle.ihe.net/jira/browse/GZL-3936)\] The find system page is slow
* \[[GZL-3999](https://gazelle.ihe.net/jira/browse/GZL-3999)\] jasper reports number_of_tests_to_realize doen't exists in tm_test table
* \[[GZL-4000](https://gazelle.ihe.net/jira/browse/GZL-4000)\] jasper repports orchestrated doesn't exist in tm_test table
* \[[GZL-4030](https://gazelle.ihe.net/jira/browse/GZL-4030)\] Fix stateful beans
* \[[GZL-4054](https://gazelle.ihe.net/jira/browse/GZL-4054)\] When a file is not present in a sample, a bug is shown in the logs
* \[[GZL-4121](https://gazelle.ihe.net/jira/browse/GZL-4121)\] The description of the test step in the edition of the test step is small, it should be bigger
* \[[GZL-4132](https://gazelle.ihe.net/jira/browse/GZL-4132)\] In List Institution, when we filter by last modifier, we have always an empty lastmodifier (in both assending and descending order)
* \[[GZL-4173](https://gazelle.ihe.net/jira/browse/GZL-4173)\] Sample page: Screen captures obscure validation controls
* \[[GZL-4183](https://gazelle.ihe.net/jira/browse/GZL-4183)\] duplicating a configuration copies the proxy port
* \[[GZL-4195](https://gazelle.ihe.net/jira/browse/GZL-4195)\] When selected testing session is disabled the selected testing session is null
* \[[GZL-4199](https://gazelle.ihe.net/jira/browse/GZL-4199)\] Unable to add an AIPO after removing it
* \[[GZL-4200](https://gazelle.ihe.net/jira/browse/GZL-4200)\] If a TLS Test is failed, the test step status displays a red and a blue circle.
* \[[GZL-4203](https://gazelle.ihe.net/jira/browse/GZL-4203)\] Admin should not be able to deactivate his/her current testing session
* \[[GZL-4204](https://gazelle.ihe.net/jira/browse/GZL-4204)\] User Preferences always in edition mode

__Story__

* \[[GZL-4205](https://gazelle.ihe.net/jira/browse/GZL-4205)\] Migrate PR to jboss7
* \[[GZL-4206](https://gazelle.ihe.net/jira/browse/GZL-4206)\] Release Product Registry Jboss7

__Improvement__

* \[[GZL-4202](https://gazelle.ihe.net/jira/browse/GZL-4202)\] Find system page: add a filter on "is a tool" attribute

# 5.2.1
_Release date: 2016-04-27_


__Bug__

* \[[GZL-4151](https://gazelle.ihe.net/jira/browse/GZL-4151)\] The Gazelle/EU 2016 registration is confused
* \[[GZL-4194](https://gazelle.ihe.net/jira/browse/GZL-4194)\] Can't connect to GMM

__Story__

* \[[GZL-4197](https://gazelle.ihe.net/jira/browse/GZL-4197)\] Release TM 5.2.1 & deploy

# 5.2.0
_Release date: 2016-04-22_


__Bug__

* \[[GZL-3656](https://gazelle.ihe.net/jira/browse/GZL-3656)\] Testing session loses its integration profiles
* \[[GZL-4169](https://gazelle.ihe.net/jira/browse/GZL-4169)\] In logReturnForMesaTest.xhtml, an error message is displayed
* \[[GZL-4172](https://gazelle.ihe.net/jira/browse/GZL-4172)\] <dt>, <dd> and <dl> are removed from test descriptions. Should be recognized as valid HTML code
* \[[GZL-4182](https://gazelle.ihe.net/jira/browse/GZL-4182)\] test description modification is not taken into acount when starting a test instance
* \[[GZL-4186](https://gazelle.ihe.net/jira/browse/GZL-4186)\] Test Description Did Not Get Carried Forward from GMM during Bochum
* \[[GZL-4188](https://gazelle.ihe.net/jira/browse/GZL-4188)\] On TestInstance, it should be good to have test instance number as page title.

__Story__

* \[[GZL-4193](https://gazelle.ihe.net/jira/browse/GZL-4193)\] Release 5.2.0

# 5.1.0
_Release date: 2016-04-05_


__Bug__

* \[[GZL-3733](https://gazelle.ihe.net/jira/browse/GZL-3733)\] Cannot sort the table "KPI for tests"
* \[[GZL-3734](https://gazelle.ihe.net/jira/browse/GZL-3734)\] Cannot sort the table "KPI for systems"
* \[[GZL-3842](https://gazelle.ihe.net/jira/browse/GZL-3842)\] Sort : TM-447:TF consistency -  Executed ON (ISO FORMAT): 2015-11-24 13:57:50
* \[[GZL-3884](https://gazelle.ihe.net/jira/browse/GZL-3884)\] Cannot sort the table "KPI for monitors"
* \[[GZL-3925](https://gazelle.ihe.net/jira/browse/GZL-3925)\] The default filter cookie is not working
* \[[GZL-3942](https://gazelle.ihe.net/jira/browse/GZL-3942)\] Use a caching mechanism
* \[[GZL-3962](https://gazelle.ihe.net/jira/browse/GZL-3962)\] Test definition is not always updated
* \[[GZL-4009](https://gazelle.ihe.net/jira/browse/GZL-4009)\] Unable to find component with ID statusFromList in view.
* \[[GZL-4013](https://gazelle.ihe.net/jira/browse/GZL-4013)\]  Error sending mail /email/toAdminOnUserRegistering.xhtml
* \[[GZL-4014](https://gazelle.ihe.net/jira/browse/GZL-4014)\] cannot acces /testing/test/mesa/logReturnForMesaTest.xhtml
* \[[GZL-4015](https://gazelle.ihe.net/jira/browse/GZL-4015)\] Changing the IP address in the configuration page blocks any additional action.
* \[[GZL-4017](https://gazelle.ihe.net/jira/browse/GZL-4017)\] When pre CAT test status is changed a message is sent to the owner with the wrong link
* \[[GZL-4018](https://gazelle.ihe.net/jira/browse/GZL-4018)\] In pre CAT test result, system name is not displayed if no file have been uploaded
* \[[GZL-4020](https://gazelle.ihe.net/jira/browse/GZL-4020)\] Errors in log
* \[[GZL-4022](https://gazelle.ihe.net/jira/browse/GZL-4022)\] cannot reach parse response testAssertion/aipo/0
* \[[GZL-4023](https://gazelle.ihe.net/jira/browse/GZL-4023)\] Missing serializable implementation
* \[[GZL-4025](https://gazelle.ihe.net/jira/browse/GZL-4025)\] Sonar blocker issue
* \[[GZL-4027](https://gazelle.ihe.net/jira/browse/GZL-4027)\] cannot export multiple reports when more than 100 tests are selected
* \[[GZL-4028](https://gazelle.ihe.net/jira/browse/GZL-4028)\] empty page if direct link
* \[[GZL-4029](https://gazelle.ihe.net/jira/browse/GZL-4029)\] cannot access to usage page due to contract fail
* \[[GZL-4032](https://gazelle.ihe.net/jira/browse/GZL-4032)\] TM-404 : Exception when saving the networkg configuration with IP and ports fields not filled in
* \[[GZL-4037](https://gazelle.ihe.net/jira/browse/GZL-4037)\] Sort in list assigned test is not working
* \[[GZL-4040](https://gazelle.ihe.net/jira/browse/GZL-4040)\] Admin : Manage monitor : add monitors organization filter should be a suggest filter
* \[[GZL-4041](https://gazelle.ihe.net/jira/browse/GZL-4041)\] Error in the logs related to configurations management in Gazelle
* \[[GZL-4042](https://gazelle.ihe.net/jira/browse/GZL-4042)\] Vendor_admin should be able to generate configs from COMPANY_KEY: Systems configurations page
* \[[GZL-4043](https://gazelle.ihe.net/jira/browse/GZL-4043)\] Admin : Manage monitor : precise it is for the current testing session
* \[[GZL-4047](https://gazelle.ihe.net/jira/browse/GZL-4047)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-352:Display the samples worklist for a system. -  E
* \[[GZL-4048](https://gazelle.ihe.net/jira/browse/GZL-4048)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-195:View readers' files -  Executed ON (ISO FORMAT)
* \[[GZL-4049](https://gazelle.ihe.net/jira/browse/GZL-4049)\] /Test Management/System Testing/10 - Samples Management/Any users/TM-354:View producers' files -  Executed ON (ISO FORMA
* \[[GZL-4050](https://gazelle.ihe.net/jira/browse/GZL-4050)\] We don't have the same feature between sample.seam and system_object.seam
* \[[GZL-4053](https://gazelle.ihe.net/jira/browse/GZL-4053)\] /Test Management/System Testing/9 - Configuration Management/As Gazelle Admin/TM-636:OID requirements - Deletion  Execut
* \[[GZL-4055](https://gazelle.ihe.net/jira/browse/GZL-4055)\] /Test Management/System Testing/10 - Samples Management/As Vendor and Vendor Admin/TM-177:Delete sample -  Executed ON (
* \[[GZL-4058](https://gazelle.ihe.net/jira/browse/GZL-4058)\] as vendor when you click on secure checkbox you loose the page
* \[[GZL-4059](https://gazelle.ihe.net/jira/browse/GZL-4059)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-283:Export configurations in a
* \[[GZL-4060](https://gazelle.ihe.net/jira/browse/GZL-4060)\] The lock in the page configuration is not clickable
* \[[GZL-4061](https://gazelle.ihe.net/jira/browse/GZL-4061)\] /Test Management/System Testing/9 - Configuration Management/As Gazelle Admin/TM-646:OID roots - Edition  Executed ON (I
* \[[GZL-4063](https://gazelle.ihe.net/jira/browse/GZL-4063)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-143:Delete one configuration -
* \[[GZL-4064](https://gazelle.ihe.net/jira/browse/GZL-4064)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-256:Add a comment to step -  Executed 
* \[[GZL-4065](https://gazelle.ihe.net/jira/browse/GZL-4065)\] System sort does not work in CAT page
* \[[GZL-4066](https://gazelle.ihe.net/jira/browse/GZL-4066)\] Test instance page, mark all steps as done, doesn't update each steps
* \[[GZL-4070](https://gazelle.ihe.net/jira/browse/GZL-4070)\] If you click twice on the list of system in the page of TestInstance, the second one does not work
* \[[GZL-4071](https://gazelle.ihe.net/jira/browse/GZL-4071)\] Edit a patient : empty selects
* \[[GZL-4075](https://gazelle.ihe.net/jira/browse/GZL-4075)\] Tree display seems to be broken
* \[[GZL-4076](https://gazelle.ihe.net/jira/browse/GZL-4076)\] Problem of display when deleting an assigning autrhority for a system
* \[[GZL-4077](https://gazelle.ihe.net/jira/browse/GZL-4077)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-248:Admin generates patient according to system i
* \[[GZL-4079](https://gazelle.ihe.net/jira/browse/GZL-4079)\] Bug in the logs
* \[[GZL-4083](https://gazelle.ihe.net/jira/browse/GZL-4083)\] The comment popupPanel should be larger
* \[[GZL-4084](https://gazelle.ihe.net/jira/browse/GZL-4084)\] The button upload a file related to a test instance does not work 
* \[[GZL-4086](https://gazelle.ihe.net/jira/browse/GZL-4086)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-255:View participants' configurations 
* \[[GZL-4087](https://gazelle.ihe.net/jira/browse/GZL-4087)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-255:View participants' configurations 
* \[[GZL-4088](https://gazelle.ihe.net/jira/browse/GZL-4088)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-255:View participants' configurations 
* \[[GZL-4089](https://gazelle.ihe.net/jira/browse/GZL-4089)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-255:View participants' configurations 
* \[[GZL-4090](https://gazelle.ihe.net/jira/browse/GZL-4090)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-266:Write in chat window -  Executed O
* \[[GZL-4093](https://gazelle.ihe.net/jira/browse/GZL-4093)\] The list of system with which I have already tested is not sortable (when you start a new test instance)
* \[[GZL-4094](https://gazelle.ihe.net/jira/browse/GZL-4094)\] If you click on the fileUpload of the step, you upload a doc, then you click on the fileupload of the testindtance and you upload a doc, it does not work
* \[[GZL-4096](https://gazelle.ihe.net/jira/browse/GZL-4096)\] global test steps status change doesn't update each test steps
* \[[GZL-4097](https://gazelle.ihe.net/jira/browse/GZL-4097)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-264:Edit step comment -  Executed ON (
* \[[GZL-4098](https://gazelle.ihe.net/jira/browse/GZL-4098)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-608:Users must be able to validate/rev
* \[[GZL-4099](https://gazelle.ihe.net/jira/browse/GZL-4099)\] cannot report issue from a test or a test instance
* \[[GZL-4100](https://gazelle.ihe.net/jira/browse/GZL-4100)\] /Test Management/System Testing/13 - Connectathon workflow/As test participant/TM-412:Add a comment on the connectathon 
* \[[GZL-4101](https://gazelle.ihe.net/jira/browse/GZL-4101)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-247:Vendor admin links systems to an authority. -
* \[[GZL-4102](https://gazelle.ihe.net/jira/browse/GZL-4102)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-247:Vendor admin links systems to an authority. -
* \[[GZL-4103](https://gazelle.ihe.net/jira/browse/GZL-4103)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-238:Generate a new patient. -  Executed ON (ISO F
* \[[GZL-4104](https://gazelle.ihe.net/jira/browse/GZL-4104)\] When I ask for a patient with an empty address, the tool try anyway to render an address
* \[[GZL-4105](https://gazelle.ihe.net/jira/browse/GZL-4105)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-472:User must be able to view patient details. - 
* \[[GZL-4107](https://gazelle.ihe.net/jira/browse/GZL-4107)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-239:List existing patients. -  Executed ON (ISO F
* \[[GZL-4108](https://gazelle.ihe.net/jira/browse/GZL-4108)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-242:Share one patient -  Executed ON (ISO FORMAT)
* \[[GZL-4109](https://gazelle.ihe.net/jira/browse/GZL-4109)\] /Test Management/System Testing/11 - Patient Generation and Sharing/TM-242:Share one patient -  Executed ON (ISO FORMAT)
* \[[GZL-4111](https://gazelle.ihe.net/jira/browse/GZL-4111)\] /Test Management/System Testing/14 - Connectathon Grading & Results/Grading/TM-392:Administator lists the AIPO to be gra
* \[[GZL-4112](https://gazelle.ihe.net/jira/browse/GZL-4112)\] /Test Management/System Testing/14 - Connectathon Grading & Results/Grading/TM-395:Refresh filters -  Executed ON (ISO F
* \[[GZL-4118](https://gazelle.ihe.net/jira/browse/GZL-4118)\] uncaught exception, passing to exception handler: org.jboss.seam.security.NotLoggedInException
* \[[GZL-4120](https://gazelle.ihe.net/jira/browse/GZL-4120)\] Unable to find monitor
* \[[GZL-4123](https://gazelle.ihe.net/jira/browse/GZL-4123)\] Bug on duplicating a test step
* \[[GZL-4127](https://gazelle.ihe.net/jira/browse/GZL-4127)\] The Section of document is not clickable in the Integration profile description
* \[[GZL-4128](https://gazelle.ihe.net/jira/browse/GZL-4128)\] errors in log : ca.uhn.hl7v2.HL7Exception: Cannot open connection
* \[[GZL-4129](https://gazelle.ihe.net/jira/browse/GZL-4129)\] NPE when sharing patient with a system without Assigning Authority
* \[[GZL-4136](https://gazelle.ihe.net/jira/browse/GZL-4136)\] If you edit a user preference and then you click on cancel, you go back to your preferences edition and not the list of users
* \[[GZL-4143](https://gazelle.ihe.net/jira/browse/GZL-4143)\] No ack code displayed in log when trying to share patient with the wrong message option
* \[[GZL-4146](https://gazelle.ihe.net/jira/browse/GZL-4146)\] test instance page display error when doing action after few minutes
* \[[GZL-4148](https://gazelle.ihe.net/jira/browse/GZL-4148)\] Error when user upload a sample
* \[[GZL-4152](https://gazelle.ihe.net/jira/browse/GZL-4152)\] system config inbounds/outbounds : unhandled response if system's keyword exists several times in DB
* \[[GZL-4154](https://gazelle.ihe.net/jira/browse/GZL-4154)\] broken pipe error on random asset files
* \[[GZL-4158](https://gazelle.ihe.net/jira/browse/GZL-4158)\] use the appropriate version of postgresql driver
* \[[GZL-4159](https://gazelle.ihe.net/jira/browse/GZL-4159)\] When user try to share patient outside the local network it's not working
* \[[GZL-4161](https://gazelle.ihe.net/jira/browse/GZL-4161)\] unrecognized cas ticket throws exception
* \[[GZL-4162](https://gazelle.ihe.net/jira/browse/GZL-4162)\] listTFConfigurationsForSystemInSession : unhandled response if system's keyword exists several times in DB
* \[[GZL-4164](https://gazelle.ihe.net/jira/browse/GZL-4164)\] In HL7v2 messages, country should be sent in PID-11.6 (currently is PID-12)
* \[[GZL-4165](https://gazelle.ihe.net/jira/browse/GZL-4165)\] In listAllHostsForAdmin "assign IP addresses" reassing all IPs
* \[[GZL-4171](https://gazelle.ihe.net/jira/browse/GZL-4171)\] support regarding XDR-I configurations

__Story__

* \[[GZL-4001](https://gazelle.ihe.net/jira/browse/GZL-4001)\] Update PatientGeneration module
* \[[GZL-4166](https://gazelle.ihe.net/jira/browse/GZL-4166)\] Release TM 5.1.0

__Improvement__

* \[[GZL-4034](https://gazelle.ihe.net/jira/browse/GZL-4034)\] Use suggest filters for list of pre-connectathon tests
* \[[GZL-4131](https://gazelle.ihe.net/jira/browse/GZL-4131)\] improve logged informations with username and path
* \[[GZL-4134](https://gazelle.ihe.net/jira/browse/GZL-4134)\] create a preference to enable or disable ping
* \[[GZL-4144](https://gazelle.ihe.net/jira/browse/GZL-4144)\] Limit ping to active tabs

# 5.0.5
_Release date: 2016-03-03_


__Bug__

* \[[GZL-3656](https://gazelle.ihe.net/jira/browse/GZL-3656)\] Testing session loses its integration profiles
* \[[GZL-3710](https://gazelle.ihe.net/jira/browse/GZL-3710)\] NA2016:  Demonstration dates -- off by one error
* \[[GZL-3989](https://gazelle.ihe.net/jira/browse/GZL-3989)\] List Systems Spreadsheet, please remove hyperlinks in column B
* \[[GZL-3990](https://gazelle.ihe.net/jira/browse/GZL-3990)\] In the registration overview spreadsheet, please remove hyperlinks
* \[[GZL-3992](https://gazelle.ihe.net/jira/browse/GZL-3992)\] Cannot deploy GMM due to PUProvider
* \[[GZL-3994](https://gazelle.ihe.net/jira/browse/GZL-3994)\] Registration Overview NPE if no system present
* \[[GZL-3995](https://gazelle.ihe.net/jira/browse/GZL-3995)\] As first user (admin) after installation, no testing session is selected
* \[[GZL-3998](https://gazelle.ihe.net/jira/browse/GZL-3998)\] import test fail due to duplicated document section
* \[[GZL-4006](https://gazelle.ihe.net/jira/browse/GZL-4006)\] The HL7V3/Webservice Endpoint is not shown anymore in the table view
* \[[GZL-4062](https://gazelle.ihe.net/jira/browse/GZL-4062)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-616:Vendor_admin must be able 

__Story__

* \[[GZL-4012](https://gazelle.ihe.net/jira/browse/GZL-4012)\] Release TM 5.0.5

# 5.0.4
_Release date: 2016-02-16_


__Bug__

* \[[GZL-3737](https://gazelle.ihe.net/jira/browse/GZL-3737)\] Review "Gazelle Configuration using TF" page
* \[[GZL-3923](https://gazelle.ihe.net/jira/browse/GZL-3923)\] Bug in the edition of test definition
* \[[GZL-3959](https://gazelle.ihe.net/jira/browse/GZL-3959)\] In financial summary add input for organization selection
* \[[GZL-3962](https://gazelle.ihe.net/jira/browse/GZL-3962)\] Test definition is not always updated
* \[[GZL-3964](https://gazelle.ihe.net/jira/browse/GZL-3964)\] Button style is missing
* \[[GZL-3965](https://gazelle.ihe.net/jira/browse/GZL-3965)\] In edit view, test steps are not sorted
* \[[GZL-3967](https://gazelle.ihe.net/jira/browse/GZL-3967)\] It would be convenient to have the test cases ordered alphabetically
* \[[GZL-3968](https://gazelle.ihe.net/jira/browse/GZL-3968)\] In test steps, use TLS is no more taken into account
* \[[GZL-3970](https://gazelle.ihe.net/jira/browse/GZL-3970)\] cannot view list sample page in TM only mode
* \[[GZL-3972](https://gazelle.ihe.net/jira/browse/GZL-3972)\] Cannot add the monitors to the list of participants
* \[[GZL-3976](https://gazelle.ihe.net/jira/browse/GZL-3976)\] In pre-CAT result, popup are not well populated and hyperlink can't be clicked
* \[[GZL-3980](https://gazelle.ihe.net/jira/browse/GZL-3980)\] Add Actor/Integration Profile pairs for a Transaction crashes
* \[[GZL-3981](https://gazelle.ihe.net/jira/browse/GZL-3981)\] Wrong encoding on pages pages using h:commandButton to save
* \[[GZL-3983](https://gazelle.ihe.net/jira/browse/GZL-3983)\] remove ActorIntegrationProfile for an actor failed
* \[[GZL-3985](https://gazelle.ihe.net/jira/browse/GZL-3985)\] Add a panel around test description in edit test role
* \[[GZL-3986](https://gazelle.ihe.net/jira/browse/GZL-3986)\] In the tool tip next to a test role, we miss the information about test tool
* \[[GZL-3987](https://gazelle.ihe.net/jira/browse/GZL-3987)\] In edit test role input number spinner are not blocked for mousewheel

__Story__

* \[[GZL-3979](https://gazelle.ihe.net/jira/browse/GZL-3979)\] Update gazelle-version
* \[[GZL-3993](https://gazelle.ihe.net/jira/browse/GZL-3993)\] Release TM 5.0.4

# 5.0.3
_Release date: 2016-02-05_


__Bug__

* \[[GZL-3718](https://gazelle.ihe.net/jira/browse/GZL-3718)\] Edit standard is not perfect
* \[[GZL-3732](https://gazelle.ihe.net/jira/browse/GZL-3732)\] Cannot export the list of hosts as a CSV file
* \[[GZL-3740](https://gazelle.ihe.net/jira/browse/GZL-3740)\] Admin is not able to print the list of tests for a monitor
* \[[GZL-3774](https://gazelle.ihe.net/jira/browse/GZL-3774)\] Missing translation
* \[[GZL-3882](https://gazelle.ihe.net/jira/browse/GZL-3882)\] Usages sort is not working for admin
* \[[GZL-3885](https://gazelle.ihe.net/jira/browse/GZL-3885)\] "Page active" check in /EU-CAT/usage.seam is not working 
* \[[GZL-3902](https://gazelle.ihe.net/jira/browse/GZL-3902)\] GMM:  Adding Participant to a Role removes a different Participant instead: Jboss7
* \[[GZL-3934](https://gazelle.ihe.net/jira/browse/GZL-3934)\] Use infinispan
* \[[GZL-3938](https://gazelle.ihe.net/jira/browse/GZL-3938)\] Link to institution goes to the wrong institution
* \[[GZL-3939](https://gazelle.ihe.net/jira/browse/GZL-3939)\] In the page https://gazelle.ihe.net/EU-CAT/usage.seam sort of the table does not work
* \[[GZL-3940](https://gazelle.ihe.net/jira/browse/GZL-3940)\] remove email address from 'Find a monitor'
* \[[GZL-3943](https://gazelle.ihe.net/jira/browse/GZL-3943)\] In application preference, remove last / of tls_url if present
* \[[GZL-3945](https://gazelle.ihe.net/jira/browse/GZL-3945)\] The label to delete an invoice is incorrect
* \[[GZL-3946](https://gazelle.ihe.net/jira/browse/GZL-3946)\] On the page Systems in Session I am missing the sorting of the table per the column "Registration Status"
* \[[GZL-3954](https://gazelle.ihe.net/jira/browse/GZL-3954)\] Page Testing session participant : filtering and sorting does not work at all
* \[[GZL-3960](https://gazelle.ihe.net/jira/browse/GZL-3960)\] Style : In Test instances status checkboxes are not align

__Story__

* \[[GZL-3969](https://gazelle.ihe.net/jira/browse/GZL-3969)\] Release 5.0.3

__Improvement__

* \[[GZL-3744](https://gazelle.ihe.net/jira/browse/GZL-3744)\] ConnectathonParticipants: Add checkboxes for filtering on status (+ show number for each) as for test intances

# 5.0.2
_Release date: 2016-01-18_


__Bug__

* \[[GZL-3735](https://gazelle.ihe.net/jira/browse/GZL-3735)\] No tooltip displayed on Organisation creation/edition page
* \[[GZL-3736](https://gazelle.ihe.net/jira/browse/GZL-3736)\] In Firefox, the popup which shows the details about a type of sample cannot be closed
* \[[GZL-3738](https://gazelle.ihe.net/jira/browse/GZL-3738)\] On actor page, the various tables are not sortable
* \[[GZL-3773](https://gazelle.ihe.net/jira/browse/GZL-3773)\] Missing id on input field
* \[[GZL-3790](https://gazelle.ihe.net/jira/browse/GZL-3790)\] When you want to sort by last user connexion in gazelle, you are not able to see the last one
* \[[GZL-3795](https://gazelle.ihe.net/jira/browse/GZL-3795)\] When you update the list of table, the list proposed is not updated
* \[[GZL-3813](https://gazelle.ihe.net/jira/browse/GZL-3813)\] TM-264:Edit step comment performance trouble, the whole page is refreshed on button click
* \[[GZL-3832](https://gazelle.ihe.net/jira/browse/GZL-3832)\] Sort : TM-624:Admin must be able to list all hosts -  Executed ON (ISO FORMAT): 2015-11-24 15:02:19
* \[[GZL-3833](https://gazelle.ihe.net/jira/browse/GZL-3833)\] Sort : TM-140:List existing configurations -  Executed ON (ISO FORMAT): 2015-11-24 15:19:19
* \[[GZL-3843](https://gazelle.ihe.net/jira/browse/GZL-3843)\] Sort : TM-488:Actors - Read details -  Executed ON (ISO FORMAT): 2015-11-24 14:18:45
* \[[GZL-3847](https://gazelle.ihe.net/jira/browse/GZL-3847)\] Sort : TM-514:Domains - Read details -  Executed ON (ISO FORMAT): 2015-11-24 15:22:18
* \[[GZL-3856](https://gazelle.ihe.net/jira/browse/GZL-3856)\] Sort : TM-538:Integration profile options - Edition -  Executed ON (ISO FORMAT): 2015-11-24 16:19:21
* \[[GZL-3860](https://gazelle.ihe.net/jira/browse/GZL-3860)\] Sort : TM-560:Transactions - Read details -  Executed ON (ISO FORMAT): 2015-11-24 16:58:42
* \[[GZL-3862](https://gazelle.ihe.net/jira/browse/GZL-3862)\] Sort : TM-437:Test editor shall be able to list the object types [1] -  Executed ON (ISO FORMAT): 2015-11-24 14:39:44
* \[[GZL-3893](https://gazelle.ihe.net/jira/browse/GZL-3893)\] NA2016: Restriction on 'same company' partners in 3-partner test is too tight
* \[[GZL-3904](https://gazelle.ihe.net/jira/browse/GZL-3904)\] Update logo tips in Session edition
* \[[GZL-3913](https://gazelle.ihe.net/jira/browse/GZL-3913)\] When user register a new account an email is sent to admin instead of company admin
* \[[GZL-3921](https://gazelle.ihe.net/jira/browse/GZL-3921)\] XSS exploit possible when using <g:link...>
* \[[GZL-3928](https://gazelle.ihe.net/jira/browse/GZL-3928)\] Problems in the financial summary page. 
* \[[GZL-3929](https://gazelle.ihe.net/jira/browse/GZL-3929)\] Printing assigned tests from Find a monitor page fails --> zero-length file
* \[[GZL-3930](https://gazelle.ihe.net/jira/browse/GZL-3930)\] Trouble when user try to import system
* \[[GZL-3931](https://gazelle.ihe.net/jira/browse/GZL-3931)\] Style : Missing severity on faces messages
* \[[GZL-3932](https://gazelle.ihe.net/jira/browse/GZL-3932)\] When company did not select address --> error in this application
* \[[GZL-3933](https://gazelle.ihe.net/jira/browse/GZL-3933)\] Closed is always displayed in the menu when in GMM only
* \[[GZL-3951](https://gazelle.ihe.net/jira/browse/GZL-3951)\] Sometimes when I login, I just see my picture

__Story__

* \[[GZL-3914](https://gazelle.ihe.net/jira/browse/GZL-3914)\] Add the possibility to filter roleInTest by the flag "is played by a tool"

__Improvement__

* \[[GZL-3755](https://gazelle.ihe.net/jira/browse/GZL-3755)\] Can we filter by standard name from the table ? /EU-CAT/tf/standard/index.seam
* \[[GZL-3756](https://gazelle.ihe.net/jira/browse/GZL-3756)\] when you add an acotr/profile to a transaction, you cannot see the list of already existing profile actor from the edition page (like we do for edition of profiles in actor page for example)
* \[[GZL-3907](https://gazelle.ihe.net/jira/browse/GZL-3907)\] Various issues with the show/edit invoice pages
* \[[GZL-3917](https://gazelle.ihe.net/jira/browse/GZL-3917)\] fix test instance layout
* \[[GZL-3918](https://gazelle.ihe.net/jira/browse/GZL-3918)\] User is not always redirected to home page after login with username, password
* \[[GZL-3922](https://gazelle.ihe.net/jira/browse/GZL-3922)\] In the page to switch session, if admin, I'd like to get a button to edit a session
* \[[GZL-3927](https://gazelle.ihe.net/jira/browse/GZL-3927)\] I use the search feature to edit a user. I want to be able to edit the user from the outcome of the search

# 5.0.1
_Release date: 2016-01-04_


__Bug__

* \[[GZL-3743](https://gazelle.ihe.net/jira/browse/GZL-3743)\] In meta test page, rows used to be colored based on the test status, it is no more the case
* \[[GZL-3757](https://gazelle.ihe.net/jira/browse/GZL-3757)\] In find system, hyperlink on organization should not be clickable
* \[[GZL-3758](https://gazelle.ihe.net/jira/browse/GZL-3758)\] Style : When user remove his profile picture the page is not renderd
* \[[GZL-3759](https://gazelle.ihe.net/jira/browse/GZL-3759)\] Style : When user upload a too large profile picture, the display goes out of the form.
* \[[GZL-3760](https://gazelle.ihe.net/jira/browse/GZL-3760)\] Style : Retrieve lost password popup
* \[[GZL-3763](https://gazelle.ihe.net/jira/browse/GZL-3763)\] Style : As admin in list institution, the "Add an organization" button must be aligned on the left and colored in green
* \[[GZL-3764](https://gazelle.ihe.net/jira/browse/GZL-3764)\] Style : As vendor, in Systems Management, Registration status and Creator columns headers need to be align left
* \[[GZL-3765](https://gazelle.ihe.net/jira/browse/GZL-3765)\] Style : When vendor remove an AIPO the popup header is not valid
* \[[GZL-3766](https://gazelle.ihe.net/jira/browse/GZL-3766)\] Style : As admin in Manage demonstrations, the "Add an demonstration" button must be aligned on the left and colored in green
* \[[GZL-3769](https://gazelle.ihe.net/jira/browse/GZL-3769)\] For Admin, in List of invoices, no reset filter button
* \[[GZL-3770](https://gazelle.ihe.net/jira/browse/GZL-3770)\] When admin,display details of an invoice the title is wrong
* \[[GZL-3771](https://gazelle.ihe.net/jira/browse/GZL-3771)\] In TF>Actor edition, the button to Edit the "list of relevent profile" is red, instead of blue
* \[[GZL-3772](https://gazelle.ihe.net/jira/browse/GZL-3772)\] As admin, in Manage sample, the view popup need to be reviewed
* \[[GZL-3776](https://gazelle.ihe.net/jira/browse/GZL-3776)\] Style : Trouble in test edition
* \[[GZL-3777](https://gazelle.ihe.net/jira/browse/GZL-3777)\] Style : preset
* \[[GZL-3779](https://gazelle.ihe.net/jira/browse/GZL-3779)\] Style : review MetaTest creation
* \[[GZL-3780](https://gazelle.ihe.net/jira/browse/GZL-3780)\] Style : Share patient
* \[[GZL-3783](https://gazelle.ihe.net/jira/browse/GZL-3783)\] Style : Date edition
* \[[GZL-3785](https://gazelle.ihe.net/jira/browse/GZL-3785)\] Style : In list role in test, Show usage popup cannot be closed.
* \[[GZL-3786](https://gazelle.ihe.net/jira/browse/GZL-3786)\] Style : Tests Definition Checklist
* \[[GZL-3791](https://gazelle.ihe.net/jira/browse/GZL-3791)\] The deletion of network configuration does not ask before (no popup panel for validation !)
* \[[GZL-3792](https://gazelle.ihe.net/jira/browse/GZL-3792)\] There is no button reset for the list of IP
* \[[GZL-3794](https://gazelle.ihe.net/jira/browse/GZL-3794)\] When you download the list of system as an excel file, we are missing the name of the organization
* \[[GZL-3802](https://gazelle.ihe.net/jira/browse/GZL-3802)\] TM-609:Vendor/Vendor_admin must be able to generate system configurations for all systems -  Executed ON (ISO FORMAT): 2015-11-24 15:10:37
* \[[GZL-3806](https://gazelle.ihe.net/jira/browse/GZL-3806)\] TM-399:Close and reset details -  Executed ON (ISO FORMAT): 2015-11-24 16:26:25
* \[[GZL-3807](https://gazelle.ihe.net/jira/browse/GZL-3807)\] TM-524:Integration profiles - Access documentation -  Executed ON (ISO FORMAT): 2015-11-24 17:38:27
* \[[GZL-3808](https://gazelle.ihe.net/jira/browse/GZL-3808)\] Filter : TM-541:HL7 message profiles - Filter/Sort/Order -  Executed ON (ISO FORMAT): 2015-11-24 17:48:35
* \[[GZL-3809](https://gazelle.ihe.net/jira/browse/GZL-3809)\] Export : TM-366:Vendor downloads the list of registered systems. -  Executed ON (ISO FORMAT): 2015-11-24 14:12:01
* \[[GZL-3810](https://gazelle.ihe.net/jira/browse/GZL-3810)\] TM-469:Download pre-connectathon worklist
* \[[GZL-3811](https://gazelle.ihe.net/jira/browse/GZL-3811)\] Filter : TM-611:Configurations - List all and access details -  Executed ON (ISO FORMAT): 2015-11-24 14:27:42
* \[[GZL-3814](https://gazelle.ihe.net/jira/browse/GZL-3814)\] TM-279:Change picture in preferences -  Executed ON (ISO FORMAT): 2015-11-24 10:50:26
* \[[GZL-3815](https://gazelle.ihe.net/jira/browse/GZL-3815)\] TM-52:Retrieve lost password -  Executed ON (ISO FORMAT): 2015-11-24 18:01:36
* \[[GZL-3821](https://gazelle.ihe.net/jira/browse/GZL-3821)\] Sort : TM-223:List testing session -  Executed ON (ISO FORMAT): 2015-11-24 09:59:01
* \[[GZL-3822](https://gazelle.ihe.net/jira/browse/GZL-3822)\] Filter : TM-301:List the users registered within the application -  Executed ON (ISO FORMAT): 2015-11-24 10:32:36
* \[[GZL-3823](https://gazelle.ihe.net/jira/browse/GZL-3823)\] TM-297:Update user preferences -  Executed ON (ISO FORMAT): 2015-11-24 10:43:02
* \[[GZL-3825](https://gazelle.ihe.net/jira/browse/GZL-3825)\] TM-153:Admin deletes all profiles/actors -  Executed ON (ISO FORMAT): 2015-11-24 11:42:57
* \[[GZL-3841](https://gazelle.ihe.net/jira/browse/GZL-3841)\] Sort : TM-455:List available paths [1] -  Executed ON (ISO FORMAT): 2015-11-24 14:08:06
* \[[GZL-3844](https://gazelle.ihe.net/jira/browse/GZL-3844)\] TM-490:Actors - Edition -  Executed ON (ISO FORMAT): 2015-11-24 14:24:00
* \[[GZL-3845](https://gazelle.ihe.net/jira/browse/GZL-3845)\] TM-498:Audit messages - Creation -  Executed ON (ISO FORMAT): 2015-11-24 14:56:56
* \[[GZL-3846](https://gazelle.ihe.net/jira/browse/GZL-3846)\] Faces messages : TM-510:Documents - Deletion -  Executed ON (ISO FORMAT): 2015-11-24 15:13:27
* \[[GZL-3848](https://gazelle.ihe.net/jira/browse/GZL-3848)\] Faces messages : TM-517:Domains - Deletion -  Executed ON (ISO FORMAT): 2015-11-24 15:23:01
* \[[GZL-3849](https://gazelle.ihe.net/jira/browse/GZL-3849)\] TM-519:Integration profiles - List all -  Executed ON (ISO FORMAT): 2015-11-24 15:51:08
* \[[GZL-3850](https://gazelle.ihe.net/jira/browse/GZL-3850)\] TM-522:Integration profiles - Read details -  Executed ON (ISO FORMAT): 2015-11-24 15:46:13
* \[[GZL-3851](https://gazelle.ihe.net/jira/browse/GZL-3851)\] TM-526:Integration profiles - Duplicate -  Executed ON (ISO FORMAT): 2015-11-24 15:58:44
* \[[GZL-3852](https://gazelle.ihe.net/jira/browse/GZL-3852)\] TM-525:Integration profiles - Creation -  Executed ON (ISO FORMAT): 2015-11-24 15:53:26
* \[[GZL-3853](https://gazelle.ihe.net/jira/browse/GZL-3853)\] Faces messages : TM-527:Integration profiles - Edition -  Executed ON (ISO FORMAT): 2015-11-24 16:03:21
* \[[GZL-3854](https://gazelle.ihe.net/jira/browse/GZL-3854)\] Sort : TM-531:Integration profiles - Manage linked actors -  Executed ON (ISO FORMAT): 2015-11-24 16:05:19
* \[[GZL-3855](https://gazelle.ihe.net/jira/browse/GZL-3855)\] Integration profile options - Creation
* \[[GZL-3857](https://gazelle.ihe.net/jira/browse/GZL-3857)\] TM-546:HL7 message profiles - Deletion -  Executed ON (ISO FORMAT): 2015-11-24 16:39:18
* \[[GZL-3858](https://gazelle.ihe.net/jira/browse/GZL-3858)\] Filter : TM-554:Standards - List all -  Executed ON (ISO FORMAT): 2015-11-24 16:52:35
* \[[GZL-3859](https://gazelle.ihe.net/jira/browse/GZL-3859)\] Faces messages : TM-559:Transactions - List all -  Executed ON (ISO FORMAT): 2015-11-24 16:56:13
* \[[GZL-3879](https://gazelle.ihe.net/jira/browse/GZL-3879)\] Some images are not loaded
* \[[GZL-3880](https://gazelle.ihe.net/jira/browse/GZL-3880)\] Cannot login with user password when on GMM only mode
* \[[GZL-3881](https://gazelle.ihe.net/jira/browse/GZL-3881)\] In connectathon result, add background color on result cell
* \[[GZL-3883](https://gazelle.ihe.net/jira/browse/GZL-3883)\] Scroll to add participant panel in start test instance 
* \[[GZL-3886](https://gazelle.ihe.net/jira/browse/GZL-3886)\] Add actor doesn't handle used keyword
* \[[GZL-3887](https://gazelle.ihe.net/jira/browse/GZL-3887)\] Add domain doesn't handle already used keyword and name
* \[[GZL-3890](https://gazelle.ihe.net/jira/browse/GZL-3890)\] Integration profile graphic throws an error on server side if no transaction are defined
* \[[GZL-3891](https://gazelle.ihe.net/jira/browse/GZL-3891)\] TM-522:Integration profiles - actors sorting is not working in the profile edition/show
* \[[GZL-3895](https://gazelle.ihe.net/jira/browse/GZL-3895)\] Add close/open information in change session page
* \[[GZL-3896](https://gazelle.ihe.net/jira/browse/GZL-3896)\] system edition: Url is not checked when adding and hl7 or dicom integration statemant
* \[[GZL-3900](https://gazelle.ihe.net/jira/browse/GZL-3900)\] Disable mouse wheel on inputNumberSpinner

__Story__

* \[[GZL-3903](https://gazelle.ihe.net/jira/browse/GZL-3903)\] Release 5.0.1

__Improvement__

* \[[GZL-3768](https://gazelle.ihe.net/jira/browse/GZL-3768)\] Remake system editon style
* \[[GZL-3829](https://gazelle.ihe.net/jira/browse/GZL-3829)\] Icon : TM-235:Consult test logs and grade test. -  Executed ON (ISO FORMAT): 2015-11-24 15:31:52
* \[[GZL-3837](https://gazelle.ihe.net/jira/browse/GZL-3837)\] TM-392:Administator lists the AIPO to be graded in the testing session -  Executed ON (ISO FORMAT): 2015-11-24 16:28:10
* \[[GZL-3894](https://gazelle.ihe.net/jira/browse/GZL-3894)\] When creating meta tests, list of Tests is not alphabetical
* \[[GZL-3898](https://gazelle.ihe.net/jira/browse/GZL-3898)\] it takes around 60 seconds to access the test instances page

# 5.0.0
_Release date: 2015-12-15_


__Technical task__

* \[[GZL-3724](https://gazelle.ihe.net/jira/browse/GZL-3724)\] Update Event.observe
* \[[GZL-3725](https://gazelle.ihe.net/jira/browse/GZL-3725)\] Update filter layouts
* \[[GZL-3728](https://gazelle.ihe.net/jira/browse/GZL-3728)\] Place filter clear and refresh actions in the panel title
* \[[GZL-3729](https://gazelle.ihe.net/jira/browse/GZL-3729)\] fix update url when filtering
* \[[GZL-3731](https://gazelle.ihe.net/jira/browse/GZL-3731)\] update buttons style

__Bug__

* \[[GZL-3258](https://gazelle.ihe.net/jira/browse/GZL-3258)\] Bug on the generation of configurations
* \[[GZL-3536](https://gazelle.ihe.net/jira/browse/GZL-3536)\] When you modify your email address with an existing email address, an empty page is shown
* \[[GZL-3545](https://gazelle.ihe.net/jira/browse/GZL-3545)\] Many companies have duplicate webservice configuration entries
* \[[GZL-3630](https://gazelle.ihe.net/jira/browse/GZL-3630)\] Last modifier of a test is not always updated
* \[[GZL-3647](https://gazelle.ihe.net/jira/browse/GZL-3647)\] /Test Management/System Testing/9 - Configuration Management/TM-140:List existing configurations -  Executed ON (ISO FORMAT): 2015-04-14 11:30:42
* \[[GZL-3648](https://gazelle.ihe.net/jira/browse/GZL-3648)\] /Test Management/System Testing/10 - Samples Management/As Admin/TM-356:Admin modifies sample -  Executed ON (ISO FORMAT): 2015-04-14 11:58:09
* \[[GZL-3649](https://gazelle.ihe.net/jira/browse/GZL-3649)\] /Test Management/System Testing/10 - Samples Management/As Vendor and Vendor Admin/TM-353:Add a file to a sample as owner of a "reader" system. -  Executed ON (ISO FORMAT): 2015-04-14 12:07:34
* \[[GZL-3660](https://gazelle.ihe.net/jira/browse/GZL-3660)\] When you search for system configurations, then you edit one of them and save, you go back to the list of confs but you loose the filtering
* \[[GZL-3661](https://gazelle.ihe.net/jira/browse/GZL-3661)\] Username in login page is remembered as "undefined"
* \[[GZL-3663](https://gazelle.ihe.net/jira/browse/GZL-3663)\] Unable to paste permanent link to ATNA Questionnaire in tests steps.
* \[[GZL-3664](https://gazelle.ihe.net/jira/browse/GZL-3664)\] Add a refresh button on Admin > Configuration > Manage host's configurations
* \[[GZL-3665](https://gazelle.ihe.net/jira/browse/GZL-3665)\] delete é from hostnames
* \[[GZL-3666](https://gazelle.ihe.net/jira/browse/GZL-3666)\] KPI for monitors : list of monitors is not accurate
* \[[GZL-3669](https://gazelle.ihe.net/jira/browse/GZL-3669)\] Fields are missing on the patient generation page when TM is in French
* \[[GZL-3670](https://gazelle.ihe.net/jira/browse/GZL-3670)\] When importing a system to a testing session the isATool attribute is not copied
* \[[GZL-3673](https://gazelle.ihe.net/jira/browse/GZL-3673)\] URL in test step error 
* \[[GZL-3680](https://gazelle.ihe.net/jira/browse/GZL-3680)\] When a comment is added to a configuration nothing is displayed
* \[[GZL-3697](https://gazelle.ihe.net/jira/browse/GZL-3697)\] GMM:  Adding Participant to a Role removes a different Participant instead
* \[[GZL-3706](https://gazelle.ihe.net/jira/browse/GZL-3706)\] GMM:  add test step button does not work
* \[[GZL-3722](https://gazelle.ihe.net/jira/browse/GZL-3722)\] Error when sample creator upload a snapshot
* \[[GZL-3742](https://gazelle.ihe.net/jira/browse/GZL-3742)\] NA2016:  Assign monitors to test - list not alphabetical
* \[[GZL-3762](https://gazelle.ihe.net/jira/browse/GZL-3762)\] Style : When user add a system, a "f" is displayed under "Save" button
* \[[GZL-3767](https://gazelle.ihe.net/jira/browse/GZL-3767)\] Validate VAT number in edit institution is no more valid
* \[[GZL-3775](https://gazelle.ihe.net/jira/browse/GZL-3775)\] Filters trouble
* \[[GZL-3778](https://gazelle.ihe.net/jira/browse/GZL-3778)\] Style : TestInstance trouble
* \[[GZL-3781](https://gazelle.ihe.net/jira/browse/GZL-3781)\] Style : List of samples
* \[[GZL-3782](https://gazelle.ihe.net/jira/browse/GZL-3782)\] Style : In edit institution change email icon 
* \[[GZL-3784](https://gazelle.ihe.net/jira/browse/GZL-3784)\] Style : too long tooltip
* \[[GZL-3787](https://gazelle.ihe.net/jira/browse/GZL-3787)\] Style : change switch testing session button
* \[[GZL-3797](https://gazelle.ihe.net/jira/browse/GZL-3797)\] TM-30:Creation of Vendor Admin account and Institution -  Executed ON (ISO FORMAT): 2015-11-24 10:09:54
* \[[GZL-3798](https://gazelle.ihe.net/jira/browse/GZL-3798)\] TM-34:Create an account for a vendor user -  Executed ON (ISO FORMAT): 2015-11-24 10:29:54
* \[[GZL-3799](https://gazelle.ihe.net/jira/browse/GZL-3799)\] TM-169:Vendor admin creates/edits system after registration period. -  Executed ON (ISO FORMAT): 2015-11-24 14:16:15
* \[[GZL-3800](https://gazelle.ihe.net/jira/browse/GZL-3800)\] TM-420:Financial summary [1] -  Executed ON (ISO FORMAT): 2015-11-24 11:45:59
* \[[GZL-3801](https://gazelle.ihe.net/jira/browse/GZL-3801)\] TM-290:Generate PDF contract -  Executed ON (ISO FORMAT): 2015-11-24 11:54:36
* \[[GZL-3803](https://gazelle.ihe.net/jira/browse/GZL-3803)\] TM-613:User must be able to add/edit a new network configuration -  Executed ON (ISO FORMAT): 2015-11-24 15:28:04
* \[[GZL-3804](https://gazelle.ihe.net/jira/browse/GZL-3804)\] TM-254:Start a new test -  Executed ON (ISO FORMAT): 2015-11-24 16:43:56
* \[[GZL-3805](https://gazelle.ihe.net/jira/browse/GZL-3805)\] TM-258:Add a link to proxy in step comments
* \[[GZL-3812](https://gazelle.ihe.net/jira/browse/GZL-3812)\] Popup : TM-241:Delete patient. -  Executed ON (ISO FORMAT): 2015-11-24 15:08:17
* \[[GZL-3816](https://gazelle.ihe.net/jira/browse/GZL-3816)\] TM-235:Consult test logs and grade test.
* \[[GZL-3817](https://gazelle.ihe.net/jira/browse/GZL-3817)\] TM-280:Print list of assigned tests -  Executed ON (ISO FORMAT): 2015-11-25 10:41:29
* \[[GZL-3818](https://gazelle.ihe.net/jira/browse/GZL-3818)\] TM-194:Comment a sample -  Executed ON (ISO FORMAT): 2015-11-25 14:07:05
* \[[GZL-3819](https://gazelle.ihe.net/jira/browse/GZL-3819)\] TM-257:Access logs in proxy -  Executed ON (ISO FORMAT): 2015-11-25 17:20:45
* \[[GZL-3820](https://gazelle.ihe.net/jira/browse/GZL-3820)\] TM-260: Change test status
* \[[GZL-3824](https://gazelle.ihe.net/jira/browse/GZL-3824)\] TM-659:Institution information should be editable -  Executed ON (ISO FORMAT): 2015-11-24 11:25:10
* \[[GZL-3827](https://gazelle.ihe.net/jira/browse/GZL-3827)\] TM-275:Admin accepts systems to testing session -  Executed ON (ISO FORMAT): 2015-11-24 12:01:32
* \[[GZL-3828](https://gazelle.ihe.net/jira/browse/GZL-3828)\] Filter and tooltip : TM-234:List pre-connectathon test instances -  Executed ON (ISO FORMAT): 2015-11-24 15:24:16
* \[[GZL-3830](https://gazelle.ihe.net/jira/browse/GZL-3830)\] TM-351:Print Monitor tests list -  Executed ON (ISO FORMAT): 2015-11-24 15:51:24
* \[[GZL-3831](https://gazelle.ihe.net/jira/browse/GZL-3831)\] TM-282:Manage hosts configuration -  Executed ON (ISO FORMAT): 2015-11-24 14:44:56
* \[[GZL-3834](https://gazelle.ihe.net/jira/browse/GZL-3834)\] TM-668:Download host file -  Executed ON (ISO FORMAT): 2015-11-24 14:54:47
* \[[GZL-3835](https://gazelle.ihe.net/jira/browse/GZL-3835)\] Export : TM-629:Hosts - Export -  Executed ON (ISO FORMAT): 2015-11-24 15:03:55
* \[[GZL-3836](https://gazelle.ihe.net/jira/browse/GZL-3836)\] /Test Management/System Testing/10 - Samples Management/As Admin/TM-359:Admin adds a file to a sample as owner of a "reader" system. -  Executed ON (ISO FORMAT): 2015-11-25 17:01:40
* \[[GZL-3838](https://gazelle.ihe.net/jira/browse/GZL-3838)\] TM-397:Administrator put a comment -  Executed ON (ISO FORMAT): 2015-11-24 16:35:20
* \[[GZL-3839](https://gazelle.ihe.net/jira/browse/GZL-3839)\] TM-479:Register a participant [1] -  Executed ON (ISO FORMAT): 2015-11-24 16:04:58
* \[[GZL-3861](https://gazelle.ihe.net/jira/browse/GZL-3861)\] TM-562:Transactions - Creation -  Executed ON (ISO FORMAT): 2015-11-24 17:01:44
* \[[GZL-3863](https://gazelle.ihe.net/jira/browse/GZL-3863)\] TM-464:Test case edition [1] -  Executed ON (ISO FORMAT): 2015-11-24 15:41:12
* \[[GZL-3864](https://gazelle.ihe.net/jira/browse/GZL-3864)\] Search Role In Test "enter" key redirect to role creation page
* \[[GZL-3865](https://gazelle.ihe.net/jira/browse/GZL-3865)\] TM-450:Role in test management [1] -  Executed ON (ISO FORMAT): 2015-11-24 16:32:17
* \[[GZL-3868](https://gazelle.ihe.net/jira/browse/GZL-3868)\] The button delete of validation result on sample is not rendering the page
* \[[GZL-3869](https://gazelle.ihe.net/jira/browse/GZL-3869)\] Gazelle installation fails after clicking on: Start installation
* \[[GZL-3872](https://gazelle.ihe.net/jira/browse/GZL-3872)\] /Test Management/System Testing/3 - Institution Management/As Vendor admin/TM-337:List contacts , add contact
* \[[GZL-3873](https://gazelle.ihe.net/jira/browse/GZL-3873)\] TM-161:Vendor admin creates a system  Executed ON 
* \[[GZL-3874](https://gazelle.ihe.net/jira/browse/GZL-3874)\] /Test Management/System Testing/6 - Invoices management/TM-420:Financial summary [1]  Executed ON (ISO FORMAT): 2015-12-
* \[[GZL-3875](https://gazelle.ihe.net/jira/browse/GZL-3875)\] /Test Management/System Testing/6 - Invoices management/TM-290:Generate PDF contract  Executed ON (ISO FORMAT): 2015-12-
* \[[GZL-3876](https://gazelle.ihe.net/jira/browse/GZL-3876)\] /Test Management/System Testing/6 - Invoices management/TM-421:Financial summary [2]  Executed ON (ISO FORMAT): 2015-12-
* \[[GZL-3877](https://gazelle.ihe.net/jira/browse/GZL-3877)\] NA2016: Adding comment to Test Instance always fails the first time
* \[[GZL-3878](https://gazelle.ihe.net/jira/browse/GZL-3878)\] Update user preference language needs 2 click
* \[[GZL-3893](https://gazelle.ihe.net/jira/browse/GZL-3893)\] NA2016: Restriction on 'same company' partners in 3-partner test is too tight
* \[[GZL-3899](https://gazelle.ihe.net/jira/browse/GZL-3899)\] NA2016:  Peer-to-peer test w/ no available partners looks like a no-peer test
* \[[GZL-3909](https://gazelle.ihe.net/jira/browse/GZL-3909)\] Remove email address from 'Find a monitor'

__Epic__

* \[[GZL-3694](https://gazelle.ihe.net/jira/browse/GZL-3694)\] Migrate gazelle-tm-tools to JBoss 7
* \[[GZL-3695](https://gazelle.ihe.net/jira/browse/GZL-3695)\] Migrate gazelle-tm to Jboss 7

__Story__

* \[[GZL-3693](https://gazelle.ihe.net/jira/browse/GZL-3693)\] Gazelle Test Management

__Improvement__

* \[[GZL-3523](https://gazelle.ihe.net/jira/browse/GZL-3523)\] /Test Management/System Testing/9 - Configuration Management/As Vendor Admin or Vendor/TM-410:Edit configuration -  Executed ON (ISO FORMAT): 2015-01-06 10:49:37
* \[[GZL-3535](https://gazelle.ihe.net/jira/browse/GZL-3535)\] Main connectathon page: improve comments from vendor
* \[[GZL-3674](https://gazelle.ihe.net/jira/browse/GZL-3674)\] update database connectathon pool
* \[[GZL-3675](https://gazelle.ihe.net/jira/browse/GZL-3675)\] Catch error Caused by: java.lang.IllegalStateException: Invalid test instance at net.ihe.gazelle.tm.gazelletest.action.TestInstanceManager.getTestInstanceIdFromPage(TestInstanceManager.java:1093)
* \[[GZL-3676](https://gazelle.ihe.net/jira/browse/GZL-3676)\] MessageManager catch invalid number format exception in logs
* \[[GZL-3681](https://gazelle.ihe.net/jira/browse/GZL-3681)\] Add some improvements on All configurations page
* \[[GZL-3708](https://gazelle.ihe.net/jira/browse/GZL-3708)\] I think we can remove Connectathon -> Connectathon (old page)
* \[[GZL-3788](https://gazelle.ihe.net/jira/browse/GZL-3788)\] Style : Review all popups
* \[[GZL-3789](https://gazelle.ihe.net/jira/browse/GZL-3789)\] Let the testing session admin set the number of participants included in the system fees Jboss7
* \[[GZL-3796](https://gazelle.ihe.net/jira/browse/GZL-3796)\] Style: fix system registration
* \[[GZL-3866](https://gazelle.ihe.net/jira/browse/GZL-3866)\] global search trigger 
* \[[GZL-3867](https://gazelle.ihe.net/jira/browse/GZL-3867)\] On datascroller: hide select row nomber if only one page is displayed
