---
title: Release note
subtitle: PatientManager
toolversion: 10.0.3
releasedate: 2024-07-30
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-PATIENT_MANAGER
---

# 10.0.3
_Release date: 2024-07-30_

__Bug__
* \[[PAM-781](https://gazelle.ihe.net/jira/browse/PAM-781)\] SUT's Assigning Authorities menu in Patient Manager is inaccessible

__Improvement__
* \[[PAM-782](https://gazelle.ihe.net/jira/browse/PAM-782)\] PDQm PDS and PIXm Manager fhir server URL must be linked to different application preferences


# 10.0.2
_Release date: 2024-06-05_

__Bug__
* \[[PAM-780](https://gazelle.ihe.net/jira/browse/PAM-780)\] [PAM PES Simulator] Cannot cancel (A11)

# 10.0.1
_Release date: 2024-02-16_

__IT Help__
* \[[PAM-779](https://gazelle.ihe.net/jira/browse/PAM-779)\] Datascroller bugged in validationParametersList.xhtml

# 10.0.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[PAM-775](https://gazelle.ihe.net/jira/browse/PAM-775)\] Integrate new sso-client-v7
* \[[PAM-776](https://gazelle.ihe.net/jira/browse/PAM-776)\] Remove username display in GUI

# 9.16.7
_Release date: 2023-08-21_

__Bug__
* \[[PAM-773](https://gazelle.ihe.net/jira/browse/PAM-773)\] PAM automaton crash at startup

# 9.16.6
_Release date: 2023-04-14_

__Bug__
* \[[PAM-769](https://gazelle.ihe.net/jira/browse/PAM-769)\] Fix TOS popup

# 9.16.5
_Release date: 2023-03-08_

__Bug__
* \[[PAM-770](https://gazelle.ihe.net/jira/browse/PAM-770)\] XCPD Initiating Gateway simulator is displaying private SUT to everyone
* \[[PAM-772](https://gazelle.ihe.net/jira/browse/PAM-772)\] PDC and PEC Simulator throws an error when the patient doesn't have an Identity Reliability Code

# 9.16.4
_Release date: 2022-02-02_

__Bug__
* \[[PAM-769](https://gazelle.ihe.net/jira/browse/PAM-769)\] Fix TOS popup

# 9.16.3
_Release date: 2022-05-19_

__Bug__
* \[[PAM-768](https://gazelle.ihe.net/jira/browse/PAM-768)\] PAM XCPD Deferred Does not Emit Sequoia/SAML headers

# 9.16.2
_Release date: 2022-02-14_

__Bug__
* \[[PAM-764](https://gazelle.ihe.net/jira/browse/PAM-764)\] [PAM-FR/INS] M must not be used in XPN-7 in PAM-FR
* \[[PAM-766](https://gazelle.ihe.net/jira/browse/PAM-766)\] [INS] PAM PDC - non-compliant behavior upon receipt of an "INS deletion" event

# 9.16.1
_Release date: 2022-01-19_

__Story__
* \[[PAM-765](https://gazelle.ihe.net/jira/browse/PAM-765 )\] GDPR Refacto banner in standalone library

# 9.16.0
_Release date: 2021-12-10_

__Story__
* \[[PAM-742](https://gazelle.ihe.net/jira/browse/PAM-742)\] [Interop/INS] Patient Manager: new fields in ZFD segment

# 9.15.3
_Release date: 2021-10-21_

__Bug__
* \[[HLSEVENCMN-91](https://gazelle.ihe.net/jira/browse/HLSEVENCMN-91)\] We cannot override fault levels for the HL7v2 Validation

# 9.15.2
_Release date: 2021-10-01_

__Bug__
* \[[PAM-761](https://gazelle.ihe.net/jira/browse/PAM-761)\] [ANS/INS] Application error when sharing a patient with an empty address
* \[[PAM-762](https://gazelle.ihe.net/jira/browse/PAM-762)\] [ANS/INS] The BDL address with the insee code must be added in a new XAD iteration
* \[[PAM-763](https://gazelle.ihe.net/jira/browse/PAM-763)\] Cannot access PIX Source ITI-44

# 9.15.1
_Release date: 2021-09-08_


__Bug__
* \[[PAM-718](https://gazelle.ihe.net/jira/browse/PAM-718)\] PIXv3 X-Reference Manager doesn't handle query with a duplicated patient identifier

__Story__
* \[[PAM-758](https://gazelle.ihe.net/jira/browse/PAM-758)\] CH Change for the obligation for the MPI-PID in asOtherIDs
* \[[PAM-759](https://gazelle.ihe.net/jira/browse/PAM-759)\] CH PIXV3 ITI-44 parameter Patient.id has been modified

# 9.15.0
_Release date: 2021-08-19_

__Story__
* \[[PAM-740](https://gazelle.ihe.net/jira/browse/PAM-740)\] [PAMSimulator] Patient Demographic Consumer - Integrate french patient in the tools
* \[[PAM-748](https://gazelle.ihe.net/jira/browse/PAM-748)\] [PAMSimulator] Patient Encounter Consumer -  Integrate french patient in the tools
* \[[PAM-751](https://gazelle.ihe.net/jira/browse/PAM-751)\] [InteropSanté/INS] Patient Manager : receive "suppression INS"

__Task__
* \[[PAM-749](https://gazelle.ihe.net/jira/browse/PAM-749)\] [PAMSimulator] Patient Demographic Consumer - Reject message if ADT is wrong
* \[[PAM-750](https://gazelle.ihe.net/jira/browse/PAM-750)\] [PAMSimulator] Patient Encounter Consumer - Reject message if ADT is wrong

# 9.14.0
_Release date: 2021-07-30_

__Improvement__
* \[[PAM-754](https://gazelle.ihe.net/jira/browse/PAM-754)\] Allow sending PAM-FR compliant message from the Connectathon feature

__Bug__
* \[[PAM-755](https://gazelle.ihe.net/jira/browse/PAM-755)\] PES doesn't generate french patient correctly

# 9.13.0
_Release date: 2021-07-28_

__Story__
* \[[PAM-739](https://gazelle.ihe.net/jira/browse/PAM-739)\] [PAMSimulator] Patient Demographic Supplier - Create french patient
* \[[PAM-741](https://gazelle.ihe.net/jira/browse/PAM-741)\] [PAMSimulator] Send "suppression INS"
* \[[PAM-745](https://gazelle.ihe.net/jira/browse/PAM-745)\] [PAMSimulator] Patient Demographic Supplier - Send ADT messages
* \[[PAM-746](https://gazelle.ihe.net/jira/browse/PAM-746)\] [PAMSimulator] Patient Encounter Supplier - Create french patient
* \[[PAM-747](https://gazelle.ihe.net/jira/browse/PAM-747)\] [PAMSimulator] Patient Encounter Supplier - Send ADT messages

# 9.12.1
_Release date: 2020-09-22_

__Bug__
* \[[PAM-719](https://gazelle.ihe.net/jira/browse/PAM-719)\] PAMSimulator does not handle incomming ITI-44 when patient.id is duplicated
* \[[PAM-721](https://gazelle.ihe.net/jira/browse/PAM-721)\] STUDY-PatientManager as Responding Gateway returns more patients than requested

# 9.12.0
_Release date: 2020-07-27_

__Sub-task__
* \[[PAM-713](https://gazelle.ihe.net/jira/browse/PAM-713)\] record soap envelop
* \[[PAM-714](https://gazelle.ihe.net/jira/browse/PAM-714)\] add tab in GUI

__Story__
* \[[PAM-695](https://gazelle.ihe.net/jira/browse/PAM-695)\] [PDQm Connector] Create GUI for PDQm R4 Consumer simulator


# 9.11.8
_Release date: 2020-04-23_

__Bug__

* \[[PAM-641](https://gazelle.ihe.net/jira/browse/PAM-641)\] Review the way XCPD Responding Gateway handles patient identifiers


# 9.11.7
_Release date: 2020-03-19_

__Bug__

* \[[PAM-674](https://gazelle.ihe.net/jira/browse/PAM-674)\] PIXV3 Query does not return patient identifiers as expected in EPR context
* \[[PAM-676](https://gazelle.ihe.net/jira/browse/PAM-676)\] PIXV3 Query does not return expected id when quering with datasource parameter

# 9.11.6
_Release date: 2020-01-17_

__Improvement__

* \[[PAM-666](https://gazelle.ihe.net/jira/browse/PAM-666)\] Change type of pam_phone_number.type to string
* \[[PAM-667](https://gazelle.ihe.net/jira/browse/PAM-667)\] Add a UUID to uniquely identify a patient regardless of its database id or patient identifiers
* \[[PAM-670](https://gazelle.ihe.net/jira/browse/PAM-670)\] We need to store the keyword of the actor

# 9.11.5
_Release date: 2020-01-03_

__Bug__

* \[[PAM-650](https://gazelle.ihe.net/jira/browse/PAM-650)\] mothersMaidenName FHIR extension does not use the correct datatype
* \[[PAM-660](https://gazelle.ihe.net/jira/browse/PAM-660)\] A patient's date of birth is different depending on whether the patient is displayed or modified
* \[[PAM-662](https://gazelle.ihe.net/jira/browse/PAM-662)\] Cannot add a phone number to a patient

__Task__

* \[[PAM-663](https://gazelle.ihe.net/jira/browse/PAM-663)\] Qualifier BR must not be used by default in ITI-44 in the EPD Domain

__Improvement__

* \[[PAM-661](https://gazelle.ihe.net/jira/browse/PAM-661)\] queryMatchObservation must always return 100


# 9.11.4
_Release date: 2019-09-05_

__Bug__

* \[[PAM-569](https://gazelle.ihe.net/jira/browse/PAM-569)\]  \[eHeathSuisse\] The XCPD Responding Gateway return the EPR-SPID instead of the MPI-PID
* \[[PAM-640](https://gazelle.ihe.net/jira/browse/PAM-640)\] ITI-45 response returned by the tool does not conform to schema nor IHE specifications

# 9.11.3
_Release date: 2019-08-01_

__Bug__

* \[[PAM-635](https://gazelle.ihe.net/jira/browse/PAM-635)\] Identifiers are not filtered when sending some ADT messages
* \[[PAM-636](https://gazelle.ihe.net/jira/browse/PAM-636)\] Changes are not sent in A31 event


# 9.11.2
_Release date: 2019-05-23_

__Bug__

* \[[PAM-632](https://gazelle.ihe.net/jira/browse/PAM-632)\] Tool does not send PID for assigning authorities with namespaceID only


# 9.11.1
_Release date: 2019-05-15_

__Bug__

* \[[PAM-629](https://gazelle.ihe.net/jira/browse/PAM-629)\] Timeout when sending hl7v2 messages to Patient Manager
* \[[PAM-630](https://gazelle.ihe.net/jira/browse/PAM-630)\] When sending an ADT message, the list of assigning authorities supported by the tool is not taken into account
* \[[PAM-631](https://gazelle.ihe.net/jira/browse/PAM-631)\] Save button does not show up on connectathon > Patient creation page

## Gazelle XUA Actors 1.0.9

__Bug__

* \[[GZLXUA-12](https://gazelle.ihe.net/jira/browse/GZLXUA-12)\] Patient Manager X-Service Provider (XCPD Resp Gw) fails at handling the second request and followings


# 9.11.0
_Release date: 2019-05-09_

__Remarks__

This version of Patient Manager is aligned with

* version 2.9 of the French extension for the PAM profile;
* SWF.b's constraints for ADT messages (RAD-1 and RAD-12 transactions now emit HL7v2.5.1 messages)

For existing instances of the tool, the configuration of the Jboss 7.2 AS server shall be updated to declare the datasource in standalone.xml
configuration file, as for a new installation. Refers to the installation manual for details.

__Sub-task__

* \[[PAM-597](https://gazelle.ihe.net/jira/browse/PAM-597)\] Z80 to Z89 are not used anymore
* \[[PAM-600](https://gazelle.ihe.net/jira/browse/PAM-600)\] Align gazelle-hl7-messagestructures with changes in PAM FR 2.9
* \[[PAM-601](https://gazelle.ihe.net/jira/browse/PAM-601)\] Empty Z* segments shall not be sent
* \[[PAM-602](https://gazelle.ihe.net/jira/browse/PAM-602)\] Update field MSH-12.1.3 with the correct version of the extension
* \[[PAM-603](https://gazelle.ihe.net/jira/browse/PAM-603)\] A new field is defined for segment ZFV
* \[[PAM-604](https://gazelle.ihe.net/jira/browse/PAM-604)\] PEC shall integrate ZFV-11 (new field)
* \[[PAM-605](https://gazelle.ihe.net/jira/browse/PAM-605)\] PDS actor shall be able to send data in the ZFS segment (new)
* \[[PAM-606](https://gazelle.ihe.net/jira/browse/PAM-606)\] PDC actor shall be able to integrate the ZFS segment (new)

__Bug__

* \[[PAM-165](https://gazelle.ihe.net/jira/browse/PAM-165)\] Update values in XTN
* \[[PAM-544](https://gazelle.ihe.net/jira/browse/PAM-544)\] The button update security header is not working
* \[[PAM-576](https://gazelle.ihe.net/jira/browse/PAM-576)\] XCPD Responding Gateway fails at handling some queries
* \[[PAM-583](https://gazelle.ihe.net/jira/browse/PAM-583)\] PIX Manager does not handle ITI-30 messages
* \[[PAM-590](https://gazelle.ihe.net/jira/browse/PAM-590)\] Patient Manager -> Administration -> Manage Companies faults on delete
* \[[PAM-593](https://gazelle.ihe.net/jira/browse/PAM-593)\] Missing information when a connection is failing
* \[[PAM-607](https://gazelle.ihe.net/jira/browse/PAM-607)\] PDQ/PDS returns unfiltered results for ITI-22
* \[[PAM-608](https://gazelle.ihe.net/jira/browse/PAM-608)\] PES: A44 event - users are not able to select the incorrect patient
* \[[PAM-609](https://gazelle.ihe.net/jira/browse/PAM-609)\] PES - A08 - NPE when sending the message
* \[[PAM-611](https://gazelle.ihe.net/jira/browse/PAM-611)\] Duplicate key violation on patient identifier when create a patient with PAM/PDS
* \[[PAM-612](https://gazelle.ihe.net/jira/browse/PAM-612)\] PAM/PDC Does not interpret MGR segment correctly
* \[[PAM-613](https://gazelle.ihe.net/jira/browse/PAM-613)\] PAM/PES messages without BP6 validations fail
* \[[PAM-614](https://gazelle.ihe.net/jira/browse/PAM-614)\] PAM/PEC Saves patient twice
* \[[PAM-615](https://gazelle.ihe.net/jira/browse/PAM-615)\] XCPD/INIT_GATEWAY Cannot send message with XUA assertion
* \[[PAM-616](https://gazelle.ihe.net/jira/browse/PAM-616)\] PAM/PDS Cannot add/delete a second address to a patient
* \[[PAM-617](https://gazelle.ihe.net/jira/browse/PAM-617)\] PAM/PDC Validation on Ack on patient update with BP6 fails
* \[[PAM-624](https://gazelle.ihe.net/jira/browse/PAM-624)\] PAM/PES Generated patient not shown
* \[[PAM-625](https://gazelle.ihe.net/jira/browse/PAM-625)\] PAM/PEC Cannot save Additionnal info
* \[[PAM-626](https://gazelle.ihe.net/jira/browse/PAM-626)\] PAM/PDS Adding a legal care mode does not update the table but are sent
* \[[PAM-627](https://gazelle.ihe.net/jira/browse/PAM-627)\] Nature of movement with BP6 is missing when validating the request of Discharge patient
* \[[PAM-628](https://gazelle.ihe.net/jira/browse/PAM-628)\] Validation of PAM/PES both messages (Update A08) fails : missing fields

__Task__

* \[[PAM-610](https://gazelle.ihe.net/jira/browse/PAM-610)\] PES: Encounters are registered twice

__Improvement__

* \[[PAM-469](https://gazelle.ihe.net/jira/browse/PAM-469)\] Update ADT actor to support SWF.b
* \[[PAM-566](https://gazelle.ihe.net/jira/browse/PAM-566)\] ITI-31:  Update to ZBE segment definition via CP by IHE-France
* \[[PAM-585](https://gazelle.ihe.net/jira/browse/PAM-585)\] Extract datasource configuration
* \[[PAM-586](https://gazelle.ihe.net/jira/browse/PAM-586)\] Update SQL scripts archive
* \[[PAM-596](https://gazelle.ihe.net/jira/browse/PAM-596)\] Update Patient Manager to support PAM FR 2.9 (attached)

## Gazelle HL7 Common 5.4.12

__Bug__

* \[[HLSEVENCMN-89](https://gazelle.ihe.net/jira/browse/HLSEVENCMN-89)\] Replay of an HL7 message does not work anymore

## Simulator Parent 4.4.9

__Bug__

* \[[SIMUCMN-59](https://gazelle.ihe.net/jira/browse/SIMUCMN-59)\] Missing information when a connection is failling

# 9.10.10
_Release date: 2019-03-07_

__Bug__

* \[[PAM-595](https://gazelle.ihe.net/jira/browse/PAM-595)\] FHIR Client should not require the CapabilityStatement of the server

# 9.10.9
_Release date: 2018-12-11_

__Bug__

* \[[PAM-579](https://gazelle.ihe.net/jira/browse/PAM-579)\] PDQSupplier throws a SOAP fault when sender/device/id is missing
* \[[PAM-581](https://gazelle.ihe.net/jira/browse/PAM-581)\] Patient manager alters messages before storing them

# 9.10.8
_Release date: 2018-12-05_

__Bug__

* \[[PAM-577](https://gazelle.ihe.net/jira/browse/PAM-577)\] PIX Manager not working properly

__Improvement__

* \[[PAM-573](https://gazelle.ihe.net/jira/browse/PAM-573)\] Review permission to delete SUT configuration

# 9.10.7
_Release date: 2018-10-31_

__Bug__

* \[[PAM-572](https://gazelle.ihe.net/jira/browse/PAM-572)\] Access denied to PIX Source/ITI-30 when PAM is not enabled

# 9.10.6
_Release date: 2018-09-14_

__Bug__

* \[[PAM-570](https://gazelle.ihe.net/jira/browse/PAM-570)\] Update patient manager according to changes in FHIR validator

# 9.10.5
_Release date: 2018-06-22_

__Bug__

* \[[PAM-558](https://gazelle.ihe.net/jira/browse/PAM-558)\] When the tool fails at imported FHIR Patients, give tips to the user
* \[[PAM-560](https://gazelle.ihe.net/jira/browse/PAM-560)\] The administrator should be able to edit connectathon patients
* \[[PAM-561](https://gazelle.ihe.net/jira/browse/PAM-561)\] MothersMaidenName is not read from the FHIR resource
* \[[PAM-562](https://gazelle.ihe.net/jira/browse/PAM-562)\] Patient's birth place address cannot be filled in

# 9.10.4
_Release date: 2018-05-28_

__Bug__

* \[[PAM-553](https://gazelle.ihe.net/jira/browse/PAM-553)\] URL of page Connectathon, Patient Demograhics is wrong
* \[[PAM-555](https://gazelle.ihe.net/jira/browse/PAM-555)\] Wrong send application and facility in response of PIX Manager

__Story__

* \[[PAM-551](https://gazelle.ihe.net/jira/browse/PAM-551)\] \[AUTOMATON\] When the application is displayed in French a tab is missing in the encounter edition section

# 9.10.3
_Release date: 2018-03-28_

__Bug__

* \[[PAM-537](https://gazelle.ihe.net/jira/browse/PAM-537)\] The administrator should be able to update the AA index from the GUI
* \[[PAM-550](https://gazelle.ihe.net/jira/browse/PAM-550)\] h:outputLink outcome is executed at page rendering which causes unexpected behaviour

# 9.10.2
_Release date: 2018-03-28_

__Bug__

* \[[PAM-543](https://gazelle.ihe.net/jira/browse/PAM-543)\] We want to keep the indexes for the CAT assigning authorities at the same value
* \[[PAM-547](https://gazelle.ihe.net/jira/browse/PAM-547)\] Even though the "Worklist" feature is disabled, the "create worklist" button still appears
* \[[PAM-548](https://gazelle.ihe.net/jira/browse/PAM-548)\] Automaton does not work in a zulu-7 environment
* \[[PAM-549](https://gazelle.ihe.net/jira/browse/PAM-549)\] When user want to add preferences for a system which already has preferences, transaction failed message is displayed

# 9.10.1
_Release date: 2018-03-23_

__Bug__

* \[[PAM-545](https://gazelle.ihe.net/jira/browse/PAM-545)\] NPE is raised when generating a patient in the automate for IHE France

# 9.10.0
_Release date: 2018-03-09_

__Remarks__

This version of the Patient Manager tool uses the jdbc driver provided by the Jboss7 AS server.

__Bug__

* \[[PAM-535](https://gazelle.ihe.net/jira/browse/PAM-535)\] Update postgresql driver to version 42.2.1-jre7
* \[[PAM-540](https://gazelle.ihe.net/jira/browse/PAM-540)\] When you want to create a patient for connectathon, the connectathon AA are not available

__Story__

* \[[PAM-539](https://gazelle.ihe.net/jira/browse/PAM-539)\] [CH:XCPD] Update XCPD actors to match the new specifications

__Improvement__

* \[[PAM-534](https://gazelle.ihe.net/jira/browse/PAM-534)\] small fix to statusCode value for PIXv3 newly added patient

# 9.9.2
_Release date: 2018-01-22_

__Bug__

* \[[PAM-528](https://gazelle.ihe.net/jira/browse/PAM-528)\] PAMRestAPI does not send all attributes for the encounter
* \[[PAM-529](https://gazelle.ihe.net/jira/browse/PAM-529)\] Users should be able to create patients with IDs in the Connectathon's assigning authorities

# 9.9.1
_Release date: 2018-01-11_

__Remarks__

This new version of the tools is to be connected to the new Gazelle SSO (Apereo). When deploying the tool, make sure you have the configuration file available at /opt/gazelle/cas/file.properties, also update the cas_url preference of the tool.


__Technical task__

* \[[PAM-504](https://gazelle.ihe.net/jira/browse/PAM-504)\] New menu entry
* \[[PAM-505](https://gazelle.ihe.net/jira/browse/PAM-505)\] List connectathons patients
* \[[PAM-506](https://gazelle.ihe.net/jira/browse/PAM-506)\] Create a "connectathon" actor
* \[[PAM-507](https://gazelle.ihe.net/jira/browse/PAM-507)\] Update All patients list not to show the "connectathon" patient
* \[[PAM-508](https://gazelle.ihe.net/jira/browse/PAM-508)\] Sharing of Connectathon demographics
* \[[PAM-509](https://gazelle.ihe.net/jira/browse/PAM-509)\] PDQm/PDQ/PDQV3 supplier shall look into the list of connectathon demographics
* \[[PAM-510](https://gazelle.ihe.net/jira/browse/PAM-510)\] PIXm/PIX/PIXV3 manager shall look into the list of connectathon demographics
* \[[PAM-511](https://gazelle.ihe.net/jira/browse/PAM-511)\] Manage the links between the system and the assigning authorities
* \[[PAM-512](https://gazelle.ihe.net/jira/browse/PAM-512)\] Add a 'connectathon purpose' option to the assigning authority object
* \[[PAM-513](https://gazelle.ihe.net/jira/browse/PAM-513)\] Allow the administrator to create patient by uploading FHIR Patient resources

__Bug__

* \[[PAM-527](https://gazelle.ihe.net/jira/browse/PAM-527)\] When the administrator imports FHIR Patient resources, the address line is not stored in database

__Story__

* \[[PAM-502](https://gazelle.ihe.net/jira/browse/PAM-502)\] Move to the new apereo CAS
* \[[PAM-503](https://gazelle.ihe.net/jira/browse/PAM-503)\] Connectathon Demographics Sharing
* \[[PAM-525](https://gazelle.ihe.net/jira/browse/PAM-525)\] Allow users to download connectathon patients as a FHIR resources

__Improvement__

* \[[PAM-331](https://gazelle.ihe.net/jira/browse/PAM-331)\] Allow the user to select a subset of identifiers to send
* \[[PAM-477](https://gazelle.ihe.net/jira/browse/PAM-477)\] PatientManager considerations for assigning authority values in HL7 FHIR


# 9.8.0
_Release date: 2017-11-30_

__Remarks__

PDQm and PIXm actors are now compliant with the latest version of the IHE ITI supplements (support of FHIR DSTU3).

__Technical task__

* \[[PAM-481](https://gazelle.ihe.net/jira/browse/PAM-481)\] Update dependencies to the latest version of hapi-fhir
* \[[PAM-482](https://gazelle.ihe.net/jira/browse/PAM-482)\] [PDQm/PDC] Update the consumer to support the latest version of the PDQm profile
* \[[PAM-483](https://gazelle.ihe.net/jira/browse/PAM-483)\] [PDQm/PDS]  Update the supplier to support the latest version of the PDQm profile
* \[[PAM-484](https://gazelle.ihe.net/jira/browse/PAM-484)\] [PIXm/Consumer] Update the consumer to support the latest version of the PIXm profile
* \[[PAM-485](https://gazelle.ihe.net/jira/browse/PAM-485)\] [PIXm/Manager] Update the manager to support the latest version of the PIXm profile

__Bug__

* \[[PAM-479](https://gazelle.ihe.net/jira/browse/PAM-479)\] Database issue when deploying the tool with postgres 9.6.5
* \[[PAM-518](https://gazelle.ihe.net/jira/browse/PAM-518)\] Message identifiers are not extracted when two servers are running using the same handler

__Story__

* \[[PAM-474](https://gazelle.ihe.net/jira/browse/PAM-474)\] Allow the user to create pre-defined queries for PIX* query
* \[[PAM-514](https://gazelle.ihe.net/jira/browse/PAM-514)\] Integrate the new Fhir Validator

__Improvement__

* \[[PAM-471](https://gazelle.ihe.net/jira/browse/PAM-471)\] We need the FHIR simulators to support DSTU3
* \[[PAM-486](https://gazelle.ihe.net/jira/browse/PAM-486)\] Update GraphWalker to its latest version


# 9.7.0
_Release date: 2017-11-02_

__Technical task__

* \[[PAM-450](https://gazelle.ihe.net/jira/browse/PAM-450)\] [API] Extract messageControlId from HL7v2 messages
* \[[PAM-452](https://gazelle.ihe.net/jira/browse/PAM-452)\] [Interop/ValAPI] Implement the web service for remote control of the validation
* \[[PAM-459](https://gazelle.ihe.net/jira/browse/PAM-459)\] [Interop/ValAPI]Extract identifiers from HL7v3 messages

__Epic__

* \[[PAM-444](https://gazelle.ihe.net/jira/browse/PAM-444)\] Allow users to launch the automaton through web service

__Bug__

* \[[PAM-434](https://gazelle.ihe.net/jira/browse/PAM-434)\] There are patients without domain in the database
* \[[PAM-455](https://gazelle.ihe.net/jira/browse/PAM-455)\] Issues with worklist feature
* \[[PAM-492](https://gazelle.ihe.net/jira/browse/PAM-492)\] XCPD query generate a NPE
* \[[PAM-498](https://gazelle.ihe.net/jira/browse/PAM-498)\] Asynchronous execution of services when called through REST interface

__Story__

* \[[PAM-442](https://gazelle.ihe.net/jira/browse/PAM-442)\] [Automaton] Documentation
* \[[PAM-449](https://gazelle.ihe.net/jira/browse/PAM-449)\] [API] Implement a web service to remotely validate messages
* \[[PAM-497](https://gazelle.ihe.net/jira/browse/PAM-497)\] [PIX Source] message structure for A05 event to ADT_A01

__Improvement__

* \[[PAM-435](https://gazelle.ihe.net/jira/browse/PAM-435)\] [Automaton] Allow user to launch the automaton from a remote application
* \[[PAM-470](https://gazelle.ihe.net/jira/browse/PAM-470)\] [PatientManager] EPR-SPID shall be transmitted as otherIDs


# 9.6.0
_Release date: 2017-09-18_

__Remarks__

This release includes the Swiss extensions to the PIXV3, PDQV3 and XCPD profiles.

__Bug__

* \[[PAM-421](https://gazelle.ihe.net/jira/browse/PAM-421)\] Acknowledgement element is missing in PIXV3 query response
* \[[PAM-456](https://gazelle.ihe.net/jira/browse/PAM-456)\] [CH:PDQ/PDC] There is no need to be able to query on the father's name, remove this field from the query form
* \[[PAM-464](https://gazelle.ihe.net/jira/browse/PAM-464)\] Cannot activate/deactivate all patients
* \[[PAM-466](https://gazelle.ihe.net/jira/browse/PAM-466)\] Troubles with "deactivate" feature of assigning authority
* \[[PAM-467](https://gazelle.ihe.net/jira/browse/PAM-467)\] Cannot delete test data

__Story__

* \[[PAM-393](https://gazelle.ihe.net/jira/browse/PAM-393)\] When a patient is generated, depending on the country, some fields shall be hidden
* \[[PAM-422](https://gazelle.ihe.net/jira/browse/PAM-422)\] [CH-PIX/PDQ] Create a new entity to manage Next-of-kins
* \[[PAM-423](https://gazelle.ihe.net/jira/browse/PAM-423)\] [CH-PIX/PDQ/XCPD] Allow the administrator of the tool to switch to CH extension
* \[[PAM-424](https://gazelle.ihe.net/jira/browse/PAM-424)\] [CH-PIX] Update PIXV3 Feed messages

__Improvement__

* \[[PAM-153](https://gazelle.ihe.net/jira/browse/PAM-153)\] [PIXV3] Patient Identifier Cross-Reference Manager updates a patient
* \[[PAM-154](https://gazelle.ihe.net/jira/browse/PAM-154)\] [PIXV3] Patient Identifier Cross-Reference Manager merges patient records
* \[[PAM-427](https://gazelle.ihe.net/jira/browse/PAM-427)\] [CH-PDQ] Allow the user to search by Father/Mother
* \[[PAM-428](https://gazelle.ihe.net/jira/browse/PAM-428)\] [CH-PDQ] Make the PDQV3 query conform to eHealthSuisse specifications
* \[[PAM-429](https://gazelle.ihe.net/jira/browse/PAM-429)\] [CH-PDQ] Make the PDQV3 Query Response conform to eHealthSuisse constraints
* \[[PAM-430](https://gazelle.ihe.net/jira/browse/PAM-430)\] [CH-PIX/PDQ/XCPD] Update user manual for PatientManager
* \[[PAM-431](https://gazelle.ihe.net/jira/browse/PAM-431)\] [CH-XCPD] Allow the user to send a CH-XCPD query
* \[[PAM-432](https://gazelle.ihe.net/jira/browse/PAM-432)\] [CH-XCPD] Make XCPD Responding GW response compliant with CH-XCPD constraints
* \[[PAM-461](https://gazelle.ihe.net/jira/browse/PAM-461)\] Allow the administrator to manage identifiers when he/she create new patients

# 9.5.0
_Release date: 2017-06-30_

__Remarks__

The database scheme has been strongly modified, application will not work properly until you have execute the 9.5.0-update.sql script to update your database.

__Technical task__

* \[[PAM-402](https://gazelle.ihe.net/jira/browse/PAM-402)\] Update model so that a patient can have multiple addresses

__Bug__

* \[[PAM-360](https://gazelle.ihe.net/jira/browse/PAM-360)\] [PDQV3/PDC] "Clean up" button does not empty the content of the birth date field
* \[[PAM-410](https://gazelle.ihe.net/jira/browse/PAM-410)\] PIX Source: When BP6 option is activated for PAM, PIX messages (ITI-30) embeds the ZFD segment and shall not
* \[[PAM-411](https://gazelle.ihe.net/jira/browse/PAM-411)\] Add a link to value sets in the menu
* \[[PAM-419](https://gazelle.ihe.net/jira/browse/PAM-419)\] Patient identifier set in the XCPD query form are not saved as part of the pre-defined query

__Story__

* \[[PAM-412](https://gazelle.ihe.net/jira/browse/PAM-412)\] [Sequoia/XCPD] Patient Manager shall be able to handle XUA assertions on XCPD messages
* \[[PAM-413](https://gazelle.ihe.net/jira/browse/PAM-413)\] [Sequoia/XCPD] User shall be able to query their XCPD Init GW with more than one name
* \[[PAM-424](https://gazelle.ihe.net/jira/browse/PAM-424)\] [CH-PIX] Update PIXV3 Feed messages
* \[[PAM-446](https://gazelle.ihe.net/jira/browse/PAM-446)\] Merge Patient Manager (Sequoia) branch into trunk

__Improvement__

* \[[PAM-115](https://gazelle.ihe.net/jira/browse/PAM-115)\] [PDQv3/PDC] Add other parameters
* \[[PAM-415](https://gazelle.ihe.net/jira/browse/PAM-415)\] [XCPD/Sequoia] XCPD Init GW shall be able to send queries with multiple names
* \[[PAM-420](https://gazelle.ihe.net/jira/browse/PAM-420)\] Allow user to send XCPD queries with multiple livingSubjectId elements


# 9.4.5
_Release date: 2017-01-23_

__Bug__

* \[[PAM-408](https://gazelle.ihe.net/jira/browse/PAM-408)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm

# 9.4.4
_Release date: 2016-11-30_

__Bug__

* \[[PAM-303](https://gazelle.ihe.net/jira/browse/PAM-303)\] PAM automaton crashes when the received ACK does not use the 2.5 version of HL7
* \[[PAM-404](https://gazelle.ihe.net/jira/browse/PAM-404)\] Any one can configure the simulator ports

__Improvement__

* \[[PAM-356](https://gazelle.ihe.net/jira/browse/PAM-356)\] As an implementer of the PAM/PEC actor, I want to be able to update the patient's account number to have a consistent workflow
* \[[PAM-319](https://gazelle.ihe.net/jira/browse/PAM-319)\] The administrator users shall be able to activate/deactivate patients

# 9.4.0
_Release date: 2017-11-07_

__Bug__

* \[[PAM-310](https://gazelle.ihe.net/jira/browse/PAM-310)\] Validation of fhir instances in PIXm / PDQm are not possible (conflict between xerces and fhir parser)
* \[[PAM-333](https://gazelle.ihe.net/jira/browse/PAM-333)\] Please fix sonar issue on socket not closed
* \[[PAM-349](https://gazelle.ihe.net/jira/browse/PAM-349)\] When asking for patient to our PDQm FHIR server, there is sometimes a "transaction failed"
* \[[PAM-384](https://gazelle.ihe.net/jira/browse/PAM-384)\] Fix issue in JSON business rules rendering
* \[[PAM-388](https://gazelle.ihe.net/jira/browse/PAM-388)\] User is required to provide receiving app and facility for fhir SUT
* \[[PAM-389](https://gazelle.ihe.net/jira/browse/PAM-389)\] PAM-132:Query a SUT with the PIXm client
* \[[PAM-391](https://gazelle.ihe.net/jira/browse/PAM-391)\] PDQm/PDS; search should be case insensitive
* \[[PAM-392](https://gazelle.ihe.net/jira/browse/PAM-392)\] PDQm/PDS: inconsistency when we query for json format
* \[[PAM-396](https://gazelle.ihe.net/jira/browse/PAM-396)\] PIXm consumer: clear parameter button does not work

__Story__

* \[[PAM-285](https://gazelle.ihe.net/jira/browse/PAM-285)\] Create a hapi-fhir based server to handle pixm and pdqm
* \[[PAM-309](https://gazelle.ihe.net/jira/browse/PAM-309)\] Display returned patient with demographics data in XML / JSON
* \[[PAM-314](https://gazelle.ihe.net/jira/browse/PAM-314)\] Configure PDQm server
* \[[PAM-322](https://gazelle.ihe.net/jira/browse/PAM-322)\] Planification on PatientManager + answer mails
* \[[PAM-328](https://gazelle.ihe.net/jira/browse/PAM-328)\] Parse FHIR HTTP GET request with ANTLR tool
* \[[PAM-332](https://gazelle.ihe.net/jira/browse/PAM-332)\] Integrate ANTLR grammar for PIXM in PatientManager (FHIR)
* \[[PAM-335](https://gazelle.ihe.net/jira/browse/PAM-335)\] Integrate ANTLR grammar for PDQm in Patient Manager
* \[[PAM-336](https://gazelle.ihe.net/jira/browse/PAM-336)\] Add Mother's Maiden name FHIR extension in PDQm server
* \[[PAM-342](https://gazelle.ihe.net/jira/browse/PAM-342)\] In PDQm client, handle address parameter to search in each of the sub-fields of database address
* \[[PAM-343](https://gazelle.ihe.net/jira/browse/PAM-343)\] Specify a target domain in PDQm to filter results
* \[[PAM-344](https://gazelle.ihe.net/jira/browse/PAM-344)\] Add a PDQm PDS configuration page
* \[[PAM-348](https://gazelle.ihe.net/jira/browse/PAM-348)\] Add the support of HTML rendering for XML and JSON FHIR instances
* \[[PAM-351](https://gazelle.ihe.net/jira/browse/PAM-351)\] Validation of responses in PIXM
* \[[PAM-352](https://gazelle.ihe.net/jira/browse/PAM-352)\] Validation of responses in PDQM
* \[[PAM-353](https://gazelle.ihe.net/jira/browse/PAM-353)\] Pagination in PDQm
* \[[PAM-359](https://gazelle.ihe.net/jira/browse/PAM-359)\] Create XSL transformer for FHIR (PIXM / PDQM)
* \[[PAM-362](https://gazelle.ihe.net/jira/browse/PAM-362)\] Link PIXM and PDQM assertions to assertion manager rules
* \[[PAM-363](https://gazelle.ihe.net/jira/browse/PAM-363)\] Update PIXM ATNTLR grammar to handle more cases
* \[[PAM-364](https://gazelle.ihe.net/jira/browse/PAM-364)\] Production report for Patient Manager 9.4.0
* \[[PAM-365](https://gazelle.ihe.net/jira/browse/PAM-365)\] Update code to new simulator-common and new hl7common for PatientManagerFhir
* \[[PAM-372](https://gazelle.ihe.net/jira/browse/PAM-372)\] Create tests on TestLink to cover Patient Manger 9.4.0 (FHIR)
* \[[PAM-373](https://gazelle.ihe.net/jira/browse/PAM-373)\] Merge FHIR branch in the trunk
* \[[PAM-380](https://gazelle.ihe.net/jira/browse/PAM-380)\] Create cookbook for PatientManager
* \[[PAM-381](https://gazelle.ihe.net/jira/browse/PAM-381)\] Update PAM Fhir Dependencies
* \[[PAM-382](https://gazelle.ihe.net/jira/browse/PAM-382)\] Create cookbook for FhirValidator
* \[[PAM-383](https://gazelle.ihe.net/jira/browse/PAM-383)\] Deploy with chef and knife solo Patient Manager Fhir on Sake
* \[[PAM-401](https://gazelle.ihe.net/jira/browse/PAM-401)\] Merge PatientManager with fhir dev in the trunk and update dependencies

__Improvement__

* \[[PAM-312](https://gazelle.ihe.net/jira/browse/PAM-312)\] PDQ demographic field "birthdate" must be a Date not a String
* \[[PAM-313](https://gazelle.ihe.net/jira/browse/PAM-313)\] Change gender selection in PDQ from String to dropdown menu with authorized values
* \[[PAM-355](https://gazelle.ihe.net/jira/browse/PAM-355)\] Add PIXm server config in Patient Manager FHIR
* \[[PAM-377](https://gazelle.ihe.net/jira/browse/PAM-377)\] CAS login redirects to the same page instead of the home page
* \[[PAM-390](https://gazelle.ihe.net/jira/browse/PAM-390)\] I would expect the tool to notify the user when no result is available on PIXm manager side
