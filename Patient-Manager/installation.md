---
title:  Installation Manual
subtitle: Patient Manager
author: Anne-Gaëlle BERGE
releasedate: 2024-07-30
toolversion: 10.X.X
function: Engineer
version: 1.07
status: Approved document
reference: KER1-MAN-IHE-PATIENT_MANAGER_INSTALLATION-1_07
customer: IHE-EUROPE
---

The PatientManager project was firstly named PAMSimulator since it was only dedicated to the testing of the actors defined in the Patient Administration Management (PAM) integration profile. Later we need to implement the PIX and PDQ actors and decided to have all the tools concerning the management of patient demographics, identifiers and encounters in a same place and the PAMSimulator became the PatientManager. However, the maven project has not been renamed and is still named PAMSimulator.

# Sources & binaries

To get the name of the latest release, visit the PAM project in *JIRA* and consult the "Change log" section.

If you intent to you the CAS service provided by Gazelle at https://gazelle.ihe.net or if you choose to not use the CAS, you can download the last release of the tool, available in our *Nexus repository*. 

Note that if you use the CAS service provided by Gazelle, you will have access to the administration tasks only if you have the admin privileges in Gazelle

if you do not use a CAS service, users will not be able to authenticate theirselves and any user can access the administration tasks (see the Configuration section for an overview of those tasks)

If you have your own CAS service, you need to package your own version of the tool. Proceed according the following steps:

1.  Checkout the last tagged version available in Gazelle sources repository ($versionName will depend of the release you want to checkout)

	```bash
	git clone https://gitlab.inria.fr/gazelle/public/simulation/patient-manager.git
	cd Patient-Manager
	git checkout tags/PAMSimulator-$versionName
	```

4.  Package the Patient Manager project. Go to the PAMSimulator folder then apply the following command line

	`mvn -P public clean package`

5.  The binary will be available in PAMSimulator/PAMSimulator-ear/target folder

# Database configuration

The PDQ part of the simulator uses fuzzystrmatch extension of postgreSQL. Follow the instructions below to enable this module in your database:

1.  Install the contrib module of postgreSQL

	`sudo apt-get install postgresql-contrib`

2.  Restart postgreSQL (if an application is using it, first close it)

	`sudo /etc/init.d/postgresql restart`

3.  Login as postgres user

	`sudo su postgres`

4.  Enable the fuzzystrmatch module

	4.1. for psql 9.1 and above

	`psql`

	`postgres\# CREATE EXTENSION fuzzystrmatch;`

	4.2. for psql 8.4

	`psql -U postgres -d pam-simulator -f fuzzystrmatch.sql`



# Installation


1.  Check the General considerations page, it will inform you about the prerequisites for installing Gazelle tools.

 **WARNING** : From version 9.11.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file PAMSimulator-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

2.  Create a new database named pam-simulator, using encoding UTF-8 and owned by gazelle

	`createdb -U gazelle -E UTF8 pam-simulator`
	
3. **WARNING** : From version 9.11.0, datasources need to be extracted from the **ear** folder (found in /src/main/application/META-INF).
    For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

    Datasource name : PAMSimulatorDS
    
    Database name : pam-simulator

4.  Copy the PAMSimulator.ear file in your JBoss server and start it.

5.  Connect to the database and execute the initialization script. If you have checkout a copy of a tagged version, this file is available at *PAMSimulator/PAMSimulator-ear/src/main/sql/import.sql*

6.  If you do not intent to use any CAS service, execute the following SQL command

	`update app\_configuration set value = 'true' where variable = 'cas\_enabled';`

7.  Finally, open your favorite browser (please avoid usage of IE), and go to

	*http://yourJbossInstance/PatientManager*

# Interaction with other applications

This section describes with which other tools Patient Manager needs to interact to offer a complete experience to the user.

## Integration diagram

![How Patient Manager interacts with other Gazelle tools](./media/interaction-diagram.png)

## Tools

|Application name | Recommended version |  Usage |
|------------------|--------------------|--------------------------------------------------------------------|
| Demographic Data Server | latest | Used to generate patients (Required when PAM, ADT features are used, optional otherwise) |
| Gazelle HL7 Validator | latest | Used to validate HL7v2/HL7v3 messages (Not used by PDQm/PIXm features)|
| Gazelle FHIR Validator | latest | Used to validate FHIR messages (Used only by PDQm/PIXm features) |
| SVS Simulator | latest | Used by the PAM/ADT actors to build the messages and display options to the user |
| Gazelle STS | latest | Used by the XUA feature to request / validate SAML assertions |
| Order Manager | latest | Create a worklist for a given patient or encounter |

## Requirements

The table below gathers which tools are required based on the activated features.

Legend:

* R: required, feature will not work correctly without it
* O: optional, not needed to use the feature but might help
* N: Not used by the feature at all

|Application Name | PAM | ADT | PDQ | PDQv3 | PDQm | PIX | PIXv3 | PIXm | XCPD | SEQUOIA | ITI-FR | CONNECTATHON | EPD | XUA | AUTOMATON | WORKLIST |
|--------------------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|Demographic Data Server | __R__ | __R__ | O | O | O | O | O | O | O | __R__ | O | __R__ | N |__R__|N|
|Gazelle HL7 Validator | __R__ | __R__ | __R__ | __R__ | N | __R__ | __R__ | N | __R__ | __R__ | __R__ | __R__ | __R__ | N |__R__|N|
|Gazelle FHIR Validator | N | N | N | N | __R__ | N | N | __R__ | N | N | N | N | N | N |N|N|
|SVS Simulator | __R__ | __R__ |O | O | O | O | O | O | O | __R__ | __R__ | __R__ | N |__R__|N|
|Gazelle STS | N | N | N | N | N | N | N | N | N | N | N | N | N | __R__ |N|N|
|Order Manager |N | N | N | N | N | N | N | N | N | N | N | N | N | N |N|__R__|

# Configuration

## Menu configuration

The menu bar of the application is configurable. That means that you can hide the profiles that you are not going to test.

To choose the features to be enabled in your instance of the tool, access Administration > Enable/Disable features. For each entry, check (enable) or uncheck (disable) the checkbox in the last column. Changes are saved automatically. To reload the menu bar with its new content, simply refresh the page or access the home page.

The table below gathers the available features

|**Feature**                                                       |**Description**                                                                                         | EPD | BP6 | IHE | CA | Sequoia |
|------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|---|---|---|
| CONNECTATHON - Connectathon demographics sharing              | Use this feature to enable the Sharing of specific demographics among test session participants |enabled|disabled|enabled| enabled | disabled |
| EPD - Electronische Patient Dossier (eHealthSuisse)              | Use this feature if the simulator is used in the context of the eHealthSuisse EPD project. Shall be used with application preference default_pdq_domain = EPD |enabled|disabled|disabled| disabled | disabled |
|	IHE - Default version of the profiles                            | Use this feature if the simulator is only used for IHE default version of the profile. Shall be used with application preference default_pdq_domain = IHE | disabled | enabled | enabled | enabled | enabled |
| ITI-FR - French version of the ITI profiles	                     | Use this feature to causes the simulator to offer the user to send PAM messages compliant with the French extension of the PAM profile | disabled | enabled | disabled | disabled | disabled |
| Modality Worklist - Users can share patients with Order Manager	 | Enable this feature will add a button in the list of patient to allow the user to share this patient with Order Manager and thus create a worklist or any other kind of orders for this patient | disabled | disabled | enabled | disabled | disabled |
| PAM Automaton	                                                    | Gives access to the automaton for the PAM profile (based on GraphWalker), you do not use this feature if you do not test the PAM profile | disabled | enabled | enabled | disabled | disabled |
| PAM - Patient Administration Management                           | Displays the PAM menu and gives access to the simulated actors for this profile	| disabled | enabled | enabled | enabled | disabled |
| PDQm - Patient Demographic Query for Mobile	                      | Displays the PDQ* menu and gives access to the simulated actors for this profile | disabled | disabled | enabled | disabled | disabled |
| PDQ - Patient Demographic Query	                                  | Displays the PDQ* menu and gives access to the simulated actors for this profile | disabled | disabled | enabled | enabled | disabled |
| PDQV3 - Patient Demographic Query HL7V3	                          | Displays the PDQ* menu and gives access to the simulated actors for this profile | enabled | disabled | enabled | enabled | enabled |
| PIXm - Patient Identifier Cross-referencing for Mobile	          | Displays the PIX* menu and gives access to the simulated actors for this profile | disabled | disabled | enabled | disabled | disabled |
| PIX - Patient Identifier Cross-referencing for MPI	              | Displays the PIX* menu and gives access to the simulated actors for this profile | disabled | disabled | enabled | enabled | disabled |
| PIXV3 - Patient Identifier Cross-referencing HL7V3	              | Displays the PIX* menu and gives access to the simulated actors for this profile | enabled | disabled | enabled | enabled | enabled |
| Radiology ADT                                                     | Displays the ADT menu to send RAD-1 and RAD-12 messages to your MPI | disabled | disabled | enabled | disabled | disabled |
| Sequoia - Extensions for The Sequoia project	                    | Use this feature if the simulator is used in the context of the Sequoia project, it will causes HL7v3 initiators to append the SOAP security header as defined by Sequoia project specifications | disabled | disabled | disabled | disabled | enabled |
| XCPD - Cross-Community Patient Discovery                          | Displays the XCPD menu and gives access to the simulated actors for this profile | enabled | disabled | enabled | enabled | enabled |
| XUA - Cross-Enterprise User Access                                | Displays the XUA menu and allows the user to ask the XCPD Initiator to include a SAML assertion in the SOAP header | disabled | disabled | enabled | disabled | enabled |

## Application preferences

Under the Administration menu, you will find a sub-menu entitled "Configure application preferences". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the PatientManager tool.

| **Variable**                       | **Description**                                                                                                                                                                                                                   | **Default value**                                                                                       |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
|  application\_url                  |  The URL used by any user to access the tool. The application needs it to build permanent links inside the tool                                                                                                                   |  http://publicUrlOfJboss/PatientManager                                                                 |
|  application\_namespace\_id        |  Defines the namespaceID of the issuer of the identifiers generated by the tool                                                                                                                                                   |  IHEPAM                                                                                                 |
|  application\_universal\_id        |  Defines the universal ID of the issuer of the identifiers generated by the tool. It's formatted as an OID and shall be unique across all instances of PatientManager tool                                                        |  a uniquely defined OID                                                                                 |
|  application\_universal\_id\_type  |  Defines the type of universal id                                                                                                                                                                                                 |  ISO                                                                                                    |
|  create\_worklist\_url             |  The URL of the OrderManager instance you may use to create DICOM worklists                                                                                                                                                       |  *OrderManager on Gazelle*                                                                              |
| default\_pdq\_domain               | For PDQv3, defines if we use SeHE or ITI rules                                                                                                                                                                                    | ITI                                                                                                     |
| hl7v3\_organization\_oid           | OID of the organization issuing/receiving HL7v3 messages                                                                                                                                                                          | a uniquely defined OID                                                                                  |
| hl7v3\_pdq\_pdc\_device\_id        | Identifies the PDQv3/PDC actor of the tool                                                                                                                                                                                        | a uniquely defined OID                                                                                  |
| hl7v3\_pdq\_pds\_device\_id        | Identifies the PDQv3/PDS actor of the tool                                                                                                                                                                                        | a uniquely defined OID                                                                                  |
| hl7v3\_pix\_consumer\_device\_id   | Identifies the PIX consumer actor of the tool                                                                                                                                                                                     | a uniquely defined OID                                                                                  |
| hl7v3\_pix\_mgr\_device\_id        | Identifies the PIX manager of the tool                                                                                                                                                                                            | a uniquely defined OID                                                                                  |
| hl7v3\_pix\_organization\_oid      | Identifies the PIX organization of the tool                                                                                                                                                                                       | a uniquely defined OID                                                                                  |
| hl7v3\_pix\_src\_device\_id        | Identifies the PIX source actor of the tool                                                                                                                                                                                       | a uniquely defined OID                                                                                  |
| hl7v3\_pixv\_src\_device\_id       | Identifies the PIXv3 source actor of the tool                                                                                                                                                                                     | a uniquely defined OID                                                                                  |
| hl7v3\_validation\_xsl\_location   | Stylesheet for displaying HL7v3 validation service reports                                                                                                                                                                        | https://gazelle.ihe.net/xsl/hl7v3validatorDetailedResult.xsl                                             |
| hl7v3\_validator\_url              | URL of the web service exposed by Gazelle HL7 Validator for validating HL7v3 messages                                                                                                                                             | https://gazelle.ihe.net/GazelleHL7v2Validator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl |
| hl7\_version                       | HL7v2.X version used                                                                                                                                                                                                              | 2.5                                                                                                     |
| pdqv3\_pds\_url                    | Endpoint of the PDQv3/PDS embedded in the tool (for display to the user)                                                                                                                                                          | https://gazelle.ihe.net/PAMSimulator-ejb/PDQSupplier_Service/PDQSupplier_PortType?wsdl    |
| pixv3\_cons\_url                   | Endpoint of the PIXv3 consumer embedded in the tool (for display to the user)                                                                                                                                                     | https://gazelle.ihe.net/PAMSimulator-ejb/PIXConsumer_Service/PIXConsumer_PortType?wsdl        |
| pixv3\_mgr\_url                    | Endpoint of the PIXv3 manager embedded in the tool (for display to the user)                                                                                                                                                      | https://gazelle.ihe.net/PAMSimulator-ejb/PIXManager_Service/PIXManager_PortType?wsdl          |
|  sending\_application               |  Used to populate MSH-3 field of the HL7 messages produced by the tool | PAMSimulator	                                  
|  sending\_facility                  |  Used to populate  MSH-4 field of the HL7 messages produced by the tool                                                                                                                                                 |                                                                                                                                                                                                                                                                                                                                                                      IHE                                                                                                     |
|  time\_zone                        |  Defines which time zone to use to display dates and timestamps                                                                                                                                                                   |  Europe/Paris                                                                                           |
|  dds\_ws\_endpoint                 |  Location of the Demographic Data server WSDL                                                                                                                                                                                     |  *DDS WS on Gazelle*                                                                                    |
|  gazelle\_hl7v2\_validator\_url    |  URL of the Gazelle HL7 Validator tool                                                                                                                                                                                            |  https://gazelle.ihe.net/GazelleHL7Validator                                                             |
|  svs\_repository\_url              |  URL of the Sharing Value Set Repository actor of the SVSSimulator                                                                                                                                                                |  https://gazelle.ihe.net                                                                                 |
|  timeout\_for\_receiving\_messages |  How long must the HL7 initiator wait for a response (in ms)                                                                                                                                                                      |  10000                                                                                                  |
|  url\_EVSC\_ws                     |  URL of the Gazelle HL7 Validator wsdl (the one for HL7v2.x validation)                                                                                                                                                           |  https://gazelle.ihe.net/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl                                                                    |
|  use\_ids\_from\_dds               |  DDS generates patient identifiers, the PatientManager can use them or generate its own using the application\_namespace\_id and application\_universal\_id. This value is used as the default choice on patient generation panel |  true                                                                                                   |
|  hl7v2_xsl\_location                     |  URL to access the XML stylesheet used to display HL7v2.x validation results                                                                                                                                                      | https://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl                                                                                         |
| fhir_validation_url| Fhir validation url | https://gazelle.ihe.net/FhirValidator-ejb/gazelleFhirValidationWSService/gazelleFhirValidationWS?wsdl
| hl7fhir_json_validation_xsl_location | XSL Stylesheet for JSON Fhir instances | |
| hl7fhir_xml_validation_xsl_location | XSL Stylesheet for JSON XML instances | |
| xcpd_respgw_cron_trigger | Period for the XCPD Responding Gateway to check if deferred responses have to be processed | 1 * * * * ? |
| gazelle_sts_url | URL of gazelle-sts to be contacted for issuing and validating SAML assertions | $$PLATFORM_URL$$/gazelle-sts?wsdl |
| sts_default_password | Default password to provide to Gazelle STS for assertion validation | connectathon |
| sts_default_username | Default username to provide to Gazelle STS for assertion validation | valid |
| sts_default_audience_url | Default audience to provide to Gazelle STS for assertion issue | http://ihe.connectathon.XUA/X-ServiceProvider-IHE-Connectathon |
| account_assigning_authority | ID of the domain to be used to generate account numbers (Configured from Assigning authority page) | |
| movement_assigning_authority | ID of the domain to be used to generate movement identifiers (Configured from Assigning authority page) | |
| visit_assigning_authority | ID of the domain to be used to generate visit numbers (Configured from Assigning authority page) | |
| patient_assigning_authority | ID of the domain to be used to generate default patient identifiers (Configured from Assigning authority page) | |
| keystore_password | Password to access the keystore (used when the SOAP header contains a signature) | |
| private_key_password | Password to access the private key in the keystore (used when the SOAP header contains a signature) | |
| path_to_keystore | Absolute path of the keystore (used when the SOAP header contains a signature) | |
| private_key_alias | Alias of the private key in the keystore (used when the SOAP header contains a signature) | |

## SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

## HL7v2.x responders

From the Administration/HL7 Responders configuration page, you will be able to configure each actor of the tool playing the role of a responder in a HL7-based transaction. A configure consists in the receiving application and facility and the port on which it listens to incoming messages. The IP address is not used by the server but must be set properly so that the users can configure their systems under test to communicate with the tool. DO NOT update the other parameters; it would prevent the tool from working correctly.

Note: When you update a configuration, do not forget to restart it.

## Home page

The first time you access the application, you may notice that the home page of the tool is not configured. To set a title and a welcome message, log into the application with admin rights (every user can update this if you are not using CAS). 

Note that you will have to set up this page for all the languages supported by the application.
