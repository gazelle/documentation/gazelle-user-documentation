---
title: User Manual
subtitle: MCSD Fhir Simulator
author: Alexandre POCINHO
releasedate: 2024-06-21
toolversion: 1.0.0
function: Engineer
version: 0.1
status: draft
---

# MCSD Fhir Simulator

## Description

The MCSD FHIR Simulator acts like a server to return queried resources.
It have a validation process to check if the Header + Body + URL are compliant regarding the IHE specification.

The version 1.0.0 implements ONLY Organization, Practitioner and PractitionerRole MCSD FHIR resources.


## Prerequisite

Before deploying the project the system MUST have:

- a folder `/opt/mcsd-simulator/`
- a `resourceSummary.json` file inside  with the following content

```json
[
  {
    "resourceType": "Organization",
    "resourcesDirectory": "/opt/mcsd-simulator/organizations"
  },
  {
    "resourceType": "OrganizationAffiliation",
    "resourcesDirectory": "/opt/mcsd-simulator/organizationAffiliations"
  },
  {
    "resourceType": "Endpoint",
    "resourcesDirectory": "/opt/mcsd-simulator/endpoints"
  },
  {
    "resourceType": "HealthcareService",
    "resourcesDirectory": "/opt/mcsd-simulator/healthcareServices"
  },
  {
    "resourceType": "Location",
    "resourcesDirectory": "/opt/mcsd-simulator/locations"
  },
  {
    "resourceType": "Practitioner",
    "resourcesDirectory": "/opt/mcsd-simulator/practitioners"
  },
  {
    "resourceType": "PractitionerRole",
    "resourcesDirectory": "/opt/mcsd-simulator/practitionerRoles"
  },
  {
    "resourceType": "ValueSet",
    "resourcesDirectory": "/opt/mcsd-simulator/valueSets"
  }
]

```

- all directories (organizations, endpoints, practitionerRoles, etc.) had to be created

It is a default configuration, the `resourceSummary.json` could be set in the `.env` with the following property : 
- `RESOURCES_SUMMARY_PATH=/opt/mcsd-simulator/resourcesSummary.json`

And all the fields `resourcesDirectory` can be any path, BUT the `resourceType` remained the exact name of MCSD FHIR Resource.

## Find Matching Care Services (ITI-90)

- [IHE Specifications](https://profiles.ihe.net/ITI/mCSD/ITI-90.html)

## Overview

Here is a quick overview of the available functionality from MCSD Fhir Simulator

| Operation | HTTP Methods | URL to call | Entry parameter | Returned value | Remarks|
|-----------|--------------|-------------|-----------------|----------------|---|
| Retrieve Care Services Resource | GET | ```https://example.com/mcsd-simulator/fhir/ihe/{Resource}/{id}``` | ID for a specific ITI-90 MCSD Fhir resource | the desired ITI-90 Resource |/|
| Find Matching Care Services     | GET       | ```https://example.com/mcsd-simulator/fhir/ihe/{Resource}?name=value&...{&_format=[mime-type]}}``` | Parameters to search for specific ITI-90 MCSD Fhir resourceITI-90 | A bundle containing 0 to many ITI-90 MCSD Fhir Specific corresponding to search criteria   | /|
| Find Matching Care Services     | POST    | ```https://example.com/mcsd-simulator/fhir/ihe/{Resource}/_search{?[parameters]{&_format=[mime-type]}}``` | Parameters to search for specific ITI-90 MCSD Fhir resourceITI-90 | A bundle containing 0 to many ITI-90 MCSD Fhir Specific corresponding to search criteria   | The parameters have to be provided in the body with a `Content-Type: application/x-www-form-urlencoded `|

Capability statement of the application can be found with : <https://example.com/mcsd-simulator/fhir/ihe/metadata>

As described in [HAPI FHIR resources](https://hapifhir.io/hapi-fhir/docs/server_plain/rest_operations_operations.html), some strings are automatically escaped when the FHIR server parses URLs:

|Given String|Parsed as|
| :---: | :---: |
|\||%7C|


## Validation process

Each operation implies before a validation of requests for `ITI-90` 
Validation is done by calling:

- [HTTP Validator](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/http-validator) for URL and Headers.

The validation is done by default but can be switched with options:  

- `MCSD_SERVER_EVS_ENDPOINT=https://${FQDN}/evs/rest/validations`
- `MCSD_SERVER_VALIDATION_ENABLED=true`




