#!/bin/bash


INSTALL_SLONY=true
INSTALL_SLONY_SCRIPT=true
START_SLONY=false
SLONY_VERSION=2.5

function cecho {
  while [ "$1" ]; do
    case "$1" in
    -normal)        color="\033[00m" ;;
    -black)         color="\033[30;01m" ;;
    -red)           color="\033[31;01m" ;;
    -green)         color="\033[32;01m" ;;
    -yellow)        color="\033[33;01m" ;;
    -blue)          color="\033[34;01m" ;;
    -magenta)       color="\033[35;01m" ;;
    -cyan)          color="\033[36;01m" ;;
    -white)         color="\033[37;01m" ;;
    -n)             one_line=1;   shift ; continue ;;
    *)              echo -n "$1"; shift ; continue ;;
    esac

    shift
    echo -en "$color"
    echo -en "$1"
    echo -en "\033[00m"
    shift

  done
  if [ ! $one_line ]; then
    echo
  fi
}

function what_to_do(){

	read -p "Do you want to install slony ? [y,n]"
	if [ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ] || [ "$REPLY" = "yes" ] || [ "$REPLY" = "YES" ] || [ "$REPLY" = "Yes" ]; then
	  INSTALL_SLONY=true
	fi
	 read -p "Do you want to install slony scripts ? [y,n]"
	if [ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ] || [ "$REPLY" = "yes" ] || [ "$REPLY" = "YES" ] || [ "$REPLY" = "Yes" ]; then
	  INSTALL_SLONY_SCRIPTS=true
	fi

	read -p "Do you want to start replication ? [y,n]"
	if [ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ] || [ "$REPLY" = "yes" ] || [ "$REPLY" = "YES" ] || [ "$REPLY" = "Yes" ]; then
	  START_SLONY=true
	fi
}


# get the packages
function install_slony() {
  if $INSTALL_SLONY ; then
	cecho -green "installing slony"
	rm -r slony1*
  	apt-get install postgresql-server-dev-9.6
	wget http://slony.info/downloads/2.2/source/slony1-2.$SLONY_VERSION.tar.bz2
	# install slony from the tar file
	tar jxvf slony1-2.$SLONY_VERSION.tar.bz2
	cd slony1-2.$SLONY_VERSION
	cecho -green " -configure"

	./configure
	cecho -green " -compiling"
	make
	cecho -green " -installing"
	make install 
	cecho -green " -update alternatives" 
	update-alternatives --remove-all slon
	update-alternatives --remove-all slonik
	update-alternatives --install /usr/bin/slon slon /usr/lib/postgresql/9.6/bin/slon 1
	update-alternatives --install /usr/bin/slonik slonik /usr/lib/postgresql/9.6/bin/slonik 1
  fi
}

# install slony synchronization automatic start
function install_slony_scripts(){
  if $INSTALL_SLONY_SCRIPTS ; then

	/etc/init.d/slony_sync stop
	mkdir /var/log/slony_sync
	touch /etc/init.d/slony_sync
	cat > /etc/init.d/slony_sync << _EOF_
#!/bin/sh
### BEGIN INIT INFO
# Provides: Starts the slon script for the synchronization of the database using slony
# Required-Start: \$local_fs \$network
# Required-Stop: \$local_fs \$remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Starts slon scripts
### END INIT INFO
case "\$1" in
  start|"")
	slon -s 60000 TF "dbname=evs-client-prod  user=gazelle" > /var/log/slony_sync/evsclient.log&
	slon -s 60000 TF "dbname=gazelle  user=gazelle" > /var/log/slony_sync/master_model.log&
	;;
  restart|reload|status|force-reload)
	echo "Error: argument '\$1' not supported" >&2
	exit 3
	;;
  stop)
	killall slon
	;;
  *)
	echo "Usage: \$0 [start|stop]" >&2
	exit 3
	;;
esac


exit 0
_EOF_
	chmod +x /etc/init.d/slony_sync
	update-rc.d  slony_sync  defaults


	# install the script for replication
	rm -r /home/gazelle/slony
	mkdir /home/gazelle/slony
	mkdir /home/gazelle/slony/scripts
	touch /home/gazelle/slony/scripts/definition.inc
	touch /home/gazelle/slony/scripts/init_evs.sk
	touch /home/gazelle/slony/scripts/init_gmm.sk
	touch /home/gazelle/slony/scripts/disconnect_evs.sk
	touch /home/gazelle/slony/scripts/replicate_evs.sk
	cecho -green "Creating definition.inc"
	cat > /home/gazelle/slony/scripts/definition.inc << _EOF2_
define CLUSTER TF;
define PRIMARY 1;
define TM 20;
define PR 30;
define EVSCLIENT 40;
define CDAGEN 90;

cluster name = @CLUSTER;

node @PRIMARY admin conninfo = 'dbname=gazelle host=localhost user=gazelle password=gazelle';
_EOF2_
	cecho -green "Creating init_evs.sk"
	cat > /home/gazelle/slony/scripts/init_evs.sk << _EOF3_
#!/usr/bin/slonik
include </home/gazelle/slony/scripts/definition.inc>;

node @EVSCLIENT admin conninfo = 'dbname=evs-client-prod host=localhost user=gazelle password=gazelle';

store node (id=@EVSCLIENT, event node=@PRIMARY, comment='EVS Client Slave');
store path (server=@PRIMARY, client=@EVSCLIENT, conninfo='dbname=gazelle  host=localhost user=gazelle');
store path (server=@EVSCLIENT, client=@PRIMARY, conninfo='dbname=evs-client-prod host=localhost user=gazelle');
_EOF3_
	cecho -green "Creating replicate_evs.sk"
	cat > /home/gazelle/slony/scripts/replicate_evs.sk << _EOF4_
#!/usr/bin/slonik
include </home/gazelle/slony/scripts/definition.inc>;

node @EVSCLIENT admin conninfo = 'dbname=evs-client-prod host=localhost user=gazelle password=gazelle';


#EVSCLIENT
subscribe set (id = 1, provider = @PRIMARY, receiver = @EVSCLIENT);
sync(id=@PRIMARY);
wait for event(origin=@PRIMARY, confirmed=@EVSCLIENT, wait on=@PRIMARY);


subscribe set (id = 3, provider = @PRIMARY, receiver = @EVSCLIENT);
sync(id=@PRIMARY);
wait for event(origin=@PRIMARY, confirmed=@EVSCLIENT, wait on=@PRIMARY);
_EOF4_
	cecho -green "Creating disconnect_evs.sk"
	cat > /home/gazelle/slony/scripts/disconnect_evs.sk << _EOF5_
#!/usr/bin/slonik
include </home/gazelle/slony/scripts/definition.inc>;

node @EVSCLIENT admin conninfo = 'dbname=evs-client-prod host=localhost user=gazelle password=gazelle';


unsubscribe set (id =1,  receiver = @EVSCLIENT);
unsubscribe set (id =3,  receiver = @EVSCLIENT);
_EOF5_
        cecho -green "Creating init_gmm.sk"
	cat > /home/gazelle/slony/scripts/init_gmm.sk << _EOF6_
#!/usr/bin/slonik
include </home/gazelle/slony/scripts/definition.inc>;

init cluster (id=@PRIMARY, comment='Gazelle Master Model');


create set (id=1, origin=@PRIMARY, comment='TF');
create set (id=2, origin=@PRIMARY, comment='TM');
create set (id=3, origin=@PRIMARY, comment='TF_EXTENDED');

set add table (id=176, set id=1, origin = @PRIMARY, fully qualified name = 'public.revinfo', comment = 'table');

set add table (id=185, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_affinity_domain', comment = 'table');
set add table (id=186, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_affinity_domain_aud', comment = 'table');
set add sequence (id=187, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_affinity_domain_id_seq', comment = 'seq');

set add table (id=174, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor', comment = 'table');
set add table (id=175, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_aud', comment = 'table');
set add sequence (id=2, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_id_seq', comment = 'seq');

set add table (id=3, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_table', comment = 'table');
set add table (id=4, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_table_aud', comment = 'table');
set add sequence (id=5, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_table_id_seq', comment = 'seq');

set add table (id=6, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_integration_profile_option', comment = 'table');
set add table (id=7, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_integration_profile_option_aud', comment = 'table');
set add sequence (id=8, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_integration_profile_option_id_seq', comment = 'seq');

set add table (id=9, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile', comment = 'table');
set add table (id=10, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_aud', comment = 'table');
set add sequence (id=11, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_id_seq', comment = 'seq');

set add table (id=12, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_option', comment = 'table');
set add table (id=13, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_option_aud', comment = 'table');
set add sequence (id=14, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_option_id_seq', comment = 'seq');

set add table (id=15, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_integration_profile', comment = 'table');
set add table (id=16, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_integration_profile_aud', comment = 'table');
set add sequence (id=17, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_actor_integration_profile_id_seq', comment = 'seq');

set add table (id=18, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_type', comment = 'table');
set add table (id=19, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_type_aud', comment = 'table');
set add sequence (id=20, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_type_id_seq', comment = 'seq');

set add table (id=21, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_link', comment = 'table');
set add table (id=22, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_link_aud', comment = 'table');
set add sequence (id=23, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_link_id_seq', comment = 'seq');

set add table (id=24, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_option_type', comment = 'table');
set add table (id=25, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_option_type_aud', comment = 'table');
set add sequence (id=26, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_option_type_id_seq', comment = 'seq');

set add table (id=27, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_profile_link', comment = 'table');
set add table (id=28, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_profile_link_aud', comment = 'table');
set add sequence (id=29, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_profile_link_id_seq', comment = 'seq');

set add table (id=30, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_status_type', comment = 'table');
set add table (id=31, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_status_type_aud', comment = 'table');
set add sequence (id=32, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_status_type_id_seq', comment = 'seq');

set add table (id=33, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_profile_inria_hl7_validation_files', comment = 'table');
set add table (id=34, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_profile_inria_hl7_validation_files_aud', comment = 'table');
set add sequence (id=35, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_profile_inria_hl7_validation_files_id_seq', comment = 'seq');

set add table (id=36, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_configuration_mapped_with_aipo', comment = 'table');
set add table (id=37, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_configuration_mapped_with_aipo_aud', comment = 'table');
set add sequence (id=38, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_configuration_mapped_with_aipo_id_seq', comment = 'seq');

set add table (id=39, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_conf_mapping_w_aipo_w_conftypes', comment = 'table');
set add table (id=40, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_conf_mapping_w_aipo_w_conftypes_aud', comment = 'table');

set add table (id=42, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_conftype_w_ports_wstype_and_sop_class', comment = 'table');
set add table (id=43, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_conftype_w_ports_wstype_and_sop_class_aud', comment = 'table');
set add sequence (id=44, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_conftype_w_ports_wstype_and_sop_class_id_seq', comment = 'seq');

set add table (id=45, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_status_type', comment = 'table');
set add table (id=46, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_status_type_aud', comment = 'table');
set add sequence (id=47, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_status_type_id_seq', comment = 'seq');

set add table (id=48, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_configuration_type', comment = 'table');
set add table (id=49, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_configuration_type_aud', comment = 'table');
set add sequence (id=50, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_configuration_type_id_seq', comment = 'seq');

set add table (id=51, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_meta_test', comment = 'table');
set add table (id=52, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_meta_test_aud', comment = 'table');
set add sequence (id=53, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_meta_test_id_seq', comment = 'seq');

set add table (id=54, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_meta_test_test_roles', comment = 'table');
set add table (id=55, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_meta_test_test_roles_aud', comment = 'table');
#set add sequence (id=56, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_meta_test_test_roles_id_seq', comment = 'seq');

set add table (id=57, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_role_in_test_test_participants', comment = 'table');
set add table (id=58, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_role_in_test_test_participants_aud', comment = 'table');
# id=59 was commented out of original, leave it out (SMM)
#set add sequence (id=59, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_role_in_test_test_participants_id_seq', comment = 'seq');

set add table (id=60, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_sop_class', comment = 'table');
set add table (id=61, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_sop_class_aud', comment = 'table');
set add sequence (id=62, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_sop_class_id_seq', comment = 'seq');

set add table (id=63, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_description', comment = 'table');
set add table (id=64, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_description_aud', comment = 'table');
set add sequence (id=65, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_description_id_seq', comment = 'seq');

set add table (id=66, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_participants', comment = 'table');
set add table (id=67, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_participants_aud', comment = 'table');
set add sequence (id=68, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_participants_id_seq', comment = 'seq');

set add table (id=69, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_test_description', comment = 'table');
set add table (id=70, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_test_description_aud', comment = 'table');
#set add sequence (id=71, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_test_description_id_seq', comment = 'seq');

set add table (id=72, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_input_ci', comment = 'table');
set add table (id=73, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_input_ci_aud', comment = 'table');
#set add sequence (id=74, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_input_ci_id_seq', comment = 'seq');

set add table (id=75, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_option', comment = 'table');
set add table (id=76, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_option_aud', comment = 'table');
set add sequence (id=77, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_option_id_seq', comment = 'seq');

set add table (id=78, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_output_ci', comment = 'table');
set add table (id=79, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_output_ci_aud', comment = 'table');
#set add sequence (id=80, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_output_ci_id_seq', comment = 'seq');

set add table (id=81, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps', comment = 'table');
set add table (id=82, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_aud', comment = 'table');
set add sequence (id=83, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_steps_id_seq', comment = 'seq');

set add table (id=84, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_status', comment = 'table');
set add table (id=85, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_status_aud', comment = 'table');
set add sequence (id=86, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_status_id_seq', comment = 'seq');

set add table (id=87, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_roles', comment = 'table');
set add table (id=88, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_roles_aud', comment = 'table');
set add sequence (id=89, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_roles_id_seq', comment = 'seq');

set add table (id=90, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_web_service_type', comment = 'table');
set add table (id=91, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_web_service_type_aud', comment = 'table');
set add sequence (id=92, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_web_service_type_id_seq', comment = 'seq');

set add table (id=93, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_type', comment = 'table');
set add table (id=94, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_type_aud', comment = 'table');
set add sequence (id=95, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_type_id_seq', comment = 'seq');

set add table (id=96, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_domain',  comment = 'table');
set add table (id=97, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_domain_aud', comment = 'table');
set add sequence (id=98, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_domain_id_seq', comment = 'seq');

set add table (id=99, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_type_link', comment = 'table');
set add table (id=100, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_type_link_aud', comment = 'table');
#set add sequence (id=101, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_integration_profile_type_link_id_seq', comment = 'seq');

set add table (id=102, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_test_steps', comment = 'table');
set add table (id=103, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_test_steps_aud', comment = 'table');
#set add sequence (id=104, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_test_steps_id_seq', comment = 'seq');

set add table (id=105, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_option', comment = 'table');
set add table (id=106, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_option_aud', comment = 'table');
set add sequence (id=107, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_option_id_seq', comment = 'seq');

set add table (id=108, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_peer_type', comment = 'table');
set add table (id=109, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_peer_type_aud', comment = 'table');
set add sequence (id=110, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_peer_type_id_seq', comment = 'seq');

set add table (id=111, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile', comment = 'table');
set add table (id=112, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_aud', comment = 'table');
set add sequence (id=113, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_id_seq', comment = 'seq');

set add table (id=114, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_domain_profile',   comment = 'table');
set add table (id=115, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_domain_profile_aud', comment = 'table');
#set add sequence (id=116, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_domain_profile_id_seq', comment = 'seq');

set add table (id=117, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction', comment = 'table');
set add table (id=118, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_aud', comment = 'table');
set add sequence (id=119, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_transaction_id_seq', comment = 'seq');

set add table (id=177, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_affinity_domain', comment = 'table');
set add table (id=178, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_hl7_message_profile_affinity_domain_aud', comment = 'table');



set add table (id=120, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test', comment = 'table');
set add table (id=121, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_aud', comment = 'table');
set add sequence (id=122, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_test_id_seq', comment = 'seq');

set add table (id=123, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_transport_layer_for_config', comment = 'table');
set add table (id=124, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_transport_layer_for_config_aud', comment = 'table');
#set add sequence (id=125, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_transport_layer_for_config_seq', comment = 'seq');
set add sequence (id=125, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_transport_layer_for_config_seq', comment = 'seq');

set add table (id=126, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_role_in_test', comment = 'table');
set add table (id=127, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_role_in_test_aud', comment = 'table');
set add sequence (id=128, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_role_in_test_id_seq', comment = 'seq');

set add table (id=129, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_contextual_information', comment = 'table');
set add table (id=130, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_contextual_information_aud', comment = 'table');
set add sequence (id=131, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_contextual_information_id_seq', comment = 'seq');

set add table (id=132, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file', comment = 'table');
set add table (id=133, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_aud', comment = 'table');
set add sequence (id=134, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_id_seq', comment = 'seq');

set add table (id=135, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_creator', comment = 'table');
set add table (id=136, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_creator_aud', comment = 'table');
set add sequence (id=137, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_creator_id_seq', comment = 'seq');

set add table (id=138, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_attribute', comment = 'table');
set add table (id=139, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_attribute_aud', comment = 'table');
set add sequence (id=140, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_attribute_id_seq', comment = 'seq');

set add table (id=141, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_method', comment = 'table');
set add table (id=142, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_method_aud', comment = 'table');
#set add sequence (id=143, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_method_id_seq', comment = 'seq');

set add table (id=144, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_attribute_option', comment = 'table');
set add table (id=145, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_attribute_option_aud', comment = 'table');
set add sequence (id=146, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_attribute_option_id_seq', comment = 'seq');

set add table (id=147, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_class_validator', comment = 'table');
set add table (id=148, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_class_validator_aud', comment = 'table');
set add sequence (id=149, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_class_validator_id_seq', comment = 'seq');

set add table (id=150, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_type', comment = 'table');
set add table (id=151, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_type_aud', comment = 'table');
set add sequence (id=152, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_file_type_id_seq', comment = 'seq');

set add table (id=153, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_instance_validation', comment = 'table');
set add table (id=154, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_instance_validation_aud', comment = 'table');
set add sequence (id=155, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_instance_validation_id_seq', comment = 'seq');

set add table (id=156, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_method_parameter', comment = 'table');
set add table (id=157, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_method_parameter_aud', comment = 'table');
#set add sequence (id=158, set id=1, origin = @PRIMARY, fully qualified name = 'public.tm_object_method_parameter_id_seq', comment = 'seq');

set add table (id=159, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_parameter_validator', comment = 'table');
set add table (id=160, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_parameter_validator_aud', comment = 'table');
set add sequence (id=161, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_parameter_validator_id_seq', comment = 'seq');

set add table (id=162, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_reader', comment = 'table');
set add table (id=163, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_reader_aud', comment = 'table');
set add sequence (id=164, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_reader_id_seq', comment = 'seq');

set add table (id=165, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_type_status', comment = 'table');
set add table (id=166, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_type_status_aud', comment = 'table');
set add sequence (id=167, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_type_status_id_seq', comment = 'seq');

set add table (id=168, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_type', comment = 'table');
set add table (id=169, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_type_aud', comment = 'table');
set add sequence (id=170, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_type_id_seq', comment = 'seq');

set add table (id=171, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_method_validator', comment = 'table');
set add table (id=172, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_method_validator_aud', comment = 'table');
set add sequence (id=173, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_object_method_validator_id_seq', comment = 'seq');


set add table (id=179, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_path', comment = 'table');
set add table (id=180, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_path_aud', comment = 'table');
set add sequence (id=181, set id=2, origin = @PRIMARY, fully qualified name = 'public.tm_path_id_seq', comment = 'seq');


set add table (id=182, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_ws_transaction_usage', comment = 'table');
set add table (id=183, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_ws_transaction_usage_aud', comment = 'table');
set add sequence (id=184, set id=1, origin = @PRIMARY, fully qualified name = 'public.tf_ws_transaction_usage_id_seq', comment = 'seq');

set add table (id=196, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_document', comment = 'table');
set add table (id=195, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_document_aud', comment = 'table');
set add sequence (id=197, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_document_id_seq', comment = 'seq');


set add table (id=187, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_rule', comment = 'table');
set add sequence (id=188, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_rule_id_seq', comment = 'seq');

set add table (id=189, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_rule_criterion', comment = 'table');
set add sequence (id=190, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_rule_criterion_id_seq', comment = 'seq');

set add table (id=191, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_rule_list_criterion', comment = 'table');

set add table (id=192, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_document_sections', comment = 'table');
set add table (id=194, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_document_sections_aud', comment = 'table');
set add sequence (id=193, set id=3, origin = @PRIMARY, fully qualified name = 'public.tf_document_sections_id_seq', comment = 'seq');

_EOF6_
	chmod +x /home/gazelle/slony/scripts/*.sk
  fi
}

function start_slony() {
  if $START_SLONY ; then
	# clean up databases
	/etc/init.d/slony_sync stop
	psql -U gazelle evs-client-prod  -c ' DROP SCHEMA "_TF" CASCADE ;'
	psql -U gazelle evs-client-prod  -c ' ALTER TABLE tf_domain ADD column used_by_pr boolean ;'
	psql -U gazelle evs-client-prod  -c ' ALTER TABLE tf_domain_aud ADD column used_by_pr boolean ;'
	psql -U gazelle evs-client-prod  -c ' ALTER TABLE tf_rule ADD COLUMN name character varying(255) ;'
	psql -U gazelle  gazelle  -c ' DROP SCHEMA "_TF" CASCADE ;'
	cecho -green "init slony on master model"
	/home/gazelle/slony/scripts/init_gmm.sk
	cecho -green "init slony on evs"
	/home/gazelle/slony/scripts/init_evs.sk
	/etc/init.d/slony_sync start
	cecho -green "start replication"
	/home/gazelle/slony/scripts/replicate_evs.sk

  fi
}

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
  cecho -red "This script must be run as root" 1>&2
  exit 1
fi
cecho -blue "This script will install Slony on this machine "
what_to_do
install_slony
install_slony_scripts
start_slony


