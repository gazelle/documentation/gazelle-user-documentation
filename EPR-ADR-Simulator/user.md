---
title:  User Manual
subtitle: ADR Simulator
author: Youn Cadoret
function: Developer
date: 02/06/2019
toolversion: 1.3
version: 1.0
status: Approved Document
reference: KER1-MAN-IHE-ADR_SIMULATOR_-0_01
customer: IHE-EUROPE
---

eHealthSuisse ADR Provider MockUp
---------------------------------

eHealthSuisse ADR Provider MockUp simulates a CH:ADR Provider actor.

ADR provider, will allow to manage access policy to the clinical data stored by an XDS Document Registry as well as to the access policies themselves, which are stored in a Policy Repository.  
With the information insert into the request, the ADR provider could determine if user can access to the information or not, with decision like **"Permit"**, **"NotApplicable"**, **"Deny"** or **"Indeterminate"**.

Whether it will be for the access request XDS, ATC or PPQ, the ADR request is build in two parts.  
The first part,that we will call "Subject", allows to inform information on user with a ID,a homeCommunity and the qualification id.  
Finally, the second part, that we will call "Resource", allows to inform information on the patient, like his identification.



*   **Permit**: the evaluation was successful.
*   **NotApplicable**: the evaluation was successful, but the patient no granted rights to the subject.
*   **Deny**: the Subject is not authorised to perform the Action on the Resource.
*   **Indeterminate**: the evaluation failed or, if access to the requested Resource is not managed by the Authorization Decisions Manager.

### Data Set

#### Healthcare Professional data

| subject-id | IdP Simulator username |
|------------|------------------------|
| 7601002469191  |  aandrews    |
| 7601002467373  |  rreynolds   |
| 7601002466565  |  mmarston    |
| 7601002468282  |  cbouchard   |


#### Patient data

| extension-id | root-id | homeCommunityId | IdP Simulator username  |
|--------------|---------|-----------------|-------------------------|
|  761337610411265304            |  2.16.756.5.30.1.127.3.10.3       | urn:oid:1.3.6.1.4.1.21367.2017.2.6.2                |  nwittwerchristen  |
|  761337610435209810            |  2.16.756.5.30.1.127.3.10.3       | urn:oid:1.3.6.1.4.1.21367.2017.2.6.2                |  bovie             |    
|  761337614194129525            |  2.16.756.5.30.1.127.3.10.3       | urn:oid:1.3.6.1.4.1.21367.2017.2.6.2                |                    |




### End Point

[https://ehealthsuisse.ihe-europe.net:10443/adr-provider?wsdl](https://ehealthsuisse.ihe-europe.net:10443/adr-provider?wsdl)

It requires TLS mutual authentication with testing certificate (from GSS PKI). [The wsdl can be browsed here](/adr-provider?wsdl)

### Request example for ADR due to XDS

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:epr="urn:e-health-suisse:2015:policy-administration" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Header>
      <wsa:Action>urn:e-health-suisse:2015:policy-enforcement:AuthorizationDecisionRequest</wsa:Action>
      <wsa:MessageID>urn:uuid:e4bb38c7-e546-4bb1-8d68-2bccf783dfbf</wsa:MessageID>
      <wsa:To>http://ehealthsuisse.ihe-europe.net/adr-provider</wsa:To>
      <wsse:Security>
            <!-- Add an assertion here <saml2:Assertion.... -->
      </wsse:Security>
   </soap:Header>
   <soap:Body>
      <xacml-samlp:XACMLAuthzDecisionQuery InputContextOnly="false" ReturnContext="false" ID="_682fee8b-46c0-442a-8c54-fd9d656412fc" Version="2.0" IssueInstant="2019-02-05T14:48:29Z" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" xmlns:hl7="urn:hl7-org:v3">
         <xacml-context:Request>
            <xacml-context:Subject>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id" DataType="http://www.w3.org/2001/XMLSchema#string">
                  <xacml-context:AttributeValue>7601000050717</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier" DataType="http://www.w3.org/2001/XMLSchema#string">
                  <xacml-context:AttributeValue>urn:gs1:gln</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:ihe:iti:xca:2010:homeCommunityId" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:2.0:subject:role" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="PAT" codeSystem="2.16.756.5.30.1.127.3.10.6" displayName="Patient(in)"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xspa:1.0:subject:organization-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" displayName="Normalzugriff"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Subject>


            <xacml-context:Resource>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:epr-subset:761337610436974489:normal</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:epr-spid" DataType="urn:hl7-org:v3#II">
                  <xacml-context:AttributeValue>
                     <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610436974489"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:ihe:iti:xds-b:2007:confidentiality-code" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="17621005" codeSystem="2.16.840.1.113883.6.96" displayName="normal"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Resource>

            <xacml-context:Resource>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:epr-subset:761337610436974489:restricted</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:epr-spid" DataType="urn:hl7-org:v3#II">
                  <xacml-context:AttributeValue>
                     <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610436974489"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:ihe:iti:xds-b:2007:confidentiality-code" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="263856008" codeSystem="2.16.840.1.113883.6.96" displayName="restricted"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Resource>

            <xacml-context:Resource>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:epr-subset:761337610436974489:secret</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:epr-spid" DataType="urn:hl7-org:v3#II">
                  <xacml-context:AttributeValue>
                     <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610436974489"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:ihe:iti:xds-b:2007:confidentiality-code" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="1141000195107" codeSystem="2.16.756.5.30.1.127.3.4" displayName="secret"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Resource>


            <xacml-context:Action>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:ihe:iti:2018:RestrictedUpdateDocumentSet</xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Action>

            <xacml-context:Environment/>
         </xacml-context:Request>
      </xacml-samlp:XACMLAuthzDecisionQuery>
   </soap:Body>
</soap:Envelope>
```


### Request example for ADR due to PPQ

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:epr="urn:e-health-suisse:2015:policy-administration" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Header>
      <wsa:Action>urn:e-health-suisse:2015:policy-enforcement:AuthorizationDecisionRequest</wsa:Action>
      <wsa:MessageID>urn:uuid:e4bb38c7-e546-4bb1-8d68-2bccf783dfbf</wsa:MessageID>
      <wsa:To>http://ehealthsuisse.ihe-europe.net/adr-provider?wsdl</wsa:To>
      <wsse:Security>
            <!-- Add an assertion here <saml2:Assertion.... -->
    </wsse:Security>
   </soap:Header>
   <soap:Body>
      <xacml-samlp:XACMLAuthzDecisionQuery InputContextOnly="false" ReturnContext="false" ID="_682fee8b-46c0-442a-8c54-fd9d656412fc" Version="2.0" IssueInstant="2019-02-05T14:22:29Z" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" xmlns:hl7="urn:hl7-org:v3">
         <xacml-context:Request>
            <xacml-context:Subject>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id" DataType="http://www.w3.org/2001/XMLSchema#string">
                  <xacml-context:AttributeValue>7601000050717</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier" DataType="http://www.w3.org/2001/XMLSchema#string">
                  <xacml-context:AttributeValue>urn:gs1:gln</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:ihe:iti:xca:2010:homeCommunityId" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:2.0:subject:role" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="PAT" codeSystem="2.16.756.5.30.1.127.3.10.6" displayName="Patient(in)"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xspa:1.0:subject:organization-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" displayName="Normalzugriff"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Subject>

            <xacml-context:Resource>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>5b15774d-61e2-4d73-98d4-15462f38d872</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:epr-spid" DataType="urn:hl7-org:v3#II">
                  <xacml-context:AttributeValue>
                     <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610436974489"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:policy-attributes:referenced-policy-set" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:policies:exclusion-list</xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Resource>

            <xacml-context:Action>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:policy-administration:AddPolicy</xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Action>

            <xacml-context:Environment/>
         </xacml-context:Request>
      </xacml-samlp:XACMLAuthzDecisionQuery>
   </soap:Body>
</soap:Envelope>
```


### Request example for ADR due to ATC


```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:epr="urn:e-health-suisse:2015:policy-administration" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Header>
      <wsa:Action>urn:e-health-suisse:2015:policy-enforcement:AuthorizationDecisionRequest</wsa:Action>
      <wsa:MessageID>urn:uuid:e4bb38c7-e546-4bb1-8d68-2bccf783dfbf</wsa:MessageID>
      <wsa:To>http://ehealthsuisse.ihe-europe.net/adr-provider</wsa:To>
      <wsse:Security>
              <!-- Add an assertion here <saml2:Assertion.... -->
      </wsse:Security>
   </soap:Header>
   <soap:Body>
      <xacml-samlp:XACMLAuthzDecisionQuery InputContextOnly="false" ReturnContext="false" ID="_682fee8b-46c0-442a-8c54-fd9d656412fc" Version="2.0" IssueInstant="2019-02-05T14:58:58Z" xmlns:xacml-samlp="urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol" xmlns:xacml-context="urn:oasis:names:tc:xacml:2.0:context:schema:os" xmlns:hl7="urn:hl7-org:v3">
         <xacml-context:Request>
             <xacml-context:Subject>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id" DataType="http://www.w3.org/2001/XMLSchema#string">
                  <xacml-context:AttributeValue>7601000050717</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier" DataType="http://www.w3.org/2001/XMLSchema#string">
                  <xacml-context:AttributeValue>urn:gs1:gln</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:ihe:iti:xca:2010:homeCommunityId" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:2.0:subject:role" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="PAT" codeSystem="2.16.756.5.30.1.127.3.10.6" displayName="Patient(in)"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xspa:1.0:subject:organization-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" DataType="urn:hl7-org:v3#CV">
                  <xacml-context:AttributeValue>
                     <hl7:CodedValue code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" displayName="Normalzugriff"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Subject>


            <xacml-context:Resource>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:epr-subset:761337610436974489:patient-audit-trail-records</xacml-context:AttributeValue>
               </xacml-context:Attribute>
               <xacml-context:Attribute AttributeId="urn:e-health-suisse:2015:epr-spid" DataType="urn:hl7-org:v3#II">
                  <xacml-context:AttributeValue>
                     <hl7:InstanceIdentifier root="2.16.756.5.30.1.127.3.10.3" extension="761337610436974489"/>
                  </xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Resource>


            <xacml-context:Action>
               <xacml-context:Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id" DataType="http://www.w3.org/2001/XMLSchema#anyURI">
                  <xacml-context:AttributeValue>urn:e-health-suisse:2015:patient-audit-administration:RetrieveAtnaAudit</xacml-context:AttributeValue>
               </xacml-context:Attribute>
            </xacml-context:Action>

            <xacml-context:Environment/>
         </xacml-context:Request>
      </xacml-samlp:XACMLAuthzDecisionQuery>
   </soap:Body>
</soap:Envelope>
```

## Mock messages on GWT

Messages sent to the simulator can be found in Mock Messages feature of Gazelle Webservice Tester.
This feature is documented at : [https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html).

Messages exchanged with EPR-ADR-Simulator can be found by filtering with the actor CH:ADR_PROVIDER.
