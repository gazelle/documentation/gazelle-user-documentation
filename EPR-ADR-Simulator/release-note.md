---
title:  Release note
subtitle: ADR Simulator
author: Youn Cadoret / Alexandre POCINHO
function: Developer
releasedate: 2023-06-13
toolversion: 1.3.7
reference: KER1-RNO-IHE-ADR_SIMULATOR
customer: IHE-EUROPE
---


# 1.3.7
_Release date: 2023-06-13_

__Task__
* \[[ADRPROV-17](https://gazelle.ihe.net/jira/browse/ADRPROV-17)\] Update specification for CH:ADR Response


# 1.3.6
_Release date: 2020-03-11_

__Bug__

* \[[ADRPROV-16](https://gazelle.ihe.net/jira/browse/ADRPROV-16)\] The displayName in the ADR simulator must be an optonal attribute



# 1.3.5
_Release date: 2019-12-31_

__Bug__

* \[[ADRPROV-12](https://gazelle.ihe.net/jira/browse/ADRPROV-12)\] [ADR due to PPQ] the referenced-policy-set attribute is written in hard in GWT's request
* \[[ADRPROV-14](https://gazelle.ihe.net/jira/browse/ADRPROV-14)\] [ADR] An error occurred when parsing this homeCommuntyId "urn:oid:2.16.756.5.30.1.194"
* \[[ADRPROV-15](https://gazelle.ihe.net/jira/browse/ADRPROV-15)\] Missing check when a HCP requests the ADR Provider for a write action and has delegation rights.


# 1.3.4
_Release date: 2019-10-09_

__Bug__

* \[[ADRPROV-11](https://gazelle.ihe.net/jira/browse/ADRPROV-11)\] Confidentiality Codes differ in Appendix 3/2.1 and published policy stack


# 1.3.3
_Release date: 2019-10-03_

__Bug__

* \[[ADRPROV-10](https://gazelle.ihe.net/jira/browse/ADRPROV-10)\] The writting rights for a DADM have changed


# 1.3.2
_Release date: 2019-09-03_

__Bug__

* \[[ADRPROV-9](https://gazelle.ihe.net/jira/browse/ADRPROV-9)\] The decision is "NotApplicable" instead of "Permit"

# 1.3.1
_Release date: 2019-08-30_

__Task__

* \[[ADRPROV-6](https://gazelle.ihe.net/jira/browse/ADRPROV-6)\] Create groovy script to record mock transaction


# 1.3.0
_Release date: 2019-08-02_

__Task__

* \[[ADRPROV-6](https://gazelle.ihe.net/jira/browse/ADRPROV-6)\] [ADR] Create groovy script to record mock transaction

# 1.2.0 (7c2a25de)
_Release date: 2019-06-19_

__Remarks__

* Update ADR Simulator [EPR 1.9]



# 1.1 (#67136)
_Release date: 2019-03-25_

__Remarks__

* Update ADR Simulator [EPR 1.8]


# 1.0 (#66468)
_Release date: 2019-02-20_

__Remarks__

* Update ADR Simulator [EPR 1.7]

__Improvement__

* \[[ADRPROV-5](https://gazelle.ihe.net/jira/browse/ADRPROV-5)\] Update ADR simulator [EPR 1.7] 

