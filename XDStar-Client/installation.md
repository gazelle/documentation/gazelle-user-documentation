---
title:  Installation Manual
subtitle: XD* Client
author: Nicolas BAILLIET
date: 30/08/2024
toolversion: 3.X.X
function: Engineer
version: 2.02
status: Approved document
reference: KER1-MAN-IHE-XDSTAR_CLIENT_INSTALLATION-2_02
customer: IHE-EUROPE
---

# XDStarClient - Installation & Configuration

Here is a guide to help you installing XDStarClient.

# Requirements

Debian squeeze or ubuntu 12.04/14.04 64bits with an Internet access.

* Database : PostGresql 9.4
* Java virtual machine : JDK 1.7
* Application server : Jboss 7.2.0.Final

To install those requirements you need to follow instructions of installation of jboss7 : [https://gazelle.ihe.net/content/general-considerations-jboss-7](https://gazelle.ihe.net/content/general-considerations-jboss-7)

# Quick start

XDStarClient can be installed using gazelle user on your server:

```bash
wget https://gazelle.ihe.net/jenkins/job/XDStarClient/ws/install_xdstar_client.sh
wget https://gazelle.ihe.net/jenkins/job/Installer_script/ws/jboss7/common.sh
sudo chmod +x install_xdstar_client.sh
sudo chmod +x common.sh
sudo ./install_xdstar_client.sh $HOSTANDPORT
```

`$HOSTANDPORT` are the host and the port number to which your application will deployed, you may use the value `localhost:8380` if you are testing locally and you have not changed the configuration of your jboss7 installed.

It will: 

1. Backup the previous EAR
1. Get the latest released version of XDStarClient and deploy it in your jboss.
1. Create the database if it doesn't exists.
1. Create /opt/XDStarClient folder (used for validation, samples storage) if this folder is not already there

All these steps are done by the script. The next paragraph are only details of how these steps are done (for developper who need more details).

# Compile Sources

XDStarClient is an open source project under Apache2 licence. The sources are available on the IHE's gitlab server:

```bash
git clone https://gitlab.inria.fr/gazelle/public/simulation/xdstar-modules
git clone https://gitlab.inria.fr/gazelle/public/simulation/xdstar-client
```

Before compiling, go to the directory XDStarClient and edit the file pom.xml. Open this file and adapt the properties of profile prod to your case :

`jdbc.connection.url` : `jdbc:postgresql:xdstar-client`
`jdbc.connection.user` / `jdbc.connection.password` : credentials for database access

```bash
cd XDStar-modules/
mvn clean install
cd ../XDStarClient/
mvn clean package -U -Pproduction
```
The archive (EAR) and the distribution file are created and placed into XDStarClient `/XDStarClient-ear/target` directory.
If maven cannot download dependencies checkout the maven configuration guide: [https://gazelle.ihe.net/content/development-support-tools](https://gazelle.ihe.net/content/development-support-tools)

# Database creation

The names of the databases are defined in the pom.xml file. Use the following commands :

```bash
su postgresql
psql
postgres=\# CREATE DATABASE "xdstar-client" OWNER gazelle ENCODING UTF-8;
postgres=\# \\q
exit

```

# Deployment

To deploy XDStar Client :
1. Paste the archive **XDStarClient.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`
2. Display JBoss server logs, start JBoss and wait for **ear** deployment.
3. The application can be browsed at http://localhost:8080/XDStarClient. *Port could also be different whether you have modified the JBoss configuration*.
4. **WARNING** : From version 2.4.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file XDStarClientDS-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : XDStarClientDS

Database name : xdstar-client

# Insert configuration values

To insert values you can connect to the database:

    psql -U gazelle xdstar-client

Then copy paste the folowing script:

```sql
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'cda.xsl', 'cda_xsl_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'XDR Test Pratician', 'default_pratician_id');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'UTC+01', 'application_time_zone');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/jira/browse/XDSTAR', 'application_issue_tracker_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'XDStarClient', 'application_name');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/jira/browse/XDSTAR#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel', 'application_release_notes_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/content/xdstarclient', 'app_documentation');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/xsl/xdsDetailedResult.xsl', 'validation_result_xslt');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'NOVAK', 'family');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'JAN', 'given');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '19501201', 'birthdate');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'root');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'gender');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'extension');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'street');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'city');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'country');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'postalcode');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'maidenname');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '50000', 'connection_timeout');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/xsl/mbcdaDetailedResult.xsl', 'cda_mbv_xslt');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/usr/bin/', 'dicom3tools_validator');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/xsd/IHE/XDSI.b_ImagingDocumentSource.xsd', 'xds_xsd');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/xsd/DSUB/soap-envelope.xsd', 'dsub_xsd');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/', 'server_local_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'hra@kereval.com', 'application_admin_email');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'N/A', 'consent_template_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'N/A', 'dds_web_service');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'N/A', 'dispensation_template_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'N/A', 'epsosFolder');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/uploadedFiles', 'registered_files_directory');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/tmp', 'uploadFolder');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'N/A', 'xdstool_xca_query_wsdl');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'SAMEORIGIN', 'X-Frame-Options');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'private, no-cache, no-store, must-revalidate, max-age=0', 'Cache-Control');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'max-age=31536000 ; includeSubDomains', 'Strict-Transport-Security');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '', 'X-Content-Security-Policy');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'Use X-Content-Security-Policy values', 'X-WebKit-CSP');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'Use X-Content-Security-Policy-Report-Only values', 'X-WebKit-CSP-Report-Only');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'false', 'sql_injection_filter_switch');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'true', 'security-policies');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/china.jks', 'truststore_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'tomcat', 'key_alias');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'password', 'key_pass');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'password', 'keystore_pass');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'true', 'avoid_nist_validation_ihe');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/SchematronValidator-SchematronValidator-ejb/ScchematronValidatorWS?wsdl', 'schematron_validator');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'default-src ''self'' *.ihe.net *.ihe-europe.net *.ihe-c.org; script-src ''self'' ''unsafe-eval'' ''unsafe-inline''; style-src ''self'' ''unsafe-inline'';', 'X-Content-Security-Policy-Report-Only');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '1.3.6.1.4.1.12559.11.13.2.1.', 'document_root_oid');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '1.3.6.1.4.1.12559.11.13.2.2', 'epsos_source_root_oid');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '1.3.6.1.4.1.12559.11.13.2.3', 'homeCommunityID');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '1.3.6.1.4.1.12559.11.13.2.4', 'home_root_oid');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '1.3.6.1.4.1.12559.11.13.2.5', 'ihe_source_root_oid');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '1.3.6.1.4.1.12559.11.13.2.6.', 'submission_set_root_oid');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'Hilary Ramanantsalama', 'application_admin_name');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/AssertionManagerGui', 'assertion_manager_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'false', 'display_KSA_menu');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/attachments/', 'attachement_files_directory');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/CDAGenerator-CDAGenerator-ejb/CDAValidatorWS?wsdl', 'cda_mbv_wsdl');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/CDAGenerator-CDAGenerator-ejb/CDAValidatorWS?wsdl', 'cda_mbv_wsdl');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'http://localhost/XDStarClient-XDStarClient-ejb/DSUBRecipientWS?wsdl', 'xdstar_dsub_recipient_endpoint');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/XDStarClient/', 'gazelle_home_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '2.16.840.1.113883.3.3731.1.1.100.1', 'ksa_aa_oid');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'http://localhost/XDStarClient-XDStarClient-ejb/XDSDeletetWS?wsdl', 'xdstar_delete_doc_endpoint');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/home/aboufahj/Téléchargements/36667D66.dcm', 'ksa_manifest_file');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator', 'svs_repository_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/XDStarClient/', 'application_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/EVSClient/', 'evs_client_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/xdstools2/', 'ksa_xdstools_url');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'false', 'display_IHE_RAD_menu');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'false', 'display_IHE_ITI_menu');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'false', 'ip_login');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'true', 'cas_enabled');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '.*', 'ip_login_admin');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '20', 'NUMBER_OF_ITEMS_PER_PAGE');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '/opt/gazelle/cert/keystore.jks', 'keystore_path');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/user.html', 'documentation_url');

```

Finally When the application is deployed, open a browser and go to [http://localhost:8080/XDStarClient](http://localhost:8080/XDStarClient).
If the deployment and the database initialization are successful you should see the home page.

## List of application preferences

| **Properties**                            | **Description**                                                                                                                            | **Default**                                                                                                                                      |
|-------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| **app\_documentation**                    | official documentation                                                                                                                     | https://gazelle.ihe.net/content/xdstarclient                                                                                                      |
| **application\_admin\_email**             | email of the admin                                                                                                                         | hra@kereval.com                                                                                                               |
| **application\_admin\_name**              | Admin name                                                                                                                                 | Hilary Ramanantsalama                                                                                                                              |
| **application\_issue\_tracker\_url**      | issues tracker's url                                                                                                                       | https://gazelle.ihe.net/jira/browse/XDSTAR                                                                                                        |
| **application\_name**                     | application name                                                                                                                           | XDStarClient                                                                                                                                     |
| **application\_release\_notes\_url**      | release notes' url                                                                                                                         | https://gazelle.ihe.net/jira/browse/XDSTAR\#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel                                |
| **application\_time\_zone**               | time zone                                                                                                                                  | UTC+01                                                                                                                                           |
| **application\_url**                      | the url of XDStarClient (too important as it is used by permanent links)                                                                   | https://gazelle.ihe.net/XDStarClient/                                                                                                            |
| **assertion\_manager\_url**               | URL to the assertion manager used                                                                                                          | https://gazelle.ihe.net/AssertionManagerGui                                                                                                       |
| **attachement\_files\_directory**         | file directory on the server, where documnent uploaded and downloaded will be saved                                                        | /opt/XDStarClient/attachments/                                                                                                                   |
| **avoid\_nist\_validation\_ihe**          | set to false when validation using the nist validator is performed                                                                         | true                                                                                                                                             |
| **birthdate**                             | used for initialization for XCPD request                                                                                                   | 19501201                                                                                                                                         |
| **Cache-Control**                         | security parameter                                                                                                                         | private, no-cache, no-store, must-revalidate, max-age=0                                                                                          |
| **cda\_mbv\_wsdl**                        | link to the CDA MBV tool ws                                                                                                                | https://gazelle.ihe.net/CDAGenerator-CDAGenerator-ejb/CDAValidatorWS?wsdl                                                                         |
| **cda\_mbv\_xslt**                        | link to the stylesheet to render the result of validation of a CDA document                                                                | https://gazelle.ihe.net/xsl/mbcdaDetailedResult.xsl                                                                                               |
| **cda\_xsl\_path**                        | link to the stylesheet to render a cda document                                                                                            | cda.xsl                                                                                                                                          |
| **city**                                  | used for initialization for XCPD request                                                                                                   | Valeur de la préférence                                                                                                                          |
| **connection\_timeout**                   | When the client want to rich a responder, this represent the time in millisecond, that the client shall wait before closing the connection | 50000                                                                                                                                            |
|**consent_template_path**                  |                                             | N/A          |
| **country**                               | used for initialization for XCPD request                                                                                                   | Valeur de la préférence                                                                                                                          |
| **dds_web_service**                               | used to call DDS webservice                                                                                                  | N/A                                                                                                                         |
| **default\_pratician\_id**                | used for epSOS purpose                                                                                                                     | XDR Test Pratician                                                                                                                               |
| **dicom3tools\_validator**                | path where the dicom3tools is installed                                                                                                    | /usr/bin/                                                                                                                                        |
| **dispensation\_template\_path**          | used for epSOS purpose                                                                                                                     | N/A                                                                                                                                              |
| **display\_IHE\_ITI\_menu**               | display, or not, the IHE ITI transactions                                                                                                  | false                                                                                                                                            |
| **display\_IHE\_RAD\_menu**               | display, or not, the IHE RAD transactions                                                                                                  | false                                                                                                                                            |
| **display_KSA_menu**               | display, or not, the KSA transactions                                                                                                  | false                                                                                                                                            |
| **document\_root\_oid**                   | unique identifier of documents                                                                                                             | 1.3.6.1.4.1.12559.11.13.2.1.                                                                                                                     |
| **documentation_url**                   | link to the documentation                                                                          | https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/user.html                                                                                                                   |
| **dsub\_xsd**                             | path to the schema of DSUB                                                                                                                 | /opt/XDStarClient/xsd/DSUB/soap-envelope.xsd                                                                                                     |
| **epsosFolder**                           | used for epSOS purpose                                                                                                                     | N/A                                                                                                                                              |
| **epsos\_source\_root\_oid**              | used for epSOS purpose                                                                                                                     | 1.3.6.1.4.1.12559.11.13.2.2                                                                                                                      |
| **evs\_client\_url**                      | link to evs client tool                                                                                                                    | https://gazelle.ihe.net/EVSClient/                                                                                                               |
| **extension**                             | used for initialization for XCPD request                                                                                                   | Valeur de la préférence                                                                                                                          |
| **family**                                | used for initialization for XCPD request                                                                                                   | NOVAK                                                                                                                                            |
| **gazelle\_home\_path**                   | path to a folder used by the tool                                                                                                          | /opt/XDStarClient/                                                                                                                               |
| **gender**                                | used for initialization for XCPD request                                                                                                   | Valeur de la préférence                                                                                                                          |
| **given**                                 | used for initialization for XCPD request                                                                                                   | JAN                                                                                                                                              |
| **homeCommunityID**                       | used by the XSD.b requests                                                                                                                 | 1.3.6.1.4.1.12559.11.13.2.3                                                                                                                      |
| **home\_root\_oid**                       | used by the XSD.b requests                                                                                                                 | 1.3.6.1.4.1.12559.11.13.2.4                                                                                                                      |
| **ihe\_source\_root\_oid**                | used by the XSD.b requests                                                                                                                 | 1.3.6.1.4.1.12559.11.13.2.5                                                                                                                      |
| **key\_alias**                            |  the alias of the keystore used for ssh connexion with responders                                                                          | tomcat                                                                                                                                           |
| **key\_pass**                             |  password of the key                                                                                                                       | password                                                                                                                                         |
| **keystore\_pass**                        |  password of the keystore                                                                                                                  | password                                                                                                                                         |
| **keystore_path**                        | the keystore location                                                                                                                  | opt/gazelle/cert/keystore.jks                                                                                                                                         |
| **ksa\_aa\_oid**                          |  assigning authority OID, for patients                                                                                                     | 2.16.840.1.113883.3.3731.1.1.100.1                                                                                                               |
| **ksa\_manifest\_file**                   |  Manifest used in test plan, only related to k-project                                                                                     | /../36667D66.dcm                                                                                                                                 |
| **ksa\_xdstools\_url**                    |  link to xdstools2 (only related to k-project)                                                                                             | https://gazelle.ihe.net/xdstools2/                                                                                                               |
| **maidenname**                            |  used for initialization of XCPD request                                                                                                   | Valeur de la préférence                                                                                                                          |
| **NUMBER_OF_ITEMS_PER_PAGE**                            |                                               | 20                                                                                     |
| **postalcode**                            | used for initialization of XCPD request                                                                                                    | Valeur de la préférence                                                                                                                          |
| **registered\_files\_directory**          |  used by the ITI-41 transaction                                                                                                            | /opt/XDStarClient/uploadedFiles                                                                                                                  |
| **root**                                  | used for initialization of XCPD request                                                                                                    | Valeur de la préférence                                                                                                                          |
| **schematron\_validator**                 | path to the ws of schematronValidator                                                                                                      | https://gazelle.ihe.net/SchematronValidator-SchematronValidator-ejb/SchematronValidatorWS?wsdl                                                 |
| **security-policies**                     | confirm if security is used                                                                                                                | true                                                                                                                                             |
| **server\_local\_path**                   |  path to a folder on the server                                                                                                            | /opt/XDStarClient/                                                                                                                               |
| **sql\_injection\_filter\_switch**        |  security parameter                                                                                                                        | false                                                                                                                                            |
| **street**                                | used for initialization of XCPD request                                                                                                    | Valeur de la préférence                                                                                                                          |
| **Strict-Transport-Security**             |   security parameter                                                                                                                       | max-age=31536000 ; includeSubDomains                                                                                                             |
| **submission\_set\_root\_oid**            |  used by ITI-41 transaction                                                                                                                | 1.3.6.1.4.1.12559.11.13.2.6.                                                                                                                     |
| **svs\_repository\_url**                  |  link to SVS rest service                                                                                                                  | https://gazelle.ihe.net:8180/SVSSimulator/rest/RetrieveValueSetForSimulator                                                                       |
| **truststore_path**                  |  path to the keystore                                                                                                                      | /opt/china.jks                                                                                                                                   |
| **uploadFolder**                          |  path to folder for uploading file                                                                                                         | /opt/XDStarClient/tmp                                                                                                                            |
| **validation\_result\_xslt**              |  path to xds transformer of validation result                                                                                              | https://gazelle.ihe.net/xsl/xdsDetailedResult.xsl                                                                                                 |
| **X-Content-Security-Policy**             |  security parameter                                                                                                                        | Valeur de la préférence                                                                                                                          |
| **X-Content-Security-Policy-Report-Only** |  security parameter                                                                                                                        | default-src 'self' \*.ihe.net \*.ihe-europe.net \*.ihe-c.org; script-src 'self' 'unsafe-eval' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; |
| **xdstar\_delete\_doc\_endpoint**         |  endpoint link to the delete webservice                                                                                                    | http://127.0.0.1:8180/XDStarClient-XDStarClient-ejb/XDSDeletetWS?wsdl                                                                            |
| **xdstar\_dsub\_recipient\_endpoint**     |   endpoint link to the dsub recipient webservice                                                                                           | http://127.0.0.1:8180/XDStarClient-XDStarClient-ejb/DSUBRecipientWS?wsdl                                                                         |
| **xds\_xsd**                              |  link to the schema of XDS-I.b                                                                                                             | /opt/XDStarClient/xsd/IHE/XDSI.b\_ImagingDocumentSource.xsd                                                                                      |
| **xdstool_xca_query_wsdl**                              |                                                     | N/A                                                                                      |
| **X-Frame-Options**                       |  security parameter                                                                                                                        | SAMEORIGIN                                                                                                                                       |
| **X-WebKit-CSP**                          |   security parameter                                                                                                                       | Use X-Content-Security-Policy values                                                                                                             |
| **X-WebKit-CSP-Report-Only**              |  security parameter                                                                                                                        | Use X-Content-Security-Policy-Report-Only values                                                                                                 |

## SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).