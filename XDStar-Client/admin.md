---
title:  Administration Guide
subtitle: XD* Client
author: Nicolas BAILLIET
date: 26/06/2023
toolversion: 2.5.x
function: Engineer
version: 1.02
status: Approved
reference: KER1-MAN-IHE-XDSTAR_CLIENT_ADMIN-1_01
customer: IHE-EUROPE
---

# How to add new transaction?

Go to Administration -> Transactions-Types.
  * Create new Transaction, click on "Create Transaction"

  ![](./media/admin/transaction.png)

  * If the transaction is from profile that is not yet defined, create new affinity domain. That matches the new transaction with the new profile. To do that, click on "Create AffinityDomaine"

  ![](./media/admin/affinity_domain.png)

  If the profile already exists, click on the "pencil" button to edit the affinity domain adding a transaction to a profile.

  * Create a transaction type, click on "Create Transaction Type"

  ![](./media/admin/transaction_types.png)

Go to XDS-Metadata -> AffinityDomain-Transaction
  * Add new usage, click on "Add new usage"

  ![](./media/admin/usages.png)

Go to XDS-Metadata -> Extrinsic-Objects
  * Edit an existing extrinsic-object or add a new one

  ![](./media/admin/extrinsic_objects.png)

  * Add the new usage in the Extrinsic-object

  ![](./media/admin/extrinsic_object_editor.png)

Go to XDS-Metadata -> Registry Packages
  * Edit an existing Registry Package, or add a new one

  ![](./media/admin/registry_package.png)

  * Add the new usage in the Registry Package

  ![](./media/admin/registry_package_editor.png)

Go to Administration -> Configurations Types list
  * Add the new usage in the corresponding configuration type.

  ![](./media/admin/configuration_types.png)

# How to create new validator?

![](./media/admin/validator_pack_class.png)

There are two ways to create validator :
  * Importing XML Validator by clicking on "XML Validator import"
  * Editing on the GUI by clicking on "Add new validator"

![](./media/admin/validator_pack_class_editor.png)

| **Fields** | **Description** |
|-------------|-----------------------------------------------------------|
| **Validator is desactivated?** | Check this box if the validator shall not be used |
| **Validator Name** | The name shall be precise, it shall start with the domain (used as distinguisher in EVSClient) |
| **Version** | Validator version |
| **Technical Framework reference** | where the profile is specified |
| **Namespace** | Namespace used in the message |
| **Extract Node** | Message type (ex : retrieve document set request, registry request, ... ) |
| **Pack class** | Validator types required for the message type |
| **Adhoc Query Metadata** | If the message is an Adhoc Query, this is the Adhoc Query type |
| **Registry Object List** | If the message contains registry object list, precise which one is used |
| **Usages** | The AffinityDomain of the validator |
| **Has nist validation** | Check the box if the validator has nist validation |
| **Metadatacodes** | From which domain the codes are taken |
| **isRegister** | Check this box if the message is a register |
| **isPnR** | Check this box if the message is a Provide and register |
| **iXca** | Check this box if the message is an XCA |
| **isXdr**| Check this box if the message is an XDR |

It is possible to add extra constraints.

# Diagram to describe how validators are organized in the XDStarClient

![](./media/admin/diagram-xdstar-validator.png)

## How to add a valueSet control in a validator ?

The validators are defined in the **XDS-Metadata > Validator Pack association** section.
When we edit a validator, we can select a RegistryObjectList from the Registry Object List Metadata field.

![](./media/admin/pack_class_association.png)


This RegistryObjectList is defined in the **XDS-Metadata > RegistryObjectList** section.
When we edit the RegistryObjectList metadata, we can select several ExtrinsicObjectMetadata or/and RegistryPackageMetadata.

The values of these different fields are defined in the **XDS-Metadata > Extrinsic-Object** and **XDS-Metadata > Registry-Package** sections.

![](./media/admin/registry_object_list_metadata.png)

In the Extrinsic-Object and the Registry-Package, at the classification fields,
we have the list of ClassificationMetaData managed by this ExtrinsicObjectMetaData or this RegistryPackageMetaData.

![](./media/admin/extrinsic_object_metadata.png)


All these ClassificationMetaData are defined in the **XDS-Metadata > Classifications** section and contain an identifier to refer to a value defined in SVSSimulator.

![](./media/admin/list_of_classification.png)

### How to link SVS Value Set to a XDS validator ?

SVSSimulator contains the values to use in a context definition.
For XDStarClient, it allows checking that a message to be validated respects the defined context.

In XDStarClient, valueSets are defined in the **XDS-Metadata > Classification section.**
Each ClassificationMetaData is linked to an SVS valueSet thanks to an identifier.


![](./media/admin/classification_metadata_editor.png)


  * Edit the value of **Valueset**
  * Edit in the *codingScheme* in the **Slot** the value of the valueset:

![](./media/admin/edit_slot.png)

Thus, through the RegistryObjectList metadata selected in the association of the validation pack,
we can check in a message if all the metadata values are valid in the defined context.
