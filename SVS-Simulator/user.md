---
title:  User Manual
subtitle: SVS Simulator
author: Nicolas Bailliet
date: 06/02/2024
toolversion: 3.X.X
function: Software Engineer
version: 1.04
status: Approved document
reference: KER1-MAN-IHE-SVS_SIMULATOR_USER-1_04
customer: IHE-EUROPE
---

# SVS Simulator – User Guide

# Introduction

The SVS Simulator emulates the Value Set Consumer and Value Set Repository actors defined in the IT-Infrastructure technical framework.
The table below gathers the supported transactions by the simulator:

|  **Simulated actor**   | **Transaction**   | **Type**      |
|------------------------|-------------------|---------------|
| SVS Consumer           | ITI-48            | HTTP / REST   |
| SVS Consumer           | ITI-60            | HTTP / REST   |
| SVS Consumer           | ITI-48            | SOAP          |
| SVS Consumer           | ITI-60            | SOAP          |
| SVS Repository         | ITI-48            | HTTP / REST   |
| SVS Repository         | ITI-60            | HTTP / REST   |
| SVS Repository         | ITI-48            | SOAP          |
| SVS Repository         | ITI-60            | SOAP          |

 

## What is this simulator able to do?

This simulator has been developed with the purpose of helping developers of IHE systems to test their systems with another IHE compliant system off connectathon periods.
You will be able to use four different components in the SVS Simulator:

* SVS Browser
* SVS Consumer
* SVS Repository
* SVS Validator

## Logged in to get more features

As some others applications from the Gazelle testing platform, SVS Simulator application includes the CAS system. That means that you can logged into the application using your "European" Gazelle account (the one created on the European instance of *TestManagement*). Once you are logged in, you can decide to be the only one to be allowed to send messages to the SUT you have configure in the application (see next section).

# SVS Validator

The SVS Simulator contains a module dedicated to the validation of requests and responses exchanged within SVS transactions. This validator is model-based and its core as been generated using the XML schemas provided by IHE, that is to say:

* [*SVS.xsd*](ftp://ftp.ihe.net/TF_Implementation_Material/ITI/schema/IHE/SVS.xsd%20) (for ITI-48)
* [*ESVS.xsd*](ftp://ftp.ihe.net/TF_Implementation_Material/ITI/schema/IHE/ESVS-20100726.xsd) (for ITI-60)

The validator performs three levels of checks:

* Is the document a well-formed XML document?
* Is the document valid against the XSD (SVS.xsd or ESVS.xsd depending the cases)?
* Is the document valid against the model and does it respect the constraint defined in the technical framework?

The SVS Validator is available through a SOAP web service, you can find the definition of this service at the following location: [*SVS Validator web service*](https://gazelle.ihe.net/SVSSimulator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl).

A Java client to call this web service is available in the gazelle-ws-clients module deployed on our Nexus repository.

Our EVS Client application contains a user friendly interface to access this validation service. You will only have to upload the XML file to validate and choose the validator to apply (ITI-48 request/response, ITI-60 request/response). See [*https://gazelle.ihe.net/EVSClient/svs/validator.seam*](https://gazelle.ihe.net/EVSClient/svs/validator.seam).


# Value Set Repository actor

The SVS Simulator is able to act as a Value Set Repository. It currently supports both HTTP binding and SOAP Binding.

The information about the repository (endpoint, details, …) is available on the home page of the application and in the “ Simulators Value Set Repository” page.
Since you send a request to our repository the transactions will be save in our database. You can consult them in the “Messages” page (see more details in the associate section).
Before sending a request to our repository you can browse the available value sets contained in the simulator's database under the menu “Value Set Browser” (see more details in the associate section) to know which request will return result.
You can consult the documentation (section 21): *IHE ITI Technical Framework - Volume 1*.

# Value Set Consumer actor

## Adding your system (SUT) in SVS Simulator

In order to send messages to your system under test, the SVS Simulator tool needs the configuration (Name, Endpoint, BindingType) of your system. This configuration is stored in the simulator, so that you can re-use it each time you need to perform a test. In order to register the configuration of your system under test, go to "SUT Configurations" and hit the "Add new SUT" button. You can also see ![](./media/image20.png) or Edit ![](./media/image21.png) an existing configuration.
When you add a new system configuration the simulator needs to know:

* A name for your configuration (displayed in the drop-down list SUT)
* The endpoint where requests will be sent
* The Binding Type of your SUT

If you are logged in when creating the configuration, you will be set as the owner of the configuration. If you do not want other testers to send messages to your SUT you can untick the box "Shared" and you will be the only one to be able to select your system in the drop-down list (if logged in !) and see it in the SUT Browser.
If your system implements several Binding Type, you are expected to create a configuration for each of them.
If you’re logged in admin mode, an additional icon is available on the “SUT Configuration” page (![](./media/image22.png)) which allows you to deactivate a SUT. Once, a SUT is deactivated, only admin user can see him and activate it again.

## How to use SVS Consumer

The SVS Simulator is able to act as a Value Set Consumer. It currently supports both HTTP and SOAP binding.

You will be able to send a request to a SUT previously registered.
First select the request type (HTTP / SOAP) and the “System Under Test” list will be loaded. If you can't see your system, you may have set it as "not shared" and you are not logged in, log onto the application to see it.
Next, select the SVS transaction you want to perform (ITI-48 or ITI-60)
 

Fill the form with the parameters and click the “Send” button, your system under test may receive the request generated by the simulator.

You can see the content of you response, in terms of value set(s), concept list(s) and concept(s) in a new panel : "Response content".

![](./media/response_content.png)

The button "Import Result into Repository..." will redirect you to a new page where you will be able to save the components (values set(s), concept list(s) and concept(s)) extracted from the response in the database.
You will have an overview on potential modifications (by comparison with actual stored data in the database). It is then possible to save them by clicking on "Import Changes button" or to go back on SVS Consumer page with the Back button without saving anything.

If your SUT is listening and sends back a response, the messages sent and received by the simulator will be displayed at the bottom of the page. You can see the content of both the request and the response and validate them. This will call the validator embedded in the simulator, as described before.
All the transactions instances are stored in the database simulator and available to the community. If you rather prefer not to have those messages publish, contact the administrator of the application and provide him/her with the ids of the messages to hide.

# Messages

All the transactions played against the simulator are stored in the simulator's database. You can consult the whole list of transactions under the "Messages" menu.

All those transactions can be validated by the SVS Validator, which allows you to know if the request and the response respect the technical framework defined by IHE.

 

If you click on the ![](./media/image23.png) icon it will call the validator for both the request and the response (if they are XML formatted).

In the Message Display page (after you click on the glass), you can see the request and the response in XML or HTML.
The Detailed Result of the validation is also display if you have already validated the transaction.

 

Below are the icons you can find in the "Messages" page:

| ![](./media/image20.png)    | Open the details of the transaction                   |
|-------------------------------------------------------------|-------------------------------------------------------|
| ![](./media/image23.png)    | The message has not been validated yet.               |
| ![](./media/image24.png)    | The message has been successfully validated.          |
| ![](./media/image25.png)    | The message has been validated but contains errors.   |

# SVS Browser - Value Sets

 The Value Set Repository actor uses the simulator's database to reply to the SVS requests it receives. To populate the database, a user interface is available. Here is a tutorial on how to browse value set.

If you click on the “SVS Browser” menu you will be redirect on the browser value set page which allows browse the content of our value set repository.

 You can use filter to search for a specific ValueSet.
If you click on the ![](./media/image20.png) icon you will be able to see more details about the value set, all its concept list and associates concepts.

You can click on a Value Set OID to be redirect in another tab to the REST request for this value set (ITI-48 request).

 
If you need to add specific value set, contact an admin.

# \[ADMIN\] Manage value sets

 The Value Set Repository actor uses the simulator's database to reply to the SVS requests it receives. To populate the database, a user interface is available. Here is a tutorial on how to create/update value sets.

If you’re logged as admin you can see more options in the Browser page.
There are additional buttons available:
- “Add a new value set”, can be used to reach the "Add value set" page. Value set will be created from scratch.
- “Import Value Set from file”, can be used to import value set from SVS formatted files. That means that files must be XML file representing a RetrieveValueSetResponse or RetrieveMultipleValueSetsResponse.
You can also see two more icons in the datatable rows, ![](./media/image21.png) to edit a value set, and ![](./media/image25.png) to delete the value set (functionality available on the edit page too).

## Add a value set

If you want to add a value set click on the associate button and fill the form with general information about the value set. Once you finished this part click on the “Save Value Set” button to save modification and now be allowed to add lists of concepts with the button “Add new ConceptList” or “Add new Translation” (if you have already defined at least one concept list in the Value Set).

When you click on the button “Add Concept List” a pop-up will raised asking you the language of the concept list you will create. Fill the input and click on “Add” button.
Now the Concept List is created, you can click on “Add Concept” to set a new Concept for all Concept List of the value set. You can also translate the existing concept in the new language.

You can click on the “Translation panel” button to open a specific module which allows you to compare two or more concept lists in order to translate them more easily.
 

You can delete concepts in the main concept list and it will delete it for all concept lists of the value set. If you edit a code in the main concept list it will update it for all concept lists.

If you edit “codeSystem”, “codeSystemName” or “codeSystemVersion” it will update it for all the concepts of the Value Set.
 

Link anchor are available to navigate more comfortably when there’s a lot ofconcept lists.

## Import value sets

If you want to import a value set click on the associate button.
You can select xml file or zip file (contains xml) to import value set.

Your xml files need to represents RetrieveValueSetResponse or RetrieveMultipleValueSetsResponse to be use by our import system.

When you select a file it will be downloaded and the application will extract the value set(s) from it.

**Prior to upload your file**, you can check/uncheck 3 checkboxes enabling update processes :

* Review changes before saving (cheked by default) to let you confirm import instead of doing it immediatly
* Update value set attributes when redefined in the uploaded file
* Delete concepts when not referenced in the uploaded file

You will then see the Value Set(s) Informations panel filled in with updated resulting from file value set extraction.
Every component (value set/concept list/concept) updated will appear in that panel, whether it has been added, updated or deleted.

A detailed report will be display once the importation is done.

It is recommended not to gather more than 100 xml files in one zip archive, otherwise the import may not complete successfully. 

# \[ADMIN\] Manage group

Value sets can be organized in groups. That means you can create a group for a specific domain (for example epSOS), and put all the value set related to epSOS in this group. A group is also identified by an OID.

Managing groups is done by going to Value Set Browser Group Browser. From there – as an administrator – you can create a new group by clicking on the “Add new group” button.

## Link a value set to a group

To add a value set in a group, you need to go to the group edit page. From there, you will find a button named “Add new Value Set to group”. When you click on it, a new panel must appears, letting you the possibility to filter to find the value set you are looking for. Then, just click on the ![](./media/image26.png) icon to link the value set to the selected group.

# \[DEVELOPERS\] Use of codes in simulator

In addition of the functionalities defined by the technical framework, the simulator is able to provide codes to other tools (from Gazelle platform or external tools).

This Repository give the ability to use more parameters:

* “random” : returns only one concept randomly extracted from the given value set.
* “code” : returns concept defined by this code and the provided value set.
* “id”: still available and mandatory for retrieving the value set
* “lang” (optional): still available to retrieve specific concept list according to its language.
