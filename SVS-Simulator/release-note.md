---
title: Release note
subtitle: SVSSimulator
toolversion: 3.0.0
releasedate: 2024-02-06
author: Nicolas BAILLIET
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-SVSSIMULATOR
---
# 3.0.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[SVS-248](https://gazelle.ihe.net/jira/browse/SVS-248)\] Integrate new sso-client-v7
* \[[SVS-249](https://gazelle.ihe.net/jira/browse/SVS-249)\] Remove username display in GUI

# 2.3.4
_Release date: 2023-04-13_

__Bug__
* \[[SVS-246](https://gazelle.ihe.net/jira/browse/SVS-246)\] Fix TOS popup

# 2.3.3
_Release date: 2023-02-01_

__Bug__
* \[[SVS-247](https://gazelle.ihe.net/jira/browse/SVS-247)\] Fix CGU 1st wave

# 2.3.2
_Release date: 2023-02-01_

__Bug__
* \[[SVS-246](https://gazelle.ihe.net/jira/browse/SVS-246)\] Fix TOS popup

# 2.3.1
_Release date: 2022-01-19_

__Story__
* \[[SVS-245](https://gazelle.ihe.net/jira/browse/SVS-245 )\] GDPR Refacto banner in standalone library

# 2.3.0
_Release date: 2019-04-04_

__Bug__

* \[[SVS-223](https://gazelle.ihe.net/jira/browse/SVS-223)\] Datasources extraction ZIP not referenced in maven assembly plugin of EAR
* \[[SVS-224](https://gazelle.ihe.net/jira/browse/SVS-224)\] Value Set Extraction from file not looping on Value Sets for Update
* \[[SVS-225](https://gazelle.ihe.net/jira/browse/SVS-225)\] SUT configurations not displaying in several languages except English

__Improvement__

* \[[SVS-77](https://gazelle.ihe.net/jira/browse/SVS-77)\] Populate a repository by querying another SVS repository
* \[[SVS-226](https://gazelle.ihe.net/jira/browse/SVS-226)\] Allow the user to first review the changes to be applied before it is persisted in DB
* \[[SVS-227](https://gazelle.ihe.net/jira/browse/SVS-227)\] To increase the usage of the tool, add menu entries under Administration

# 2.2.0
_Release date: 2019-03-22_

__Remarks__

Datasource management has changed, refer to the installation manual to correctly update your instance of the tool.

__Bug__

* \[[SVS-204](https://gazelle.ihe.net/jira/browse/SVS-204)\] Review the way updates of value sets are performed
* \[[SVS-209](https://gazelle.ihe.net/jira/browse/SVS-209)\] The SVS simulator is not using the correct wsaw:Action
* \[[SVS-218](https://gazelle.ihe.net/jira/browse/SVS-218)\] Issue Executing Step number  4 - Test Case: Edit a value set 
* \[[SVS-219](https://gazelle.ihe.net/jira/browse/SVS-219)\] Issue Executing Step number  3 - Test Case: Import value set from file 
* \[[SVS-221](https://gazelle.ihe.net/jira/browse/SVS-221)\] Issue Executing Step number  3 - Test Case: Check HTTP requests are working 

__Story__

* \[[SVS-217](https://gazelle.ihe.net/jira/browse/SVS-217)\] /SVS Simulator/System testing/Value Set Browser/SVS-2:Value set browser filters -  Executed ON (ISO FORMAT): 2019-03-21 
* \[[SVS-220](https://gazelle.ihe.net/jira/browse/SVS-220)\] Issue Executing Step number  2 - Test Case: RetrieveMultipleValueSetForSimulator rest call 
* \[[SVS-222](https://gazelle.ihe.net/jira/browse/SVS-222)\] Issue Executing Step number  3 - Test Case: Check SOAP request are working 

__Improvement__

* \[[SVS-210](https://gazelle.ihe.net/jira/browse/SVS-210)\] Extract datasource configuration
* \[[SVS-211](https://gazelle.ihe.net/jira/browse/SVS-211)\] Update SQL scripts archive

# 2.1.4
_Release date: 2018-10-09_

__Remarks__


__Bug__

* \[[SVS-208](https://gazelle.ihe.net/jira/browse/SVS-208)\] RetrieveMultipleValueSets : can't query with string parameters

# 2.1.3
_Release date: 2018-09-04_

__Remarks__


__Story__

* \[[SVS-193](https://gazelle.ihe.net/jira/browse/SVS-193)\] Allow the administrator to hide the validation/simulator part of the tool

# 2.1.2
_Release date: 2018-07-31_

__Remarks__


__Bug__

* \[[SVS-201](https://gazelle.ihe.net/jira/browse/SVS-201)\] Can't access the messages page

# 2.1.1
_Release date: 2018-07-31_

__Remarks__


__Bug__

* \[[SVS-200](https://gazelle.ihe.net/jira/browse/SVS-200)\] Can't access the messages page

# 2.0.15
_Release date: 2017-06-21_

__Remarks__


__Bug__

* \[[SVS-182](https://gazelle.ihe.net/jira/browse/SVS-182)\] Unable to download zip file

# 2.0.14
_Release date: 2017-06-21_

__Remarks__


__Bug__

* \[[SVS-181](https://gazelle.ihe.net/jira/browse/SVS-181)\] Upload multiple ValueSet from a zip file doesn't work

__Improvement__

* \[[SVS-180](https://gazelle.ihe.net/jira/browse/SVS-180)\] Add possibility to export all valueset from a group in a zip file

# 2.0.13
_Release date: 2017-06-13_

__Remarks__


__Bug__

* \[[SVS-175](https://gazelle.ihe.net/jira/browse/SVS-175)\] Value Set Browser does not work anymore

# 2.0.12
_Release date: 2017-03-24_

__Remarks__

# 2.0.11
_Release date: 2017-01-16_

__Remarks__


__Bug__

* \[[SVS-172](https://gazelle.ihe.net/jira/browse/SVS-172)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm

__Improvement__

* \[[SVS-170](https://gazelle.ihe.net/jira/browse/SVS-170)\] CAS login redirects to the same page instead of the home page

# 2.0.10
_Release date: 2016-08-24_

__Remarks__

# 2.0.9
_Release date: 2016-08-24_

__Remarks__


__Bug__

* \[[SVS-164](https://gazelle.ihe.net/jira/browse/SVS-164)\] When SVS reports errors the xpath of the error is incorrect

__Story__

* \[[SVS-165](https://gazelle.ihe.net/jira/browse/SVS-165)\] Release svs-code-jar 2.0.1 and SVSSimulator 2.0.9

# 2.0.8
_Release date: 2016-08-10_

__Remarks__

# 2.0.7
_Release date: 2016-08-10_

__Remarks__


__Story__

* \[[SVS-162](https://gazelle.ihe.net/jira/browse/SVS-162)\] Calling SVS Validation from EVS Client result in an error

# 2.0.6
_Release date: 2016-06-29_

__Remarks__


__Bug__

* \[[SVS-161](https://gazelle.ihe.net/jira/browse/SVS-161)\] UF for Interopsanté are displayed with ? for the accentuated characters. 

__Story__

* \[[SVS-88](https://gazelle.ihe.net/jira/browse/SVS-88)\] Make migration on OVH

# 2.0.5
_Release date: 2016-04-27_

__Remarks__

# 2.0.4
_Release date: 2016-04-27_

__Remarks__

# 2.0.3
_Release date: 2016-04-07_

__Remarks__

# 2.0.2
_Release date: 2016-04-07_

__Remarks__


__Bug__

* \[[SVS-153](https://gazelle.ihe.net/jira/browse/SVS-153)\] Correct translations on the footer

# 2.0.1
_Release date: 2016-04-06_

__Remarks__


__Story__

* \[[SVS-152](https://gazelle.ihe.net/jira/browse/SVS-152)\] Sonar Analysus

# 2.0.0
_Release date: 2016-04-05_

__Remarks__


__Bug__

* \[[SVS-89](https://gazelle.ihe.net/jira/browse/SVS-89)\] In the page list of message, when filtering by Initiator or Responder application crashes
* \[[SVS-134](https://gazelle.ihe.net/jira/browse/SVS-134)\] Errors on Value set browser filters
* \[[SVS-135](https://gazelle.ihe.net/jira/browse/SVS-135)\] Minor issues in page layout (edition of value set)
* \[[SVS-136](https://gazelle.ihe.net/jira/browse/SVS-136)\] some errors on value set edition page (concept lists section)
* \[[SVS-137](https://gazelle.ihe.net/jira/browse/SVS-137)\] /SVS Simulator/System testing/Value Set Browser/SVS-5:Edit a value set -  Executed ON (ISO FORMAT): 2016-03-15 22:08:07
* \[[SVS-139](https://gazelle.ihe.net/jira/browse/SVS-139)\] import of zip file does not work
* \[[SVS-140](https://gazelle.ihe.net/jira/browse/SVS-140)\] Group created twice when clicking several times on save button
* \[[SVS-141](https://gazelle.ihe.net/jira/browse/SVS-141)\] Cannot add value sets to group
* \[[SVS-143](https://gazelle.ihe.net/jira/browse/SVS-143)\] Cannot remove value set from group
* \[[SVS-145](https://gazelle.ihe.net/jira/browse/SVS-145)\] improve SUT Configurations page
* \[[SVS-146](https://gazelle.ihe.net/jira/browse/SVS-146)\] Review SUT configurations creation page
* \[[SVS-147](https://gazelle.ihe.net/jira/browse/SVS-147)\] Issue with SUT deletion
* \[[SVS-149](https://gazelle.ihe.net/jira/browse/SVS-149)\] Translation panel: put the code and the various display names on the same row 

__Epic__

* \[[SVS-94](https://gazelle.ihe.net/jira/browse/SVS-94)\] Migration to Jboss7

__Improvement__

* \[[SVS-142](https://gazelle.ihe.net/jira/browse/SVS-142)\] Why cannot we update a group keyword ?
* \[[SVS-150](https://gazelle.ihe.net/jira/browse/SVS-150)\] Separate value set edit page and create page

# 1.7.3
_Release date: 2014-11-28_

__Remarks__


__Bug__

* \[[SVS-65](https://gazelle.ihe.net/jira/browse/SVS-65)\] SUT Configurations which are deactivated are not listed anymore even for admin
* \[[SVS-69](https://gazelle.ihe.net/jira/browse/SVS-69)\] browsing from group OID to the list of valueSet, then from the list of value sets to the list of concept does not work.
* \[[SVS-78](https://gazelle.ihe.net/jira/browse/SVS-78)\] Invalid response from the simulator when querying for Retrieve Multiple Value Set Response (ITI-60 transaction)
* \[[SVS-80](https://gazelle.ihe.net/jira/browse/SVS-80)\] ITI-60 When using the id in the REST request. id should be lower case and not uppercased. 
* \[[SVS-81](https://gazelle.ihe.net/jira/browse/SVS-81)\] The GUI does not seem to display the actual query to the Repository
* \[[SVS-82](https://gazelle.ihe.net/jira/browse/SVS-82)\] Response from SVS should not contain codeSystemVersion when it has zero length
* \[[SVS-84](https://gazelle.ihe.net/jira/browse/SVS-84)\] Broken link
* \[[SVS-85](https://gazelle.ihe.net/jira/browse/SVS-85)\] Add id in messages table
* \[[SVS-87](https://gazelle.ihe.net/jira/browse/SVS-87)\] In group browser the link on OID is missing

__Story__

* \[[SVS-83](https://gazelle.ihe.net/jira/browse/SVS-83)\] We need to enter the installation manual in Gazelle website

# 1.7.2
_Release date: 2014-03-31_

__Remarks__


__Bug__

* \[[SVS-75](https://gazelle.ihe.net/jira/browse/SVS-75)\] The application does not deploy with the new version of jboss-ws-natives

__Improvement__

* \[[SVS-76](https://gazelle.ihe.net/jira/browse/SVS-76)\] Allow users/tools to query the version number of the application

# 1.7.1
_Release date: 2013-11-29_

__Remarks__


__Bug__

* \[[SVS-67](https://gazelle.ihe.net/jira/browse/SVS-67)\] Cannot delete group
* \[[SVS-71](https://gazelle.ihe.net/jira/browse/SVS-71)\] Problem to delete a valueSet in the version 1.7.0

__Improvement__

* \[[SVS-72](https://gazelle.ihe.net/jira/browse/SVS-72)\] NA2014:  Add codes for CSD profile to the SVS simulator

# 1.7.0
_Release date: 2013-11-05_

__Remarks__


__Bug__

* \[[SVS-68](https://gazelle.ihe.net/jira/browse/SVS-68)\] Cannot delete value set

# 1.6
_Release date: 2013-10-08_

__Remarks__


__Story__

* \[[SVS-64](https://gazelle.ihe.net/jira/browse/SVS-64)\] Add the possibility to temporarily ignore the validation before importing SVSValueSet 

# 1.5
_Release date: 2013-09-27_

__Remarks__


__Bug__

* \[[SVS-63](https://gazelle.ihe.net/jira/browse/SVS-63)\] When a user has only admin_role, the cas login does not work

# 1.4
_Release date: 2013-09-26_

__Remarks__


__Bug__

* \[[SVS-53](https://gazelle.ihe.net/jira/browse/SVS-53)\] When updating the codeSystemName of a concept, the value is updated for all the concepts of the selected Concept list
* \[[SVS-54](https://gazelle.ihe.net/jira/browse/SVS-54)\] When displaying the content of a value set, content of columns codeSystemName and codeSystemVersion are reversed
* \[[SVS-56](https://gazelle.ihe.net/jira/browse/SVS-56)\] I/O Exception when invoking SOAP method

__Story__

* \[[SVS-55](https://gazelle.ihe.net/jira/browse/SVS-55)\] create profile for esante
* \[[SVS-62](https://gazelle.ihe.net/jira/browse/SVS-62)\] Add the profile of TW

__Improvement__

* \[[SVS-57](https://gazelle.ihe.net/jira/browse/SVS-57)\] Use the new documentation module for the model-based validation

# 1.3-GA
_Release date: 2012-12-06_

__Remarks__


__Technical task__

* \[[SVS-15](https://gazelle.ihe.net/jira/browse/SVS-15)\] Review generated classes/constraints
* \[[SVS-16](https://gazelle.ihe.net/jira/browse/SVS-16)\] Develop validation service
* \[[SVS-17](https://gazelle.ihe.net/jira/browse/SVS-17)\] Check for additional constraints on datatypes and SVS
* \[[SVS-18](https://gazelle.ihe.net/jira/browse/SVS-18)\] Include validator client in EVSClient

__Bug__

* \[[SVS-1](https://gazelle.ihe.net/jira/browse/SVS-1)\] [HTTP binding] The warning header is not filled when the requested OID does not match any value set
* \[[SVS-45](https://gazelle.ihe.net/jira/browse/SVS-45)\] It is not possible to update an existing SUT

__Story__

* \[[SVS-3](https://gazelle.ihe.net/jira/browse/SVS-3)\] Define the architecture of the project
* \[[SVS-5](https://gazelle.ihe.net/jira/browse/SVS-5)\] Implementation material
* \[[SVS-7](https://gazelle.ihe.net/jira/browse/SVS-7)\] First tasks
* \[[SVS-12](https://gazelle.ihe.net/jira/browse/SVS-12)\] DAO design
* \[[SVS-13](https://gazelle.ihe.net/jira/browse/SVS-13)\] Application design
* \[[SVS-14](https://gazelle.ihe.net/jira/browse/SVS-14)\] Validation engine
* \[[SVS-19](https://gazelle.ihe.net/jira/browse/SVS-19)\] Import existing value sets within the new architecture
* \[[SVS-20](https://gazelle.ihe.net/jira/browse/SVS-20)\] value sets browser
* \[[SVS-28](https://gazelle.ihe.net/jira/browse/SVS-28)\] [ITI-60] Implement HTTP binding part of SVS Repository
* \[[SVS-31](https://gazelle.ihe.net/jira/browse/SVS-31)\] Add a page for displaying messages sent/received by the simulator
* \[[SVS-32](https://gazelle.ihe.net/jira/browse/SVS-32)\] Log validator usage
* \[[SVS-35](https://gazelle.ihe.net/jira/browse/SVS-35)\] [Bug] Add Translation in SVS Browser
* \[[SVS-37](https://gazelle.ihe.net/jira/browse/SVS-37)\] [Bug] in SVS Browser, importValueSet
* \[[SVS-39](https://gazelle.ihe.net/jira/browse/SVS-39)\] [Bug] SOAP Repository - Xml Not Well-Formed
* \[[SVS-40](https://gazelle.ihe.net/jira/browse/SVS-40)\] [Feature] Messages section - XSL for each type transaction + pretty xml
* \[[SVS-41](https://gazelle.ihe.net/jira/browse/SVS-41)\] [Feature] Validate System in Messages Page
* \[[SVS-42](https://gazelle.ihe.net/jira/browse/SVS-42)\] [Feature] Import from folder
* \[[SVS-46](https://gazelle.ihe.net/jira/browse/SVS-46)\] rich:dataScroller does not work for navigating through the tables of concepts
* \[[SVS-47](https://gazelle.ihe.net/jira/browse/SVS-47)\] Some constraints are wrong in the validator part of the tool
* \[[SVS-50](https://gazelle.ihe.net/jira/browse/SVS-50)\] Prepare simulator for new release

__Improvement__

* \[[SVS-44](https://gazelle.ihe.net/jira/browse/SVS-44)\] [SOAP Binding] messages sent by the Value Set Consumer actor are not using SOAP 1.2

# 1.0
_Release date: 2011-06-01_

__Remarks__


__Story__

* \[[SVS-21](https://gazelle.ihe.net/jira/browse/SVS-21)\] [ITI-48] Implement SOAP binding part of SVS Consumer
* \[[SVS-23](https://gazelle.ihe.net/jira/browse/SVS-23)\] [ITI-60] Implement SOAP binding part of SVS Consumer
* \[[SVS-24](https://gazelle.ihe.net/jira/browse/SVS-24)\] [ITI-60] Implement HTTP binding part of SVS Consumer






















