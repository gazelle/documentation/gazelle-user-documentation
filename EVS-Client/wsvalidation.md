---
title:  Validation Webservice API
subtitle: EVS Client
author: Anne-Gaëlle BERGE
function: Software developer
date: 4/18/2023
toolversion: 5.15.x+
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-EVS_CLIENT_WSV-1_02
customer: IHE-EUROPE
---
# Introduction

To increase the code maintainability and the power of the validation of XML documents and messages, we chose to develop model-based validation services. Those services are available through the External Validation Service Front-end (aka EVSClient) of the Gazelle platform but you can also implement your own client to the web services of validation. All of them are built on the same model so you will only need to develop the client once and then "play" with the WSDL location.

# Description of the API
Below, we describe the methods offered by the web services and the expected parameters.

* about : Gives information about the called web service
* validateDocument : Validates an XML document using the given model-based validator
  * @param document : the XML document to be validated
  * @param validator : the name of the validator kind
  * @return : an XML based structure of the result of the validation
* validateBase64Document : Validates an XML document using the given model-based validator based on a B64 description of the document
  * @param base64Document : base64 encoded document
  * @param validator : the name of the validator kind
  * @return : an XML based structure of the result of the validation
* getListOfValidators : Returns the list of available validators
  * @param descriminator: in some cases we may need to use the descriminator in order to select the validators to return (example : IHE, EPSOS, etc)
  * @return : a list of string containing allowed validators' name

# Web service client
We have developed a web service client for our proper needs to access these services. It's a Maven based project available in our Nexus repository. The latest release is available in our [Nexus repository](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~gazelle-ws-client*~~~)


# Presentation about the validation process

You will find [*here*](media/wsv//Conformance_Checking_Tools.pdf) a PPT presentation about how EVSClient use the webservices of validation.

# Validation tools with SOAPUI

You will find [*here*](media/wsv/CDAValidationWS-soapui-project.xml) a SOAPUI project about the validation webservice for CDA documents
(the version of SOAPUI tool used is 5.1.3).
This project contains examples of how to use the Webservice, samples of requests of validation.

# Link to the validation tools

Here a list of webservice provided by IHE-Europe, using the same API of validation :

|Validation Service |	WSDL Location	 |Descriminators|
|-----|-------------------------------------------|-----|
|CDA documents|https://gazelle.ihe.net/CDAGenerator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl | IHE, epSOS, ASIP|
|ATNA logging messages | https://gazelle.ihe.net/gazelle-atna-ejb/AuditMessageValidationWSService/AuditMessageValidationWS?wsdl |IHE, epSOS |
|XD* metadata	|https://gazelle.ihe.net/XDStarClient-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl | IHE, epSOS|
|DSUB messages	|https://gazelle.ihe.net/XDStarClient-ejb/DSUBModelBasedWSService/DSUBModelBasedWS?wsdl |IHE|
|HPD messages	| https://gazelle.ihe.net/HPDSimulator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl | IHE, CH|
|SVS messages	| https://gazelle.ihe.net/SVSSimulator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl | IHE|
|HL7v3 messages	|https://gazelle.ihe.net/GazelleHL7v2Validator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl |IHE, KSA, CH|
|XDW documents	|https://gazelle.ihe.net/XDWSimulator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl |IHE|
|SAML assertions	|https://gazelle.ihe.net/gazelle-xua-jar/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl |IHE|
|WADO queries	|https://gazelle.ihe.net/XDStarClient-ejb/WADOModelBasedWSService/WADOModelBasedWS?wsdl |IHE|
|XML Signature Validator |https://gazelle.ihe.net/xml-signature-validator-jar/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl |IHE|

# Documentation of the webservices in the tools

Here a collect of documentations coming from different IHE-Europe validation tools regarding their exposed  webservices for validation :

* CDA validation webservice : [*https://gazelle.ihe.net/gazelle-documentation/CDA-Generator/user.html#cda-web-service*](https://gazelle.ihe.net/gazelle-documentation/CDA-Generator/user.html#cda-web-service)
* XDS validation webservice : [*https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/user.html#web-service*](https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/user.html#web-service)
