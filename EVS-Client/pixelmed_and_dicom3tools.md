---
title:  Update and Install Pixelmed
subtitle: EVS Client
author: Nicolas BAILLIET
function: Software developer
date: 24/08/2022
toolversion: 5.14.x+
version: 1.00
status: to be reviewed
reference: KER1-MAN-IHE-EVS_CLIENT-1_00
customer: IHE-EUROPE
---

## How to update DICOM Pixelmed in EVSClient ?


###### 1) Get pixelmed.jar and push it to Nexus

- Get the latest version of pixelmed with this link : http://www.dclunie.com/pixelmed/software/index.html

- If it is not up-to-date, push the pixelmed.jar file to nexus as an artifact at 3rd party > com > pixelmed > dicom (administration rights needed).


###### 2) Retrieve, update and configure Pixelmed project

- Retrieve the Pixelmed-jar project from Subversion : 

```bash
svn checkout https://svn.ihe-europe.net/gazelle/validators/packaging/pixelmed-jar/trunk 
```
(svn+ssh for commit automatic rights)


- In the pom.xml, update the version of pixelmed dependency :

Example :

```xml
	<dependency>
            <groupId>com.pixelmed</groupId>
            <artifactId>dicom</artifactId>
            <version>20220618</version>
            <exclusions>
                <exclusion>
                    <groupId>hsqldb</groupId>
                    <artifactId>hsqldb</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
```


- Then compile the project, and copy the Pixelmed-jar-VERSION-script.zip from the target directory to /opt/pixelmed/.
- Unzip the file in /opt/pixelmed/ and give it the jboss:jboss-admin ownership.

```bash
cp target/Pixelmed-jar-VERSION-script.zip /opt/pixelmed/
unzip /opt/pixelmed/Pixelmed-jar-VERSION-script.zip
chown jboss:jboss-admin -R /opt/pixelmed
```

- Add the property "JAVA_HOME=/usr/lib/jvm/zulu7" at the beginning of the script /opt/pixelmed/bin/pixelmedValidator.sh.

```bash
vim /opt/pixelmed/bin/pixelmedValidator.sh
```

###### 3) Retrieve and update DICOMEVS-Api project

- Retrieve the DICOMEVS-Api project from subversion repository : 
- 
```bash
svn checkout https://svn.ihe-europe.net/gazelle/Maven/DICOMEVS-Api/trunk
```

- In the pom.xml file, update the version of the pixelmed dependency :

Example :

```xml
	<dependency>
            <groupId>com.pixelmed</groupId>
            <artifactId>dicom</artifactId>
            <version>20220618</version>
            <exclusions>
                <exclusion>
                    <groupId>hsqldb</groupId>
                    <artifactId>hsqldb</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
```


###### 4) Retrieve and update EVSClient project 

- Retrieve EVSClient project from GitLab : 
```bash
git clone https://gitlab.inria.fr/gazelle/applications/test-execution/validator/EVSClient.git
```

- In the EJB's pom.xml file, update the version of DICOMEVS-Api dependency :

Example :

```xml
 	<dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>DICOMEVS-Api</artifactId>
            <version>3.0.14</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
                <exclusion>
                    <artifactId>log4j</artifactId>
                    <groupId>log4j</groupId>
                </exclusion>
            </exclusions>
        </dependency>
```

## How to update Dicom3Tools in EVSClient ?

###### 1) Get dicom3tools_1.00.snapshot.XXX.tar.bz2

- Install xutils-dev & man-db
  ```
   apt-get install xutils-dev man-db
   ```

- Get the latest version of Dicom3tools with this link : https://www.dclunie.com/dicom3tools/workinprogress/index.html

- Unzip the folder and follow the INSTALL file instructions :
    ```
   ./Configure
   imake -I./config
   make World
   make install
   make install.man
   ```
You should then get the binaries created in /usr/local/bin folder.

For EVSClient V5 and prior:

- Create the /opt/dicom3tools folder

- Create symbolic links on dcdump and dciodvfy in this newly created folder.
    ```
   cd /opt/dicom3tools
   ln -s /usr/local/bin/dcdump dcdump
   ln -s /usr/local/bin/dciodvfy dciodvfy
   ```
