---
title:  User Manual (EVS V5)
subtitle: EVS Client 
author: Eric Poiseau 
function: IHE Europe Technical Project Manager 
date: 18/05/2023 
toolversion: 5.15.x
version: 1.04 
status: Approved document 
reference: KER1-MAN-IHE-EVS_CLIENT_USER-1_04 
customer: IHE-EUROPE
---

# Introduction

This application has been developed with the purpose of aggregating in a same user interface, the access to all validation services developed for IHE
and its national extensions. Called services are the followings (non-exhaustive):

* Gazelle HL7 Validator for HL7v2.x and HL7v3 messages
* Schematron-based Validator (CDA, Audit messages, HL7v3, assertions...)
* Model-based validator for CDA
* Model-based validator for XD\* messages
* Model-based validator for XDW
* Model-based validator for DSUB
* Model-based validator for SVS
* Model-based validator for HPD
* Certificates validation
* JHOVE for PDF files validation
* Dicom3Tools, DCMCHECK, Dcm4Che, Pixelmed for validating DICOM objects

In the menu bar of the user interface, we have chosen to sort the validators by affinity domains; currently several affinity domains are available in
the instance deployed at [https://gazelle.ihe.net/EVSClient](https://gazelle.ihe.net/EVSClient): IHE, IHE Germany, epSOS, CI-SIS France, e-sens,
Sequoia and so on.

The  **External Validation Service Client** application can be used for validating the following types of messages or documents:

* HL7 CDA files
* HL7v2.x and HL7v3 messages
* HPD messages
* SVS messages
* DSUB metadata
* SAML assertions
* Audit messages
* X509 Certificates
* DICOM objects
* PDF files
* XD\* messages (metadata)
* XDW documents
* Digital Covid Certificate QR Code file

## Important notice

Note that when using the EVS Client application, if you are NOT logged in, every document/message you validate is stored in our database, referenced
and available to everybody. If you do not want to have your documents/messages public, then you need to log in using the "CAS login", it uses your
Gazelle's ([*EU-CAT*](https://gazelle.ihe.net/EU-CAT)) credentials.

### Note about the privacy of validation results

If the user is not logged on the application, his/her validation requests and results (that means the document/message submitted to the tool and the
validation outcome) are available to everybody. We say that the result is "public"; it will be listed in the list of validation requests and everybody
will be able to access and download both the validated file and the validation report.

If the user is logged on the application, by default, his/her validation requests and results will be set as "private". That means that he/she will be
the only one to see the validation requests in the list and to access it. A permanent link is created for each validation request, the ones leading to
a private request have restricted permissions; only the owner of the validation requests (the one who performed the validation)
will be able to access this page. If the preference value show_logs_from_colleagues is set to true then the user will share automatically all his/her
validations log with the users beloging to the same organization.

A logged on user can choose to have a given validation request available to everybody. In this case, everybody will be able to list the request and to
access both the validated file and the validation report. To do so, once the validation is performed (or at any moment from the result page), click on
the "Make this result public" button. At any time, the owner of the request (and only him/her) will be able to change back the visibility of the
result to private.

A logged on user can choose to keep his/her validation request (file + result) private but to allow certain users to access it also. In this case,
clicking on the "share this result" button will generate a random key which, added to the URL will ensure that only the persons who know the permanent
link (including the key) will be able to access the content of the validation request. The owner of the validation request will still be the only one
to see the result in the list gathering all results but everyone knowing the link will be allowed to display the page. In case the preference value
show_logs_from_colleagues is set to true, the log are still visibles by users belonging to the same organization.

Note that the admin and monitor users are able to access all the validation requests. It's obvious that they were use them only for verification
purposes and will not publish neither use them for other purposes.

# Validation of XML files/documents

By XML file we mean all messages or documents based on XML (CDA, HL7v3 messages, XD\* metadata ...) All those kinds of files can be validating using a
schematron and/or a model-based validator. Once you have selected (in the top bar menu) the kind of XML object you want to validate, you will reach a
page which ask you to upload the XML file to validate (be careful, the application allows only files with .xml extension) and to select the schematron
and/or model-based validators to use.

Below is an example of the different steps for validating an XD-LAB report.

1. **Select the menu CDA Validation in the IHE drop-down menu**
   ![How to access CDA Validation from the menu](./media/user-manual/access-to-cda-validation.png)

2. **Hit the "Add" button and select the XML file to validate in your system explorer**
   ![CDA Validation Form](./media/user-manual/select-file-for-cda-validation.png)

3. **Select the schematron and/or the ObjectsChecker validator to use in one of the 2 drop-down list(s)**
   ![Select one or two validators](./media/user-manual/select-validator.png)

4. **Finally, hit the "validate" button.** After a while, the validation result will be displayed below the selection area.

The validation result page is divided into differents panels :

* "Download result" button enables you to download an XML file gathering the validation result.
* "Information" gives information about the validated file, the validation date, the schematron used and the result of the validation. In this part,
  you will also find a permanent link to the current validation result. If you have asked for both schematron and model-based validation, two tabs
  will be displayed, one by validation result.

# Validation of HL7v2.x messages

**Select the menu HL7v2 menu in the IHE drop-down menu**

![](./media/user-manual/access-to-hl7v2-validation.png)

1. **Upload or paste your message**
   Enter your message in the box (paste your message, ER7 format) OR upload the file containing your message (be careful, the application allows only
   files with .hl7 or .txt extension).

![](./media/user-manual/upload-paste-hl7v2.png)

Then, you must choose the HL7 message profile to use to validate your HL7 message. The "Guess profile" button, just below the box, can be used to
guess automatically the HL7 message profile to use, it extracts fields MSH-9 and MSH-12 and filter on those values.

Finally, to launch the validation process, hit the arrow on the right side of the line corresponding to the message profile to use. This will launch
the request for validation

![](./media/user-manual/select-hl7-message-profile.png)

# Accessing the validation logs

For each of the validation services configured in the application, there is the possibility to access to the logs of validated files.
![](./media/user-manual//access-to-validation-logs.png)
The page to access the logs present the various logs of validation performed by the users of the application.

* if an non identified user performs a validation, the log is visible by every one
* if an identified user performs a validation, the log is private to the user, but is also visible by the users with the roles **monitor** or **
  admin**
* an identified user can change the visibility of one of his/her log public. In that case it will be visible by every one.

User with the admin role have now the ability to delete a specific log from this page.

# Add-ons

The add-ons menu gives the user access to other tools that do not belong to any of the other menus. Among the add-ons

* Download of schematrons
* Cross validation
* DicomSCP Screener
* Message Content Analyzer
* Statistic

## Download schematrons

Schematrons section allows the user to download the schematrons which are used to validate XML files. Those schematrons are sorted according to the
type of objects they validate.

## Cross validation

The Gazelle X Validator module has been developed in order to be able to check the consistency of a document and/or a message in the context in which
it has been produced. That means that we are not going to check the conformance of the message syntax and content against the specifications and
underlying standards, conformance validation tools exist for this purpose. In the contrary, the Gazelle X Validator takes at least two files (
documents, messages) and checks that they respect a set of rules which have been clearly defined, using one file as a reference to check the
correctness of the others.

For more details, refer to Gazelle X Validator Rule Editor user manual.

## Dicom SCP Screener

The purpose of the DicomSCP screener is to query a DICOM SCP in order to identified the various SOP Class and Transfer Syntax that the SCP supports.

## Message Content Analyzer

The message content analyzer is a tool that scans the content of message for known content.

If you don't know the content of your file, or the validator you need to choose to validate your document you can use the Message Content Analyzer.

1. Upload your file
2. Click on analyze
3. You should have the file description in result part (if you have any trouble, don't hesitate to write a Jira)

You can click on each part of the tree or in the table to display the contain. You also can download each part.

You can click on the green arrow or validate part (I'm lucky validation can directly send you to the result valitation of your part)
After you click on the refresh button, the validation permanent link is added to the table and the validation result is displayed in the tree.

## Statistics

![](./media/user-manual//Statistics.png)
The statistic page displays statistics at a very high level. If you want to consult more precise statistics, follow the link available for each kind
of validation.

Logged **as admin**, it's possible to download last month validation results by using the button "get previous month". The export is a CSV file and
concerns only last month validation, from 1st currentMonth-1 to currentDate.

# Rest services offered by the application

## Accessing the version of the deployed application

The version of the application can be queried using a rest request in the form of

```bash
wget http://localhost:8080/EVSClient/rest/version
```

The application responds with an xml content as follows

```xml

<version>5.0.0-SNAPSHOT</version>
```

## Accessing Validation results using rest

Access to the result of validation performed using the EVS Client application can be accessed using a REST request. The rest request is constructed as
follow

```bash
http://localhost:8080/EVSClient/rest/GetValidationStatus?oid=1.3.6.1.4.1.12559.11.1.2.1.4.283904
```

where the oid argument is the oid of the validation instance of interest.

The application returns one of the following values

* PASSED
* FAILED
* ABORTED

# CDA Scorecard

Release 2.1.0 of CDA Generator introduces a new service called CDA Scorecard. Based on the detailed report of validation of a CDA document, the tool
computes statistics. This service can be used directly from EVS Client (5.1.0 and later) once the validation using a model-based validator has been
performed. The feature is also available for validation logs generated by previous releases of CDA Generator. Once you get the validation report, a "
Scorecard" tab is available. Move to this tab, and hit the "Get Scorecard" button. After a few seconds, you will see the scorecard.

![Scorecard Tab](./media/user-manual/getscorecard.png)

The scorecard is divided into three parts:

* Scoring on constraints gives you statistics regarding the constraints which were executed during the validation process
* Scoring on templates gives you statistics regaring the conformance and the richness of the templates present in your document
* Scoring on document levels gives you statistics on the conformance of the various levels of validation which were performed (CDA basic, IHE,
  National extension and so on)

Currently, only the scoring on constraints is available.

![Summary of statistics on constraints](./media/user-manual/scorecard-constraint-summary.png)

On the screenshot above, the summary sections reports three indicators:

* __Summary of outcomes__ reports the number of failures, warnings, infos and successful checks just as you can see in the validation report
* __Overal conformity__ reports the number of rules which were successfully executed vs the number of rules which failed regardless of their
  criticality
* Not all the rules implemented in the validator will be executed on your document because it might not contains everything that is defined in the CDA
  model. In addition, a unique rule can be executed several times. The __Conformity of unique constraints__ indicator reports how many unique rules
  exist in the validator and shows you how many of them failed, how many were successfully executed and how many were not executed at all.

In addition, we have types the constraints and the __Type of constraints__ section shows you where are the gaps in your document.

![Details per constraint](./media/user-manual/scorecard-constraint-details.png)

The __Detail__ section lists all the constraints and shows you how many time each contraint was executed and how many times it was successfully
executed.

# Validation of Digital Signatures

From version 5.5.0, the EVSClient tool can be configured to detect and validate the digital signatures (ds:Signature where ds stands
for http://www.w3.org/2000/09/xmldsig#) present in XML documents. As an administrator, to allow this feature, you shall make sure that the
preference 'enable_dsig_validation' is present and set to 'true'. Setting this preference to false will cause the tool not to look for a ds:Signature
element in the uploaded XML files.

In addition,

* a new referenced standard shall be created with keyword "DSIG"
* a digital signature validator shall be declared as a validation service
* the default value for the signature validator name shall be declared as a preference of the application, with name: "default_dsig_validator_name"

The SQL script update-5.5.0.sql will help you creating those entries in the database.

## How does it work ?

If the feature is activated, when the user uploads an XML file (CDA document, HL7v3 message, SAML Assertion and so on), the tool will look for a ds:
Signature element in the file. If such element exists, a notification is displayed to the user.

![Signature detected](./media/user-manual/validateSignature.png)

By checking the box, the user asks the tool to call the default XML Signature Validation service in parallel to the other validation(s) (model-based,
schematron). When the validation ends, a new tab "Signature Validation" is displayed and shows the report of the XML Signature Validator.

![Signature Validator report](./media/user-manual/dsSignatureReport.png)
