---
title:  Installation Manual
subtitle: Gazelle-SSO
author: Valentin Lorand
function: Engineer
date: 26/06/2023
toolversion: N/A
version: 1.04
status: Approved document
reference: KER1-MAN-IHE-SSO_INSTALLATION-1_03
customer: IHE-EUROPE
---

# [DEPRECATED] Apereo CAS 5.1.X Installation Manual

{% capture sso_alert %}
Gazelle SSO, formerly Gazelle CAS, is deprecated since 2023-07-17. The replacement is a new 
authorization server that is part of Gazelle User Management. 
<a href="https://doc-ihe.kereval.cloud/gazelle-applications/v/gazelle-user-management/">See its documentation</a>.
{% endcapture %}

{% include warning.html content=sso_alert %}

The purpose of this document is to guide you through the installation process of the Apereo CAS 5.1.X. This SSO is used by Gazelle tools to authenticate users.

## Prerequisites

Installation needs to be performed on a Debian linux __Stretch__ or newer. This is know to not work on jessie.
Apereo needs to be embedded into a Tomcat 8 server. Tomcat 8 runs under Java 8. Make sure you have zulu-8 installed.

## Installation

### Install tomcat 8

```
apt-get install tomcat8
```
_Note: the next steps assume that tomcat8 installation folder is at /var/lib/tomcat8_

You can force tomcat8 to use zulu-8 by adding `JAVA_HOME=/usr/lib/jvm/zulu-8-amd64` in `/etc/init.d/tomcat8`.

The following line is necessary in order to securise the tomcat server

```
sudo rm -r /var/lib/tomcat8/webapps/ROOT
```

### Configure tomcat 8

Identify the offset to use for the apache tomcat8 server. If you already have some services listening on 8080 you need to change the default port in the configuration file of tomcat.
Make sure to enable AJP while you are editing the port numbers.

```bash
sudo vi /var/lib/tomcat8/conf/server.xml
```
In the example below, we shift the listening port to __8580__.

```xml
<Server port="8505" shutdown="SHUTDOWN">
...
<Connector port="8580" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
```

Probably you want to use AJP connector. You need to uncomment this line and customize the port

```xml
<Connector port="8509" protocol="AJP/1.3" redirectPort="8443" />
```

Also add `secretRequired="false"` in the AJP configuration.

### Download and deploy Gazelle SSO

```
cd /var/lib/tomcat8/webapps
sudo wget https://gazelle.ihe.net/apereo-cas-gazelle/cas.war
```

The following step is necessary if you want to have the application to respond at the URL https://XXXXX/sso

```
mv cas.war sso.war
```

## Configure Apereo CAS

### GUI configuration

It is easy to customize the portal with the logo of the installation.

```
cd /var/lib/tomcat8/webapps/sso/WEB-INF/classes/static/themes/gazelle/images
wget URL_TO_LOGO
convert LOGO logo.png
cp logo.png cas-logo.png
chown tomcat8:tomcat8 cas-logo.png
```
Edit css file to adjust the size of the images. You may want to use your browser inspector in order to find the right values.
Once you have them add them to the following file.

```
cd /var/lib/tomcat8/webapps/sso/WEB-INF/classes/static/themes/gazelle/css
vi cas.css
```

### Lost Password and Create Account link on home page

On the home page it is possible to add some links for lost password and create account.

Edit the file /var/lib/tomcat8/webapps/sso/WEB-INF/classes/messages.properties to add the two lines below

```
screen.pm.button.forgotpwd=<a href="https://stretch.localdomain/gazelle/users/login/passwordAssist.seam">Forgot your password? </a>
screen.pm.button.createAccount=<a href="https://stretch.localdomain/gazelle/users/user/register.seam">Create an account </a>
```

Then in the file loginform.html add the following text on line 95

```
vi /var/lib/tomcat8/webapps/sso/WEB-INF/classes/templates/fragments/loginform.html
```

```xml
<div th:unless="${passwordManagementEnabled}">
    <span class="fa fa-question-circle"></span>
    <span th:utext="#{screen.pm.button.createAccount}" />
    <p/>
</div>
<div th:unless="${passwordManagementEnabled}">
    <span class="fa fa-question-circle"></span>
    <span th:utext="#{screen.pm.button.forgotpwd}" />
    <p/>
</div>
```

### Access to user database

The apereo configuration files are located in the folder /etc/cas. Sample configuration architecture is provided in the archive file located here [https://gazelle.ihe.net/apereo-cas-gazelle/cas.tgz]https://gazelle.ihe.net/apereo-cas-gazelle/cas.tgz

```bash
sudo su
cd /etc
wget https://gazelle.ihe.net/apereo-cas-gazelle/cas.tgz
tar zxvf cas.tgz
mkdir /etc/cas/log
chown -R tomcat8:tomcat8 cas
```

You then need to edit the file /etc/cas/config/cas.properties and match your system configuration

```
vi /etc/cas/config/cas.properties
```

In this file you need to replace all occurence of the string __stretch.localdomain__ with the FQDN of your machine. You need also to make sure the database configuration matches your database setting.

In particular, two lines references the database. We usually bind the CAS to the database of Gazelle Test Management, so you might want to replace __cas__ by __gazelle__. Your users database may be hosted in another server, in that case, make sure you will be able to access it from outside.

```
cas.authn.attributeRepository.jdbc[0].url=jdbc:postgresql://localhost:5432/cas
cas.authn.jdbc.query[0].url=jdbc:postgresql://localhost:5432/cas
```

Check also the path of the log files. Per default log files are set to be stored in the folder `/var/log/cas`. Make sure that the directory exists and is owned by the correct user

```
sudo mkdir /var/log/cas
sudo chown tomcat8:tomcat8 /var/log/cas
```

### Configure logs

To rotate logs, in /etc/cas/config/log4j2.xml, add the following code in the first `<RollingFile>`, below `</Policies>`:
```xml
            <DefaultRolloverStrategy max="5">
                <Delete basePath="${sys:cas.log.dir}">
                  <IfFileName glob="*.log" />
                  <IfLastModified age="7d" />
                </Delete>
            </DefaultRolloverStrategy>
```

Restart tomcat.

## Configure the applications to use this new SSO

Gazelle tools read the configuration of the SSO in a file located in `/opt/gazelle/cas`. We need to create this folder and the configuration file `file.properties`.

```
 mkdir /opt/gazelle/cas
 touch /opt/gazelle/cas/file.properties
 chown -R jboss:jboss-admin /opt/gazelle/cas
 chmod -R g+w /opt/gazelle/cas
```

Here is a sample of the content of the property file. Please replace __stretch.localdomain__ with the __FQDN__ of your server.

```
serverName=https://stretch.localdomain
casServerUrlPrefix=https://stretch.localdomain/sso
casServerLoginUrl=https://stretch.localdomain/sso/login
casLogoutUrl=https://stretch.localdomain/sso/logout
```
### Restart tomcat8

You almost there, restart tomcat8.

## Apereo CAS-management
cf : <https://apereo.github.io/cas/5.1.x/installation/Installing-ServicesMgmt-Webapp.html>

## Configure Apache2
The apache2 server needs to be aware of the proxy_pass to the tomcat8 server.
So if you have configured tomcat to listen on ports with an offset of 200 you will have to add something like this :

```xml
<Location /sso>
   ProxyPass        ajp://localhost:8209/sso
   ProxyPassReverse ajp://localhost:8209/sso
 </Location>
```

Check that apache2 configuration is still ok

```
 sudo apache2ctl configtest
```

If everything is ok you can  then  restart apache2

```
 sudo apache2ctl restart
```

You should now access the Gazelle SSO login page at https://stretch.localname/sso.

## Fix log4j vulnerability

Add the following line in setenv.sh. Create the file if it does not exist in {CATALINA_HOME}/bin or {CATALINA_HOME}/bin : it is usually in /usr/share/tomcat8/bin if tomcat was installed via apt.

```
CATALINA_OPTS="$CATALINA_OPTS -Dlog4j2.formatMsgNoLookups=true"
```

Replace the sso.war by the patch version available at https://gazelle.ihe.net/apereo-cas-gazelle/cas.war

### You may want to be connected to the CAS before going on a page :
cf : <https://github.com/apereo/mod_auth_cas/blob/master/README>

Edit or create file /etc/apache2/sites-available/gazelle-site-le-ssl.conf :
```xml
<IfModule mod_ssl.c>
<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    ServerName FQDN
    Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains;"

    DocumentRoot /var/www
    CASCookiePath /var/cache/apache2/mod_auth_cas/
    CASLoginURL https://FQDN/sso/login
    CASValidateURL https://FQDN/sso/serviceValidate
<Location /index.html>
    Authtype CAS
    require valid-user
</Location>
...
</VirtualHost>
</IfModule>
```
**FQDN** is like gazelle.ihe.net

Activate some mods :
```
sudo a2enmod mod_ssl
sudo a2enmod mod_auth_cas
```
