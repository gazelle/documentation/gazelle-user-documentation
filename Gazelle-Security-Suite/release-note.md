---
title: Release note
subtitle: Gazelle Security Suite
toolversion: 7.2.2
releasedate: 2025-02-24
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GSS
---

# 7.2.2

__Task__
* \[[GSS-606](https://gazelle.ihe.net/jira/browse/GSS-606)\] Updade eHDSI Certificate & SAML validators version

# 7.2.1

__Bug__
* \[[GSS-604](https://gazelle.ihe.net/jira/browse/GSS-604)\] [XUA Test Case] Response is not captured if the http status code is not 200/500

__Task__
* \[[GSS-603](https://gazelle.ihe.net/jira/browse/GSS-603)\] Add preferences for the contact name, title and email


# 7.2.0
_Release date : 2025-01-22_

Context : GUM Step 4

__Task__
* \[[GSS-602](https://gazelle.ihe.net/jira/browse/GSS-602)\] Upgrade SSO Client V7 to version 5.0.0

# 7.1.2
_Release date : 2024-06-21_
__Bug__
* \[[GSS-600](https://gazelle.ihe.net/jira/browse/GSS-600)\] Cannot request a certificate with a CSR without a private


# 7.1.0
_Release date : 2024-05-16_

__Story__
* \[[GSS-591](https://gazelle.ihe.net/jira/browse/GSS-591)\] [DGSanté/eHDSI] Transfer SAML validator from Schematron to GSS


# 7.0.0
_Release date : 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[GSS-593](https://gazelle.ihe.net/jira/browse/GSS-593)\] Integrate new sso-client-v7
* \[[GSS-595](https://gazelle.ihe.net/jira/browse/GSS-595)\] Remove second CAS possibility

__Improvement__
* \[[GSS-594](https://gazelle.ihe.net/jira/browse/GSS-594)\] Display firstname and lastname instead of username.

# 6.3.3
_Release date : 2023-06-08_

__Task__
* \[[GSS-586](https://gazelle.ihe.net/jira/browse/GSS-586)\] XUA Validator - CH New ordinance EPDBEP-89 Purpose of use DICOM_AUTO

# 6.3.2
_Release date : 2023-06-05_

__Story__
* \[[GSS-585](https://gazelle.ihe.net/jira/browse/GSS-585)\] [DGSanté] Update certificates validator for wave 6

__Task__
* \[[GSS-584](https://gazelle.ihe.net/jira/browse/GSS-584)\] XUA Validator - CH New ordinance EPDBEP-145


# 6.3.1
_Release date : 2022-07-08_

__Bug__
* \[[GSS-583](https://gazelle.ihe.net/jira/browse/GSS-583)\] Messages with only RSAKeyValue are faulting

# 6.3.0
_Release date: 2021-09-15_

__Story__
* \[[GSS-580](https://gazelle.ihe.net/jira/browse/GSS-580)\] XUA Validator - CH constraint on HomeCommunityID

# 6.2.7
_Release date: 2020-11-12_

__Bug__
* \[[GSS-573](https://gazelle.ihe.net/jira/browse/GSS-573)\] Add a preference to manage http content size for the proxy

__Story__
* \[[GSS-398](https://gazelle.ihe.net/jira/browse/GSS-398)\] Add a web-service to retrieve private key



# 6.2.6
_Release date: 2020-01-21_

__Remarks__

This release includes the sub-module release of atna-questionnaire module in 2.5.3 :
 
## 2.5.3
_Release date: 2020-01-21_

__Bug__
 
 * \[[QUEST-94](https://gazelle.ihe.net/jira/browse/QUEST-94)\] Viewing the questionnaire in proofread mode results in a white page

# 6.2.5
_Release date: 2019-12-03_

__Bug__

* \[[GSS-567](https://gazelle.ihe.net/jira/browse/GSS-567)\]  idScheme and identifiers are not correct in the Irish XUA validator

# Atna Questionnaire 2.5.2

__Improvement__

* \[[QUEST-91](https://gazelle.ihe.net/jira/browse/QUEST-91)\] ATNA Questionnaire:  update to template changes old/complete questionnaires
* \[[QUEST-92](https://gazelle.ihe.net/jira/browse/QUEST-92)\] Copy template description in new atna-questionnaire instance

# 6.2.4
_Release date: 2019-11-07_

__Bug__

* \[[GSS-565](https://gazelle.ihe.net/jira/browse/GSS-565)\] Rules on multiple OU in subject for ePSOS validator lost in bouncycastle refactor

# 6.2.3
_Release date: 2019-11-05_

__Bug__

* \[[GSS-563](https://gazelle.ihe.net/jira/browse/GSS-563)\] Class Cast exception in ePSOS validators
* \[[GSS-564](https://gazelle.ihe.net/jira/browse/GSS-564)\] Fix unit tests for epsos validators

# 6.2.2
_Release date: 2019-10-14_

__Bug__

* \[[GSS-561](https://gazelle.ihe.net/jira/browse/GSS-561)\] Exception in calibration process "validate_invalid_values TestCase"
* \[[GSS-562](https://gazelle.ihe.net/jira/browse/GSS-562)\] [CH:XUA] Two constraints contradict each other

# 6.2.1
_Release date: 2019-10-10_

__Bug__

* \[[GSS-548](https://gazelle.ihe.net/jira/browse/GSS-548)\] Certificate validation do not pass when delivered by a CA trusted by GSS
* \[[GSS-558](https://gazelle.ihe.net/jira/browse/GSS-558)\] [CH-XUA] Purpose Of Use "AUTO" for TCU

# 6.2.0
_Release date: 2019-07-03_

__Remarks__

This version update BouncyCastle to the last version and isolate this library with the rest of application.

__Bug__

* \[[GSS-541](https://gazelle.ihe.net/jira/browse/GSS-541)\] Some certificate view links do not resolve
* \[[GSS-543](https://gazelle.ihe.net/jira/browse/GSS-543)\] GSS-210 - Issue Executing Step number  6 - Test Case: Request a certificate by CSR without private key
* \[[GSS-545](https://gazelle.ihe.net/jira/browse/GSS-545)\] Port over 65536 generates error for TLS server
* \[[GSS-546](https://gazelle.ihe.net/jira/browse/GSS-546)\] Handshake error with a certificate containing an "E" tag for email.

__Story__

* \[[GSS-540](https://gazelle.ihe.net/jira/browse/GSS-540)\] [eHealthIreland] Develop a SAML validator

__Improvement__

* \[[GSS-224](https://gazelle.ihe.net/jira/browse/GSS-224)\] Update Bouncycastle

# 6.1.0
_Release date: 2019-03-15_

__Story__
* \[[GSS-521](https://gazelle.ihe.net/jira/browse/GSS-521)\] Verify the in the gui the select list can be searched for.

__Improvement__
* \[[GSS-530](https://gazelle.ihe.net/jira/browse/GSS-530)\] Extract datasource configuration
* \[[GSS-531](https://gazelle.ihe.net/jira/browse/GSS-531)\] Update SQL scripts archive
* \[[GSS-538](https://gazelle.ihe.net/jira/browse/GSS-538)\] Update EPR SAML validator for version 1.7 and 1.8

# 6.0.2
_Release date: 2019-02-08_

__Bug__

* \[[GSS-536](https://gazelle.ihe.net/jira/browse/GSS-536)\] Gazelle Certificate validator issues

# 6.0.0
_Release date: 2018-11-23_

__Remarks__

This version can only be integrated with the new CAS

__Bug__
* \[[GSS-500](https://gazelle.ihe.net/jira/browse/GSS-500)\] GSS shows "Unexpected error" on "Back to List Button" in XUA tests
* \[[GSS-513](https://gazelle.ihe.net/jira/browse/GSS-513)\] Signature validation error with ID-based reference
* \[[GSS-514](https://gazelle.ihe.net/jira/browse/GSS-514)\] Navigation GSS XUA -> X-Service... -> Test Cases : Results -> Button "Back to list" givves "Unexpected error, please try again"
* \[[GSS-516](https://gazelle.ihe.net/jira/browse/GSS-516)\] Deprecate some certificate validators which are not used anymore
* \[[GSS-517](https://gazelle.ihe.net/jira/browse/GSS-517)\] Rename certificate validators
* \[[GSS-518](https://gazelle.ihe.net/jira/browse/GSS-518)\] [CH-XUA] The format of the EPR-SPID should not be tested
* \[[GSS-519](https://gazelle.ihe.net/jira/browse/GSS-519)\] [CH-XUA] ch_xua_007_PurposeOfUse is using wrong xpath
* \[[GSS-520](https://gazelle.ihe.net/jira/browse/GSS-520)\] Schema.sql has wrong cipher_suite column types.
* \[[GSS-524](https://gazelle.ihe.net/jira/browse/GSS-524)\] [eHDSI Certificate Validator] Test in error

__Story__
* \[[GSS-53](https://gazelle.ihe.net/jira/browse/GSS-53)\] Add a field to enter the subjectAlternativeName in when creating the request certificate.

__Improvement__
* \[[GSS-522](https://gazelle.ihe.net/jira/browse/GSS-522)\] Remove old CAS

## Audit Message 2.3.1

__Story__
* \[[AUDITVAL-187](https://gazelle.ihe.net/jira/browse/AUDITVAL-187)\] Update Dependencies

## ATNA Questionnaire 2.5.0

__Improvement__
* \[[QUEST-82](https://gazelle.ihe.net/jira/browse/QUEST-82)\] Remove old CAS

# 5.7.1

__Bug__
* \[[GSS-493](https://gazelle.ihe.net/jira/browse/GSS-493)\] Validation not performed when using the validators epSOS V4
* \[[GSS-494](https://gazelle.ihe.net/jira/browse/GSS-494)\] [CH] Various errors in the XUA SAML X-Assertion Validator


# 5.7.0
_Release date: 2018-04-12_

__Remarks__

This version of Gazelle Security Suite introduces a new validation service for the Digital Signatures embedded in XML documents.

__Bug__
* \[[GSS-479](https://gazelle.ihe.net/jira/browse/GSS-479)\] Update Postgresql driver version
* \[[GSS-486](https://gazelle.ihe.net/jira/browse/GSS-486)\] Cannot import certificate with indentical subject name
* \[[GSS-489](https://gazelle.ihe.net/jira/browse/GSS-489)\] Error when uploading a crt instead of a key while keystore generation

__Improvement__
* \[[GSS-476](https://gazelle.ihe.net/jira/browse/GSS-476)\] [KELA] Implement a validator for digital signature

## ATNA Questionnaire - 2.4.0

__Bug__
* \[[QUEST-80](https://gazelle.ihe.net/jira/browse/QUEST-80)\] Update Postgresql driver version

## Audit Message Validator - 2.3.0

__Bug__
* \[[AUDITVAL-181](https://gazelle.ihe.net/jira/browse/AUDITVAL-181)\] Update Postgresql driver version

## gazelle-syslog - 1.1.0

__Bug__
* \[[SYS-14](https://gazelle.ihe.net/jira/browse/SYS-14)\] Update Postgresql driver version

# 5.6.1

__Story__
* \[[GSS-475](https://gazelle.ihe.net/jira/browse/GSS-475)\] [DGSANTE] Update the validation tool for certificate

# 5.6.0
_Release date: 2017-11-15_

__Bug__
* \[[GSS-434](https://gazelle.ihe.net/jira/browse/GSS-434)\] The timestamp filter doesn't work for time
* \[[GSS-448](https://gazelle.ihe.net/jira/browse/GSS-448)\] CH-XUA assertion validator, wrong assertion IDs
* \[[GSS-471](https://gazelle.ihe.net/jira/browse/GSS-471)\] After editing a TLS Server Simulator, the status is set to "stopped" but the simulator is still running

__Story__
* \[[GSS-280](https://gazelle.ihe.net/jira/browse/GSS-280)\] Allow admin to duplicate a server configuration
* \[[GSS-284](https://gazelle.ihe.net/jira/browse/GSS-284)\] Add the possibility to edit a TLS simulator

__Improvement__
* \[[GSS-337](https://gazelle.ihe.net/jira/browse/GSS-337)\] Add TLSv1.2 to the list of supported protocols
* \[[GSS-395](https://gazelle.ihe.net/jira/browse/GSS-395)\] Test Instance could be easier to understand if they displayed the test description
* \[[GSS-439](https://gazelle.ihe.net/jira/browse/GSS-439)\] Private keys generated by Gazelle are too short to use as keys for Identity Providers in IUA

## ATNA-Questionnaire 2.3.0

__Bug__
* \[[QUEST-74](https://gazelle.ihe.net/jira/browse/QUEST-74)\] Missing protection on ATNA Questionnaire edition and view

__Story__
* \[[QUEST-73](https://gazelle.ihe.net/jira/browse/QUEST-73)\] Update ATNA questionnaire from TM

## Audit-Message 2.2.0

__Technical task__
* \[[AUDITVAL-23](https://gazelle.ihe.net/jira/browse/AUDITVAL-23)\] The page : Audit Message Documentation and Configuration should be merged

__Bug__
* \[[AUDITVAL-110](https://gazelle.ihe.net/jira/browse/AUDITVAL-110)\] Audit Validation must allow ParticipantObjectIdentification not declared in cardinality table.
* \[[AUDITVAL-143](https://gazelle.ihe.net/jira/browse/AUDITVAL-143)\] Human User requited in all ATNA records, but not always available
* \[[AUDITVAL-166](https://gazelle.ihe.net/jira/browse/AUDITVAL-166)\] Audit message creation Gui does not respond if no EventIdentification
* \[[AUDITVAL-172](https://gazelle.ihe.net/jira/browse/AUDITVAL-172)\] Last modification must not be updated when importing an audit-message

__Story__
* \[[AUDITVAL-57](https://gazelle.ihe.net/jira/browse/AUDITVAL-57)\] Refactoring of audit message configuration

__Improvement__
* \[[AUDITVAL-15](https://gazelle.ihe.net/jira/browse/AUDITVAL-15)\] Add an edit button to the page: show audit message

# 5.5.1

__Task__
* \[[GSS-441](https://gazelle.ihe.net/jira/browse/GSS-441)\] Integrate new version XUA SAML validator (CH extension)

# 5.5.0
_Release date: 2017-07-06_

__Bug__
* \[[GSS-316](https://gazelle.ihe.net/jira/browse/GSS-316)\] CSR submit failure depends on PEM tags
* \[[GSS-431](https://gazelle.ihe.net/jira/browse/GSS-431)\] Error if user hit 'Enter' key on list certificate page

__New Feature__
* \[[GSS-437](https://gazelle.ihe.net/jira/browse/GSS-437)\] Create a new feature for testing XUA X-Service Provider actor

# 2.1.1

__Bug__
* \[[AUDITVAL-103](https://gazelle.ihe.net/jira/browse/AUDITVAL-103)\] Update audit schema to 2017a

# 5.4.0
_Release date: 2017-03-23_

__Bug__
* \[[GSS-376](https://gazelle.ihe.net/jira/browse/GSS-376)\] Error in this application when trying to download keystore
* \[[GSS-415](https://gazelle.ihe.net/jira/browse/GSS-415)\] GSS HL7v2 client use CRLF as segment separator instead of standard CR
* \[[GSS-416](https://gazelle.ihe.net/jira/browse/GSS-416)\] After creating a Certificate Authority as a admin, the certificate does not instantly appear as signed

__Improvement__
* \[[GSS-88](https://gazelle.ihe.net/jira/browse/GSS-88)\] Show the status of the forward host
* \[[GSS-419](https://gazelle.ihe.net/jira/browse/GSS-419)\] Add permanent link to syslog view message page
* \[[GSS-420](https://gazelle.ihe.net/jira/browse/GSS-420)\] Change level message from Error to Warning :  list of certificate_authorities is empty
* \[[GSS-423](https://gazelle.ihe.net/jira/browse/GSS-423)\] Add permanent link to TLS connection view

## ATNA-Questionnaire 2.2.0

__Bug__
* \[[QUEST-56](https://gazelle.ihe.net/jira/browse/QUEST-56)\] Status of TLS test is not updated when the URL is deleted
* \[[QUEST-70](https://gazelle.ihe.net/jira/browse/QUEST-70)\] Update AuditMessage config WS to match new WS definition

__Improvement__
* \[[QUEST-69](https://gazelle.ihe.net/jira/browse/QUEST-69)\] Ability to switch between View and Proofread in an ATNA Questionnaire

## Audit-Message 2.1.0

__Technical task__

* \[[AUDITVAL-94](https://gazelle.ihe.net/jira/browse/AUDITVAL-94)\] Audit message downloaded files must be named with their OID

__Story__

* \[[AUDITVAL-92](https://gazelle.ihe.net/jira/browse/AUDITVAL-92)\] We need to be able to backup the definition of the audit messages in the forge

## Gazelle-Syslog 1.0.2

__Bug__
* \[[SYS-12](https://gazelle.ihe.net/jira/browse/SYS-12)\] No record in syslog collector if message contains 0x00 byte

# 5.3.3

__Bug__
* \[[GSS-410](https://gazelle.ihe.net/jira/browse/GSS-410)\] Configuration conflict multipart/form-data

## ATNA-Questionnaire 2.1.3

__Bug__
* \[[QUEST-66](https://gazelle.ihe.net/jira/browse/QUEST-66)\] Configuration conflict multipart/form-data

## Audit-Message 2.0.5

__Bug__
* \[[AUDITVAL-93](https://gazelle.ihe.net/jira/browse/AUDITVAL-93)\] Configuration conflict multipart/form-data

# 5.3.2

__Improvement__
* \[[GSS-409](https://gazelle.ihe.net/jira/browse/GSS-409)\] Add ws to link XUA assertions

## Gazelle Syslog 1.0.1

__Bug__
* \[[SYS-7](https://gazelle.ihe.net/jira/browse/SYS-7)\] gazelle-syslog might not handle Syslog UTF-8 messages
* \[[SYS-10](https://gazelle.ihe.net/jira/browse/SYS-10)\] To much records when SUT doesn't close socket.

# 5.3.1

__Bug__
* \[[GSS-385](https://gazelle.ihe.net/jira/browse/GSS-385)\] SyslogCollector administration, several faces messages are displayed when stopping the SyslogCollector
* \[[GSS-393](https://gazelle.ihe.net/jira/browse/GSS-393)\] When SyslogCollector receive a message, JBoss server is crashing.

__Improvement__
* \[[GSS-285](https://gazelle.ihe.net/jira/browse/GSS-285)\] Syslog-collector should reject old syslog transport layer versions.
* \[[GSS-375](https://gazelle.ihe.net/jira/browse/GSS-375)\] Create missing index in DB to improve search performance

## Gazelle Syslog 1.0.0

__Bug__
* \[[SYS-6](https://gazelle.ihe.net/jira/browse/SYS-6)\] When SyslogCollector receive a message, JBoss server is crashing.

# 5.3.0
_Release date: 2016-10-11_

__Sub-task__
* \[[GSS-367](https://gazelle.ihe.net/jira/browse/GSS-367)\] Industrialisation of syslog database as storage.
* \[[GSS-368](https://gazelle.ihe.net/jira/browse/GSS-368)\] View for listing and filtering audit-messages recorded by Syslog

__Bug__
* \[[GSS-74](https://gazelle.ihe.net/jira/browse/GSS-74)\] The CA certificate of the tool is not checked.
* \[[GSS-336](https://gazelle.ihe.net/jira/browse/GSS-336)\] SN subject attribute is transformed by bouncycastle as SERIALNUMBER
* \[[GSS-350](https://gazelle.ihe.net/jira/browse/GSS-350)\] Issue with the validator for epSOS Signature V1
* \[[GSS-366](https://gazelle.ihe.net/jira/browse/GSS-366)\] TLSClient simulator does not record messages anymore
* \[[GSS-378](https://gazelle.ihe.net/jira/browse/GSS-378)\] Application does not work without CAS
* \[[QUEST-63](https://gazelle.ihe.net/jira/browse/QUEST-63)\] In proofread view, system type (SA or SN) is not displayed.
* \[[QUEST-64](https://gazelle.ihe.net/jira/browse/QUEST-64)\] Error displayed at GSS shut down.
* \[[AUDITVAL-8](https://gazelle.ihe.net/jira/browse/AUDITVAL-8)\] Fail to create an audit message specification without Event
* \[[AUDITVAL-80](https://gazelle.ihe.net/jira/browse/AUDITVAL-80)\] There is no way to add an AuditSource element to a validator once it's created
* \[[AUDITVAL-83](https://gazelle.ihe.net/jira/browse/AUDITVAL-83)\] Edition of unsaved constraint duplicates the entries in the list

__Story__
* \[[GSS-214](https://gazelle.ihe.net/jira/browse/GSS-214)\] GUI for the syslog collector
* \[[GSS-340](https://gazelle.ihe.net/jira/browse/GSS-340)\] Admin should be able to start and stop syslog-collector manually
* \[[GSS-343](https://gazelle.ihe.net/jira/browse/GSS-343)\] Admin configures SyslogCollector ports
* \[[GSS-344](https://gazelle.ihe.net/jira/browse/GSS-344)\] GSS should start and stop SyslogCollector automatically
* \[[GSS-346](https://gazelle.ihe.net/jira/browse/GSS-346)\] GSS display SyslogCollector Status

__Improvement__
* \[[GSS-341](https://gazelle.ihe.net/jira/browse/GSS-341)\] Uploaded CSR must also be automatically signed if automatic signature is enabled.
* \[[AUDITVAL-6](https://gazelle.ihe.net/jira/browse/AUDITVAL-6)\] Export as XML button display the XML file inline whereas it would be more convenient to save it
* \[[AUDITVAL-14](https://gazelle.ihe.net/jira/browse/AUDITVAL-14)\] Rename "Add ParticipantIdentification description" button

# 5.2.1
_Release date: 2016-05-02_

__Bug__
* \[[GSS-333](https://gazelle.ihe.net/jira/browse/GSS-333)\] ATNA layer TLS validation
* \[[GSS-334](https://gazelle.ihe.net/jira/browse/GSS-334)\] fix HQL logs and evs-client-connector issue
* \[[GSS-335](https://gazelle.ihe.net/jira/browse/GSS-335)\] remove unused column on tls_test_connection_data_item table.

# 5.2.0
_Release date: 2016-04-08_

__Bug__
* \[[GSS-329](https://gazelle.ihe.net/jira/browse/GSS-329)\] Certificates TLS Client, TLS Server and TLS Client and Server cannot be requested by non-admin
* \[[QUEST-60](https://gazelle.ihe.net/jira/browse/QUEST-60)\] Update SystemConfigurationWSClient to match new TFconfigurationTypeWS API
* \[[QUEST-61](https://gazelle.ihe.net/jira/browse/QUEST-61)\] Failed to lazily initialize TLSTests
* \[[AUDITVAL-65](https://gazelle.ihe.net/jira/browse/AUDITVAL-65)\] The AuditMessage edition says "Modification saved" however nothing is saved

__Remarks__

There is an API change on Gazelle Test Management side regarding the issue \[[QUEST-60](https://gazelle.ihe.net/jira/browse/QUEST-60)\]. Gazelle TM versions prior to 5.1.0 are not compatible any more with this GSS version.
