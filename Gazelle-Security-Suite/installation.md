---
title:  Installation Manual
subtitle: Gazelle Security Suite
author: Cédric EOCHE-DUVAL
function: Engineer
date: 24/02/2025
toolversion: 7.2.x
version: 2.01
status: Approved document
reference: KER1-MAN-IHE-GSS_INSTALLATION-2_02
customer: IHE-EUROPE
---

# Introduction

The Gazelle project entitled Gazelle-Security-Suite (alias GSS) gathers the following tools used for the testing of IHE ATNA integration profile:

* Gazelle PKI to generate and share certificates for testing purposes
* ATNA Questionnaire used to collect security properties of systems under test
* Gazelle TLS to test implementation of secured connections
* The Syslog Collector to validate SYSLOG protocol implementation
* A model-based validation service for validating Audit Messages
* A model-based validation service for validating XUA assertions
* A certificate validator

Since version 5.0.0 the tool is called Gazelle-Security-Suite and run on JBoss 7. It was prior called Gazelle ATNA Tools and ran on JBoss 5. The main title has changed, but a lot of modules have kept the old name, so do not be surprise to find both names in code sources.

# Sources & binaries

Gazelle-Security-Suite is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via git at [https://gitlab.inria.fr/gazelle/public/core/security-suite.git](https://gitlab.inria.fr/gazelle/public/core/security-suite.git).

The latest public packaged release can be downloaded from our Sonatype Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22) (search for **gazelle-security-suite-X.X.X-gss.ear**).

**Important**, if you download the **ear** from Nexus it will have a name such as `gazelle-security-suite-ear-5.0.0-gss.ear`, then be sure to rename it to `gazelle-gss.ear` otherwise the deployment will fail.

If you need for any reason a more recent version (hotfix, experimental feature...), The public packaged application of our development trunk can be found at [https://gazelle.ihe.net/jenkins/job/gazelle-atna/ws/gazelle-security-suite-ear/target/gazelle-gss.ear](https://gazelle.ihe.net/jenkins/job/gazelle-atna/ws/gazelle-security-suite-ear/target/gazelle-gss.ear). This package can be unstable.

# Installation

If you are installing a Gazelle tool for the first time in your environment, make sure to read carefully the [general considerations for JBoss7](/gazelle-documentation/General/jboss7.html).

If Gazelle Security Suite is already installed in your environment and you are looking for the update process, jump to the next section [Update from a previous version](update-from-a-previous-version).

## Bouncycastle

Bouncycastle, as a security library, is very sensible to the classloader. This library is prepared during GSS build and available from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22) (search for **gazelle-security-suite-X.X.X-lib.zip**)

bcprov library must be installed as a module of the application server.

1. Unzip the dowloaded archive

1. Create the followings directories in the JBoss 7 installation : ``${JBOSS_HOME}/modules/org/bouncycastle/main/``

1. Copy the **bcprov** jar and **module.xml** files into ``${JBOSS_HOME}/modules/org/bouncycastle/main/``

## Java key length policy

The USA government restricts the allowed Java key size by default. It is needed to change the policy of the JVM if you get the error “**java.security.InvalidKeyException: Illegal key size**”.

This can be fixed by overwriting policy files in the Java runtime by those provided on the Java download page

Java 7 : [http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html](http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html)

Unzip The 2 files jce/US\_export\_policy.jar and jce/local\_policy.jar and paste them into ``${JAVA_HOME}/jre/lib/security/.``

To check whether it's working, try to create a custom certificate with a key size of 2048 once GSS is installed.

## Database creation

Your database must have a user **gazelle**

1. Download the SQL scripts archive from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22) (search for **gazelle-security-suite-X.X.X-sql.zip**)

1. Unzip the archive into a folder named `gss-sql`

1.  Connect to your database
```bash
psql -U gazelle
```

1.  Execute the SQL statement to create the database.
```sql
CREATE DATABASE gss OWNER gazelle ENCODING UTF8 ;
```

1.  From the bash, create the schema by running :
```bash
psql -U gazelle gss < gss-sql/schema.sql
```

1. Initialize database by running :
```bash
psql -U gazelle gss < gss-sql/init.sql
```

1. You don't need those SQL scripts any more, you can delete the archive and the folder `/gss-sql`.

## Deployment

To deploy Gazelle Security Suite :

1.  Paste the archive **gazelle-gss.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`

1.  Display JBoss server logs, start JBoss and wait for **ear** deployment.

1.  The application can be browsed at [http://localhost:8080/gss](http://localhost:8080/gss) or [http://localhost:8080/gss-dev](http://localhost:8080/gss-dev), depending on the compilation profile (Public or dev).  
_Port could also be different whether you have modified the JBoss configuration._

## Data storage

GSS need
If the ATNA-questionnaire is enable (true by default), GSS will need a directory to record Audit Messages and validation results. By default, the application is configured to use `/opt/tls/`.
```bash
sudo mkdir /opt/tls
```

Be sure this directory can be read/written by JBoss.
```bash
sudo chmod -R 775 /opt/tls
sudo chown -R jboss:jboss-admin /opt/tls
```
### Syslog directory

Syslog needs some configuration files installed on the same machine GSS is running.

1. Create a directory
```bash
sudo mkdir /opt/syslog
```

1. Download the syslog archive from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22) (search for **gazelle-security-suite-X.X.X-syslog.zip**)

1. Unzip the archive and copy every files into `/opt/syslog`

1. Be sure this directory can be read/written by JBoss.
```bash
sudo chmod -R 775 /opt/syslog
sudo chown -R jboss:jboss-admin /opt/syslog
```

# Update from a previous version

The update mechanism has changed at version 5.1.0. Be careful to strictly follow the process associated to the version you come from.

## Update from version prior to GSS 5.1.0

_Reminder: Before version 5.0.0, Gazelle Security Suite was called Gazelle-ATNA-Tools._

1. Download the new **ear** and its associated update SQL script from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-security-suite-ear%22) (search for **gazelle-security-suite-X.X.X-gss.ear** and **gazelle-security-suite-X.X.X-sql.zip**). Not each version has an update sql to execute.

1. Undeploy the old **ear** from JBoss by removing it from the deployment directory.

1. Backup your database

1. Unzip the SQL archive

1. Execute SQL statements that must be run before deployment (Open the update-X.X.X.sql file with a text editor to see which one).

1. Deploy the new **ear**

1. Execute SQL statements that must be run after deployment (Open the update-X.X.X.sql file with a text editor to see which one).

**Due to the update mechanism of the database, it is important to not skip any version of the application in an overall update. You cannot go directly from 4.7.4 to 4.7.12, you will have to repeat the process from 4.7.4 to 4.7.5, 4.7.6, 4.7.7 and so on.**

To update Gazelle-ATNA-tools 4.7.12 to Gazelle-Security-Suite 5.0.0 the process is the same, except that the deployment of the new **ear** (step 5) must be done on a JBoss7 properly installed for GSS.

## Update from version GSS 5.1.0 and younger

1. Undeploy the old **ear** from JBoss by removing it from the deployment directory.

1. Backup your database

1. Download the new **ear** and all intermediate update SQL scripts from your old version (excluded) to your target version (included). Those scripts can be found in *https://gitlab.inria.fr/gazelle/public/core/security-suite/tree/master/gazelle-security-suite-ear/src/main/sql.*. Not each version has an update sql to execute.

1.  Execute the update SQL scripts

1.  Deploy the new **ear**

Example (version number are hypothetical) : to update GSS from 5.1.0 to 5.1.5 you have to execute 5.1.1, 5.1.2, 5.1.3, 5.1.4 and 5.1.5 update SQL scripts, but you only have to deploy the latest gss 5.1.5 **ear**.

# Configuration

Application can be configured directly from the web interface. From the menu, go to Administration > Application preferences _(you need to be logged in as admin\_role)_.

## Gazelle PKI specific considerations

PKI features of GSS require to define a certificate authority (CA) for :

* Signing test participant certificate requests
* Generating Auto-Login CAS certificate request
* Certificates creation from participants table (CSV format)

The certicate authority of the tool can be set in the administration of the application. Set the preference `certificate_authority_Id` to the id (primary key in database) of the certificate of the selected CA. GSS requires that the private key of this certificate is stored in the application, otherwise it would not be possible to sign any requests.

There is several ways to have a certificate authority stored into the application :

### Creating a new self-signed CA

Go to Administration > Create a Certificate Authority, Fill the form and validate. This is the most recommended way.

### Importing an existing CA

Depending on the format go to Administration > Import p12 or Import PEM. Import also the private key.

If you import an existing CA, do not use a CA chained to an issuer publicly trusted. GSS provide certificates for testing purpose only, they must not be valid outside of this context.

## List of properties

| Property name                                         | Description                                                                                                                                                                                                                          | Default value                                                                                                     |
|-------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| **application\_api\_key**                             |                                                                                                                                                                                                                                      | riendutoutpourlinstant                                                                                            |
| **application\_documentation**                        | The link to the user manual.                                                                                                                                                                                                         | https://gazelle.ihe.net/content/gazelle-security-suite                                                            |
| **application\_issue\_tracker**                       | The link to the section of the issue tracker where to report issues about Gazelle-Security-Suite                                                                                                                                     | https://gazelle.ihe.net/jra/browse/TLS                                                                            |
| **application\_release\_notes**                       | The link to the application release notes                                                                                                                                                                                            | https://gazelle.ihe.net/jira/browse/TLS\#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel   |
| **application\_url**                                  | The URL used by any user to access the tool. The application needs it to build permanent links inside the tool                                                                                                                       | http://localhost:8080/gss                                                                                         |
| **assertion\_manager\_url**                           | To link tests and validators to assertions, you will need to deploy Assertion Manager in the test bed. Provide its URL here.                                                                                                         | http://localhost:8080/AssertionManagerGui                                                                         |
| **atna\_mode\_enabled**                               | Enable/disable Audit Trail features : ATNA-Questionnaire and Audit-Message validation.                                                                                                                                               | true                                                                                                              |
| **atna\_questionnaire\_directory**                    | Absolute system path to the directory used to store Audit-Messages samples and validation results linked with a questionnaire.                                                                                                       | /opt/tls/questionnaire                                                                                            |
| **audit\_message\_index**                             | Index used to generate next Audit-Message Specification OID. This value is automatically incremented by the system.                                                                                                                  | 1                                                                                                                 |
| **audit\_message\_root\_oid**                         | Root OID for Audit Message Specification. This value is concatenated with the index at runtime.                                                                                                                                      | 1.1.1.1.1.1.1.                                                                                                    |
| **audit\_message\_validation\_xsl**                   | URL to the validation result stylesheet. Must be in the same domaine, otherwise the majority of modern browser will not perform the transformation. A default one is embedded in the application.                                    | http://localhost:8080/gss/resources/stylesheet/auditMessagevalidatorDetailedResult.xsl                            |
| **certificate\_authority\_Id**                        | Id of the certificate in database of the main certification authority used by the tool to deliver and sign certificates.                                                                                                             | 1                                                                                                                 |
| **crl\_url**                                          | Base URL to print into certificates as revocation list. The tool will add /crl/{id}/cacrl.crl at runtime.                                                                                                                            | http://localhost:8080/gss                                                                                         |
| **dicom\_xsd**                                        | Absolute system path to the DICOM schema (Audit Message validation).                                                                                                                                                                 | /opt/tls/dicom\_syslog\_ext\_us.xsd                                                                               |
| **dcmdump_path**                                      | Absolute system path to the dcmdump binary                                                                                                                                                                                           | /usr/bin/dcmdump                                                                                                  |
| **evs\_client\_url**                                  | The URL of the Gazelle EVSClient application. This is required to validate the messages captured by the proxy.                                                                                                                       | http://localhost:8080/EVSClient                                                                                   |
| **java\_cacerts\_truststore\_pwd**                    | GSS is also using the **cacerts** JVM truststore to validate certificates (located in ${JAVA\_HOME}/jre/lib/security/cacerts). Provide here its password.                                                                            | changeit                                                                                                          |
| **jms_communication_is_enabled**                      | Enable/disable JMS communication through the proxy.                                                                                                                                                                                  | false                                                                                                             |
| **main\_cas\_keyword**                                | Key used to distinct authentication service (maximum length 8).                                                                                                                                                                      | 1ST                                                                                                               |
| **main\_cas\_name**                                   | Name of the authentication service displayed in the GUI.                                                                                                                                                                             | 1st Authentication Service                                                                                        |
| **main\_tm\_application\_url**                        | URL of Gazelle Test Management linked with the first CAS.                                                                                                                                                                            | http://localhost:8080/gazelle                                                                                     |
| **main\_tm\_message\_ws**                             | URL of the Messages web-service of Gazelle Test Management linked with the first CAS.                                                                                                                                                | http://localhost:8080/gazelle-tm-ejb/GazelleMessageWSService/GazelleMessageWS?wsdl                                |
| **main\_tm\_pki\_admins**                             | List of PKI admins usernames in Gazelle Test Management linked with the first CAS (separated with a coma). They will receive a message alert each time a certificate is requested and require a manual validation.                   | admin1,admin2,admin3                                                                                              |
| **NUMBER\_OF\_ITEMS\_PER\_PAGE**                      | Default number of rows displayed in tables (20, 50 or 100).                                                                                                                                                                          | 20                                                                                                                |
| **pki\_automatic\_request\_signing**                  | By default, all certificate signing requests must be validated by hand by an administrator. If you enable the automatic request signing mode, users will get the signed certificate immediatley after submiting their request.       | false                                                                                                             |
| **pki\_mode\_enabled**                                | Enable/disable PKI features.                                                                                                                                                                                                         | true                                                                                                              |
| **proxy\_oid**                                        | This is the OID that uniquely identify the instance of the tool when submitting message validation to EVSClient.                                                                                                                     | to-define                                                                                                         |
| **questionnaire\_display\_access\_phi**               | Enable/disable the *Non network means for accessing PHI* tab in ATNA Questionnaire                                                                                                                                                   | true                                                                                                              |
| **questionnaire\_display\_audit\_messages**           | Enable/disable the *Audit Messages* tab in ATNA Questionnaire                                                                                                                                                                        | true                                                                                                              |
| **questionnaire\_display\_authentication\_process**   | Enable/disable the *Authentication process for local users* tab in ATNA Questionnaire                                                                                                                                                | true                                                                                                              |
| **questionnaire\_display\_inbounds**                  | Enable/disable the *Inbound network communications* tab in ATNA Questionnaire                                                                                                                                                        | true                                                                                                              |
| **questionnaire\_display\_outbounds**                 | Enable/disable the *Outbound network communications* tab in ATNA Questionnaire                                                                                                                                                       | true                                                                                                              |
| **questionnaire\_display\_tls\_tests**                | Enable/disable the *TLS Tests* tab in ATNA Questionnaire                                                                                                                                                                             | true                                                                                                              |
| **rfc3881\_xsd**                                      | Absolute system path to the RFC3881 schema (Audit Message validation).                                                                                                                                                               | /opt/tls/RFC3881.xsd                                                                                              |                                                                                                              |
| **storage\_dicom**                                    | Absolute path to the system folder used to store the DICOM datasets                                                                                                                                                                  | /opt/tls/DICOM                                                                                                    |
| **syslog_automatic_start_enabled**                    | Enable/disable automatic startup of Syslog simulators at application startup.                                                                                                                                                        | false                                                                                                             |
| **syslog_collector_enabled**                          | Enable/disable Syslog simulator feature.                                                                                                                                                                                             | false                                                                                                             |
| **syslog_directory**                                  | Absolute system path to the folder where SyslogCollector configuration files are stored.                                                                                                                                             | /opt/syslog/                                                                                                      |
| **time\_zone**                                        | The time zone used to display the timestamps                                                                                                                                                                                         | Europe/Paris                                                                                                      |
| **tls\_automatic\_simulator\_address\_enabled**       | SSL/TLS simulators detects their own IP address and host and display it to the GUI. If you prefer manually define the address, set this value to false and set the variable tls\_simulator\_address with the value of your choice.   | true                                                                                                              |
| **tls\_mode\_enabled**                                | Enable/disable SSL/TLS simulator features.                                                                                                                                                                                           | true                                                                                                              |
| **tls\_simulator\_address**                           | Manual SSL/TLS simulators address (host / IP). Only displayed if tls\_automatic\_simulator\_address\_enabled is set to false.                                                                                                        | null                                                                                                              |
| **xua\_mode\_enabled**                                | Enable/disable XUA assertions validator.                                                                                                                                                                                             | true                                                                                                              |
| **xmldsig_core_schema_location** | URL to the XML schema for verifying ds:Signature element |https://gazelle.ihe.net/XSD/IHE/XUA/xmldsig-core-schema.xsd |
| **http_max_content_size** | The maximum size of http content that can be supported by the http proxy (the size is in MB) |16 |


## SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).
