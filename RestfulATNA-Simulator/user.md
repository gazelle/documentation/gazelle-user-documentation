---
title: User Manual
subtitle: Patient Manager
author: Alexandre POCINHO
releasedate: 2022-06-21
toolversion: 1.0.0
function: Software Engineer
version: 1.0.0
status: Validated document
reference: KER1-MAN-IHE_CATALYST-CH_RESTFUL_ATNA_USER-1_00 
customer: IHE CATALYST
---

## Objectives
This `restful-atna-simulator` (**RAS**) project is a simulator for RestfulATNA transaction.<br/>
The objective is to simulate Audit Record Repository for ITI-20 transaction through RESTful transactions based on FHIR standard using HAPI FHIR server: 

![ITI-20 Transaction](media/transaction.png)


## How the application works
The project shall do this following action :

- An `Audit Resource Request` is sent to the **RAS**. --> The **RAS** shall return an `Audit Resource Response` with a **HTTP 2xx code**.

- An `Audit Bundle Request` is sent to the **RAS**. --> The **RAS** shall return an `Audit Bundle Response` with a **HTTP 2xx code**. The Audit Bundle Request shall respect these constraints :
![Audit Bundle Request constraints](media/requirements_audit_bundle_request.png)
 

If no resource, an empty one, another FHIR resource or a non-existant FHIR Resource is provided, the RAS shall return a **HTTP 400, 422 or 500 error**.<br/>
NB: For `Audit Bundle Request` cases, when a `entry[0].resource` is declared **a resourceType SHALL BE present**, if not a error "theResource is empty" with a stack java errors appears.

## How to use the application

1. Once the application is deployed, the endpoint is constructed like below :


   POST `endpoint`/`type`?_format=`mime-type` HTTP/1.1

  * `endpoint`: where the application is deployed
  * `type`: The type of the resource you want to send `Bundle` or `AuditEvent`
  * `mime-type`: The format of your response sent by the application `json` or `xml`

2. The Content-Type is declared as below : 
  * Content-Type: `application/fhir+xml` or `application/fhir+json`<br/>
(if the request is sent respectively in `xml` or `json` format)

For example if the application is deployed on `example.com`, and a `AuditEvent` is `sent in JSON` and the `response must be returned in XML`, the endpoint and content-type should be :


- Endpoint: `https://example.com/restful-atna-simulator/AuditEvent?_format=xml`
- Content-Type: `application/fhir+json` 

The **RAS** ONLY accept `POST` request and `Bundle` or `AuditEvent` FHIR resources.











