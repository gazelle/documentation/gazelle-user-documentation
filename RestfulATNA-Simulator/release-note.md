---
title: Release note
subtitle: RESTful ATNA Simulator
toolversion: 1.0.0
releasedate: 2022-06-20
author: Alexandre POCINHO
function: Software Engineer
customer: IHE CATALYST
reference: KER1-MAN-IHE_CATALYST-CH_RESTFUL_ATNA_USER-1_00
---
# 1.0.0
_Release date: 2022-06-20_

**Epic**
* \[[EHSBP-1201](https://gazelle.ihe.net/jira/browse/RécitEHSBP-1206)\] [CH] RESTFul ATNA integration - Development


**Stories**
* \[[RFATNASIMU-1](https://gazelle.ihe.net/jira/browse/RFATNASIMU-1)] Create REST Interface
* \[[RFATNASIMU-2](https://gazelle.ihe.net/jira/browse/RFATNASIMU-2)] Create mustache template for Operation Outcome (json and xml)
* \[[RFATNASIMU-3](https://gazelle.ihe.net/jira/browse/RFATNASIMU-3)] Create service for successful request (200)
* \[[RFATNASIMU-4](https://gazelle.ihe.net/jira/browse/RFATNASIMU-4)] Create service for error request (400,404,500)

