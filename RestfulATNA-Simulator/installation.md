---
title:  Installation Manual
subtitle: RESTful ATNA Simulator
author: Alexandre POCINHO
releasedate: 2022-06-21
toolversion: 1.0.0
function: Software Engineer
version: 1.0.0
status: Validated document
customer: IHE CATALYST
reference: KER1-MAN-IHE_CATALYST-CH_RESTFUL_ATNA_USER-1_00
---

# Sources and package

RESTful ATNA Simulator is a Maven project. Sources are available on the INRIA’s Gitlab project at the following URL :

https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/restful-atna-simulator

If you would like to checkout the sources on your system you might want to use the following git command, provided git is installed on your system.

git clone https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/restful-atna-simulator.git

Note that the latest executable can be extracted from our nexus repository as well at : [Nexus Repository Manager](https://gazelle.ihe.net/nexus/#welcome).

# Installation Guide

## Prequisites

- Installation of Wildfly21 on your host, the link refers to but the installation procedure is exactly the same, just replace all `18` occurences by `21` : https://gazelle.ihe.net/gazelle-documentation/General/wildfly18.html

Link to `WildFly 21.0.2` : https://download.jboss.org/wildfly/21.0.2.Final/wildfly-21.0.2.Final.zip

## Installation steps

1. Get the project from Nexus.
```
cd /tmp
wget https://gazelle.ihe.net/nexus/service/local/repositories/releases/content/net/gazelle/ihe/RESTfulATNASimulator/1.0.0/RESTfulATNASimulator-1.0.0.war
```

2. Move the WAR file on the deployment folder in Wildfly21.
```
mv RESTfulATNASimulator-1.0.0.war /usr/local/wildfly21/standalone/deployments
```
3. Start the wildfly21 service.
```
sudo service wildfly21 start
```
4. The application can be browsed at : 
http://localhost:8080/restful-atna-simulator/<br/>
Port could also be different whether you have modified the JBoss configuration.