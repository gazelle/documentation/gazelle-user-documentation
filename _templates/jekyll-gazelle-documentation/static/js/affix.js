$(document).ready(function () {
    var sidebarTop = $('#nav-sidebar').offset().top;
    $(window).bind('scroll', function () {
        var navHeight = sidebarTop;
        if ($(window).scrollTop() > navHeight) {
            $('nav#nav-sidebar').addClass(' gzl-affix-right');
        }
        else {
            $('nav#nav-sidebar').removeClass(' gzl-affix-right');
        }
    });
});
