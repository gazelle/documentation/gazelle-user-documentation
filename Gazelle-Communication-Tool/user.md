---
title:  User Manual
subtitle: Gazelle Communication Tool
author: Alassane Gaye
function: Software Engineer Intern
releasedate: 2022-07-22
customer: IHE-EUROPE
---

# Introduction

The purpose of the communication tool is to facilitate communication between the participants of a Projectathon or Connectathon.
The current connector is developed for Rocket chat.
Rocket chat like Slack or Teams allows you to discuss , share or talk across the platform with partners or organisations
![img.png](media/user/img.png)
 

# GCT activation

For the moment, the activation is done in a general way for all the testing sessions.

In the application preferences, a section "Gazelle communication tools" allows the configuration.

The checkbox "Gazelle communication tools enabled" allows to globally activate/deactivate the GCT integration.

When the GCT URL is changed, the preferences must be saved before the Rocket.Chat can be refreshed.
![configuration.png](./media/installation/configuration.png)


# Mapping TM/Rocket.Chat

A testing session is linked to a team in Rocket.Chat.
![testing_sessions.png](./media/user/testing_sessions.png)

An institution in a testing session has two channels in the team, one public and one private.
![ad_swiss_public.png](./media/user/ad_swiss_public.png)
![ad_swiss_private.png](./media/user/ad_swiss_private.png)

A test run has a private channel in the testing session's team.A private channel name : "TR_XXXX_TestName"
![img_1.png](media/user/TR.png)

Monitors have a private channel in the testing session team.
[Monitors.png](media/user/Monitors.png)

# Create Test Run
If GCT is enabled,  a private channel is created when a "peer to peer" test run is created  in Rocket.Chat .

First, all systems participating in the test run are retrieved. The person who created the test run is designated as the test run owner.

In Rocket.Chat:

A private channel is created in the team, with the link to the test run in Gazelle

The creator of the test run is added as moderator to the channel

The presence of channels in the team of systems institutions is checked

For each system institution :

A message is sent on the private channel of the institution's team with a notification about the creation of a test run on one of their systems. The message contains the links to the channel and the test run in Gazelle, as well as the list of participating systems

A message is sent on the test run




# Join Test Run Channel

Allow test partners to join the channel
 ![join.png](media/user/join.png)

# Archive and Unarchive

The archiving status of the channel is changed according to the new status:
    - Archive : verified, broken, self verified, aborted, failed
    - Unarchive : others status 

# Data 

The gateway never deletes anything: teams, channels, users in teams/channels.

If a user is present in a team or a channel at a given time, and he/she should not be part of this team/channel afterwards, he/she keeps his/her membership and his/her role in the team/channel.

If a team/channel has been created at a given time, it is never deleted.

If the user is deactivated in Gazelle, it remains present in Rocket.Chat but the user will not be able to connect anymore.

