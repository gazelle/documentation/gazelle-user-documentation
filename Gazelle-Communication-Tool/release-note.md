---
title: Release note
subtitle: Gazelle Communication Tool
toolversion: 1.0.1
releasedate: 2022-07-22
author: Alassane Gaye
function: Software Engineer
customer: IHE Europe
---

# 1.0.1 

_Release date: 2022-07-22_ 

__Bug__
* \[[COMT-12](https://gazelle.ihe.net/jira/browse/COMT-12)\] [QUAL] PIN not recognized during Two Factor Authentification configuration
* \[[COMT-15](https://gazelle.ihe.net/jira/browse/COMT-15)\] [QUAL] creation of gazelle account in TM make trouble in permission rights
* \[[COMT-16](https://gazelle.ihe.net/jira/browse/COMT-16)\] [QUAL] vip account cannot change pwd from rocket chat
* \[[COMT-17](https://gazelle.ihe.net/jira/browse/COMT-17)\] [FIX] change testing testing session
* \[[COMT-25](https://gazelle.ihe.net/jira/browse/COMT-25)\] [QUAL] User rights are not correct on RC
* \[[COMT-26](https://gazelle.ihe.net/jira/browse/COMT-26)\] [QUAL] A monitor in TM don't have a monitor channel in RC
* \[[COMT-27](https://gazelle.ihe.net/jira/browse/COMT-27)\] [QUAL] modification of name of testing session or orga in TM doesn't impact RC
* \[[COMT-29](https://gazelle.ihe.net/jira/browse/COMT-29)\] [QUAL] a comment in TM don't report the author of the message in RC
* \[[COMT-33](https://gazelle.ihe.net/jira/browse/COMT-33)\] [QUAL] gazelle user write to many log message on RC
* \[[COMT-37](https://gazelle.ihe.net/jira/browse/COMT-37)\] [QUAL] a simple user in RC don't see the channels of his company
* \[[COMT-38](https://gazelle.ihe.net/jira/browse/COMT-38)\] [QUAL] changing role to become a TSM on a testing session don't add monitor channel
* \[[COMT-39](https://gazelle.ihe.net/jira/browse/COMT-39)\] [QUAL] a Monitor is not added to the TR channel when he claim the test
* \[[COMT-41](https://gazelle.ihe.net/jira/browse/COMT-41)\] [QUAL] Invite link redirect to a 404 page

__Story__
* \[[COMT-5](https://gazelle.ihe.net/jira/browse/COMT-5)\] Story create testrun channel
* \[[COMT-6](https://gazelle.ihe.net/jira/browse/COMT-6)\] Story join testrun channel
* \[[COMT-22](https://gazelle.ihe.net/jira/browse/COMT-22)\] Archive Channel
* \[[COMT-23](https://gazelle.ihe.net/jira/browse/COMT-23)\] Create channel
* \[[COMT-24](https://gazelle.ihe.net/jira/browse/COMT-24)\] Send message
* \[[COMT-48](https://gazelle.ihe.net/jira/browse/COMT-48)\] UnArchive Channel

__Task__
* \[[COMT-4](https://gazelle.ihe.net/jira/browse/COMT-4)\] Create java 7 client (in TM)
* \[[COMT-47](https://gazelle.ihe.net/jira/browse/COMT-47)\] rocket chat  permissions to interoperate with  gazelle tm
* \[[COMT-49](https://gazelle.ihe.net/jira/browse/COMT-49)\] Ajust communication-tool-gateway docker image for indus
* \[[COMT-50](https://gazelle.ihe.net/jira/browse/COMT-50)\] Create mongoDB  docker image
* \[[COMT-51](https://gazelle.ihe.net/jira/browse/COMT-51)\] Ajust Rocket.chat  docker image for indus




