---
title: Release note
subtitle: Schematron Validator
toolversion: 2.6.1
releasedate: 2024-02-26
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-SCHEMATRON_VALIDATOR
---

# 2.6.2

_Release date: 2025-01-10_

__Feature__

* \[[SCHVAL-197](https://gazelle.ihe.net/jira/browse/SCHVAL-197)\] Add a compilation timestamp column in 
Manage Schematron view

# 2.6.1
_Release date: 2024-02-26_

__Bug__
* \[[SCHVAL-196](https://gazelle.ihe.net/jira/browse/SCHVAL-196)\] Cannot use DnonProxyHosts option

__Task__
* \[[SCHVAL-194](https://gazelle.ihe.net/jira/browse/SCHVAL-194)\] Oudated file iso_svrl_for_xslt2.xsl used for schematron compilation, now use 2010-04-14

# 2.6.0
_Release date: 2024-02-06

Context : Gazelle User Management Renovation step 2

__Task__
* \[[SCHVAL-192](https://gazelle.ihe.net/jira/browse/SCHVAL-192)\] Integrate new sso-client-v7

# 2.5.2
_Release date: 2023-04-04_

__Bug__
* \[[SCHVAL-186](https://gazelle.ihe.net/jira/browse/SCHVAL-186)\] ANS CR-Bio validation

# 2.5.1
_Release date: 2021-12-21_

__Improvement__
* \[[SCHVAL-181](https://gazelle.ihe.net/jira/browse/SCHVAL-181)\] Externalize the configuration of the mail server

# 2.5.0
_Release date: 2019-03-18_

__Remarks__
Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.

__Improvement__

* \[[SCHVAL-174](https://gazelle.ihe.net/jira/browse/SCHVAL-174)\] Extract datasource configuration
* \[[SCHVAL-175](https://gazelle.ihe.net/jira/browse/SCHVAL-175)\] Update SQL scripts archive

# 2.4.2
_Release date: 2019-01-28_

__Bug__

* \[[SCHVAL-179](https://gazelle.ihe.net/jira/browse/SCHVAL-179)\] The checkbox "Process Unknown check type as an error?" does not work no more

# 2.4.1
_Release date: 2018-10-18_


__Bug__

* \[[SCHVAL-173](https://gazelle.ihe.net/jira/browse/SCHVAL-173)\] The namespace xmlns:cda="urn:hl7-org:v3" is defined twice after schematron compilation

# 2.4.0
_Release date: 2018-10-05_

__Remarks__
This version is compatible with 5.8.0 EVSClient version and more. 

__Bug__

* \[[SCHVAL-135](https://gazelle.ihe.net/jira/browse/SCHVAL-135)\] [LUX] Problem with the compilation with schematrons typed ART-DECOR
* \[[SCHVAL-136](https://gazelle.ihe.net/jira/browse/SCHVAL-136)\] Bug on the generation of xsl based on art-decor styled schematrons
* \[[SCHVAL-156](https://gazelle.ihe.net/jira/browse/SCHVAL-156)\] Un-understandable error if XSD version is wrongly defined
* \[[SCHVAL-157](https://gazelle.ihe.net/jira/browse/SCHVAL-157)\] If no XSD is defined for validation, it fails without explanation
* \[[SCHVAL-159](https://gazelle.ihe.net/jira/browse/SCHVAL-159)\] Schematron Validator does not implement the CAS client properly
* \[[SCHVAL-161](https://gazelle.ihe.net/jira/browse/SCHVAL-161)\] Review GazelleObjectValidatorWS structure
* \[[SCHVAL-165](https://gazelle.ihe.net/jira/browse/SCHVAL-165)\] Mixing up between label, keyword, name of entities... 
* \[[SCHVAL-171](https://gazelle.ihe.net/jira/browse/SCHVAL-171)\] When adding or editing a Schematron, the path is not trimed
* \[[SCHVAL-172](https://gazelle.ihe.net/jira/browse/SCHVAL-172)\] The validation stopped when there is a namespace URI that start by a number

__Story__

* \[[SCHVAL-169](https://gazelle.ihe.net/jira/browse/SCHVAL-169)\] Create new xsl to display validation result
* \[[SCHVAL-170](https://gazelle.ihe.net/jira/browse/SCHVAL-170)\] Release 2.4.0

__Improvement__

* \[[SCHVAL-166](https://gazelle.ihe.net/jira/browse/SCHVAL-166)\] Improve code and business location through the code for better unit-test coverage
* \[[SCHVAL-168](https://gazelle.ihe.net/jira/browse/SCHVAL-168)\] Restructure validation result

# 2.3.1
_Release date: 2018-03-23_


__Bug__

* \[[SCHVAL-160](https://gazelle.ihe.net/jira/browse/SCHVAL-160)\] Not all paths to external files are relative

# 2.3.0
_Release date: 2018-02-27  _

__Remarks__

SchematronValidator integrates the new client module for gazelle-transformation-ws-client, this 2.3.0 version is compatible with Gazelle Transformation 2.0.0 and later.
The PostgreSQL driver has been externalized and must then be deployed as a JBoss module.


__Bug__

* \[[SCHVAL-153](https://gazelle.ihe.net/jira/browse/SCHVAL-153)\] Update postgresql driver to version 42.2.1.jre7

__Story__

* \[[SCHVAL-151](https://gazelle.ihe.net/jira/browse/SCHVAL-151)\] Integration with new Gazelle Transformation Client

# 2.2.4
_Release date: 2018-01-22_


__Bug__

* \[[SCHVAL-133](https://gazelle.ihe.net/jira/browse/SCHVAL-133)\] When we dupplicate a schematron, we do not dupplicate the daffodil description and if need dafodil transformation
* \[[SCHVAL-149](https://gazelle.ihe.net/jira/browse/SCHVAL-149)\] When the method "getAllSchematrons" is called some values in the table schematron are set to null
* \[[SCHVAL-150](https://gazelle.ihe.net/jira/browse/SCHVAL-150)\] For some reason, all the attiribute daffodil_transformation_needed in the table sch_validator_schematron were lost and initialized to NULL

__Improvement__

* \[[SCHVAL-140](https://gazelle.ihe.net/jira/browse/SCHVAL-140)\] Missing translations

# 2.2.2
_Release date: 2017-11-03_

__Bug__

* \[[SCHVAL-138](https://gazelle.ihe.net/jira/browse/SCHVAL-138)\] When an XSD processing failed, user or admin have no clue

# 2.2.0
_Release date: 2017-08-23_

__Remarks__

From version 2.2.0, Schematron Validator can be used to validate flat files by calling Daffodil Transformation tool before running the schematron validator on the resulting XML file.

__Bug__

* \[[SCHVAL-113](https://gazelle.ihe.net/jira/browse/SCHVAL-113)\] When an entry only as an XSD (no schematron), the compile button shall not be available
* \[[SCHVAL-128](https://gazelle.ihe.net/jira/browse/SCHVAL-128)\] When creating a schematron entry, the path to the xsd and the schematron file is required
* \[[SCHVAL-129](https://gazelle.ihe.net/jira/browse/SCHVAL-129)\] [EFS] Unable to retrieve schematron in EVSClient

__Story__

* \[[SCHVAL-126](https://gazelle.ihe.net/jira/browse/SCHVAL-126)\] Validation report gives wrong information

__Improvement__

* \[[SCHVAL-131](https://gazelle.ihe.net/jira/browse/SCHVAL-131)\] In the edit Schematron page, shows only the daffodil oid field when daffodil transformation is checked

# 2.1.1
_Release date: 2017-01-17_

__Bug__

* \[[SCHVAL-43](https://gazelle.ihe.net/jira/browse/SCHVAL-43)\] When updating the XSD schema on the server, it is not taken into account by the tool
* \[[SCHVAL-123](https://gazelle.ihe.net/jira/browse/SCHVAL-123)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm

__Story__

* \[[SCHVAL-109](https://gazelle.ihe.net/jira/browse/SCHVAL-109)\] Support to the Schematrons generated by ART-DECOR

# 2.1.0
_Release date: 2017-01-05_

__Remarks__


As the web service of Schematron Validator has been modified in this release, these tools have also been updated :
 - gazelle-tm-tools with a new release generated :   4.0.18
 - gazelle-ws-clients with a new release generated : 2.14
 
 Thus, before deploying this Schematron Validator tool, It's mandatory to use the next release of EVSClient, which use the tools shown above, to avoid incompatibility issues : The version 5.0.15 or ealier.

__Bug__

* \[[SCHVAL-23](https://gazelle.ihe.net/jira/browse/SCHVAL-23)\] Need additional field to characterize a schematron
* \[[SCHVAL-108](https://gazelle.ihe.net/jira/browse/SCHVAL-108)\] When Schematron contains no schematron (only XSD), then soap request from EVSClient to get schematron fails
* \[[SCHVAL-118](https://gazelle.ihe.net/jira/browse/SCHVAL-118)\] transformUnknowns is reseted to null after calling Schematron webservice

__Story__

* \[[SCHVAL-69](https://gazelle.ihe.net/jira/browse/SCHVAL-69)\] Update of the user guide on SchematronValidator
* \[[SCHVAL-106](https://gazelle.ihe.net/jira/browse/SCHVAL-106)\] Missing line numbers and column in XSD validation report for schematron validation
* \[[SCHVAL-107](https://gazelle.ihe.net/jira/browse/SCHVAL-107)\] Add support to XSD 1.1
* \[[SCHVAL-114](https://gazelle.ihe.net/jira/browse/SCHVAL-114)\] XSD Validation of SVS document does not report line numbers and column numbers of error as it is done for CDA, XDW, XDS...

__Improvement__

* \[[SCHVAL-48](https://gazelle.ihe.net/jira/browse/SCHVAL-48)\] we need a button on the list of manageSchematrons that allow to compile all schematrons
* \[[SCHVAL-115](https://gazelle.ihe.net/jira/browse/SCHVAL-115)\] Add some field in the report of the Schematron Validation
* \[[SCHVAL-117](https://gazelle.ihe.net/jira/browse/SCHVAL-117)\] Add button to select between XSD 1.0 and XSD 1.1 for schematron validation

# 2.0.4
_Release date: 2016-11-24_

__Bug__

* \[[SCHVAL-63](https://gazelle.ihe.net/jira/browse/SCHVAL-63)\] Migration to jboss7

__Improvement__

* \[[SCHVAL-111](https://gazelle.ihe.net/jira/browse/SCHVAL-111)\] CAS login redirects to the same page instead of the home page
* \[[SCHVAL-112](https://gazelle.ihe.net/jira/browse/SCHVAL-112)\] Automatically recompile a schematron if source is newest

# 2.0.3
_Release date: 2016-07-20_


__Story__

* \[[SCHVAL-105](https://gazelle.ihe.net/jira/browse/SCHVAL-105)\] Bug in the definition of Schematrons

# 2.0.2
_Release date: 2016-06-06_

__Bug__

* \[[SCHVAL-100](https://gazelle.ihe.net/jira/browse/SCHVAL-100)\] Can't edit an existing schematron

# 2.0.0
_Release date: 2016-06-01_


__Bug__

* \[[SCHVAL-87](https://gazelle.ihe.net/jira/browse/SCHVAL-87)\] In Manage Schematrons, edition feilds are not the same like creation fields
* \[[SCHVAL-90](https://gazelle.ihe.net/jira/browse/SCHVAL-90)\] The footer is not well populated
* \[[SCHVAL-91](https://gazelle.ihe.net/jira/browse/SCHVAL-91)\] We can't get the version with the rest WS
* \[[SCHVAL-92](https://gazelle.ihe.net/jira/browse/SCHVAL-92)\] Title trouble
* \[[SCHVAL-94](https://gazelle.ihe.net/jira/browse/SCHVAL-94)\] /Schematron Validator/System testing/Manage Object Types/SCHVAL-22:Access to the object type management  list page -  Ex
* \[[SCHVAL-95](https://gazelle.ihe.net/jira/browse/SCHVAL-95)\] Create a schematron with an already existing keyword leads to an unexpected error.

__Story__

* \[[SCHVAL-85](https://gazelle.ihe.net/jira/browse/SCHVAL-85)\] Deploy Schematron Validator on kujira for testday (+ configuration)
* \[[SCHVAL-97](https://gazelle.ihe.net/jira/browse/SCHVAL-97)\] Release SchematronValidator 2.0.0
* \[[SCHVAL-98](https://gazelle.ihe.net/jira/browse/SCHVAL-98)\] Installation of SchematronValidator 2.0.0 on ovh3
* \[[SCHVAL-99](https://gazelle.ihe.net/jira/browse/SCHVAL-99)\] Update documentation on drupal of SchematronValidator (2.0.0 release)
* \[[SCHVAL-102](https://gazelle.ihe.net/jira/browse/SCHVAL-102)\] Test report of SchematronValidator 2.0.0 - migration to Jboss 7

# 1.17.4
_Release date: 2015-11-27_


__Bug__

* \[[SCHVAL-61](https://gazelle.ihe.net/jira/browse/SCHVAL-61)\] bug in the validation of unknown check


# 1.17.3
_Release date: 2015-11-26_


__Bug__

* \[[SCHVAL-61](https://gazelle.ihe.net/jira/browse/SCHVAL-61)\] bug in the validation of unknown check

# 1.17.2
_Release date: 2015-11-26_


__Bug__

* \[[SCHVAL-60](https://gazelle.ihe.net/jira/browse/SCHVAL-60)\] Add the possibility to convert unknown checks to errors

# 1.17.1
_Release date: 2015-10-12_

__Remarks__

* \[[SCHVAL-59](https://gazelle.ihe.net/jira/browse/SCHVAL-59)\] Add the referencing to the CAS from the db

# 1.17.0
_Release date: 2014-06-06_


__Bug__

* \[[SCHVAL-20](https://gazelle.ihe.net/jira/browse/SCHVAL-20)\] WebService aboutThisApplication returns un-initialized values
* \[[SCHVAL-45](https://gazelle.ihe.net/jira/browse/SCHVAL-45)\] delete of schematron does not work
* \[[SCHVAL-50](https://gazelle.ihe.net/jira/browse/SCHVAL-50)\] Need to fix the pointer to the CAS for the GE and PROD profiles

# 1.14
_Release date: 2013-09-23_


__Story__

* \[[SCHVAL-44](https://gazelle.ihe.net/jira/browse/SCHVAL-44)\] Add the possibility to login by ip address

# 1.12
_Release date: 2013-06-27_


__Bug__

* \[[SCHVAL-40](https://gazelle.ihe.net/jira/browse/SCHVAL-40)\] add the possibility to validate according to a schema, no schematron provided

# 1.11
_Release date: 2013-03-11_


__Bug__

* \[[SCHVAL-35](https://gazelle.ihe.net/jira/browse/SCHVAL-35)\] Improvement of the process of validation

__Improvement__

* \[[SCHVAL-27](https://gazelle.ihe.net/jira/browse/SCHVAL-27)\] Improvement of the schematronValidator project
* \[[SCHVAL-36](https://gazelle.ihe.net/jira/browse/SCHVAL-36)\] Add the possibility to force the view of successful reports of validation, when the schematron is configured only to report errors

# 1.10
_Release date: 2013-03-08_


__Bug__

* \[[SCHVAL-34](https://gazelle.ihe.net/jira/browse/SCHVAL-34)\] Configuration fo the schematrons of pharm to gazelle style of output

# 1.8
_Release date: 2013-02-21_

__Bug__

* \[[SCHVAL-32](https://gazelle.ihe.net/jira/browse/SCHVAL-32)\] When a document is not valid, the schematron validation is not performed, but the message displayed is "PASSED"

# 1.7
_Release date: 2013-02-21_

__Bug__

* \[[SCHVAL-29](https://gazelle.ihe.net/jira/browse/SCHVAL-29)\] When a document is not well formed, some errors occures

# 1.5
_Release date: 2013-02-12_

__Improvement__

* \[[SCHVAL-18](https://gazelle.ihe.net/jira/browse/SCHVAL-18)\] We need a notion of AffinityDomain
* \[[SCHVAL-25](https://gazelle.ihe.net/jira/browse/SCHVAL-25)\] Improve the schema validation
* \[[SCHVAL-26](https://gazelle.ihe.net/jira/browse/SCHVAL-26)\] Problem on the creation of versions

# 1.4
_Release date: 2011-09-12_

__Bug__

* \[[SCHVAL-21](https://gazelle.ihe.net/jira/browse/SCHVAL-21)\] In some cases that needs to be identified the result file returned by the Schematron Validator is inconsistent.
* \[[SCHVAL-22](https://gazelle.ihe.net/jira/browse/SCHVAL-22)\] [MIF validation] conflict with other validations in the CDA validation

__Story__

* \[[SCHVAL-17](https://gazelle.ihe.net/jira/browse/SCHVAL-17)\] Maven module: create a ws client for validation which will be used in other projects to validate files

# 1.2
_Release date: 2011-07-04_


__Improvement__

* \[[SCHVAL-19](https://gazelle.ihe.net/jira/browse/SCHVAL-19)\] addition of MIF validation

# 1.1-GA
_Release date: 2011-01-06_


__Story__

* \[[SCHVAL-15](https://gazelle.ihe.net/jira/browse/SCHVAL-15)\] User Management
* \[[SCHVAL-16](https://gazelle.ihe.net/jira/browse/SCHVAL-16)\] add a schematron view page

# 1.0-SR9
_Release date: 2010-10-22_


__Bug__

* \[[SCHVAL-12](https://gazelle.ihe.net/jira/browse/SCHVAL-12)\] Validation using XDS-SD schematrons always aborts

# 1.0-SR7
_Release date: 2010-09-15_


__Story__

* \[[SCHVAL-11](https://gazelle.ihe.net/jira/browse/SCHVAL-11)\] Add a button which enables the admin to update schematron versions according the version mentionned in the file

# 1.0-SR6
_Release date: 2010-09-21_


__Bug__

* \[[SCHVAL-8](https://gazelle.ihe.net/jira/browse/SCHVAL-8)\] File received for validation will be Base64 encoded, decode them before validation

# 1.0-SR5
_Release date: 2010-09-09_


__Story__

* \[[SCHVAL-2](https://gazelle.ihe.net/jira/browse/SCHVAL-2)\] Add schematron for XCPD validation
* \[[SCHVAL-7](https://gazelle.ihe.net/jira/browse/SCHVAL-7)\] Aborted validation : need to inform the administrator

# 1.0-SR1
_Release date: 2010-07-30_


__Bug__

* \[[SCHVAL-6](https://gazelle.ihe.net/jira/browse/SCHVAL-6)\] validation aborts when XML document is not well-formed

__Improvement__

* \[[SCHVAL-1](https://gazelle.ihe.net/jira/browse/SCHVAL-1)\] We must check that the document to validate is well-formed and valid

# 1.0-GA
_Release date: 2010-07-13_


__Bug__

* \[[SCHVAL-5](https://gazelle.ihe.net/jira/browse/SCHVAL-5)\] Uncomplete result details when validation aborts

__Story__

* \[[SCHVAL-14](https://gazelle.ihe.net/jira/browse/SCHVAL-14)\] EVS: We need a stand alone project to validate file by schematrons using web services
