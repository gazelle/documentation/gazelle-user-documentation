---
title:  Installation Manual
subtitle: Schematron Validator
author: Hilary RAMANANTSALAMA
date: 2024-02-26
toolversion: 2.X.X
function: Engineer
version: 2.01
status: For review
reference: KER1-MAN-IHE-SCHEMATRON_VALIDATOR_INSTALLATION-1_04
customer: IHE-EUROPE
---
# Sources and package

Schematron-based Validator is a maven project. Sources are available on the INRIA Source Forge at the following URL:

[*https://gitlab.inria.fr/gazelle/public/validation/schematron-validator.git*](https://gitlab.inria.fr/gazelle/public/validation/schematron-validator.git)

If you'd like to checkout the sources on your system you might want to use the following git command, provided subversion is installed on your system.

```bash
git clone https://gitlab.inria.fr/gazelle/public/validation/schematron-validator.git
```

Note that the latest executable can be extracted from our nexus repository as well  at
[*Nexus Repository Manager*](https://gazelle.ihe.net/nexus/index.html#nexus-search;quick%7ESchematronValidator-ear).

**Important**, if you download the **ear** from Nexus it will have a name such as `SchematronValidator-ear-2.0.0.ear`, then be sure to rename it to `SchematronValidator.ear` otherwise the deployment will fail.

# Installation

This tool is to be installed in a Jboss 7.2 application server and runs with a PosgreSQL database.

If you do not have yet a Jboss installed in your environment, proceed as described in [General consideration page](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

## Database creation

Your database must have a user **gazelle**.
1. Download the SQL scripts archive from [*Nexus Repository Manager*](https://gazelle.ihe.net/nexus/index.html#nexus-search;quick%7ESchematronValidator-ear) (search for **SchematronValidator-ear-X.X.X-sql.zip**).
2. Unzip the archive into a folder `SchematronValidator-sql`
3. Connect to your database
```bash
psql -U gazelle
```
4. Excute the SQL statement to create the database.
```sql
CREATE DATABASE schematron-validator-prod OWNER gazelle ENCODING UTF8 ;
```
5. Initialize database by running :
```bash
psql -U gazelle schematron-validator-prod < SchematronValidator-sql/init.sql
```

## Deployment
To deploy Schematron Validator:
1. Paste the archive **SchematronValidator.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`
2. Display JBoss server logs, start JBoss and wait for **ear** deployment.
3. The application can be browsed at http://localhost:8080/SchematronValidator. *Port could also be different whether you have modified the JBoss configuration*.
4. **WARNING** : From version 2.5.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file Schematronvalidator-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).


Datasource name : SchematronValidatorDS

Database name : schematron-validator-prod

5. **WARNING** : Since version 2.5.1, you need to configure the mail session that is needed by the tool in the jboss configuration file `standalone.xml`. Read details in the "Externalize mail server configuration" section of [JBoss 7 installation page](../General/jboss7.html) for further information. 


## Data storage
Schematron Validator will need directories to store XSD and schematrons. By default, the application is configured to use `/opt/SchematronValidator_prod`.
```bash
sudo mkdir /opt/SchematronValidator_prod
sudo mkdir /opt/SchematronValidator_prod/bin
sudo mkdir /opt/SchematronValidator_prod/xsd
```

The repository `/bin` shall contain `schematron`, `compilation` and `ART-DECOR_precompilation_tools` repositories and the jar file `XSDValidator-1.0-jar-with-dependencies.jar`. It is available via Subversion at https://svn.ihe-europe.net/gazelle/Maven/ihe-xmltools-jar/trunk/src/test/resources/jar/

Be sure the directory can be read/written by JBoss.
```bash
sudo chmod -R 775 /opt/SchematronValidator_prod
sudo chown -R jboss:jboss-admin /opt/SchematronValidator_prod
```
# Update from a previous version

1. Download the new **ear** and its associated update SQL script from our [*Nexus Repository Manager*](https://gazelle.ihe.net/nexus/index.html#nexus-search;quick%7ESchematronValidator-ear) (search for **SchematronValidator-X.X.X.ear** and **SchematronValidator-X.X.X-sql.zip**). Not each version has an update sql to execute.

1. Unzip the SQL archive

1. Execute SQL statements. Check that the database is correctly updated.

1. Deploy the new **ear**

**Due to the update mechanism of the database, it is important to not skip any version of the application in an overall update. You cannot go directly from 2.0.0 to 2.3.0, you will have to repeat the process from 2.0.0 to 2.0.1, 2.0.2, 2.0.3 and so on.**

# Configuration

| Preference name | Description | Default value |
|-----------------|-----------------------------------------|---------------------|
|application_admin_email         | An email address. Administrator of the tool might received messages when errors occur in the application such as compilation failure of schematron                                        | -                    |
|application_admin_name   |Application admin name   |-                    |
|application_admin_title    | Application admin title     |Application Administrator|
|application_build_time       | Application build time      |July 12, 2010 - 11:59 AM |
|application_db_drop_and_import | | update |
|application_drools_loaded_at_startup |  | false|
|application_email_account_for_history |    |net.gazelle.dev@gmail.com |
|application_gazelle_release_notes_url |   |https://gazelle.ihe.net/jira/projects/SCHVAL?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page&status=released |
|application_issue_tracker_url  |     |https://gazelle.ihe.net/jira/projects/SCHVAL/summary  |
|application_name         | The name of the application                                        | Schematron Validator                     |
|application_profile         | The profile of the deployed application                                        | prod                    |
|application_release_notes_url |   |https://gazelle.ihe.net/jira/projects/SCHVAL?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page&status=released |
|application_url         | The public URL of the tool                                         |[http://yourDomain/SchematronValidator](http://yourDomain/SchematronValidator)                     |
|application_url_basename         |                                         | SchematronValidator                     |
|application_zone         |                                         | EUROPE                     |
|bin_path         | Relative path to the folder in which are stored the schematrons                                        | bin                     |
|cda_xsd_path         | absolute path to the XSD file used for validating CDA                                        |     /opt/SchematronValidator_prod/xsd/cda/CDA.xsd                |
|data_path         |                                        |   data                  |
|documentation_url         |     Link to the user guide                                   |   https://gazelle.ihe.net/gazelle-documentation/Schematron-Validator/user.html                  |
|epsos_cda_xsd_path         | absolute path to the XSD file used for validating epSOS CDA                                        |                     |
|gazelle_home_path         | absolute path to the directory in which are stored the files used by the tool                                         | /opt/SchematronValidator                    |
|mif_root_directory         | Relative path to the folder where are stored the MIF files                                         | /mif                     |
|monitor_email         | The email of the second person who will receive failure notification from the tool                                         |    -                 |
|reports_path        |                                          |  reports                |
|time_zone        | Time zone                                         |  UTC+01                |
|gazelle_transformation_url |     | http://localhost:8580/transformation-ejb/GazelleTransformationService/Transformation?wsdl |
|xsd_1_1_validator_path  |    |/opt/SchematronValidator_prod/bin/XSDValidator-1.0-jar-with-dependencies.jar|


# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).