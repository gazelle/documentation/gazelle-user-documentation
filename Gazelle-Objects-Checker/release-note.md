---
title: Release note
subtitle: Gazelle Objects Checker
toolversion: 3.2.2
releasedate: 2024-10-01
author: Achraf ACHKARI-BEGDOURI
function: Software engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GOC
---

# 3.2.3

_Release date: 2024-11-29_

__Improvement__
* \[[GOC-283](https://gazelle.ihe.net/jira/browse/GOC-283)\] Update ehdsi build scripts to exclude templates from validator

# 3.2.2

_Release date: 2024-10-01_

__Bug__
* \[[GOC-284](https://gazelle.ihe.net/jira/browse/GOC-284)\] XML Schema Definition (XSD) are outdated for ehdsi
* \[[GOC-285](https://gazelle.ihe.net/jira/browse/GOC-285)\] [eHDSI Substance Administration] Rule not verified by the validator

# 3.2.1

_Release date: 2024-02-09_

__Bug__
* \[[GOC-280](https://gazelle.ihe.net/jira/browse/GOC-280)\] GOC Crash if datatype is not found [3.2.0 Regression]

# 3.2.0

_Release date: 2024-02-07_

__Improvement for xehealth project__
* \[[GOC-258](https://gazelle.ihe.net/jira/browse/GOC-258)\] Create uml model for each namespace
* \[[GOC-259](https://gazelle.ihe.net/jira/browse/GOC-259)\] Update goc to support new models
* \[[GOC-257](https://gazelle.ihe.net/jira/browse/GOC-257)\] Create cdaxheallth-code-jar project
* \[[GOC-261](https://gazelle.ihe.net/jira/browse/GOC-261)\] Add System Tests for XHealth usecase
* \[[GOC-260](https://gazelle.ihe.net/jira/browse/GOC-260)\] Create x-health script

__Bug__
* \[[GOC-246](https://gazelle.ihe.net/jira/browse/GOC-246)\] Elements with same name are not well processed in XHealth Project
* \[[GOC-253](https://gazelle.ihe.net/jira/browse/GOC-253)\] Prohibited option not well converted in attributes in XHealth project
* \[[GOC-264](https://gazelle.ihe.net/jira/browse/GOC-264)\] Wrong Model Based validation constraint

# 3.1.4

_Release date: 2023-02-10_

__Bug__
* \[[GOC-255](https://gaz* \[[GOC-249](https://gazelle.ihe.net/jira/browse/GOC-249)\] XSD Pharm extension namespace issue with cdaepsos model
elle.ihe.net/jira/browse/GOC-255)\] Issue with friendly validator


# 3.1.3

_Release date: 2023-02-03_

__Bug__
* \[[GOC-249](https://gazelle.ihe.net/jira/browse/GOC-249)\] XSD Pharm extension namespace issue with cdaepsos model


# 3.1.2

_Release date: 2022-11-22_

__Bug__
* \[[GOC-252](https://gazelle.ihe.net/jira/browse/GOC-252)\] Problems with the CDA FRIENDLY L3 Validator

# 3.1.1

_Release date: 2022-10-24_

__Bug__
* \[[GOC-247](https://gazelle.ihe.net/jira/browse/GOC-247)\] Cardinality processing issue with same name elements

__Improvement__
* \[[GOC-248](https://gazelle.ihe.net/jira/browse/GOC-248)\] Improve Generation Performance

# 3.1.0

_Release date: 2022-06-29_

__Improvement__
* \[[GOC-208](https://gazelle.ihe.net/jira/browse/GOC-208)\] Review functional & technical "Choices" implementation in GOC
* \[[GOC-209](https://gazelle.ihe.net/jira/browse/GOC-209)\] Choices Functional analysis

__Bug__
* \[[GOC-235](https://gazelle.ihe.net/jira/browse/GOC-235)\] Issues with CDA validatiors Wave 6 generation

__Task__
* \[[GOC-203](https://gazelle.ihe.net/jira/browse/GOC-203)\] Schematron Validation with GOC Analysis
* \[[GOC-224](https://gazelle.ihe.net/jira/browse/GOC-224)\] Verify HL7 Rules  for ANS MBVal
* \[[GOC-223](https://gazelle.ihe.net/jira/browse/GOC-223)\] Identify wrong raised errors compared to Schematron
* \[[GOC-206](https://gazelle.ihe.net/jira/browse/GOC-206)\] GOC System Testing




# 3.0.4
_Release date: 2022-04-20_

__Improvement__

* \[[GOC-227](https://gazelle.ihe.net/jira/browse/GOC-227)\] Support new UCUM ValueSets validations
* \[[GOC-206](https://gazelle.ihe.net/jira/browse/GOC-206)\] GOC System Testing
* \[[GOC-215](https://gazelle.ihe.net/jira/browse/GOC-215)\] Add "choices disable processing" feature
* \[[GOC-221](https://gazelle.ihe.net/jira/browse/GOC-221)\]  Documentation


# 3.0.2

_Release date: 2021-11-10_


__Improvement__
* \[[GOC-197](https://gazelle.ihe.net/jira/browse/GOC-197)\] Automate SEQUOIA CDA Adaption with SVS

# 3.0.1

_Release date: 2021-11-03_


__Epic__
* \[[GOC-171](https://gazelle.ihe.net/jira/browse/GOC-171)\] Improve GOC Logs and error handlings
* \[[GOC-177](https://gazelle.ihe.net/jira/browse/GOC-177)\] [GOC] GOC Renovation

__Bug__
* \[[GOC-186](https://gazelle.ihe.net/jira/browse/GOC-186)\] Fix '@xsi:type' in XPATH assertions
* \[[GOC-187](https://gazelle.ihe.net/jira/browse/GOC-187)\] Implement choices in GOC

__Improvement__
* \[[GOC-160](https://gazelle.ihe.net/jira/browse/GOC-160)\] Move the change of imports from packager to the generation plugin
* \[[GOC-190](https://gazelle.ihe.net/jira/browse/GOC-190)\] Make GOC Releasable


