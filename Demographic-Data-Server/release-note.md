---
title: Release note
subtitle: Demographic Data Server
toolversion: 4.5.0
releasedate: 2025-01-27
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-DDS
---

# 4.5.0
_Release date: 2025-01-27_

Context : GUM Step 4 renovation
__Task__
* \[[DDS-208](https://gazelle.ihe.net/jira/browse/DDS-208)\] Upgrade sso-client-v7 to version 5.0.0

# 4.4.0
_Release date: 2024-02-05_

__Task__
* \[[DDS-207](https://gazelle.ihe.net/jira/browse/DDS-207)\] Integrate new sso-client-v7

# 4.3.4
_Release date: 2023-04-14_

__Bug__
* \[[DDS-206](https://gazelle.ihe.net/jira/browse/DDS-206)\] Fix TOS popup

# 4.3.3
_Release date: 2023-02-13_

__Bug__
* \[[DDS-206](https://gazelle.ihe.net/jira/browse/DDS-206)\] Fix TOS popup

# 4.3.2
_Release date: 2023-02-01_

__Bug__
* \[[DDS-206](https://gazelle.ihe.net/jira/browse/DDS-206)\] Fix TOS popup

# 4.3.1
_Release date: 2022-01-19_

__Story__
* \[[DDS-205](https://gazelle.ihe.net/jira/browse/DDS-205 )\] GDPR Refacto banner in standalone library

# 4.3.0
_Release date: 2021-07-28_

__Story__
* \[[DDS-204](https://gazelle.ihe.net/jira/browse/DDS-204)\] [DDS] Patient creation: support Identité qualifiée

__Task__
* \[[DDS-202](https://gazelle.ihe.net/jira/browse/DDS-202)\] [DDS] Use the appropriate OID for INS-C

# 4.2.2
_Release date: 2019-04-04_

__Bug__

* \[[DDS-191](https://gazelle.ihe.net/jira/browse/DDS-191)\] cx_4_code field from CountryCode is returned which breaks the communication with DDS clients

# 4.2.1
_Release date: 2019-04-04_

__Remark__

This version of DDS is incompatible with the applications which call its webservice. You should rather move directly to version 4.2.2.

__Story__
* \[[DDS-190](https://gazelle.ihe.net/jira/browse/DDS-190)\] Add new attribute to enable to make difference between country_code and CX-4.1

# 4.2.0
_Release date: 2019-03-15_

__Remarks__
Management of the datasource has changed. Refer to the installation manual to configure your Jboss when updating to this version.

__Bug__
* \[[DDS-180](https://gazelle.ihe.net/jira/browse/DDS-180)\] Link to the issue tracker cannot be configured from the application preferences

__Improvement__
* \[[DDS-184](https://gazelle.ihe.net/jira/browse/DDS-184)\] Extract datasource configuration
* \[[DDS-185](https://gazelle.ihe.net/jira/browse/DDS-185)\] Update SQL scripts archive

# 4.1.1
_Release date: 2018-10-12_

__Bug__
* \[[DDS-158](https://gazelle.ihe.net/jira/browse/DDS-158)\] URL to geo services shall not be hard coded
* \[[DDS-181](https://gazelle.ihe.net/jira/browse/DDS-181)\] Google geocoding API changed its policy
* \[[DDS-182](https://gazelle.ihe.net/jira/browse/DDS-182)\] Allow the administrator of the tool to choose which Geocoding system to use in priority

# 4.1.0
_Release date: 2018-03-14_

__Remarks__

This version of the DDS tool uses the jdbc driver provided by the Jboss7 AS server. In addition, it has to be bound to the new Gazelle SSO (apereo).

__Story__
* \[[DDS-177](https://gazelle.ihe.net/jira/browse/DDS-177)\] Use new CAS SSO

__Improvement__
* \[[DDS-178](https://gazelle.ihe.net/jira/browse/DDS-178)\] Update postgresql driver to version 42.2.1-jre7

# 4.0.2
_Release date: 2016-11-30_

__Bug__
* \[[DDS-154](https://gazelle.ihe.net/jira/browse/DDS-154)\] Missing translations
* \[[DDS-155](https://gazelle.ihe.net/jira/browse/DDS-155)\] Update c3p0 configuration
* \[[DDS-156](https://gazelle.ihe.net/jira/browse/DDS-156)\] Length of generated INS-C is not correct
