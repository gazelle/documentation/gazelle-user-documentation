---
title: User Manual
subtitle: XDS XCA Simulator
author: Malo TOUDIC
function: Developer
releasedate: 2019-09-17
toolversion: 1.0.0
version: 1.0.0
status: Approved
reference: KER1-MAN-IHE-XDS_XCA_SIMULATOR_USER
customer: eHealth Suisse
---

# XDS XCA Simulator

XDS XCA Simulator simulates XDS and XCA and support transactions with swiss specifics constraints only : it does not cover transactions not specified by the swiss project.

The following actor and transaction are supported :
* Document Registry : ITI-18 and ITI-42
* Responding Gateway : ITI-38

This simulator act as a proxy : it validate the swiss constraints then forward the message to a [XDS toolkit](https://ehealthsuisse.ihe-europe.net/xdstools7/) simulator. The simulators used are the following (in the epr-testing Test Session):
* Document Registry : epr-testing__one_registry_to_rule_them_all
* Responding Gateway : epr-testing__for_init_gw_testing

The patient known by the simulators are the one defined in XDS Toolkit.

## Mock messages on GWT

Messages sent to the simulator can be found in Mock Messages feature of Gazelle Webservice Tester.
This feature is documented at : [https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html).

Messages exchanged with EPR-XDSXCA-Simulator can be found by filtering with the actors CH:DOCUMENT_REGISTRY (XDS) and RESPONDING_GATEWAY (XCA).
