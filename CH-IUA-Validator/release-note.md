---
title: Release note
subtitle: CH:IUA Validator
toolversion: 1.0.3
releasedate: 2022-09-06
author: Nicolas BAILLIET
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-CH-IUA-VALIDATOR
---
# CH:IUA Validation Service 1.0.3
_Release date: 2022-09-06_

__Story__
* \[[IUAINFRA-66](https://gazelle.ihe.net/jira/browse/IUAINFRA-66)\] ITI71 Validator

# CH:IUA Validation Service 1.0.2
_Release date: 2020-09-15_

__Remarks__
Bugfix in Access Token Provider

## CH:IUA Access Token Provider 1.0.2
_Release date: 2020-09-15_

__Bug__
* \[[IUAINFRA-63](https://gazelle.ihe.net/jira/browse/IUAINFRA-63)\] [Dummy Authz Server] AudienceSecretRetriever can be null while instantiating Authrization Service

# CH:IUA Validation Service 1.0.1
_Release date: 2020-09-07_

__Remarks__
Bugfix in Access Token Provider

## CH:IUA Access Token Provider 1.0.1
_Release date: 2020-09-07_

__Bug__
* \[[IUAINFRA-60](https://gazelle.ihe.net/jira/browse/IUAINFRA-60)\] JNDI Operational Properties property in Access Token Provider is wrong

# CH:IUA Validation Service 1.0.0
_Release date: 2020-09-04_

__Story__
* \[[IUAINFRA-1](https://gazelle.ihe.net/jira/browse/IUAINFRA-1)\] Validate Base JWT Access Token
* \[[IUAINFRA-10](https://gazelle.ihe.net/jira/browse/IUAINFRA-10)\] CH:IUA Validation Web Service and deployable