---
title: User Manual
subtitle: PDQm Fhir Simulator
author: Romuald DUBOURG
releasedate: 2024-06-25
toolversion: 1.0.0
function: Engineer
version: 1.0.0
status: validated
---

# PDQm Fhir Simulator

## Description

The PDQm FHIR Simulator acts like a server to return queried resources fetch from Patient Registry.
It had a validation process to check if the Header + Body + URL are compliant regarding the IHE specification.

Only Patients are fetched from the Patient Registry.


## Find Matching Patients (ITI-78) - IHE

- [IHE Specifications](https://profiles.ihe.net/ITI/PDQm/ITI-78.html)

## Overview

Here is a quick overview of the available functionality from PDQm Fhir Simulator

| Operation                   | HTTP Methods | URL to call                                                                                               | Entry parameter                                                    | Returned value                                                                           | Remarks|
|-----------------------------|--------------|-----------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|------------------------------------------------------------------------------------------|---|
| Retrieve Patient Resource   | GET | ```https://example.com/pdqm-fhir-simulator/IHE/fhir/Patient/{id}```                                       | ID for a specific ITI-78 PDQm Fhir resource                        | the desired ITI-78 Resource                                                              |/|
| Find Matching Patients      | GET       | ```https://example.com/pdqm-fhir-simulator/IHE/fhir/Patient?name=value&...{&_format=[mime-type]}}```        | Parameters to search for specific ITI-78 PDQm Fhir resource ITI-78 | A bundle containing 0 to many ITI-78 PDQm Fhir Specific corresponding to search criteria | /|


Capability statement of the application can be found with : <https://example.com/pdqm-fhir-simulator/IHE/fhir/metadata>

As described in [HAPI FHIR resources](https://hapifhir.io/hapi-fhir/docs/server_plain/rest_operations_operations.html), some strings are automatically escaped when the FHIR server parses URLs:

|Given String|Parsed as|
| :---: | :---: |
|\||%7C|


## Find Matching Patients (ITI-78) - CH

- [CH Specifications](https://fhir.ch/ig/ch-epr-mhealth/iti-78.html)

## Overview

Here is a quick overview of the available functionality from PDQm Fhir Simulator

| Operation                   | HTTP Methods | URL to call                                                                                         | Entry parameter                                                    | Returned value                                                                           | Remarks|
|-----------------------------|--------------|-----------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|------------------------------------------------------------------------------------------|---|
| Retrieve Patient Resource   | GET | ```https://example.com/pdqm-fhir-simulator/CH/fhir/Patient/{id}```                                  | ID for a specific ITI-78 PDQm Fhir resource                        | the desired ITI-78 Resource                                                              |/|
| Find Matching Patients      | GET       | ```https://example.com/pdqm-fhir-simulator/CH/fhir/Patient?name=value&...{&_format=[mime-type]}}``` | Parameters to search for specific ITI-78 PDQm Fhir resource ITI-78 | A bundle containing 0 to many ITI-78 PDQm Fhir Specific corresponding to search criteria | /|


Capability statement of the application can be found with : <https://example.com/pdqm-fhir-simulator/CH/fhir/metadata>

As described in [HAPI FHIR resources](https://hapifhir.io/hapi-fhir/docs/server_plain/rest_operations_operations.html), some strings are automatically escaped when the FHIR server parses URLs:

|Given String|Parsed as|
| :---: | :---: |
|\||%7C|


## Validation process

Each operation implies a validation of requests for `ITI-78` 
Validation is done by calling:

- [HTTP Validator](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/http-validator) for URL and Headers.

The validation is done by default but can be switched with options:  

- `evs.endpoint=https://${FQDN}/evs/rest/validations`
- `MCSD_SERVER_VALIDATION_ENABLED=true`
- `ihe.http.validation.name=HTTP Message Validator`
- `ihe.http.validation.schematron.name-iti-78-search=CH-ITI-78-Patient-PDQmQuery-ValidationProfile`
- `ihe.http.validation.schematron.name-iti-78-read=CH-ITI-78-Patient-PDQmRetrieve-ValidationProfile`

- `ch.http.validation.name=HTTP Message Validator`
- `ch.http.validation.schematron.name-iti-78-search=CH-ITI-78-Patient-PDQmQuery-ValidationProfile`
- `ch.http.validation.schematron.name-iti-78-read=CH-ITI-78-Patient-PDQmRetrieve-ValidationProfile`

Patients are fetched from the Patient Registry(https://gitlab.inria.fr/gazelle/public/processing/patient-registry).

You can choose which Patient Registry target by setting the `patient.registry.service` parameter



