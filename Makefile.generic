SHELL := /bin/bash

.SUFFIXES:
.SUFFIXES: .md

PROJECT = $(shell basename $(CURDIR))
PROJECT_NAME_LOWERCASE = $(shell echo $(PROJECT) | tr A-Z a-z)

CHANGES = changes-
OUTPUT_NAME = KER1-MAN-IHE-$(PROJECT)-
OUTPUT_FOLDER = target

PLATEFORM_ROOT_URL = https://gazelle.ihe.net
TEMPLATE_HTML_FOLDER = ../_templates/html-template
TEMPLATE_HTML = --template=$(TEMPLATE_HTML_FOLDER)/template.html
TEMPLATE_PDF = --template=../_templates/pandoc-templates/ihedev-manual-template.tex

RELEASE_NOTES_MD = release-note.md
RELEASE_NOTES_NAME = KER1-RNO-IHE-$(PROJECT)-
RELEASE_NOTES_PDF += $(addprefix $(RELEASE_NOTES_NAME), $(RELEASE_NOTES_MD:.md=.pdf))
TEMPLATE_RELEASE_NOTE = --template=../_templates/pandoc-templates/releasenote-template.tex

TEST_REPORTS_MD = $(notdir $(wildcard test-reports/*.md))
TEST_REPORTS_NAME = KER2-RDT-IHE-$(PROJECT)-
TEST_REPORTS_PDF += $(addprefix $(TEST_REPORTS_NAME), $(TEST_REPORTS_MD:.md=.pdf))
TEMPLATE_TEST_REPORT_PDF = --template=../_templates/pandoc-templates/ihedev-report-template.tex

DELIVERY_NOTE_MD = $(wildcard *.md)
PVL_OUTPUT_NAME = KER2-PVL-IHE-
DELIVERY_NOTE_PDF += $(addprefix $(PVL_OUTPUT_NAME), $(DELIVERY_NOTE_MD:.md=.pdf))
DELIVERY_NOTE_NAME = KER2-PVL-
TEMPLATE_DELIVERY_NOTE_PDF = --template=../_templates/pandoc-templates/delivery-note-template.tex

PRODUCTION_REPORTS_MD = $(notdir $(wildcard production-reports/*.md))
PRODUCTION_REPORTS_NAME = KER2-PRT-IHE-$(PROJECT)-
PRODUCTION_REPORTS_PDF += $(addprefix $(PRODUCTION_REPORTS_NAME), $(PRODUCTION_REPORTS_MD:.md=.pdf))
TEMPLATE_PRODUCTION_REPORT_PDF = --template=../_templates/pandoc-templates/ihedev-report-template.tex

USE_AUTHORIZATION_MD = $(notdir $(wildcard use-authorizations/*.md))
USE_AUTHORIZATION_PDF += $(addprefix $(USE_AUTHORIZATION_NAME), $(USE_AUTHORIZATION_MD:.md=.pdf))
USE_AUTHORIZATION_NAME = KER2-UAU-HEALTHLAB-
TEMPLATE_USE_AUTHORIZATION = --template=../_templates/pandoc-templates/use-authorization-template.tex

MARKDOWN = $(wildcard *.md)
DOCS += $(MARKDOWN:.md=.html)

# Create final pdf document name
# OUTPUT_NAME<filename>.pdf
# KER1-MAN-IHE-$(PROJECT)-<filename>.pdf
# This name will match the rule: $(OUTPUT_NAME)%.pdf
# % in the rule is a kind of wilcard and will match the filename
DOCS += $(addprefix $(OUTPUT_NAME), $(MARKDOWN:.md=.pdf))
DOCS += $(addprefix $(OUTPUT_NAME), $(MARKDOWN:.md=_revised.pdf))

all: $(DOCS) $(RELEASE_NOTES_PDF)

pdf: $(DOCS)

release-note: $(RELEASE_NOTES_PDF)

test-reports: $(TEST_REPORTS_PDF)

production-reports: $(PRODUCTION_REPORTS_PDF)

delivery-notes: $(DELIVERY_NOTE_PDF)

use-authorizations: $(USE_AUTHORIZATION_PDF)

$(RELEASE_NOTES_NAME)%.pdf: release-note.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	pandoc $< $(TEMPLATE_RELEASE_NOTE) --latex-engine=xelatex -o $(OUTPUT_FOLDER)/$@

$(TEST_REPORTS_NAME)%.pdf: test-reports/%.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	pandoc $< $(TEMPLATE_TEST_REPORT_PDF) -N -s --toc --latex-engine=xelatex -o $(OUTPUT_FOLDER)/$@

$(PVL_OUTPUT_NAME)%.pdf: %.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	@pandoc -N $(OUTPUT_FOLDER)/$< $(TEMPLATE_DELIVERY_NOTE_PDF) --latex-engine=xelatex  $< -o $(OUTPUT_FOLDER)/$@

$(PRODUCTION_REPORTS_NAME)%.pdf: production-reports/%.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	pandoc $< $(TEMPLATE_PRODUCTION_REPORT_PDF) -N -s --toc --latex-engine=xelatex -o $(OUTPUT_FOLDER)/$@

$(OUTPUT_NAME)%.pdf: %.md $(OUTPUT_FOLDER)/$(CHANGES)%.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	-@pandoc --listings -N --toc $(OUTPUT_FOLDER)/$(CHANGES)$< $(TEMPLATE_PDF) --latex-engine=xelatex  $< -o $(OUTPUT_FOLDER)/$@

$(USE_AUTHORIZATION_NAME)%.pdf: use-authorizations/%.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	pandoc $< $(TEMPLATE_USE_AUTHORIZATION) --latex-engine=xelatex -o $(OUTPUT_FOLDER)/$@


# Revised pdf matching rule, force output folder creation and triggers changes rule
$(OUTPUT_NAME)%_revised.pdf: %.md $(OUTPUT_FOLDER)/$(CHANGES)%.md| $(OUTPUT_FOLDER)
	@echo "Create" $@
	-@git show `git rev-list --tags --max-count=1` $< > last_version.md
	-@pandoc -s --toc $(OUTPUT_FOLDER)/$(CHANGES)$< $< -o revised.tex --listings $(TEMPLATE_PDF)
	-@pandoc -s --toc $(OUTPUT_FOLDER)/$(CHANGES)$< last_version.md -o last_version.tex --listings $(TEMPLATE_PDF)
# Needed to fix an issue in generated .tex that blocked revised pdf genration
	-@sed -i 's|\\end{Highlighting}|\n\\end{Highlighting}|g' *.tex
	latexdiff last_version.tex revised.tex > diff.tex 2> /dev/null

	xelatex diff.tex > /dev/null # Will output diff.pdf
	-@mv diff.pdf  $(OUTPUT_FOLDER)/$@
	-@rm last_version.md last_version.tex revised.tex diff.*

# html matching rule, force output folder creation
%.html: %.md  |$(OUTPUT_FOLDER)
	@echo "Create" $@
	-@pandoc -s --toc -t html5 $(TEMPLATE_HTML) $< -o $(OUTPUT_FOLDER)/site/$(PROJECT_NAME_LOWERCASE)/$@
	-@cp $(TEMPLATE_HTML_FOLDER)/{affix.js,gzl-table.css,gazelle-style.min.css} $(OUTPUT_FOLDER)/site/
	-@cp $(TEMPLATE_HTML_FOLDER)/highlight -r $(OUTPUT_FOLDER)/site/
	-@cp media -r $(OUTPUT_FOLDER)/site/$(PROJECT_NAME_LOWERCASE)

# Changes matching rule
# It will generate a file containing changes since last tag (git rev-list --tags --max-count=1)
$(OUTPUT_FOLDER)/$(CHANGES)%.md: %.md | $(OUTPUT_FOLDER)
	@echo "Create" $@
	@echo "## Changes" > $@
	@echo "" >> $@
	-@git log --pretty=format:"* **%an**: %ad %s%n%b" --date=short --since="$$(git show -s --format=%ad `git rev-list --tags --max-count=1`)" $< >> $@
	-@sed -i "/git-svn-id:/d" $@

$(OUTPUT_FOLDER):
	@echo "Create" $(OUTPUT_FOLDER)
	-@mkdir -p $(OUTPUT_FOLDER)/site/$(PROJECT_NAME_LOWERCASE)

.PHONY: clean
clean:
	-@rm -rf $(OUTPUT_FOLDER) diff.* last_version.* revised.*

rebuild: clean all
