---
title:  User Manual
subtitle: Gazelle-Hl7-simulator
author: CLaude LUSSEAU, Romuald DUBOURG
function: Software Engineer
date: 2023-14-08
toolversion: 1.3.x
version: 1.00
status: draft
reference:
customer: IHE-EUROPE
---

# Application overview

HL7V2Simulators is the part of the Gazelle platform dedicated to the simulation of Hl7v2 messages transmission.

One simulator mimics the transmission of a Clinical Document Architecture (CDA) to a receiving application. The creator of the simulator expects to receive as many acknowledgments as requested by the message.

Three simulator acts as a server with different charset handled. For every CDA messages received, they send back as many acknowledgments as requested in the message to their sender.

Concerning HL7v2 simulators: the application also gathers the HL7 templates.

# User interface

As shown on picture below, the [User Interface] provides two simulators tools: **Creator** and **Managers**:

![](./media/HL7V2Home_UI.png)

**Creator** allows user to send HL7 messages to an receiving App:

![](./media/HL7V2Creator_UI.png)

And **Managers** allows user to receive Hl7 messages from a creator:

![](./media/HL7V2Manager_UI.png)

Both tools use templates included in the application or imported by user.


## Browse Configuration SUT 

There are 10 fields that define a Configuration SUT:
   - Name : Define the name of your configuration
   - Host/ip: define the  destination device of the message 
   - Port: Define on which port send the message on the destination device 
   - Application: Define the name of the application which you send the message it will help fulfill the message
   - Facility: Define the name of the facility which you send the message it will help fulfill the message
   - Owner: Show the creator of the configuration 
   - CompanyOwner: Show the company owner of the configuration, only user can see his company's not share configuration.
   - isShare: Define if the configuration is share to others users. Only admin or people of the company can edit it.
   - Charset: Define on a server it tells with which charset message should be sent and replied.
   - isServerSide: Define if the configuration is for a server or a client 


Once connected with CAS(Central Authentication Service) Select option ![](./media/Configuration_Button.png) of **HL7v2 simulators tool** menu.

It will display the list of Configuration SUT :

![](./media/ConfigurationSUT_List.png)

Only admin cans see all configuration but a lambda user can see his company configuration also either create, edit or delete one.

## Browse Template

There are 4 fields that define a template :
   -Title : Define the title of the template 
   -isEnabled : Define if the template can be used in a message 
   -Filename : Define the name of the stocked file corresponding at the mustache template
   -Description: Explain what is the purpose of the template ,or it's particularity 


Once connected with CAS(Central Authentication Service) Select option ![](./media/Template_Button.png) of **HL7v2 simulators tool** menu.

It will display the list of Template :

![](./media/Template_List.png)

All Template available are gathered in the tool. You can either create, edit or delete one.


## Browse Preference

There are 4 fields that define a preference:
   - PreferenceName : define the name a of a preference
   - ClassName: define the type of value contain by the preference
   - PreferenceValue: define the value of a preference
   - Description: describe  how is use the preference

Once connected with CAS(Central Authentication Service) Select option ![](./media/Preference_Button.png) of **HL7v2 simulators tool** menu.

It will display the list of Preference :

![](./media/Preference_List.png)

All Preference available are gathered in the tool. You can either create, edit or delete one.

Preferences enable to custom the working of the application there are 4 things that can be custom.
   Endpoint_Validator: The validation Endpoint to define which HL7 validator should be used.
   UTF8_Manager_Name, UTF8_Manager_Host, UTF8_Manager_Application, UTF8_Manager_Facility, UTF8_Manager_Port	:The configuration of the UTF-8 manager
   I885915_Manager_Name, I885915_Manager_Host, I885915_Manager_Application, I885915_Manager_Facility, I885915_Manager_Port	:The configuration of  the 8859-15 manager
   Creator_Name, Creator_Host ,Creator_Application , Creator_Port:The configuration of the creator


# Creator

The Creator Simulator simulate the transmission of a Clinical Document Architecture (CDA) to a receiving application.

There are two things that must be configured before sending a message.
First a message should have a destination, so the creator need a Configuration SUT to be selected this will represent the destination where you want the message to be sent.
It is possible to use default  configuration ,but it  can be custom on the preferences' menu.

![](./media/HL7v2Creator_SelectDropdown.png)

Second the message to be sand must be indicated for that a template must be selected.
In the same idea as configurations. It is possible to use default template or add customs template with the template UI.
Once the needed template is set it can be selected with dropdown on the creator UI.

![](./media/HL7V2Creator_TemplateDropdown.png)

For sending the receiving application Configuration must have been insert in the configuration list.  
On the Creator GUI before sending, a Configuration Receiver and a Template must have been selected.  
Once the message is sent the message history print messages who have been sent and receive including ACK/NACK AND ZAM.



## Managers

There is a manager for each charsets: UTF-8 and 8859-15.
Managers are easier to use because it's just a server that can be started/stopped.
For replying ZAM managers must have all required ZAM templates. See TEMPLATE section for details.  
For replying ZAM managers should also have the configuration SUT defined of the sender. Else managers cannot know where to send ZAM ( because ACK with HAPI is automatic).  
For starting servers managers go to the Manager GUI and press the start button.  
Once servers are started they will try to parse every incoming messages and reply ACK/NACK AND corresponding ZAM.

## Default Import

-DEFAULT_ANS_TEMPLATES
This variable can be set to true to have a group of messages example with a description that explain what they are use for. 

-DEFAULT_ANS_SERVERS
This variable can be set to true to initialize default server of the application instead of created them manually 
