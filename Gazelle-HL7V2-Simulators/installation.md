---
title:  Installation Manual
subtitle: Gazelle-Hl7-simulator
author: CLaude LUSSEAU, Romuald DUBOURG
function: Software Engineer
date: 2023-14-08
toolversion: 1.3.x
version: 1.00
status: draft
reference:
customer: IHE-EUROPE
---
# Purpose

Here is a guide to help you install the Hl7-Simulator.

# Deployment

## Requirements
* Java virtual machine : JDK 17


## Installation
First you should pull the project from:
https://gitlab.inria.fr/gazelle/specific-tools/ans/ans-hl7v2-simulators.git

Before compiling the source code, you have to create 3 repositories :

```sh
mkdir -p /opt/ans-hl7v2-simulators/templates/CDA
mkdir -p /opt/ans-hl7v2-simulators/templates/ZAM
mkdir -p /opt/ans-hl7v2-simulators/JDV
```

These repositories will contain the templates and be linked with your container, so if they do not exist your application will not work as expected.

Then you can use quarkus with :
``
mvn clean package
cd ans-hl7v2-simulators-ui
mvn docker:start
``
Or
you can use the application with 
``
mvn clean install
``

It will start a container who will contain the gazelle-database needed as a database and a container with the application
