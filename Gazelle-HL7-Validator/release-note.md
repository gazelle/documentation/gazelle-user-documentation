---
title: Release note
subtitle: Gazelle HL7 Validator
toolversion: 3.9.2
releasedate: 2024-08-07
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_HL7_VALIDATOR
---

# 3.9.2

_Release date: 2024-08-07_

__Bug__
* \[[HLVAL-478](https://gazelle.ihe.net/jira/browse/HLVAL-478)\]  [eHDSI HL7v3] - Identification Service Request validator doesn't allow for 'UN' code for livingSubjectAdministrativeGender

# 3.9.1

_Release date: 2024-02-15_

__IT Help__
* \[[HLVAL-477](https://gazelle.ihe.net/jira/browse/HLVAL-477)\] Problem of access to profilesList.xhtml page

# 3.9.0

_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[HLVAL-476](https://gazelle.ihe.net/jira/browse/HLVAL-476)\] Integrate new sso-client-v7

# 3.8.5
_Release date: 2023-08-03_

__Bug__
* \[[HLVAL-474](https://gazelle.ihe.net/jira/browse/HLVAL-474)\] Diplaying acute character in HL7v2 validation report

__Improvement__
* \[[HLVAL-475](https://gazelle.ihe.net/jira/browse/HLVAL-475)\] New mapping for GVT 1.6.3

# 3.8.4
_Release date: 2023-06-30_

__Task__
* \[[HLVAL-473](https://gazelle.ihe.net/jira/browse/HLVAL-473)\] Update NIST dependencies to latest Gazelle versions

# 3.8.3
_Release date: 2023-04-18_

__Bug__
* \[[HLVAL-467](https://gazelle.ihe.net/jira/browse/HLVAL-467)\] Fix TOS popup
* \[[HLVAL-468](https://gazelle.ihe.net/jira/browse/HLVAL-468)\] Exceptions of type RE shall be reported as Warnings
* \[[HLVAL-472](https://gazelle.ihe.net/jira/browse/HLVAL-472)\] Fix validation rules in the ANS validator
* \[[HLVAL-469](https://gazelle.ihe.net/jira/browse/HLVAL-469)\] HL7 Validation reports needs to be ABORTED when ProfileExceptions are present

# 3.8.2
_Release date: 2022-11-21_

__Bug__
* \[[HLVAL-466](https://gazelle.ihe.net/jira/browse/HLVAL-466)\] [EHDSI] Wrong constraint in XCPD validator

# 3.8.1
_Release date: 2022-10-28_

__Bug__
* \[[HLVAL-465](https://gazelle.ihe.net/jira/browse/HLVAL-465)\] [EHDSI] Wrong constraint in XCPD validator

# 3.8.0
_Release date: 2022-10-06_

__Task__
* \[[HLVAL-464](https://gazelle.ihe.net/jira/browse/HLVAL-464)\] EHDSI HL7V3 Validator for wave 6

# 3.7.8
_Release date: 2022-09-02_

__Task__
* \[[HLVAL-463](https://gazelle.ihe.net/jira/browse/HLVAL-463)\] Delete constraint

# 3.7.7
_Release date: 2022-07-19_

__Bug__
* \[[HLVAL-462](https://gazelle.ihe.net/jira/browse/HLVAL-462)\] PIXV3 Query request validator is abusively including Swiss constraints

# 3.7.6
_Release date: 2022-04-19_

__Bug__
* \[[HLVAL-461](https://gazelle.ihe.net/jira/browse/HLVAL-461)\] [INS] Add warning constraint on PID-32 (fix)

# 3.7.5
_Release date: 2022-04-07_

__Bug__
* \[[HLVAL-461](https://gazelle.ihe.net/jira/browse/HLVAL-461)\] [INS] Add warning constraint on PID-32

# 3.7.4
_Release date: 2022-01-19_

__Story__
* \[[HLVAL-460](https://gazelle.ihe.net/jira/browse/HLVAL-460 )\] GDPR Refacto banner in standalone library

# 3.7.3
_Release date: 2021-09-19_

__Story__
* \[[HLVAL-459](https://gazelle.ihe.net/jira/browse/HLVAL-459)\] Swiss Adaptations 3.1 Validation Constraints


__Remarks__

This release includes the releases of the following sub-modules :

* chhl7v3-validator-jar 1.0.6

# 3.7.2
_Release date: 2021-09-13_

__Bug__
* \[[HLVAL-458](https://gazelle.ihe.net/jira/browse/HLVAL-458)\] [ANS] Qualified identity should be verified with the presence of an INS instead of PID-32 = VALI

# 3.7.1
_Release date: 2021-07-29_

__Bug__
* \[[HLVAL-457](https://gazelle.ihe.net/jira/browse/HLVAL-457)\] [Gazelle HL7 Validator] INS requirements are not implemented in the correct validators

# 3.7.0
_Release date: 2021-07-09_

__Task__
* \[[HLVAL-455](https://gazelle.ihe.net/jira/browse/HLVAL-455)\] - \[Gazelle HL7 Validator\] Implementation of INS requirements
* \[[HLVAL-454](https://gazelle.ihe.net/jira/browse/HLVAL-454)\] - \[Interop/INS\] Gazelle HL7 Validator: Update message profiles


# 3.6.0
_Release date: 2020-09-15_

__Remarks__


__Bug__
* \[[HLVAL-443](https://gazelle.ihe.net/jira/browse/HLVAL-443)\] EVSClient not showing validation report for HL7v2 validation (HealthLink)

__Story__
* \[[HLVAL-426](https://gazelle.ihe.net/jira/browse/HLVAL-426)\] Use IGAMT and GVT to perform the HL7v2 validation
* \[[HLVAL-427](https://gazelle.ihe.net/jira/browse/HLVAL-427)\] Distinguish message profiles (format)
* \[[HLVAL-428](https://gazelle.ihe.net/jira/browse/HLVAL-428)\] Allow user to view the content of the IGAMT conformance profile

__Task__
* \[[HLVAL-442](https://gazelle.ihe.net/jira/browse/HLVAL-442)\] Inconsistency in GVT validation report

__Improvement__
* \[[HLVAL-425](https://gazelle.ihe.net/jira/browse/HLVAL-425)\] Read HL7 message profile / resources from the file system
* \[[HLVAL-429](https://gazelle.ihe.net/jira/browse/HLVAL-429)\] Do not store validation result anymore
* \[[HLVAL-431](https://gazelle.ihe.net/jira/browse/HLVAL-431)\] The tool shall automatically detect the encoding of the incoming message

# 3.5.1
_Release date: 2019-10-10_

__Remarks__

All the following bugs affect the Swiss validators.

__Bug__

* \[[HLVAL-433](https://gazelle.ihe.net/jira/browse/HLVAL-433)\] [CH:PIXV3] The validator for the PIX query response does not check if the EPR-SPID is present in the asOtherId attribute
* \[[HLVAL-434](https://gazelle.ihe.net/jira/browse/HLVAL-434)\] PIXv3 update - asOtherIDs element not properly verified
* \[[HLVAL-435](https://gazelle.ihe.net/jira/browse/HLVAL-435)\] DataSource Parameter should be mandatory in the test PIXv3_CLIENT


# 3.5.0
_Release date: 2019-05-13_

__Remarks__

This version of the tool complies with version 2.9 of the French extension for the PAM profile as well as with the Rev 15 of the IHE PAM Profile.

For existing instances of the tool, the configuration of the Jboss 7.2 AS server shall be updated to declare the datasource in standalone.xml
configuration file, as for a new installation. Refers to the installation manual for details.

__Bug__

* \[[HLVAL-405](https://gazelle.ihe.net/jira/browse/HLVAL-405)\] Prevent the user from sending an incomplete request to GMM/TM
* \[[HLVAL-407](https://gazelle.ihe.net/jira/browse/HLVAL-407)\] Home page in edition model is uppercase
* \[[HLVAL-413](https://gazelle.ihe.net/jira/browse/HLVAL-413)\] LAW profile is now part of PaLM TF, double check the references in the conditional rules
* \[[HLVAL-420](https://gazelle.ihe.net/jira/browse/HLVAL-420)\] For CE datatype, the validator uses CE-3 even though a table is defined for CE-1
* \[[HLVAL-423](https://gazelle.ihe.net/jira/browse/HLVAL-423)\] For better readibility, give a severity to the faces messages

__Improvement__

* \[[HLVAL-408](https://gazelle.ihe.net/jira/browse/HLVAL-408)\] Extract datasource configuration
* \[[HLVAL-409](https://gazelle.ihe.net/jira/browse/HLVAL-409)\] Update SQL scripts archive
* \[[HLVAL-421](https://gazelle.ihe.net/jira/browse/HLVAL-421)\] Update dependency to gazelle-hl7-messagestructures

# 3.4.2
_Release date: 2018-12-06_

__Bug__
* \[[HLVAL-402](https://gazelle.ihe.net/jira/browse/HLVAL-402)\] \[CH:XCPD\] The optional parameters are forbidden in the validator


# 3.4.0
_Release date: 2018-03-09_

__Remarks__

This version of the Gazelle HL7 Validator tool uses the jdbc driver provided by the Jboss7 AS server.


__Bug__
* \[[HLVAL-396](https://gazelle.ihe.net/jira/browse/HLVAL-396)\] Update postgresql driver to version 42.2.1-jre7

__Improvement__
* \[[HLVAL-384](https://gazelle.ihe.net/jira/browse/HLVAL-384)\] Detect and validate the soap envelop when transmitted

# 3.3.3

__Bug__
* \[[HLVAL-381](https://gazelle.ihe.net/jira/browse/HLVAL-381)\] Wrong validation rule in HL7v3 General Acknowledgement

__Story__
* \[[HLVAL-390](https://gazelle.ihe.net/jira/browse/HLVAL-390)\] Integrate the new Gazelle SSO


# 3.3.2

__Bug__
* \[[HLVAL-376](https://gazelle.ihe.net/jira/browse/HLVAL-376)\] When imported tables contain errors, the button to import them anyway does not show up anymore
* \[[HLVAL-380](https://gazelle.ihe.net/jira/browse/HLVAL-380)\] [CH:PIX] PIX Query: Add a constraint on optionality of DataSource
* \[[HLVAL-382](https://gazelle.ihe.net/jira/browse/HLVAL-382)\] [GazelleHL7Validator] Error message is not correct
* \[[HLVAL-383](https://gazelle.ihe.net/jira/browse/HLVAL-383)\] [GazelleHL7Validator] issue in ch_pix_002_revise_Name
* \[[HLVAL-388](https://gazelle.ihe.net/jira/browse/HLVAL-388)\] [GazelleHL7Validator] downgrade severity of rule ch_pix_006_add_mpipid

__Improvement__
* \[[HLVAL-385](https://gazelle.ihe.net/jira/browse/HLVAL-385)\] [GazelleHL7Validator] Consider renaming the HL7v3 validator for EPD by adding the transaction keyword


# 3.3.1

__Bug__
* \[[HLVAL-335](https://gazelle.ihe.net/jira/browse/HLVAL-335)\] Acknowledgement element shall be set as required in PIXv3 query response
* \[[HLVAL-375](https://gazelle.ihe.net/jira/browse/HLVAL-375)\] [CH:PIX/PDQ/XCPD] issues encountered in CH validators
* \[[HLVAL-379](https://gazelle.ihe.net/jira/browse/HLVAL-379)\] PersonalRelationship/code@displayName is not a required attribute

# 3.3.0
_Release date: 2017-07-04_

__Remarks__

This release of the tool introduces the validator for the eHealthSuisse profiles: CH:PIX, CH:XCPD, and CH:PDQ.

__Technical task__
* \[[HLVAL-339](https://gazelle.ihe.net/jira/browse/HLVAL-339)\] \[CH\] Testing with PatientManager and EVSClient
* \[[HLVAL-341](https://gazelle.ihe.net/jira/browse/HLVAL-341)\] \[CH-PIX\] Implement the validator for PIXV3Feed
* \[[HLVAL-342](https://gazelle.ihe.net/jira/browse/HLVAL-342)\] \[CH-PIX\] Samples for PIXV3Feed messages
* \[[HLVAL-343](https://gazelle.ihe.net/jira/browse/HLVAL-343)\] \[CH-PIX\] Unit testing of PIXV3Feed validator
* \[[HLVAL-349](https://gazelle.ihe.net/jira/browse/HLVAL-349)\] \[CH-XCPD\] Implement the validator for XCPD query message
* \[[HLVAL-350](https://gazelle.ihe.net/jira/browse/HLVAL-350)\] \[CH-XCPD\] Samples for XCPD query messages
* \[[HLVAL-351](https://gazelle.ihe.net/jira/browse/HLVAL-351)\] \[CH-XCPD\] Unit testing of XCPD query validator
* \[[HLVAL-353](https://gazelle.ihe.net/jira/browse/HLVAL-353)\] \[CH-XCPD\] Implement the validator for XCPD query response message
* \[[HLVAL-354](https://gazelle.ihe.net/jira/browse/HLVAL-354)\] \[CH-XCPD\] Samples for XCPD query response messages
* \[[HLVAL-355](https://gazelle.ihe.net/jira/browse/HLVAL-355)\] \[CH-XCPD\] Unit testing of XCPD query response validator
* \[[HLVAL-358](https://gazelle.ihe.net/jira/browse/HLVAL-358)\] \[CH-PDQ\] Implement the validator for PDQ query message
* \[[HLVAL-359](https://gazelle.ihe.net/jira/browse/HLVAL-359)\] \[CH-PDQ\] Samples for PDQ query messages
* \[[HLVAL-360](https://gazelle.ihe.net/jira/browse/HLVAL-360)\] \[CH-PDQ\] Unit testing of PDQ query validator
* \[[HLVAL-362](https://gazelle.ihe.net/jira/browse/HLVAL-362)\] \[CH-PDQ\] Implement the validator for PDQ query response message
* \[[HLVAL-363](https://gazelle.ihe.net/jira/browse/HLVAL-363)\] \[CH-PDQ\] Samples for PDQ query response messages
* \[[HLVAL-364](https://gazelle.ihe.net/jira/browse/HLVAL-364)\] \[CH-PDQ\] Unit testing of PDQ query response validator
* \[[HLVAL-369](https://gazelle.ihe.net/jira/browse/HLVAL-369)\] \[CH-PIX\] Unit testing of PIXV3QueryResponse validator
* \[[HLVAL-370](https://gazelle.ihe.net/jira/browse/HLVAL-370)\] \[CH-PIX\] Samples for PIXV3QueryResponse messages
* \[[HLVAL-371](https://gazelle.ihe.net/jira/browse/HLVAL-371)\] \[CH-PIX\] Implement the validator for PIXV3QueryResponse

__Bug__
* \[[HLVAL-332](https://gazelle.ihe.net/jira/browse/HLVAL-332)\] Wrong constraints on PatientTelecom/value and PatientAddress/value elements

__Story__
* \[[HLVAL-336](https://gazelle.ihe.net/jira/browse/HLVAL-336)\] \[CH-PIX\] Create a new validator for PIXV3 Feed message
* \[[HLVAL-338](https://gazelle.ihe.net/jira/browse/HLVAL-338)\] \[CH-PIX\]\[CH-PDQ\]\[CH-XCPD\] Add new entries in HL7v3 validator list
* \[[HLVAL-361](https://gazelle.ihe.net/jira/browse/HLVAL-361)\] \[CH-PDQ\] Create a new validator for PDQ query response message
* \[[HLVAL-367](https://gazelle.ihe.net/jira/browse/HLVAL-367)\] Create projects for HL7v3-CH validators
* \[[HLVAL-368](https://gazelle.ihe.net/jira/browse/HLVAL-368)\] \[CH-PIX\] Create a new validator for PIXV3 Query Response messages

__Improvement__
* \[[HLVAL-326](https://gazelle.ihe.net/jira/browse/HLVAL-326)\] We lost the transaction numbers in the schematron & model-based dropdown lists

#  3.2.6

__Bug__
* \[[HLVAL-327](https://gazelle.ihe.net/jira/browse/HLVAL-327)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm


# 3.2.5
_2016-11-30_

__Bug__
* \[[HLVAL-283](https://gazelle.ihe.net/jira/browse/HLVAL-283)\] Allow Commit ACK for PIXV3 - Patient Identity Feed HL7V3 - Acknowledgement
* \[[HLVAL-305](https://gazelle.ihe.net/jira/browse/HLVAL-305)\] Problem using Profile OID 2.16.840.1.113883.2.6.9.38
* \[[HLVAL-310](https://gazelle.ihe.net/jira/browse/HLVAL-310)\] Error on constraint constraint_mccimt000200UV01_acknowledgementTypeCodeValue
* \[[HLVAL-319](https://gazelle.ihe.net/jira/browse/HLVAL-319)\] HL7 Message profiles are no more listed
* \[[HLVAL-311](https://gazelle.ihe.net/jira/browse/HLVAL-311)\] Hapi does not correctly parse ADT_A01 messages when the ROL segment is included at several places

__Improvement__
* \[[HLVAL-320](https://gazelle.ihe.net/jira/browse/HLVAL-320)\] CAS login redirects to the same page instead of the home page

__Story__
* \[[HLVAL-316](https://gazelle.ihe.net/jira/browse/HLVAL-316)\] Update HL7 message profiles for PAM-DE with the latest version
