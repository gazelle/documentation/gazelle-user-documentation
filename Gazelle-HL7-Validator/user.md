---
title:  User Manual
subtitle: Gazelle HL7 Validator
author: Anne-Gaëlle Bergé
function: Engineer
date: 07/08/2024
toolversion: 3.X.X
version: 1.04
status: Approved document
reference: KER1-MAN-IHE-GAZELLE_HL7_VALIDATOR_USER-1_04
customer: IHE-EUROPE
---

# Application overview

GazelleHL7Validator is the part of the Gazelle platform dedicated to the validation of HL7 messages.

HL7v2.x messages are validating against HL7 conformance profiles. The validation engine is based on HAPI libraries as well as the JAR validation module from GVT and the conformance profiles are written by the Gazelle team (helped by NIST).

HL7v3 messages are validating using a model-based engine.

These validation services are available through a web service API so it can be integrated in your application.

If you wish to validate messages occasionally you can use the Gazelle Validation Front-End called [EVS Client](https://gazelle.ihe.net/EVSClient) which puts at your disposal a user interface to validate HL7 messages, CDA documents, XD\* requests and so on.

Concerning HL7v2 validation: the application also gathers the HL7 conformance profiles and the HL7 tables (codes) which can be browsed by any user. For each conformance profile, you will find information about its issuer (actor, domain, transaction) and the message type and HL7 version. Each conformance profile can be bound to one or several HL7 tables which gather a list of allowed values for a given field of the message.

# User interface

As shown on picture below, the [User Interface](https://gazelle.ihe.net/GazelleHL7Validator) provides two validation tools: **HL7v2.x Validation tool** and **HL7v3 Validation tool**:

![](./media/HL7Validator_UserInterface.png)

**HL7v2.x Validation tool** allows user to browse HL7 messages profiles and resources:

![](./media/NewHL7Validator_HL7v2ValidationTool.png)

And **HL7v3 Validation tool** allows user to access to validator documentation:

![](./media/HL7Validator_HL7v3ValidationTool.png)

Both tools provide the possibility to display the result statistics chart forms.


## Browse HL7 message profiles

Select option ![](./media/HL7Validator_HL7v2BrowseHL7MessageProfilesOption.png) of **HL7v2 validation tool** menu.

It will display the list of HL7 messages profiles:

![](./media/NewHL7Validator_MessageProfiles.png)

All the HL7 message profiles available are gathered in the tool. You can either select a profile by its full OID (if known) or put filters on the table columns. Each profile is viewable inline and can also be downloaded.

Click on button ![](./media/HL7Validator_MessageProfilesRefresh.png) to update the list of message profiles displayed.

## Browse HL7 resources

Select option ![](./media/HL7Validator_HL7v2BrowseRessourcesOption.png) of **HL7v2 validation tool** menu.

It will display the list of HL7 resources:

![](./media/HL7Validator_Resources.png)

An HL7 resource is an XML file gathering several HL7 tables.

An HL7 table is uniquely defined by an ID and contains the set of allowed values.

Those tables are referenced in the message profiles and at validation time, the validation engine can check that the value of a given field of the submitted message comes from the set of allowed codes for this field.

As for the message profiles, you can see those tables inline or download them.

## Browse HL7v3 validation service documentation

Select entry ![](./media/HL7Validator_HL7v3DocumentationOption.png) of **HL7v3 validation tool** menu.

It will display the documentation of the constraints expressed in the model-based validation service:

![](./media/HL7Validator_Documentation.png)

## Validation statistics

This option is accessible only for Administrator users.

Access from ![](./media/HL7Validator_HL7v3StatisticsOption.png) of **HL7v3 validation tool** menu.

It will display the validation requests received by the tool and the associated results presented in chart forms:

![](./media/HL7Validator_Statistics.png)

# Web service API

## Gazelle HL7v2.x validation service

The web service API of the tool offers two methods:


* validate(): validates the given message against the given message profiles and sends back a validation report
* about(): gives information about the running version of the tool

The definition of the web service API is available [here](https://gazelle.ihe.net/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl).

The validate() method has the following prototype:

**public String validate()**;


This method uses an attribute named **ValidationContext**, it is an XML formatted String respecting the XSD schema given at [https://gazelle.ihe.net/xsd/ValidationContext.xsd](https://gazelle.ihe.net/xsd/ValidationContext.xsd). It gives information about the HL7 message profile to use for the validation process. 

Then, there are a String stands for the **message to validate**. The message must use ER7 syntax (ie. pipe-based syntax).

Moreover, we have an other attribute, **xmlValidationMetadata**, it is an XML formatted String respecting the XSD schema given at [https://gazelle.ihe.net/xsd/MessageMetadata.xsd](https://gazelle.ihe.net/xsd/MessageMetadata.xsd). By now, this parameter is not used but our intent is to add custom validation. That means that, in addition of the basic Hapi validator, the user will be able to put additionnal validation requests as constraints on field usage, component value...

Depending on the profile format, this method call either the Hapi or the GVT based validator to obtain the validation report.

As we also need the client side of this validation service, we have created a useful **SoapUi** project example available [here](https://gazelle.ihe.net/files/GazelleHL7v2Validator-soapui-project.xml).

Note that this validation tool is also available through the simulators based on HL7v2.x (the messages sent and received by the simulator can be validated) and the [EVSClient](https://gazelle.ihe.net/EVSClient).

## Gazelle HL7v3 validation service

See: [Model-based validation web services](https://gazelle.ihe.net/content/model-based-validation-web-services).

# Bug tracking

Error is human. We try to maintain the message profiles and HL7 tables doing our best but we may do mistakes. If you think there are errors in one/several of our message profiles, please report an issue in our [bug tracking system]( https://gazelle.ihe.net/jira/projects/HLVAL) with mention of the profie OID, error location, appropriate fix and reference to the documentation. 


# HL7 conformance profiles and HL7 tables management

This part of the documentation is dedicated to the manager/administrator of the Gazelle HL7 Validator tool. 

The  Hapi validation of HL7v2 messages within the Gazelle platform is based on:

1. **HL7 conformance profiles**: XML files which describe the structure of the HL7 messages, they are constrainable. For each segment, field, component and subcomponent, the usage (required, optional, not allowed...), the cardinality and the datatypes are defined.

2. **HL7 resources**: XML files which declare a set of HL7 tables (kind of value sets). HL7 conformance profiles may reference HL7 table for a field, component or subcomponent, in order to constrain the set of allowed values for this element of the message.


The  GVT validation of HL7v2 messages within the Gazelle platform is based on 3 files:

1. **Profile.xml** : XML file which describes the structure of the HL7 messages. For each segment, field, component and subcomponent, the usage (required, optional, not allowed...), the cardinality and the datatypes are defined.

2. **Constrain.xml** : XML file which describes the constraints of the HL7 messages (Constraints and Predicates). 

3. **ValueSets.xml** : XML file which declares a set of HL7 tables (kind of value sets). HL7 conformance profiles may reference HL7 table for a field, component or subcomponent, in order to constrain the set of allowed values for this element of the message.

4. **Display.html** : HTML file which is used to format the structure of the profile webpage. 



In the previous version of GazelleHL7Validator, Message Profiles and HL7 Tables were stored in the database to perform validation, xml file only were used to update the database. To perform a validation, GazelleValidator needs access to three xml files in both validation. File now are stored on the disk, each validation needs access to files to be performed. The OID of the resource links to the file path. 

HL7 conformance profiles and HL7 Tables are linked in the database of the tool  with their OID. Note that the conformance profile file only reference the number of the HL7 table, that means that, for a given conformance profile,the tool must be able to know in which HL7 resources looking for the table.

For each HL7 conformance profile, we need to know to which part of the technical framework, which message it applies. That's why, the HL7 conformance profiles are actually referenced in Gazelle Master Model. This enables us to link a conformance profile to a tuple (Domain, Actor, Transaction, HL7 version, message type \[, order control code\]). GazelleHL7Validator and Gazelle Master Model are two tools which have independent databases; as a consequence, the OID is used as a foreign key and two web services offered by Gazelle Master Model allow us to exchange data between those two applications:

1. **IHEConcepts**: This web service lists the different concepts (domain, actor, transaction) based on other parameters. For instance, you can get the list of all domains, the list of actors involved in a given domain and so on.

2. **Hl7MessageProfile**: This web service offers methods to retrieve the complete list of conformance profiles registered in GMM, a subset of these profiles which match certain parameters (actor, message type, HL7 version...). This web service also offers a method to add a new conformance profile reference into the database of GMM.

The URL of the Gazelle Master Model web services are configurable within the application preferences part of Gazelle HL7 Validator. Make sure to reference the correct instance of Gazelle Master Model.

The following sections detail the different actions you may need to perform to maintain the set of conformance profiles available in your instance of Gazelle HL7 Validator. In order to keep a consistency between the references in Gazelle Master Model and the conformance profiles actually available in Gazelle HL7Validator, only one instance of Gazelle HL7 Validator must be used per instance of Gazelle Master Model.

## OID assignments

HL7 conformance profiles and HL7 tables are assigned OIDs. Remember that an OID must be unique through the world; that means that, the first thing to do when you install a new instance of Gazelle HL7 Validator is to update the root of the OIDs which will be generated by the application. Three OIDs are used which are defined in oid\_generator table:

* One for the conformance profiles
* One for the HL7 resources
* One for the validation results (messages)

Currently, no user interface is available to perform this update, you will need to modify those values, manually, in the database.

## Add a new conformance profile

Adding a new conformance profile consists in two things:


1.  Creating a new reference in Gazelle Master Model
2.  Importing the relative path to the XML file representing the conformance profile to the database of GazelleHL7v2Validator in order to load the content

For an IGAMT profile, the path must lead to the folder containing the xml files (Constraints, Profile and ValueSets) and the html file Display.   For files in gazelle format, the path leads directly to the xml file of the profile. In both cases, the path starts from the data folder.

Then select option ![](./media/HL7Validator_AdminRegisterNewProfilesOption.png) of Administration menu (you must be logged in as an administrator).

A form is displayed (see screenshot below). Fill out this form with the information that match the conformance profile you are working with. Note that, first, only the domain drop-down list is displayed, then the list of actors is computed depending on the domain you have selected and finally the transactions will be available depending on the actor/domain pair previously selected.
Then, select the format of the conformance profile. As you can see on the screenshot below, a button ![](./media/HL7Validator_DefaultPath.png) is available on the right of the "Path" field. This button generates the relative path of the conformance profile according to the format and the OID previously entered as well as the preferences. 

![](./media/RegisterNewProfiles.png)

As you can notice it on the screenshot, a button ![](./media/HL7Validator_RequestOID.png) is available on the right of the "Profile OID" field. Clicking on this button will ask the application to automatically assign an OID to this conformance profile.

Depending on the profile format, Gazelle HL7 Validator uses HAPI or GVT as validation engine. In the case of HAPI, at validation time, message to validate is converted to a java object. Although all the message structures defined by HL7 are available in HAPI, in some cases, you will need to generate your own java class describing the message and tell the application in which java package it will find it. That is the case when IHE defines a new segment or change the structure of a message. 

A project called gazelle-hl7-messagestructures available on [Gitlab](https://gitlab.inria.fr/gazelle/library/gazelle-hl7-messagestructures) and based on HAPI libraries is available to generate the java classes from the conformance profiles.

Finally, upload the XML file representing the conformance profile. As soon as the form is filled and the file is uploaded a button ![](./media/HL7Validator_Submit.png) is available.

Click on this button, the information will be stored in database and the reference to the profile will be sent to Gazelle Master Model.

***In order to facilitate the management of the registered profiles, we recommend to rename the XML file (on your file system) with the OID assigned to the conformance profile.***

## Add a new resource

Basically, one HL7 resource is registered for each version of HL7. In some cases, you will need to customize a table for a given conformance profile or for a set of conformance profiles. In that case, you may need to register a new resource.

Select option ![](./media/HL7Validator_AdminRegisterNewResourcesOption.png) of Administration menu (you must be logged in as an administrator).

A new form is displayed (see screenshot below).

![](./media/RegisterNewResources.png)

As for the conformance profile, you can ask the application to generate an OID. You can also generate the relative path of the resource according to the OID and the preferences thanks to the button ![](./media/HL7Validator_DefaultPath.png) which is available on the right of the "Path" field.
Once the form is filled out and the XML file is uploaded, click on the button ![](./media/HL7Validator_Submit.png) and the HL7 resource will be stored in database. 

 
**Tip for filling out the weight attribute**

When the tool initializes a validation, it retrieves all the HL7 resources linked to the selected conformance profile. Then, at validation time, for each reference to an HL7 table, it browses the selected resources to extract the set of allowed values. We have defined a mechanism of weight which allow us to tell the application in which order it must browse the resources. Actually, it may happen that you have overridden a table defined in several of the selected resources. The resource is the higher weight will be processed first. HL7 resources defined by HL7 have usually a weight equals to 1.

***In order to facilitate the management of the registered resources, we recommend to rename the XML file (on your file system) with the OID assigned to the resource.***

## Link conformance profiles to resources

Select option ![](./media/HL7Validator_AdminManageLinksOption.png) of Administration menu (you must be logged in as an administrator).

It will display the page to manage links between profiles and ressources as shown below:

![](./media/HL7Validator_ManageLinks1.png)
![](./media/HL7Validator_ManageLinks2.png)

This page is composed of four parts:

* The list of available HL7 resources
* The list of selected HL7 resources
* A panel to select a set of conformance profile or a profile knowing its OID
* A panel which lists the selected conformance profiles

To link HL7 resources to conformance profiles, process as follows:

1. First select the HL7 resource(s) you want to link to a set of profiles. Click on the blue plus icon of each row.
2. Then, select a set of conformance profile (use filters)

Finally hit the button "Link the profiles displayed below to the selected profiles"

## Update conformance profiles and resources

This section assumes that you have renamed your XML files according to the previous advices; that means that on your file system, you have a set of files named \[profile\_oid\|resource\_oid\].xml. Maintenance will be easier if you store the profiles and resources in two different directories.

It will display the definition of application preferences table and a section entitled ***HL7 message profiles and tables import*** as shown below:

![](./media/HL7Validator_ConfigAppli1.png)
![](./media/HL7Validator_ConfigAppli2.png)

To update the conformance profiles, configure the ***"Path to HL7 message profiles"*** and click on the button ![](./media/HL7Validator_ImportUpdateProfiles.png). **The path must point to a directory on the server**.

To update the conformance profiles, configure the ***"Path to HL7 tables"*** and click on the button ![](./media/HL7Validator_ImportUpdateTables.png). **The path must point to a directory on the server**.

In both cases, the application browses the given folder and lists all the XML files. For each file, it tries to retrieve the relative conformance profile / resource and compare the date of the file with the date of the last changed.

If the file on disk is newer than the last changed, then, the file will be imported into the database to replace the old content.


## How conformance profiles and resources are organized on Gazelle's GitLab

This section describes how we have chosen to organize the conformance profiles and resources on Gazelle's GitLab and how they are maintained and managed.

Currently, both IHE Europe development team and the NIST contribute to maintain profiles.

A project is called gazelle-hl7v2-validator-resources and is available at [Gitlab](https://gitlab.inria.fr/gazelle/test-content/gazelle-hl7v2-validator-resources).

This project is made of three main folders: 

1. **GVTValidatorResources** : In this folder, the GVT jar need to perform validation with IGAMT profile. 

2. **HL7ConformanceProfiles** : In this folder

    2.1. **HL7MessageProfiles**: In this folder, conformance profiles are organized by firstly actor keyword then transaction keyword and finally message type (messageType\_triggerEvent). For instance, you will find the conformance profile for validating the ADT^A28^ADT\_A05 message issued by the Patient Demographic Supplier actor in the context of the ITI-30 transaction at the following location: **PDS/ITI-30/ADT\_A28/profile/ADT\_A28.xml**. A sub-directory of HL7MessageProfiles is named ProfilesPerOid and contains a symbolic link to each profile. Name of the link is the OID assigned to the targeted message profile. In this way, when you want to update a profile you do not have to know its OID and in the same way, when updating the message profiles in the database of the tool, the mapping is correctly performed thanks to the symbolic links.

    2.2 **HL7Tables** contains the HL7 resources all with a meaningful name and a child directory named TablesPerOid which contains a symbolic link to each resource. Name of the link is the OID assigned to the targeted resource.

3. **IGAMTConformanceProfiles** :  In this folder, conformance profiles, generated using IGAMT are organized by firstly actor keyword then transaction keyword and finally message type (messageType\_triggerEvent). For instance, you will find the 3 three xml files for validating the ADT^A28^ADT\_A05 message issued by the Patient Demographic Supplier actor in the context of the ITI-30 transaction at the following location: **PDS/ITI-30/ ADT^A28^ADT\_A05/**

IHE profiles are available on the master branch. You need to be on your branch to add new profiles. 


# Manage HL7v3 constraints file

Select option ![](./media/HL7Validator_AdminManageConstraintsFileOption.png) of Administration menu (you must be logged in as an administrator).

It will display the list of HL7v3 constraints as shown below:

![](./media/HL7Validator_ManageConstraintsFile.png)

From there, it’s possible to delete individually each HL7v3 constraint by clicking on the button ![](./media/HL7Validator_Icon_Trash.png).

It’s also possible to import an XML file with the constraints written inside. This file must be generated with “Topcased” software from the OCL constraints.

# Manage users accesses

Select option ![](./media/HL7Validator_ManageUserAccessesOption.png) of Administration menu (you must be logged in as an administrator).

It will display the page that allow the management of accesses for users:

![](./media/HL7Validator_ManageUserAccesses.png)

You can restrict the messages an user is allowed to see in the logs page by editing the user preferences.

By adding an user, you can restrict the allowed IP addresses to constraint this user to see only the messages coming from this IP address.

You can add many IP addresses for a single user.

![](./media/image21.png)

# HL7v3 validation service

Gazelle HL7 Validator embeds a web service interface to query the HL7v3 validation service. This validation service has been developed using the model-based engine. All model-based validation service exposes a web service which uses the same definition.

Refers to the *general documentation* if you want to use this service from your application.

This service can be easily called using the [EVS Client](https://gazelle.ihe.net/EVSClient) application:

Start a validation as shown on picture below:

![](./media/EVSClient_HL7v3Validation.png)


The model has been generated from the HL7v3 schema available at [ftp://ftp.ihe.net/TF_Implementation_Material/ITI/schema/HL7V3/NE2008](ftp://ftp.ihe.net/TF_Implementation_Material/ITI/schema/HL7V3/NE2008/). Constraints have been written in accordance with the IHE specifications available in ITI Technical Framework Appendix O and the section from the ITI Technical Framework volume 2 which deals with the HL7v3 protocol.

The following messages can be validated using this service : 

* PDQv3 Accept Acknowledgement
* PDQv3 Patient Demographic Query
* PDQv3 Patient Demographic Query Cancellation
* PDQv3 Patient Demographic Query Continuation
* PDQv3 Patient Demographic Query Response
* PIXV3 Patient Identity Feed HL7V3 (Add Patient Record)
* PIXV3 Patient Identity Feed HL7V3 (Revise Patient Record)
* PIXV3 Patient Identity Feed HL7V3 (Patient Identity Feed)
* PIXV3 Patient Identity Feed HL7V3 (Acknowledgement)
* PIXV3 Query
* PIXV3 Query Response
* PIXV3 Update Notification 
* PIXV3 Update Notification Acknowledgement
* XCPD Cross Gateway Patient Discovery Request
* XCPD Cross Gateway Patient Discovery Request (Deferred option)

Two additional messages can be validated with this tool though they are not HL7v3 based (but defined in the context of XCPD):

* XCPD Patient Location Request
* XCPD Patient Location Response
