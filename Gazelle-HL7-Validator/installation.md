---
title:  Installation Manual
subtitle: Gazelle HL7 Validator
author: Anne-Gaëlle Bergé
function: Engineer
date: 07/08/2024
toolVersion: 3.X.X
version: 1.04
status: Approved document
reference: KER1-MAN-IHE-GAZELLE_HL7_VALIDATOR_INSTALLATION-1_04
customer: IHE-EUROPE
---

# Sources & binaries

To get the name of the latest release, visit the Gazelle HL7 Validator project in *JIRA* and consult the "Releases" section.

A maven artifact is published in our Nexus repository each time we release the application. You can use it, but be aware that the link to the database is hardly expressed within the artifact so you will have to use the same database name, hosted in the server running Jboss, and with the same owner (and password).

To get the artifact on Nexus browse [here](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22GazelleHL7v2Validator-ear%22) and download the latest version.

If you rather want to build the project by yourself, you must checkout the latest tag and package it. You may want to create a new profile to customize your build.

1. Checkout the latest tag available on Inria’s gitlab: *git clone --branch "TAG_VERSION" https://gitlab.inria.fr/gazelle/public/validation/hl7v2-validator.git*

2. \[Optional\] Edit the pom.xml file and create a new profile

3. Package the application: `mvn \[-P profile\] clean package`

4. The EAR is available at GazelleHL7v2Validator/GazelleHL7v2Validator-ear/target/GazelleHL7v2Validator.ear

# Step by step installation

To get the jar on Nexus browse [here](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22GazelleHL7v2Validator-ear%22) and download the latest version.
## Requirements 

First, please consider reading the General installation manual for Jboss 7.2.0 and reading 
the General installation manual for Gazelle Test Management. It shall be deployed and initialized. 

The following tools are required : 

| Tool | Version |
|----------------------------------|----------------------------------|
| java 7 | zulu 7 / open jkd-7| 
| java 8 |openjdk-8|
| psql | version 9 | 



## Database configuration

If you use the artifact available on Nexus or if you have not change this parameter in the pom.xml file, create a database named gazelle-hl7-validator, owned by the user gazelle.

```
$ createdb -U gazelle -E UTF8 gazelle-hl7-validator
```

 **WARNING** : From version 3.5.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/datasource in the source or in the file GazelleHL7v2Validator-ear-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : GazelleHL7v2ValidatorDS

Database name : gazelle-hl7-validator


## Deploy the application

* Copy EAR to the deploy folder of JBoss 7.2.0 AS(do not forget to change its name to GazelleHL7v2Validator.ear)
* Start Jboss ⇒ *sudo service jboss start*
* Wait until the application has been completly deployed and configure the database running the SQL script for set-up.

## Initialize the application

You first need to initialize the database with some data available in a SQL scrip,. If you have checked out the project, the scripts are available in *GazelleHL7v2Validator-ear/src/main/sql/*.

__IMPORTANT NOTICE__: Before to apply this script into your database, open it and replace the __'to be defined.'__ values to be inserted in the oid_generator table by the root OIDs defined for your instance of the tool. Be carefull and keep the final dot.

Otherwise, download it from Inria’s forge (See Sources section)

Before executing the script, open the file and checked the various preferences to be inserted in the app\_configuration table, especially the application\_url and other preferences relative to the user authentication (see Application configuration section). **Context path for deployment is /GazelleHL7Validator**.

Finally, execute the script: 

```
$ psql -U gazelle gazelle-hl7-validator < schema-3.6.0.sql
```
```
$ psql -U gazelle gazelle-hl7-validator < init-3.6.0.sql
```

To take those parameters into account, you need to restart either the whole Jboss ```sudo service jboss7 restart```, either only the application (```$ touch GazelleHL7v2Validator.ear``` in the deploy folder of Jboss)



# Application configuration

## Gazelle HL7v2 Validator resources

A git repository is available to initialize data resource. You shall create a directory GazelleHL7Validator and checkout the last tag on Inria's GitLab
`git clone https://gitlab.inria.fr/gazelle/test-content/gazelle-hl7v2-validator-resources.git`

It shall create the correct structure and add required resources. 

 Project shall be structured as follows : 

 
```
 gazelle-hl7v2-validator-resources
         |GVTValidatorResources
                 |gvt-validation-jar-jar-with-dependencies.jar
         |HL7ConformanceProfile
                 |HL7MessageProfiles
                        |ProfilesPerOid
                 |HL7Tables
                        |TablesPerOid
         |IGAMTConformanceProfile
                 |see_structure     
```

## gvt-validation-jar

To perform  hl7v2 validation, Gazelle HL7 Validator need the __gvt-validation-jar-jar-with-dependencies.jar__
The jar is available in our Nexus repository. To get it on Nexus browse [here](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gvt-validation-jar%22) and download the latest version.



## Application configuration

Use the Administration menu, you will find a sub-menu entitled "Configure application preferences". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the Gazelle HL7 Validator tool.

| Variable                         | Description                                                                                                                                                                                                                                                                                                                                                                        | Default value                                                            |
|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------|
| application\_issue\_tracker\_url | The URL of the bug tracking system where to post issues                                                                                                                                                                                                                                                                                                                            | https://gazelle.ihe.net/jira/projects/HLVAL/summary                                 |
| application\_url                 | The URL used by any user to access the tool. The application needs it to build permanent links inside the tool                                                                                                                                                                                                                                                                     | http://publicUrlOfJboss/GazelleHL7Validator                              |
| force\_stylesheet                | HL7MessageProfiles and HL7 resources are XML files displayed with a stylesheet declared in the file and referenced on gazelle.ihe.net. To avoid cross-site references, most browsers do not follow those links and as a consequence do not display the XML file correctly. This property tells the tool to change the link to the stylesheet before sending the file to the client | true                                                                     |
| gmm\_hl7messageprofile\_wsdl     | Access Hl7MessageProfile webservice exposed by GMM (or TM configured to work also as GMM)                                                                                                                                                                                                                                                                                          | https://publicUrlOfJboss/gazelle-gmm-gazelle-tm-ejb/Hl7MessageProfile?wsdl |
| gmm\_iheconcepts\_wsdl           | Access IHEConcepts web service exposed by GMM (or TM configured to work also as GMM)                                                                                                                                                                                                                                                                                               | http://131.254.209.12:8080/gazelle-gmm-gazelle-tm-ejb/IHEConcepts?wsdl   |
| hl7\_directory                   | Absolute path of the directory where data (HL7 profiles and tables) are stored                                                                                                                                                                                                                                                                                                                          | path to Data, Example     /opt/GazelleHL7Validator/gazelle-hl7v2-validator-resources/
| hl7\_profiles\_directory         | Relative path (from the hl7_directory) of the directory where XML files representing the message profiles in Gazelle format are stored (used as a basis for updating the database, might be a folder sync with the forge)                                                                                                                                                                  | example : /HL7ConformanceProfiles/HL7MessageProfiles/ProfilesPerOid                                                                                        | $PathToDataProject$/ProfilesPerOid                                       |
| hl7\_profile\_xsd                | Location of the schema for verifying the structure of the XML message profiles.                                                                                                                                                                                                                                                                                                    | path to HL7MessageProfileSchema.xsd                                      |
| hl7\_resource\_xsd               | Location of the schema for verifying the structure of the XML resources.                                                                                                                                                                                                                                                                                                           | path to HL7TableSchema.xsd                                               |
| hl7\_table\_directory            | Relative path (from the hl7_directory) of the directory where XML files are stored (used as a basis for updating the database, might be a folder sync with forge)                                                                                                                                                                                                                           | example : /HL7ConformanceProfiles/HL7Tables/TablesPerOid                                        |
| IGAMT\_directory                 | Relative path (from the hl7_directory) of the directory where XML files representing the message profiles in IGAMT format are stored                                                                                                                                                                                                                                                     | example : /IGAMTConformanceProfiles            
| jar_path                         | Location of the GVT jar                                                                                                                                                                                                                                                                                                                                                            | path to gvt-validation-jar-jar-with-dependencies.jar Example : /opt/GazelleHL7Validator/gazelle-hl7v2-validator-resources/GVTValidatorResources/gvt-validation-jar-jar-with-dependencies.jar
| length\_is\_an\_error            | how to behave when an error on length is encountered                                                                                                                                                                                                                                                                                                                               | false                                                                    |
| NUMBER\_OF\_ITEMS\_PER\_PAGE     | How many items are displayed in a datatable element by default                                                                                                                                                                                                                                                                                                                     | 20                                                                       |
| profile\_xsl\_url                | used if force\_stylesheet = true                                                                                                                                                                                                                                                                                                                                                   | link to the stylesheet (FDQN must be the same as for the application)    |
| report\_xsl\_location            | link to the stylesheet for displaying reports                                                                                                                                                                                                                                                                                                                                      | https://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl            |
| resource\_xsl\_url               | used if force\_stylesheet = true                                                                                                                                                                                                                                                                                                                                                   | link to the stylesheet (FDQN must be the same as for the application)    |
| svs\_repository\_url             | URL to the Gazelle value set repository. Used to check codes in HL7v3 validation service                                                                                                                                                                                                                                                                                           | https://gazelle.ihe.net                                                   |
| time\_zone                       | Used to display date/time in the appropriate time zone                                                                                                                                                                                                                                                                                                                             | Europe/Paris                                                             |
| value\_is\_an\_error             | how to behave when an error on data value is encountered                                                                                                                                                                                                                                                                                                                           | false                                                                    |
| xsd\_directory\_location         | where to find the XSD files for HL7v3 validation service                                                                                                                                                                                                                                                                                                                           | example : /home/gazelle/xsd/HL7V3/NE2008/multicacheschemas               |
| xcpd\_plq\_xsd\_location         | where to find the XSD files for XCPD/PLQ validation service                                                                                                                                                                                                                                                                                                                        | example : /home/gazelle/xsd/IHE/XCPD\_PLQ.xsd                            |

# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

# Home page

The first time you access the application, you may notice that the home page of the tool is not configured. To set a title and a welcome message, log into the application with admin rights.

Note that you will have to set up this page for all the languages supported by the application.


# Upload of the documentation of the model-based validation service (for HL7v3 messages)

The administrator is able to configure the documentation of XDS/DSUB constraints and rules

First the user shall be logged in as an administrator.

Then, go to administration -&gt; Constraints Management -&gt; Constraints-admin

![](./media/image19.png)

The user shall then upload the XML file generated from the model of constraints into the tool by using the add button from the previous page.

After the upload is ended, click on the button "Delete and Generate". This button will delete all the constraints related to packages that are mentioned
 into the uploaded XML file. If there are some constraints related to other packages, they won’t be deleted. The information into the XML document is inserted in the database of the tool.

# What's new in 3.6.x

 In the new GazelleHL7v2Validator version, few major changes have been released. 
 
 Regard to the GDPR (General Data Protection Regulation), this new version will not store some data anymore. The Database will not store  all details from 
 a validation, it will keep only the ID, The validation date, caller ip, the profile oid and the test result. 

GazelleHL7Validator is now able to perform validation on a new message profile format : IGAMT profile.  This new format is composed of four files : 3 XML files, 
Constraints.xml that describes all constraints and conditions, and ValueSets that contains all values required to perform validation;  and one HTML file Display. HL7 Conformance 
format will be migrated in this new format. 

In the previous version of GazelleHL7Validator, Message Profiles were stored in the database to perform validation, xml file only were used to update the database.
 To perform a validation, GVT needs access to three xml files (Constraints, Profile and ValueSets) which compose a Conformance Profile from [IGAMT](https://hl7v2.igamt.nist.gov/igamt/#/home). 
 File now are stored on the disk, each validation needs access to files to perform. Files shall have bee upload in the correct IGAMT directory. 
 
 Also, GazelleHL7Validator now integrates two validator engines : HAPI based engine and General Validation Tool ([GVT](https://hl7v2.gvt.nist.gov/gvt/#/home))
 from the NIST. This last is executed as an external jar and its path shall be specified in application configuration. Depending on the Message Profile format 
 selected to validate, GazelleHL7v2Validator will use the good one. 
 
 
 
 
 
 
