---
subtitle:  LCSD Simulator
title: User Manual
author: Nicolas LEFEBVRE
date: 23/08/2016
toolversion: 1.x
function: Engineer
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-LCSD_SIMULATOR_USER-1_02
customer: IHE-EUROPE
---

# Introduction

The LCSD simulator is developed in conformance with the IHE technical framework, that means that national extensions are not (yet) taken into account.

This simulator can be an Initiator and a Responder.

As an initiator, this simulator is aimed to send messages to a responder. So, if your system is ready to listen and accessible from the Internet, you will be able to send some messages to it.

As a Responder, this simulator is aimed to listen messages from an initiator.

By now, this simulator can act as two different actors:

* CSM (Code Set Master)
* CSC (Code Set Consumer)

The table below gathers the supported affinity domains, transactions and SUT actors.

## What is this simulator able to do ?

The simulator has been developed with the purpose of helping the developers of actors, such as the_ Code Set Master_ and the _Code Set Consumer_ actors.  We have tried to manage most of the use cases, and  most of message  defined in the technical framework for those actors  are supported by the simulator.

The following table summarize the actors, profiles and transactions supported by the LCSD simulator.


Integration Profile |  Actor |  Affinity Domain |  Transaction |  System Under Test |
----------------------|--------|------------------|--------------|--------------------|
LCSD | CSM | IHE | LAB-51 | Code Set Master
LCSD | CSC | IHE | LAB-51 |  Code Set Consumer



Today the simulator supports all type of messages defined in the TF for the LCSD profile, except the batch option for which further development is required.

* Observation Codes (MFN^M08^MFN_M08 message type)
* Non-numeric Observation Codes (MFN^M09^MFN_M09 message type)
* Battery Codes (MFN^M10^MFN_M10 message type)
* Calculated Observation Codes (MFN^M11^MFN_M11 message type)
* _Batch option (IHE Lab Vol 1 section 8.3.1 )(Not Yet Available)_

# How to get started

## Get a Gazelle account

First of all, note that, like the other applications from Gazelle testing platform, the HMW Simulator is linked to our CAS service. That means that, if you have an account created in Gazelle, you can use it, if you do not have one, you can create one now by filling the form [here](https://gazelle.ihe.net/EU-CAT/users/user/register.seam). Create an account in Gazelle is free. The login link ("cas login") is located in the top right corner of the page.

### Registration of Systems Under Test (SUT) acting as HL7 responders

If your system acts as an HL7 responder in one of the transactions offered by the simulator (for example your system is Code Set Consumer and supports LAB-51 transaction), you will have to enter its configuration in the application.

In order to proceed, go to "System Configurations" and hit the "Create a Configuration" button. You can also copy ![copy](./media/editcopy.gif) or Edit ![](./media/edit.gif) an existing configuration (one of yours !).

In both cases, the simulator needs to know:

* A name for your configuration (displayed in the drop-down list menus)
* The actor played by your system under test
* The receiving facility/application
* The IP address
* The port the system is listening on
* The charset expected by your SUT

If you are logged in when creating the configuration, you will be set as the owner of the configuration. If you do not want other testers to send messages to your SUT you can uncheck the box "Do you want this configuration to be public?" and you will be the only one to be able to select your system in the drop-down list and to edit it (if logged in!).

Before sending messages to your system under test, ensure that your firewall options give to LCSD Simulator the access to your system.

# Code Set Master

## Using the Code Set Master simulator to send Codes to your Code Set Consumer SUT

The CSM Simulator allows the users of the simulator to send Code Sets to CSC actors. The CSM simulator provides some sample Code Sets (at least one for each message type that can be used to feed client Code Set Consumer. In this case, the CSM Simulator acts as an initiator and the CSC SUT as a responder.

To communicate with your system under test, the simulator needs your system's endpoint configuration. The CSC actor acts as a  Go to the "How to get started" part of this tutorial for further details.

1. First at all, go to LCSD Simulator menu bar and select the CSM menu entry.
2. A sequence Diagram Picture is available to help you to understand the transaction and the role of each actors.
3. Choose your SUT configuration in the System Under Test drop-down list.
4. You must choose the Codes to send to the CSC. Begin to choose the Code Set Category in the drop-down list, then choose the Code Sets in the Code Sets drop-down list. Use the  ![Icon used to display LCSD Codes.](./media/view_codes_50_50.png) button to display the selected codes. (The screeshot below illustrates this part).
5. At least, hit the Send message button to send the selected codes to the CSC SUT.

![CSM Actor GUI screenshot.](./media/CSMActor.jpg)

A tab with all messages for the transaction and the actor selected is available at the bottom. You can find your messages in using the filter fields.

You can display the codes send in each HL7 messages. To do that, just hit the ![Icon used to display LCSD Codes.](./media/view_codes_50_50.png)  buton in the action column. A new window will appear to display all codes contained in the selected HL7 message and send by the CSM actor.

# Code Set Consumer

## Send HL7 messages to the Code Set Consumer simulator

The simulator can act as a Code Set Consumer and receive messages from a distant system that implements the Code Set Master actor.

1. First at all, go to LCSD Simulator menu bar and select the CSC menu entry.
2. A sequence Diagram Picture is available to help you to understand the transaction and the role of each actors.
3. Below this picture, a tab with all messages for the transaction and the actor selected is available. You can find your messages in using the filter fields.
4. Don't forget to refresh this messages list after to have send your message to the Simulator. To do that, only hit the "Refresh List" buttom.

You can display the codes send in each HL7 messages. To do that, just hit the ![Icon used to display LCSD Codes.](./media/view_codes_50_50.png) buton in the action column. A new window will appear to display all codes contained in the selected HL7 message and send by the CSM actor.

The CSC Simulator is a responder and is able to respond to your CSM SUT with the appropriate Acknowledgment message. If the CSM message send to the CSC Simulator is not consistent with the IHE technical framework, the CSC will respond with an error Acknowledgment.

# HL7 validation

For further details about this functionnality of the LCSD Simulator, go to the [*HL7 messages validation page.*](https://gazelle.ihe.net/content/hl7-messages-validation)
