---
title: Release note
subtitle: Gazelle Webservice Tester
toolversion: 2.0.1
releasedate: 2024-02-22
author: Malo TOUDIC
function: Software developer
customer: IHE-Europe
reference: KER1-RNO-IHE-GAZELLE_WEBSERVICE_TESTER
---
# 2.0.1
_Release date: 2024-02-22_

__Bug__
* \[[GWT-152](https://gazelle.ihe.net/jira/browse/GWT-152)\] Cannot access Manage calling tools and Manage keystore pages

__Improvement__
* \[[GWT-151](https://gazelle.ihe.net/jira/browse/GWT-151)\] Configuration to limit the number of DICOM document displayed

# 2.0.0
_Release date: 2024-02-02_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[GWT-148](https://gazelle.ihe.net/jira/browse/GWT-148)\] Integrate new sso-client-v7
* \[[GWT-149](https://gazelle.ihe.net/jira/browse/GWT-149)\] Remove username display in GUI

# 1.8.0
_Release date: 2023-09-21_

__Improvement__
* \[[GWT-146](https://gazelle.ihe.net/jira/browse/GWT-146)\] Display dump of dicom document when displaying WADO-RS response
* \[[GWT-147](https://gazelle.ihe.net/jira/browse/GWT-147)\] Display response HTTP header

# 1.7.4
_Release date: 2021-07-19_

__Remarks__
This version of Gazelle Webservice Tester is only compatible with EVSClient 5.13.4 and forward because of a change in gazelle-evsclient-connector.

# 1.7.3
_Release date: 2020-09-17_

__Improvement__
* \[[GWT-143](https://gazelle.ihe.net/jira/browse/GWT-143)\] Improve performance on Execution Result Display


# 1.7.2
_Release date: 2020-09-15_
 
 __Improvement__
* \[[GWT-137](https://gazelle.ihe.net/jira/browse/GWT-137)\]GWT doesn't display the POST element for REST requests
 
# 1.7.1
_Release date: 2020-01-13_

__Improvement__

* \[[GWT-136](https://gazelle.ihe.net/jira/browse/GWT-136)\] The SoapUI response must not be pretty print

# 1.7.0
_Release date: 2019-08-02_

__Story__

* \[[GWT-122](https://gazelle.ihe.net/jira/browse/GWT-122)\] Add a Mock messages browsing feature
* \[[GWT-123](https://gazelle.ihe.net/jira/browse/GWT-123)\] SoapUI-mock record their messages in GWT database

# 1.6.0
_Release date: 2019-03-05_

__Bug__

* \[[GWT-117](https://gazelle.ihe.net/jira/browse/GWT-117)\] No check is performed on cross validator existence when retrieving its informations on Execution page
* \[[GWT-119](https://gazelle.ihe.net/jira/browse/GWT-119)\] Add calling tools functionality generates errors

# 1.5.0
_Release date: 2019-01-28_

__Bug__

* \[[GWT-108](https://gazelle.ihe.net/jira/browse/GWT-108)\] Remote validator called status not checked
* \[[GWT-115](https://gazelle.ihe.net/jira/browse/GWT-115)\] Modified properties are not taking in account when executing a project
* \[[GWT-116](https://gazelle.ihe.net/jira/browse/GWT-116)\] Editing a projet after being imported generates persistance error

__Improvement__

* \[[GWT-110](https://gazelle.ihe.net/jira/browse/GWT-110)\] Extract datasource configuration
* \[[GWT-111](https://gazelle.ihe.net/jira/browse/GWT-111)\] Update SQL scripts archive

# 1.4.0
_Release date: 2018-09-18_

__Improvement__

* \[[GWT-44](https://gazelle.ihe.net/jira/browse/GWT-44)\] The tool must be able to cross-validate.
* \[[GWT-93](https://gazelle.ihe.net/jira/browse/GWT-93)\] Integrate the changes related to the X Validation for steps
* \[[GWT-96](https://gazelle.ihe.net/jira/browse/GWT-96)\] We want to be able to cross validate messages from different steps
* \[[GWT-97](https://gazelle.ihe.net/jira/browse/GWT-97)\] Put an hyperlink on the X Validator OID to redirect to the documentation

# 1.3.0
_Release date: 2018-09-12_

__Bug__

* \[[GWT-90](https://gazelle.ihe.net/jira/browse/GWT-90)\] Fix performance issue
* \[[GWT-98](https://gazelle.ihe.net/jira/browse/GWT-98)\] When selecting wich test component to launch, the test cases are duplicated

__Improvement__

* \[[GWT-99](https://gazelle.ihe.net/jira/browse/GWT-99)\] Add Rest support in GWT
* \[[GWT-100](https://gazelle.ihe.net/jira/browse/GWT-100)\] Retrieve and display attachements

# 1.2.1
_Release date: 2018-07-12_

__Improvement__

* \[[GWT-48](https://gazelle.ihe.net/jira/browse/GWT-48)\] Be able to select all displayed custom properties
* \[[GWT-92](https://gazelle.ihe.net/jira/browse/GWT-92)\] Improve the way GWT map the actor code from xdstools and the role in test from tm

# 1.2.0
_Release date: 2018-04-12_

__Bug__

* \[[GWT-53](https://gazelle.ihe.net/jira/browse/GWT-53)\] SoapUI Disabled test-suites / test-cases must not be displayed
* \[[GWT-69](https://gazelle.ihe.net/jira/browse/GWT-69)\] The kind of runned soap is missing from the table of execution list

__Story__

* \[[GWT-9](https://gazelle.ihe.net/jira/browse/GWT-9)\] Log messages exchanged with the soapUI mock
* \[[GWT-43](https://gazelle.ihe.net/jira/browse/GWT-43)\] The tool must validate request and response with EVSClient
* \[[GWT-50](https://gazelle.ihe.net/jira/browse/GWT-50)\] As admin I wand to enable/disable SoapUI projects
* \[[GWT-51](https://gazelle.ihe.net/jira/browse/GWT-51)\] Be able to select which test-suite / test-case can be run
* \[[GWT-52](https://gazelle.ihe.net/jira/browse/GWT-52)\] Be able to edit SoapUI project configurations

__Improvement__

* \[[GWT-60](https://gazelle.ihe.net/jira/browse/GWT-60)\] Make the input text fields longer
* \[[GWT-64](https://gazelle.ihe.net/jira/browse/GWT-64)\] Problem with the configuration of the certificate
* \[[GWT-65](https://gazelle.ihe.net/jira/browse/GWT-65)\] We need to be able to download the request, the response after executing a test
* \[[GWT-68](https://gazelle.ihe.net/jira/browse/GWT-68)\] I need to see the parameter with which was executed the test; they are not shown in the permanent link
* \[[GWT-71](https://gazelle.ihe.net/jira/browse/GWT-71)\] Apero CAS
* \[[GWT-78](https://gazelle.ihe.net/jira/browse/GWT-78)\] Create webservices beteween TM and GWT
* \[[GWT-84](https://gazelle.ihe.net/jira/browse/GWT-84)\] Update postgresql driver to version 42.2.1-jre7

# 1.1.1
_Release date: 2017-12-26_

__Bug__

* \[[GWT-57](https://gazelle.ihe.net/jira/browse/GWT-57)\] Unable to import SoapUI Project if a custom property value is too long

# 1.1.0
_Release date: 2017-12-22_

__Bug__

* \[[GWT-39](https://gazelle.ihe.net/jira/browse/GWT-39)\] Timestamp should be displayed for request and response
* \[[GWT-40](https://gazelle.ihe.net/jira/browse/GWT-40)\] Wrong username displayed on menu after logout - login
* \[[GWT-42](https://gazelle.ihe.net/jira/browse/GWT-42)\] Gazelle TM URL in admin must be complete

__Story__

* \[[GWT-41](https://gazelle.ihe.net/jira/browse/GWT-41)\] Be able to launch a test without having a test instance in TM
* \[[GWT-46](https://gazelle.ihe.net/jira/browse/GWT-46)\] Define custom properties to input, when importing a Soapui project
* \[[GWT-47](https://gazelle.ihe.net/jira/browse/GWT-47)\] Save and display the validations of assertions of SoapUI


# 1.0.0
_Release date: 2017-11-27_

__Technical task__

* \[[GWT-16](https://gazelle.ihe.net/jira/browse/GWT-16)\] Create the page to add a SoapUi Project
* \[[GWT-17](https://gazelle.ihe.net/jira/browse/GWT-17)\] Create the page Project details
* \[[GWT-18](https://gazelle.ihe.net/jira/browse/GWT-18)\] Create the page List of project
* \[[GWT-19](https://gazelle.ihe.net/jira/browse/GWT-19)\] Create the page Run project
* \[[GWT-20](https://gazelle.ihe.net/jira/browse/GWT-20)\] Create the page Execution result

__Bug__

* \[[GWT-38](https://gazelle.ihe.net/jira/browse/GWT-38)\] Rerun action should be visible on Result detail page

__Story__

* \[[GWT-15](https://gazelle.ihe.net/jira/browse/GWT-15)\] Create GUI
* \[[GWT-32](https://gazelle.ihe.net/jira/browse/GWT-32)\] Access control

__Task__

* \[[GWT-26](https://gazelle.ihe.net/jira/browse/GWT-26)\] Create Unit tests

__Improvement__

* \[[GWT-37](https://gazelle.ihe.net/jira/browse/GWT-37)\] Add status test in execution list.
