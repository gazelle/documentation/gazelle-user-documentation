---
title:  Installation Manual
subtitle: Gazelle Webservice Tester
author: Malo TOUDIC
function: Software developer
date: 2024-02-22
toolversion: 2.0.1
version: 2.0.1
status: Approved document
reference: KER1-MAN-IHE-GAZELLE_WEBSERVICE_TESTER_USER
customer: IHE-EUROPE
---

# Sources & binaries

Gazelle Webservice Tester (GWT) is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via Git at [https://gitlab.inria.fr/gazelle/public/simulation/webservice-tester.git](https://gitlab.inria.fr/gazelle/public/simulation/webservice-tester.git).

The latest public packaged release can be downloaded from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-webservice-tester-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-webservice-tester-ear%22) (search for **gazelle-webservice-tester-X.X.X.ear**).

If you need for any reason a more recent version (hotfix, experimental feature...), The public packaged application of our development trunk can be found at [https://gazelle.ihe.net/jenkins/job/gazelle-webservice-tester-SNAPSHOT/ws/gazelle-webservice-tester-ear/target/gazelle-webservice-tester.ear](https://gazelle.ihe.net/jenkins/job/gazelle-webservice-tester-SNAPSHOT/ws/gazelle-webservice-tester-ear/target/gazelle-webservice-tester.ear). This package can be unstable.

# Installation

If you are installing a Gazelle tool for the first time in your environment, make sure to read carefully the [general considerations for JBoss7](/gazelle-documentation/General/jboss7.html)

## Database creation

Your database must have a user **gazelle**

1.  Connect to your database
```bash
psql -U gazelle
```

1.  Execute the SQL statement to create the database.
```sql
CREATE DATABASE "gazelle-webservice-tester" OWNER gazelle ENCODING 'UTF8' ;
```

## Deployment

To deploy Gazelle Webservice Tester :

1. Download the ear from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-webservice-tester-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-webservice-tester-ear%22) (search for **gazelle-webservice-tester-X.X.X.ear**)

1.  Paste the archive **gazelle-webservice-tester.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`

1.  Display JBoss server logs, start JBoss and wait for **ear** deployment.

1.  The application can be browsed at [http://localhost:8080/gazelle-webservice-tester](http://localhost:8080/gazelle-webservice-tester)
_Port could also be different whether you have modified the JBoss configuration._

1. **WARNING** : From version 1.5.0, datasources need to be extracted from the **ear** folder (found in /src/main/application/META-INF).
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : gazelle-webservice-testerDS

Database name : gazelle-webservice-tester

## Application configuration

1. Download the SQL scripts archive from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-webservice-tester-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-webservice-tester-ear%22) (search for **gazelle-webservice-tester-X.X.X-sql.zip**)

1. Unzip the archive into a folder named `gwt-sql`

1. Edit the application_url value in init.sql.

1.  From the bash, update the application configuration by running :
```bash
psql -U gazelle gazelle-webservice-tester < gwt-sql/init.sql
```


## Data storage

GWT will need directories to store SoapUI projects, logs and keystores. By default, the application is configured to use `/opt/gazelle-webservice-tester/`.
```bash
sudo mkdir /opt/gazelle-webservice-tester
sudo mkdir /opt/gazelle-webservice-tester/projects
sudo mkdir /opt/gazelle-webservice-tester/logs
sudo mkdir /opt/gazelle-webservice-tester/keystores
```

Be sure these directories can be read/written by JBoss.
```bash
sudo chmod -R 775 /opt/gazelle-webservice-tester
sudo chown -R jboss:jboss-admin /opt/gazelle-webservice-tester
```
