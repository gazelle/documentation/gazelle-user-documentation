---
title: User Manual
subtitle: Gazelle-Keycloak
author: Gabriel Landais
date: 12/05/2022
---

# Keycloak

Keycloak is used as an identity and access management solution.

It allows to connect to Gazelle applications using Gazelle TM credentials.

# Keycloak console

Your account information is accessible through the [account console](/auth/realms/gazelle/account).

![Console](media/user/01_console.png)

Click on "Sign in".

![Sign in](media/user/02_signin.png)

## Password recovery

Click on "Forgot password?".

![Forgot password](media/user/03_forgot_password.png)

Fill text field with username or email. Click on "Submit".

![Mail sent](media/user/04_mail_sent.png)

You will receive an email to reset your password.

## Password change

Once logged in the console, click on "Account security/Signing in".

Then click on "Update" next to "My password".

![Change password](media/user/05_update_password.png)

Fill fields with your new password and click on "Submit".

![Change password](media/user/06_password_input.png)

Your password has been changed. Gazelle TM password has changed too.
