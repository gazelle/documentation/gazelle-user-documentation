---
title: Release note
subtitle: Proxy
toolversion: 6.3.1
releasedate: 2025-01-27
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-Proxy
---

# V17-1.0.3
_Release date: 2025-01-27_


__Bug__
* \[[PROXY-511](https://gazelle.ihe.net/jira/browse/PROXY-511)\] Proxy does not support Expect-continue of HTTP 1.1
* \[[PROXY-528](https://gazelle.ihe.net/jira/browse/PROXY-528)\] version of HL7v2 messages is not correctly extracted
* \[[PROXY-529](https://gazelle.ihe.net/jira/browse/PROXY-529)\] Recording of HTTP response is blocked in case of wrong request HTTP Frame
* \[[PROXY-690](https://gazelle.ihe.net/jira/browse/PROXY-690)\] Wrong Content-length causes Decoder Error
* \[[PROXY-691](https://gazelle.ihe.net/jira/browse/PROXY-691)\] DICOM - Appel API Validation non fonctionnel
* \[[PROXY-693](https://gazelle.ihe.net/jira/browse/PROXY-693)\] DICOM - Absence d'affichage des données au sein de l'onglet "hex"
* \[[PROXY-694](https://gazelle.ihe.net/jira/browse/PROXY-694)\] DICOM - Ajout d'une latence intrinsèque au proxy
* \[[PROXY-696](https://gazelle.ihe.net/jira/browse/PROXY-696)\] DICOM - Absence d'affichage du body de la réponse RAD-69
* \[[PROXY-697](https://gazelle.ihe.net/jira/browse/PROXY-697)\] Affichage d'un message d'erreur lors de l'utilisation de la fonctionnalité "copy URL"
* \[[PROXY-698](https://gazelle.ihe.net/jira/browse/PROXY-698)\] HL7v2 - Parsing non fonctionnel parsing lorsque le séparateur de champs diffère du pipe « | »
* \[[PROXY-699](https://gazelle.ihe.net/jira/browse/PROXY-699)\] Unexpected message in 100-continue connection
* \[[PROXY-700](https://gazelle.ihe.net/jira/browse/PROXY-700)\] HTTP CONNECT Method not supported
* \[[PROXY-702](https://gazelle.ihe.net/jira/browse/PROXY-702)\] Memory leak after separating request/response threads
* \[[PROXY-703](https://gazelle.ihe.net/jira/browse/PROXY-703)\] HTTP - BodySize not set without content-length for HTTP Response


# V17-1.0.2
_Release date: 2024-12-13_


__Bug__
* \[[PROXY-513](https://gazelle.ihe.net/jira/browse/PROXY-513)\] Memory leak
* \[[PROXY-514](https://gazelle.ihe.net/jira/browse/PROXY-514)\] pancanadianio.ihe-catalyst proxy showing incorrect Content Length
* \[[PROXY-515](https://gazelle.ihe.net/jira/browse/PROXY-515)\] decode error with pancanadianio on proxy engine 1.0.1
* \[[PROXY-516](https://gazelle.ihe.net/jira/browse/PROXY-516)\] Host header in HTTP Rewrite is missing the port number
* \[[PROXY-518](https://gazelle.ihe.net/jira/browse/PROXY-518)\] Recording of HTTP messages is blocked in case of wrong HTTP Frame
* \[[PROXY-521](https://gazelle.ihe.net/jira/browse/PROXY-521)\] HL7v2 frame boundaries does not split frames
* \[[PROXY-522](https://gazelle.ihe.net/jira/browse/PROXY-522)\] HTTP Response is missing the reason phrase
* \[[PROXY-524](https://gazelle.ihe.net/jira/browse/PROXY-524)\] HEAD Requests are not well captured


# 6.3.0
_Release date: 2024-11-12_

__Story__
* \[[PROXY-470](https://gazelle.ihe.net/jira/browse/PROXY-470)\] Recorder should record dataSet of dicom message as attachements on datahouse

__Task__
* \[[PROXY-450](https://gazelle.ihe.net/jira/browse/PROXY-450)\] Remove message list view from v7
* \[[PROXY-452](https://gazelle.ihe.net/jira/browse/PROXY-452)\] Multipart - Messages
* \[[PROXY-453](https://gazelle.ihe.net/jira/browse/PROXY-453)\] Disable Fallback capture on V7
* \[[PROXY-468](https://gazelle.ihe.net/jira/browse/PROXY-468)\] add missing sql migration to add datahouse-url in proxy-v7 preferences
* \[[PROXY-472](https://gazelle.ihe.net/jira/browse/PROXY-472)\] Support http multipart
* \[[PROXY-476](https://gazelle.ihe.net/jira/browse/PROXY-476)\] Fix auto channels closing mecanism
* \[[PROXY-483](https://gazelle.ihe.net/jira/browse/PROXY-483)\] Improve part visualisation
* \[[PROXY-499](https://gazelle.ihe.net/jira/browse/PROXY-499)\] The date filter doesn't work

__Bug__
* \[[PROXY-471](https://gazelle.ihe.net/jira/browse/PROXY-471)\] MLLP frame boundaries are wrong
* \[[PROXY-473](https://gazelle.ihe.net/jira/browse/PROXY-473)\] capture of message on the proxy IHEUSA is KO
* \[[PROXY-478](https://gazelle.ihe.net/jira/browse/PROXY-478)\] conf filter sender_port
* \[[PROXY-479](https://gazelle.ihe.net/jira/browse/PROXY-479)\] Could not parse chunked HTTP
* \[[PROXY-481](https://gazelle.ihe.net/jira/browse/PROXY-481)\] Proxy provide certificate in client mode out of mTLS context
* \[[PROXY-485](https://gazelle.ihe.net/jira/browse/PROXY-485)\] Persistance channels does not work as excepted with restart of application
* \[[PROXY-486](https://gazelle.ihe.net/jira/browse/PROXY-486)\] Latency on forwarding DICOM Web on TLS
* \[[PROXY-487](https://gazelle.ihe.net/jira/browse/PROXY-487)\] difference behavior between TLS message or not, with DICOM Web
* \[[PROXY-488](https://gazelle.ihe.net/jira/browse/PROXY-488)\] Proxy doesn't capture the response of a multipart - Dicom Web
* \[[PROXY-497](https://gazelle.ihe.net/jira/browse/PROXY-497)\] DICOM file Downloaded from Datahouse is not viewable
* \[[PROXY-498](https://gazelle.ihe.net/jira/browse/PROXY-498)\] Special Char on HL7V2 Content on Chrome and Edge
* \[[PROXY-501](https://gazelle.ihe.net/jira/browse/PROXY-501)\] New Line added in HL7v2 and HTTP non-prettify XML Content
* \[[PROXY-504](https://gazelle.ihe.net/jira/browse/PROXY-504)\] URL not found when user does not login to Proxy and click on Message List
* \[[PROXY-505](https://gazelle.ihe.net/jira/browse/PROXY-505)\] Gzip multipart is not decoded
* \[[PROXY-506](https://gazelle.ihe.net/jira/browse/PROXY-506)\]  Validation Tab should not be hidden when Validation is deactivated in Datahouse
* \[[PROXY-507](https://gazelle.ihe.net/jira/browse/PROXY-507)\] Download HTTP Content  - The body is not decompressed


__Improvement__
* \[[PROXY-477](https://gazelle.ihe.net/jira/browse/PROXY-477)\] Record connection errors as separate items

# 6.2.5
_Release date: 2024-09-02_

__Bug__
* \[[PROXY-381](https://gazelle.ihe.net/jira/browse/PROXY-381)\] Replace HL7 by HL7v2
* \[[PROXY-395](https://gazelle.ihe.net/jira/browse/PROXY-395)\] Decoding error: the content displayed is quite weird
* \[[PROXY-397](https://gazelle.ihe.net/jira/browse/PROXY-397)\] Arrow  between Initiator and Responder
* \[[PROXY-404](https://gazelle.ihe.net/jira/browse/PROXY-404)\] Memory leaks on proxy-v17
* \[[PROXY-419](https://gazelle.ihe.net/jira/browse/PROXY-419)\] Calendar filters lack rules

__Task__
* \[[PROXY-414](https://gazelle.ihe.net/jira/browse/PROXY-414)\] Decode Dicom dump in the backend
* \[[PROXY-418](https://gazelle.ihe.net/jira/browse/PROXY-418)\] Hl7v2 XML renderer
* \[[PROXY-421](https://gazelle.ihe.net/jira/browse/PROXY-421)\] Proxy decoders should save original message
* \[[PROXY-422](https://gazelle.ihe.net/jira/browse/PROXY-422)\] Make it easier to read the rows of a table
* \[[PROXY-423](https://gazelle.ihe.net/jira/browse/PROXY-423)\] Auto indexes verification
* \[[PROXY-424](https://gazelle.ihe.net/jira/browse/PROXY-424)\] ID for Automation tests
* \[[PROXY-425](https://gazelle.ihe.net/jira/browse/PROXY-425)\] Remove autoscroll
* \[[PROXY-426](https://gazelle.ihe.net/jira/browse/PROXY-426)\] List of messages - 1 line to display timestamp
* \[[PROXY-427](https://gazelle.ihe.net/jira/browse/PROXY-427)\] calendar not fully visible
* \[[PROXY-428](https://gazelle.ihe.net/jira/browse/PROXY-428)\] Calendar - Add a quick picker
* \[[PROXY-449](https://gazelle.ihe.net/jira/browse/PROXY-449)\] Download raw
* \[[PROXY-454](https://gazelle.ihe.net/jira/browse/PROXY-454)\] Replace BETA by NEW

__Improvement__
* \[[PROXY-405](https://gazelle.ihe.net/jira/browse/PROXY-405)\] Persist raw content alongside with decoded message

# 6.2.4
_Release date: 2024-06-25_

__Improvement__
* \[[PROXY-407](https://gazelle.ihe.net/jira/browse/PROXY-407)\] Upgrade sso-client-v7 library to 4.1.1

# 6.2.2
_Release date: 2024-05-30_

__Bug__

* \[[PROXY-417](https://gazelle.ihe.net/jira/browse/PROXY-417)\] Create HTTP Rewrite channel fails from TM

# 6.2.1
_Release date: 2024-05-29_

__Bug__
* \[[PROXY-416](https://gazelle.ihe.net/jira/browse/PROXY-416)\] Channels creation from TM is not Fault Tolerent

# 6.2.0
_Release date: 2024-05-28_

__Improvement__
* \[[PROXY-335](https://gazelle.ihe.net/jira/browse/PROXY-335)\] Remove irrelevant fields from HTTP Response structure

__Bug__
* \[[PROXY-328](https://gazelle.ihe.net/jira/browse/PROXY-328)\] HTTP rewrite X-Forwarded-Host and X-Forwarded-Port values are wrong

# 6.1.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[PROXY-308](https://gazelle.ihe.net/jira/browse/PROXY-308)\] Integrate new sso-client-v7

# 6.0.0
_Release date: 2024-01-18

Step 1 of Proxy renovation.

__Story__

* \[[PROXY-274](https://gazelle.ihe.net/jira/browse/PROXY-274)\] Create and start channels from TM
* \[[PROXY-275](https://gazelle.ihe.net/jira/browse/PROXY-275)\] Create/start one TCP channel from Proxy UI.
* \[[PROXY-276](https://gazelle.ihe.net/jira/browse/PROXY-276)\] Create start of several channels from an uploaded CSV in proxy UI.
* \[[PROXY-277](https://gazelle.ihe.net/jira/browse/PROXY-277)\] Support DICOM Channel
* \[[PROXY-278](https://gazelle.ihe.net/jira/browse/PROXY-278)\] Support HL7v2 Channel
* \[[PROXY-279](https://gazelle.ihe.net/jira/browse/PROXY-279)\] Support TCP SYSLOG Channel
* \[[PROXY-281](https://gazelle.ihe.net/jira/browse/PROXY-281)\] Support HTTP Channel
* \[[PROXY-286](https://gazelle.ihe.net/jira/browse/PROXY-286)\] Support secured channels
* \[[PROXY-287](https://gazelle.ihe.net/jira/browse/PROXY-287)\] Support TLS 1.3 for secured channels
* \[[PROXY-288](https://gazelle.ihe.net/jira/browse/PROXY-288)\] HTTP Rewrite
* \[[PROXY-289](https://gazelle.ihe.net/jira/browse/PROXY-289)\] Support SNI for secured channels
* \[[PROXY-291](https://gazelle.ihe.net/jira/browse/PROXY-291)\] Stop channel individually from UI
* \[[PROXY-292](https://gazelle.ihe.net/jira/browse/PROXY-292)\] Stop All channel from UI
* \[[PROXY-293](https://gazelle.ihe.net/jira/browse/PROXY-293)\] Record messages grouped in connections
* \[[PROXY-294](https://gazelle.ihe.net/jira/browse/PROXY-294)\] Record TLS handshake information
* \[[PROXY-307](https://gazelle.ihe.net/jira/browse/PROXY-307)\] Improve channels save/upload format to JSON

__Bug__

* \[[PROXY-312](https://gazelle.ihe.net/jira/browse/PROXY-312)\] Wrong Responder IP
* \[[PROXY-313](https://gazelle.ihe.net/jira/browse/PROXY-313)\] Bad DICOM dump
* \[[PROXY-314](https://gazelle.ihe.net/jira/browse/PROXY-314)\] HL7v2 unclear decoding error message
* \[[PROXY-315](https://gazelle.ihe.net/jira/browse/PROXY-315)\] Incoherent Cipher-Suites list in proxy-v7
* \[[PROXY-316](https://gazelle.ihe.net/jira/browse/PROXY-316)\] HTTP Wrong decoding error message
* \[[PROXY-317](https://gazelle.ihe.net/jira/browse/PROXY-317)\] Channels filters not working
* \[[PROXY-318](https://gazelle.ihe.net/jira/browse/PROXY-318)\] Empty IP is allowed in channels upload
* \[[PROXY-319](https://gazelle.ihe.net/jira/browse/PROXY-319)\] Unclear message when saving secured config
* \[[PROXY-320](https://gazelle.ihe.net/jira/browse/PROXY-320)\] Unclear TLS error message
* \[[PROXY-321](https://gazelle.ihe.net/jira/browse/PROXY-321)\] Raw message display text not working
* \[[PROXY-322](https://gazelle.ihe.net/jira/browse/PROXY-322)\] Remove All not working in secured channels
* \[[PROXY-324](https://gazelle.ihe.net/jira/browse/PROXY-324)\] Go to the last page not working
* \[[PROXY-325](https://gazelle.ihe.net/jira/browse/PROXY-325)\] Share message icon not working
* \[[PROXY-326](https://gazelle.ihe.net/jira/browse/PROXY-326)\] Copy button not working
* \[[PROXY-327](https://gazelle.ihe.net/jira/browse/PROXY-327)\] Proxy opens too many SQL Connections


__Task__
* \[[PROXY-280](https://gazelle.ihe.net/jira/browse/PROXY-280)\] Support TCP Raw Channel
* \[[PROXY-284](https://gazelle.ihe.net/jira/browse/PROXY-284)\] Native image
* \[[PROXY-285](https://gazelle.ihe.net/jira/browse/PROXY-285)\] Proxy Channel Performence Testing
* \[[PROXY-295](https://gazelle.ihe.net/jira/browse/PROXY-295)\] Pipeline handlers extension
* \[[PROXY-296](https://gazelle.ihe.net/jira/browse/PROXY-296)\] Proxy J7 adaptation implementation
* \[[PROXY-297](https://gazelle.ihe.net/jira/browse/PROXY-297)\] proxy service sync with channel socket service
* \[[PROXY-298](https://gazelle.ihe.net/jira/browse/PROXY-298)\] Custom pipeline fork
* \[[PROXY-311](https://gazelle.ihe.net/jira/browse/PROXY-311)\] System Tests Proxy V17

# 5.1.1
_Release date: 2023-12-13_

__Bug__
* \[[PROXY-302](https://gazelle.ihe.net/jira/browse/PROXY-302)\] Cannot log with ipLogin

__Story__
* \[[PROXY-303](https://gazelle.ihe.net/jira/browse/PROXY-303)\] Dump Dicom to xml in WADO-RS response

# 5.1.0
_Release date: 2023-07-11_

Step 1 of Gazelle User Management renovation.

__Task__
* \[[PROXY-268](https://gazelle.ihe.net/jira/browse/PROXY-268)\] [GUM] Cas-client-v7 update integration

# 5.0.8
_Release date: 2023-06-09_

__Bug__
* \[[PROXY-270](https://gazelle.ihe.net/jira/browse/PROXY-270)\] Missing some French translation in Gazelle Proxy

__Improvement__
* \[[PROXY-263](https://gazelle.ihe.net/jira/browse/PROXY-263)\] The proxy must be able to include the HTTP Header when sending the message to EVSClient for validation
* \[[PROXY-264](https://gazelle.ihe.net/jira/browse/PROXY-264)\] The proxy must be able to persit some channel

# 5.0.7
_Release date: 2023-04-17_

__Bug__
* \[[PROXY-262](https://gazelle.ihe.net/jira/browse/PROXY-262)\] Fix TOS popup

# 5.0.6
_Release date: 2023-02-02_

__Bug__
* \[[PROXY-262](https://gazelle.ihe.net/jira/browse/PROXY-262)\] Fix TOS popup

# 5.0.5
_Release date: 2023-01-24_

__Bug__
* \[[PROXY-261](https://gazelle.ihe.net/jira/browse/PROXY-261)\] NPE occurs when sending messages without mutual authentication

# 5.0.4
_Release date: 2022-04-01_

__Bug__
* \[[PROXY-259](https://gazelle.ihe.net/jira/browse/PROXY-259)\] Error message about wrong message format

# 5.0.3
_Release date: 2022-01-19_

__Story__
* \[[PROXY-255](https://gazelle.ihe.net/jira/browse/PROXY-255 )\] GDPR Refacto banner in standalone library

# 5.0.2
_Release date: 2021-07-19_

__Bug__
* \[[PROXY-254](https://gazelle.ihe.net/jira/browse/PROXY-254)\] Base64 Injection of forbidden filePAth value in Query Param of PEOXY is not protected

# 5.0.1
_Release date: 2021-06-11_

__Bug__
* \[[EUCAT-38](https://gazelle.ihe.net/jira/browse/EUCAT-38)\] Proxy should rather trust the issuer rather than each single certificate

# 5.0.0
_Release date: 2021-06-01 : Major version for TLS support_

__Improvement__
* \[[PROXY-216](https://gazelle.ihe.net/jira/browse/PROXY-216)\] Create secured channel 
* \[[PROXY-217](https://gazelle.ihe.net/jira/browse/PROXY-217)\] Manage TLS protocol versions 
* \[[PROXY-218](https://gazelle.ihe.net/jira/browse/PROXY-218)\] Manage cipher-suites 
* \[[PROXY-219](https://gazelle.ihe.net/jira/browse/PROXY-219)\] Display cipher-suites compatibility with TLS protocols 
* \[[PROXY-225](https://gazelle.ihe.net/jira/browse/PROXY-225)\] Manage channel Truststore V1 (JVM) 
* \[[PROXY-226](https://gazelle.ihe.net/jira/browse/PROXY-226)\] Enable/disable mutual authentication 
* \[[PROXY-227](https://gazelle.ihe.net/jira/browse/PROXY-227)\] Download channels backup (CSV file) 
* \[[PROXY-228](https://gazelle.ihe.net/jira/browse/PROXY-228)\] Upload channels backup (CSV file) 
* \[[PROXY-229](https://gazelle.ihe.net/jira/browse/PROXY-229)\] Filter secured or not secured messages 
* \[[PROXY-231](https://gazelle.ihe.net/jira/browse/PROXY-231)\] Secured channel details 
* \[[PROXY-232](https://gazelle.ihe.net/jira/browse/PROXY-232)\] Secured message details 
* \[[PROXY-233](https://gazelle.ihe.net/jira/browse/PROXY-233)\] Persistence of secured connections 
* \[[PROXY-234](https://gazelle.ihe.net/jira/browse/PROXY-234)\] Ease secured channels visibility 
* \[[PROXY-237](https://gazelle.ihe.net/jira/browse/PROXY-237)\] Manage server keystore V1 (JVM) 

# 4.7.1
_Release date: 2020-11-12_

__Bug__
* \[[PROXY-214](https://gazelle.ihe.net/jira/browse/PROXY-214)\] Add a preference to manage http content size for the proxy


# 4.7.0
_Release date: 2019-03-26_

__Remarks__

Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.


__Improvement__

* \[[PROXY-207](https://gazelle.ihe.net/jira/browse/PROXY-207)\] Extract datasource configuration
* \[[PROXY-208](https://gazelle.ihe.net/jira/browse/PROXY-208)\] Update SQL scripts archive

# 4.6.0
_Release date: 2019-03-05_

__Remarks__

Use of net.ihe.gazelle.maven:version:2.0.0 is limited to applications running on Jboss8.
This new version of the Gazelle Proxy has to be bound to new Gazelle SSO.


__Bug__

* \[[PROXY-205](https://gazelle.ihe.net/jira/browse/PROXY-205)\] Missing translation

__Improvement__

* \[[PROXY-211](https://gazelle.ihe.net/jira/browse/PROXY-211)\] Create "Admin only" mode for message list

# 4.5.0
_Release date: 2018-03-08_

__Remarks__

Use of net.ihe.gazelle.maven:version:2.0.0 is limited to applications running on Jboss8.
This new version of the Gazelle Proxy has to be bound to new Gazelle SSO.

__Bug__

* \[[PROXY-183](https://gazelle.ihe.net/jira/browse/PROXY-183)\] Error when deploying proxy, jdbc driver

__Improvement__

* \[[PROXY-186](https://gazelle.ihe.net/jira/browse/PROXY-186)\] Use new CAS SSO

# 4.4.3
_Release date: 2017-07-05_

__Remarks__


__Improvement__

* \[[PROXY-182](https://gazelle.ihe.net/jira/browse/PROXY-182)\] Add soap ui project as unit test + release 4.4.3

# 4.4.2
_Release date: 2017-03-16_

__Bug__

* \[[PROXY-163](https://gazelle.ihe.net/jira/browse/PROXY-163)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm
* \[[PROXY-166](https://gazelle.ihe.net/jira/browse/PROXY-166)\] Some proxy link contain IP instead of url

__Story__

* \[[PROXY-132](https://gazelle.ihe.net/jira/browse/PROXY-132)\] Proxy should not open channels if it misses the remote host and/or port

__Improvement__

* \[[PROXY-165](https://gazelle.ihe.net/jira/browse/PROXY-165)\] Need to catch errors in logs
* \[[PROXY-170](https://gazelle.ihe.net/jira/browse/PROXY-170)\] Update Entity Manager with provider

# 4.4.0
_Release date: 2016-05-02_

__Bug__

* \[[PROXY-145](https://gazelle.ihe.net/jira/browse/PROXY-145)\] ihej-dicom library code clean up based on static analysis

__Epic__

* \[[PROXY-139](https://gazelle.ihe.net/jira/browse/PROXY-139)\] Update style with gazelle-assets

__Story__

* \[[PROXY-140](https://gazelle.ihe.net/jira/browse/PROXY-140)\] Management
* \[[PROXY-146](https://gazelle.ihe.net/jira/browse/PROXY-146)\] Merge proxy new style branch on trunk
* \[[PROXY-147](https://gazelle.ihe.net/jira/browse/PROXY-147)\] Sonar analysis
* \[[PROXY-148](https://gazelle.ihe.net/jira/browse/PROXY-148)\] Release Proxy 4.4.0


# 4.3.3-NEWSTYLE
_Release date: 2016-04-05_


__Bug__

* \[[PROXY-126](https://gazelle.ihe.net/jira/browse/PROXY-126)\] need to escape content of HL7v2 messages
* \[[PROXY-130](https://gazelle.ihe.net/jira/browse/PROXY-130)\] prettyFormat Transformation fails in java7
* \[[PROXY-137](https://gazelle.ihe.net/jira/browse/PROXY-137)\] Missing transfer syntax in message details

__Story__

* \[[PROXY-143](https://gazelle.ihe.net/jira/browse/PROXY-143)\] Use the appropriate version of postgresql driver in proxy
* \[[PROXY-144](https://gazelle.ihe.net/jira/browse/PROXY-144)\] Release 4.3.3-NEWSTYLE

__Improvement__

* \[[PROXY-134](https://gazelle.ihe.net/jira/browse/PROXY-134)\] Add button to download messages exchanged in a connection

# 4.3.2-NEWSTYLE
_Release date: 2016-01-21_

__Bug__

* \[[PROXY-129](https://gazelle.ihe.net/jira/browse/PROXY-129)\] Copy to clipboard button does not work any more.

__Story__

* \[[PROXY-128](https://gazelle.ihe.net/jira/browse/PROXY-128)\] migrate proxy-common-war to new assets style for GSS

# 4.3.1
_Release date: 2015-10-30_


__Bug__

* \[[PROXY-125](https://gazelle.ihe.net/jira/browse/PROXY-125)\] Home page : Cancel() method of HomeManager does not cancel modifications.

# 4.3.0
_Release date: 2015-06-11_

__Bug__

* \[[PROXY-107](https://gazelle.ihe.net/jira/browse/PROXY-107)\] Sometime admin can't click on open a new channel and start a new channel
* \[[PROXY-114](https://gazelle.ihe.net/jira/browse/PROXY-114)\] Some messages are probably not stored/displayed
* \[[PROXY-115](https://gazelle.ihe.net/jira/browse/PROXY-115)\] Can't delete validation result
* \[[PROXY-116](https://gazelle.ihe.net/jira/browse/PROXY-116)\] Start all channels doesn't work well
* \[[PROXY-119](https://gazelle.ihe.net/jira/browse/PROXY-119)\] Message content not displayed in some cases
* \[[PROXY-120](https://gazelle.ihe.net/jira/browse/PROXY-120)\] Sonar analyses

__Improvement__

* \[[PROXY-118](https://gazelle.ihe.net/jira/browse/PROXY-118)\] Add permanent link in list message
* \[[PROXY-121](https://gazelle.ihe.net/jira/browse/PROXY-121)\] Remake proxy menu with bootstrap
* \[[PROXY-122](https://gazelle.ihe.net/jira/browse/PROXY-122)\] Format xml in details message

# 4.2.0
_Release date: 2015-04-13_


__Bug__

* \[[PROXY-101](https://gazelle.ihe.net/jira/browse/PROXY-101)\] Error in message list with empty DB
* \[[PROXY-109](https://gazelle.ihe.net/jira/browse/PROXY-109)\] View XXX.seam could not be restored.
* \[[PROXY-111](https://gazelle.ihe.net/jira/browse/PROXY-111)\] Trouble with the input number spinner in message details

__Improvement__

* \[[PROXY-112](https://gazelle.ihe.net/jira/browse/PROXY-112)\] Create filter on channel list page

# 4.1.3
_Release date: 2015-01-26_


__Bug__

* \[[PROXY-104](https://gazelle.ihe.net/jira/browse/PROXY-104)\] Sort channel list by Remote address

# 4.1.2
_Release date: 2015-01-25_


__Bug__

* \[[PROXY-103](https://gazelle.ihe.net/jira/browse/PROXY-103)\] In Channel list details button doesn't work

# 4.1.1
_Release date: 2015-01-20_

__Bug__

* \[[PROXY-99](https://gazelle.ihe.net/jira/browse/PROXY-99)\] Filter by last month doesn't work well
* \[[PROXY-100](https://gazelle.ihe.net/jira/browse/PROXY-100)\] HL7 Message Type filter is not populated

# 4.1.0
_Release date: 2015-01-20_


__Bug__

* \[[PROXY-95](https://gazelle.ihe.net/jira/browse/PROXY-95)\] Update Dicom file location
* \[[PROXY-98](https://gazelle.ihe.net/jira/browse/PROXY-98)\] CLONE - Variable in application configuration should have a unique name

__Improvement__

* \[[PROXY-58](https://gazelle.ihe.net/jira/browse/PROXY-58)\] NA2015: make HL7v2 message type visible in first column of Message List
* \[[PROXY-85](https://gazelle.ihe.net/jira/browse/PROXY-85)\] Improve postgresql request time
* \[[PROXY-88](https://gazelle.ihe.net/jira/browse/PROXY-88)\] When the proxy captures binary data, provide an hex dump of the data through the GUI.
* \[[PROXY-89](https://gazelle.ihe.net/jira/browse/PROXY-89)\] Add argument in filters in order to query without distinct for counts
* \[[PROXY-92](https://gazelle.ihe.net/jira/browse/PROXY-92)\] In the table list of messages there is an info column. I need to be able to search or sort that column
* \[[PROXY-96](https://gazelle.ihe.net/jira/browse/PROXY-96)\] Add a dump for DICOM messages in details page

# 4.0.0
_Release date: 2014-12-11_


__Improvement__

* \[[PROXY-94](https://gazelle.ihe.net/jira/browse/PROXY-94)\] Merge Jboss 7 branche on trunk

# 3.3.4
_Release date: 2015-01-19_

__Bug__

* \[[PROXY-98](https://gazelle.ihe.net/jira/browse/PROXY-98)\] CLONE - Variable in application configuration should have a unique name

# 3.3.3
_Release date: 2014-12-02_

__Bug__

* \[[PROXY-93](https://gazelle.ihe.net/jira/browse/PROXY-93)\] Exception Could not instantiate Seam component: proxyMessageSender

# 3.3.2
_Release date: 2014-11-03_


__Bug__

* \[[PROXY-91](https://gazelle.ihe.net/jira/browse/PROXY-91)\] Downgrade Pixelmed to 20140128

# 3.3.1
_Release date: 2014-11-03_


__Story__

* \[[PROXY-90](https://gazelle.ihe.net/jira/browse/PROXY-90)\] Update Pixelmed version

# 3.3.0
_Release date: 2014-10-10_


__Bug__

* \[[PROXY-86](https://gazelle.ihe.net/jira/browse/PROXY-86)\] Move "message type" in new form in proxy form

__Improvement__

* \[[PROXY-87](https://gazelle.ihe.net/jira/browse/PROXY-87)\] extract the methods used to communicate with EVSClient

# 3.2.0
_Release date: 2014-07-24_

__Bug__

* \[[PROXY-79](https://gazelle.ihe.net/jira/browse/PROXY-79)\] error message in JBoss log is not appropriate when a DICOM file is missing
* \[[PROXY-80](https://gazelle.ihe.net/jira/browse/PROXY-80)\] The first time you access the proxy user can't go on "Message List" because it's empty

__Improvement__

* \[[PROXY-82](https://gazelle.ihe.net/jira/browse/PROXY-82)\] Add interoperability-testbed feature

# 3.1.1
_Release date: 2014-04-29_

__Bug__

* \[[PROXY-77](https://gazelle.ihe.net/jira/browse/PROXY-77)\] Download of Dicom file and view dumped dicom files sometimes show a different file that the one expected
* \[[PROXY-78](https://gazelle.ihe.net/jira/browse/PROXY-78)\] Remake search panel

# 3.1.0
_Release date: 2014-03-26_

__Bug__

* \[[PROXY-67](https://gazelle.ihe.net/jira/browse/PROXY-67)\] Sort "DICOM Affected Sop Class UID" field by name
* \[[PROXY-72](https://gazelle.ihe.net/jira/browse/PROXY-72)\] If DCMTK is not install no error message is provided
* \[[PROXY-73](https://gazelle.ihe.net/jira/browse/PROXY-73)\] On message list datascroller is not updated

__Story__

* \[[PROXY-71](https://gazelle.ihe.net/jira/browse/PROXY-71)\] Update pixelmed version to 20140128
* \[[PROXY-76](https://gazelle.ihe.net/jira/browse/PROXY-76)\] Release for M3

__Improvement__

* \[[PROXY-68](https://gazelle.ihe.net/jira/browse/PROXY-68)\] Add possibility to filter DICOM by SOP Class UID
* \[[PROXY-74](https://gazelle.ihe.net/jira/browse/PROXY-74)\] Optimize proxy
* \[[PROXY-75](https://gazelle.ihe.net/jira/browse/PROXY-75)\] Create a WebService which return version number

# 3.0.0
_Release date: 2013-11-05_

__Bug__

* \[[PROXY-62](https://gazelle.ihe.net/jira/browse/PROXY-62)\] Help button is a broken link
* \[[PROXY-63](https://gazelle.ihe.net/jira/browse/PROXY-63)\] Number of open channels is not totally correct

__Improvement__

* \[[PROXY-64](https://gazelle.ihe.net/jira/browse/PROXY-64)\] Use application preferences to store the CAS url

# Proxy-2.16
_Release date: 2013-10-24_


__Improvement__

* \[[PROXY-59](https://gazelle.ihe.net/jira/browse/PROXY-59)\] ERROR [GenericServiceLoader] Failed to load an implementation for the service net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences !

# Proxy-2.14
_Release date: 2013-10-23_


__Bug__

* \[[PROXY-53](https://gazelle.ihe.net/jira/browse/PROXY-53)\] When starting a channel using the WS call, one can start a channel out of the configuration range
* \[[PROXY-60](https://gazelle.ihe.net/jira/browse/PROXY-60)\] NullPointerException when proxy use last version of gazelle-seams-tools
* \[[PROXY-61](https://gazelle.ihe.net/jira/browse/PROXY-61)\] Web service definition has changed, need to update gazelle-ws-clients using the new wsdl

# Proxy-2.13
_Release date: 2013-10-17_

__Bug__

* \[[PROXY-57](https://gazelle.ihe.net/jira/browse/PROXY-57)\] Problem at channel creation

# Proxy-2.12
_Release date: 2013-10-15_

__Bug__

* \[[PROXY-55](https://gazelle.ihe.net/jira/browse/PROXY-55)\] Error when stopping all channels

__Improvement__

* \[[PROXY-50](https://gazelle.ihe.net/jira/browse/PROXY-50)\] Update pom.xml to have link in Sonar
* \[[PROXY-51](https://gazelle.ihe.net/jira/browse/PROXY-51)\] Uniquely identify an instance of the proxy tool

# Proxy-2.11
_Release date: 2013-07-29_


__Bug__

* \[[PROXY-48](https://gazelle.ihe.net/jira/browse/PROXY-48)\] Can't validate DICOM from proxy to TM

__Story__

* \[[PROXY-47](https://gazelle.ihe.net/jira/browse/PROXY-47)\] Installation, configuration and documentation for NIST

# Proxy-2.10
_Release date: 2013-07-23_


__Story__

* \[[PROXY-24](https://gazelle.ihe.net/jira/browse/PROXY-24)\] Work on DICOM proxy side
* \[[PROXY-45](https://gazelle.ihe.net/jira/browse/PROXY-45)\] Add a mechanism to export/import channels configuration

# Proxy-2.9
_Release date: 2013-07-08_

__Technical task__

* \[[PROXY-43](https://gazelle.ihe.net/jira/browse/PROXY-43)\] add missing translation

__Bug__

* \[[PROXY-44](https://gazelle.ihe.net/jira/browse/PROXY-44)\] An error occurred when sending a message to EVSClient for validation

__Improvement__

* \[[PROXY-41](https://gazelle.ihe.net/jira/browse/PROXY-41)\] Improve GUI layout to match the graphic templates of Gazelle

# Proxy-2.8
_Release date: 2013-07-08_

__Story__

* \[[PROXY-40](https://gazelle.ihe.net/jira/browse/PROXY-40)\] Allow the application to work without cas

__Improvement__

* \[[PROXY-42](https://gazelle.ihe.net/jira/browse/PROXY-42)\] store application preferences in a table instead of using a property file

# Proxy-2.7
_Release date: 2013-06-26_


__Improvement__

* \[[PROXY-39](https://gazelle.ihe.net/jira/browse/PROXY-39)\] Proxy GUI Layout is not conform to our common layout

# Proxy-2.6
_Release date: 2013-06-14_


__Bug__

* \[[PROXY-38](https://gazelle.ihe.net/jira/browse/PROXY-38)\] Captured DICOM dataset may have duplicate names.

__Improvement__

* \[[PROXY-36](https://gazelle.ihe.net/jira/browse/PROXY-36)\] Add possibilies to filter by ip or proxy in proxy channels

# Proxy-2.3
_Release date: 2013-04-12_


__Bug__

* \[[PROXY-34](https://gazelle.ihe.net/jira/browse/PROXY-34)\] View inline for dicom
* \[[PROXY-35](https://gazelle.ihe.net/jira/browse/PROXY-35)\] SearchMessageStep : origine, destination and date to check

__Improvement__

* \[[PROXY-33](https://gazelle.ihe.net/jira/browse/PROXY-33)\] Add stop all channels button

# Proxy-2.2
_Release date: 2013-03-18_

__Improvement__

* \[[PROXY-32](https://gazelle.ihe.net/jira/browse/PROXY-32)\] Update proxy page from TM

# Proxy-2.1
_Release date: 2013-03-14_


__Story__

* \[[PROXY-28](https://gazelle.ihe.net/jira/browse/PROXY-28)\] Proxy url paramaters

# Proxy-2.0
_Release date: 2013-03-07_


__Story__

* \[[PROXY-29](https://gazelle.ihe.net/jira/browse/PROXY-29)\] Create url to see result validation.

# Proxy-0.2-GA
_Release date: 2012-12-30_

__Story__

* \[[PROXY-6](https://gazelle.ihe.net/jira/browse/PROXY-6)\] PROXY - Module for TCIP communication proxying
* \[[PROXY-23](https://gazelle.ihe.net/jira/browse/PROXY-23)\] Use new framework tooling

# Proxy-0.1-GA
_Release date: 2010-04-05_

__Story__

* \[[PROXY-1](https://gazelle.ihe.net/jira/browse/PROXY-1)\] PROXY - Specialisation of the TCP/IP proxy feature for HL7 V2 message proxying
* \[[PROXY-2](https://gazelle.ihe.net/jira/browse/PROXY-2)\] PROXY - Create API between the proxy and the Gazelle Control Module 
* \[[PROXY-4](https://gazelle.ihe.net/jira/browse/PROXY-4)\] PROXY - Specialisation of the TCP/IP proxy feature for Dicom message proxying
* \[[PROXY-5](https://gazelle.ihe.net/jira/browse/PROXY-5)\] PROXY - Specialisation of the TCP/IP proxy feature for Syslog message proxying
