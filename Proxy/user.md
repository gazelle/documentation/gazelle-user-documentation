---
title:  User Manual
subtitle: Gazelle Proxy
author: Nicolas BAILLIET
date: 04/10/2024
toolversion: 6.X.X
function: Engineer
version: 2.01
status: Approved document
reference: KER1-MAN-IHE-PROXY_USER-2_01
customer: IHE-EUROPE
---

# Project Overview

The proxy is used to capture TCP/IP packets exchanged by test participants using secured or not secured channel. 
The packed flow is analyzed and stored in a database for further analysis by protocol specific analysers.

The packet analysers available are :

* HTTP  
* DICOM  
* HL7V2  
* Syslog  
* Raw  

Each message is saved with the network details, including an id of the socket (named channel id) used for that message 
as a socket can transport many messages (HTTP, DICOM).

**The proxy is set up on ovh1.ihe-europe.net, and accessed with [*the web interface*](https://gazelle.ihe.net/proxy). 
ovh1.ihe-europe.net has a limited range of port numbers available from the Internet. Ports from 10200 to 11000 must be used for channel creation.**

## Usage

The web interface allows to create channels. A channel opens a port on the server hosting the proxy and redirecting all 
traffic to a configured server on a specific port.

Data stream is not modified, but analyzed using the chosen packet analyser.

## Channel List

This page displays the list of current running channels. A channel can be deleted if password is known.

## New channel

It allows to create a new channel if password is known. All fields are required.

## Messages list

A grid displays all messages matching provided filter. Reset button sets all fields to default value.

Each row allows to display message details if id is clicked. Network details can also be clicked to define filter values.

For HTTP(S) messages, matching request/response is displayed in parenthesis.

# Gazelle integration

The proxy is integrated with Gazelle using web standards.

It publishes a web service allowing Gazelle to send test instance steps and configurations. Also, when a step is done, 
Gazelle calls the web service.

The proxy then opens the needed channels and listen on specified ports (provided in the system configurations). It also 
records the test instance chronology for further searches.

In Gazelle, if the test instance has proxy enabled, a link is available on each step. This link opens the proxy with the 
Gazelle step technical id as a parameter. The proxy then builds a filter to get messages matching the step and displays the matching messages.

## Introduction

Gazelle TestManagement tools can be used in conjunction with a proxy for the capture of the messages exchanged between a 
test participants.

The proxy is able to capture :

* HL7v2 messages
* Dicom Transactions
* Webservices messages
* Syslog messages

The advantages of using the proxy when running a test are the followings :

* the Proxy is a neutral way to capture the exchanged messages.
* the Proxy displays the captured messages in a unified way for all the tests performed, simplifying the work of the 
monitors when examining the logs
* the Proxy provides permanent link to the captured messages that can be linked to test instance steps and avoid the cut 
and paste of logs in the chat window of the test instance. It then helps linking the logs to the test and enables all the participants to the test to see the entire set of messages exchanges between the test participants.
* the Proxy helps to verify the captured messages through a direct link to the EVS Client GUI.

![HL7 Message interface with button to download, see or send to EVS for validation](./media/hl7_message.png)

## Limitations

Proxy acts as a network relay between two SUTs. As a result, system configuration has to be modified. The TCP connection 
must be established on the proxy on the system configuration's proxy port instead of opening a connection to the responder SUT directly.

## How does it work ?

For each system in Gazelle TestManagement tool there is a set of configuration parameters. For each port that an SUT needs 
to open, there is a mirror port number on the proxy.

All proxy ports must be opened by a Gazelle admin, each system configuration being mapped to a proxy port.

The proxy GUI can be access at the following URL : [*https://gazelle.ihe.net/proxy*](https://gazelle.ihe.net/proxy)

## Start a new channel

To start a new channel, you have to be connected as an administrator.

![](./media/channel_list_page.png)

The form displayed should be completed with message type, proxy's port, responder's IP or hostname and responder's port. 
The channel can be secure or not secure by activating the lock icon.

![](./media/open_new_channel.png)

Then, one the channel is started, it can be visible in the channel list page

![](./media/channel_list_page_with_channel.png)

For secured configuration details, you can go to Administration/Secured Channel Configuration (admin only) or click on 
the lock icon to be redirected in read only mode (any user)

![](./media/secured_channel_configuration_page.png)

NB : Update the configuration induce a restart of all previously opened channels.

## Synchronize with Channel Socket Service (Version >= 6.0.0)

As the low level socket management is now delegated to a dedicated service, the proxy needs to be synchronized with this
service to be able to open new channels. This synchronization is done automatically every 5 minutes, but can be done manually
by clicking on the "Synchronize with Channel Socket Service" button in the Channels List page.

![](./media/synchronize_with_channel_socket_service.png)


## Automated filtering

Proxy and Gazelle know each other, and each test step in Gazelle has a proxy link.

![](./media/test_instance.png)

This link displays the list of the messages matching the test step configuration. It also filters the messages by time, 
showing only messages sent after the last test step marked as verified (or test instance started) and this test step marked as to be verified.

![](./media/messages_from_TI.png)

## Finding captured messages manually

By accessing proxy directly using [*https://gazelle.ihe.net/proxy*](https://gazelle.ihe.net/proxy), messages can be 
filtered on different criterias. By clicking a value in the table, it either opens the message details for id column, 
or set the filter for other columns.

The messages list displays only one type of message, if HTTP is selected, HL7v2 messages are not shown.

Each captured message has a permanent link that can be used in Gazelle. The best way to use it is to add this link to a 
test step instance. The monitor will be then able to validate the message using EVSClient.
![](./media/secured_message_details.png)

## Persist a channel

**For Version >6.0.0**, gazelle-proxy migrate the persistent channels to a JSON format. The file is located in 
`/opt/proxy/proxyPersistentChannels.json` and is accessible by jboss user.

**For version >5.0.8**, gazelle-proxy can now have a persist feature that allowed user to persist created channel into csv 
file present in server.
To perform this feature it needs 2 steps :

1. create a file named `proxyPersistentChannels.csv` into `/opt/proxy/proxyPersistentChannels.csv` and make it accessible by jboss user

    ```bash
      cd /opt/proxy/
      sudo touch proxyPersistentChannels.csv
      sudo chmod -R 755 . && sudo chown -R jboss:jboss-admin . 
    ```

2. Add property into Administration > Configuration

  |           Preference name           |                 Value                  |
  |:-----------------------------------:|:--------------------------------------:|
  | proxy_persistent_channels_file_path | /opt/proxy/proxyPersistentChannels.csv |

Once done you can know persist your channel during creation of new channel by ticking the case `Make the channel persistent?`

![](./media/persist_new_channel.png)

The channel is now created with an `Unpersit` button.

![](./media/persist_channel_in_list.png)

The chanel is written in `/opt/proxy/proxyPersistentChannels.csv` (or `/opt/proxy/proxyPersistentChannels.json`) in this format :

```csv
"HTTP","true","10000","example.com","443","true"
```

The fields are in this strict order:  

```csv
"Type of Message","is Secured?","Proxy port","Responder Host","Responder port","is Persistent ?"
```


# WebService API

* *startAllChannels* : It takes "List&lt;Configuration&gt; configurations" in argument. It starts a new channel in proxy for each configuration set.

* *startTestInstance* : It takes "TestInstance testInstance" in argument. It starts a new channel in proxy for a test instance.

* *markTestStep* : It takes "int testStepId" in argument. It set the date of a test step with the current.

* *getMinProxyPort* : It returns the min\_proxy\_port define in proxy configuration.

* *getMaxProxyPort* : It returns the max\_proxy\_port define in proxy configuration.

# Admin Only Mode

The proxy can be configured to use the "Admin only" mode. This mode restrict the access to the message list and details to administrators only. 
Not logged in users or logged in users without admin rights will not be able to see any message if this mode is activated. 
Any captured message will have a private access in this mode by default. Id the message is captured with this mode disabled, 
when switching the mode on, the message will still be private by default.  
An admin user can however Share connection. This basically means that a privacy key is associated to the connection and 
that any user with any rights knowing this privacy key will be able to access messages from this connection.  
When this mode is enabled, the Message List is not accessible in the menu for not admin users :

![](./media/not_logged_in_admin_only.png)

When an Admin user access the message list with this mode enabled, he can see which message is shared and which message 
is private. He can also filter messages based on this criterion.

![](./media/message_list_admin.png)

Finally when on a message detail page, the admin user can shared the associated connection, or make it private.

![](./media/make_private.png)
![](./media/share_connection.png)

The current privacy state of the connection can be found as an icon on the top right corner of the Message details panel. 
For instance, the connection associated to the message in the following picture is private.

![](./media/message_details.png)

At any moment this mode can be enabled or disabled. Between two activations, connection will keep the same privacy status 
(shared or private). When the mode is disabled, all connection and all messages can be accessed by all users again. 
The fact that they are private in Admin only mode has no impact when the mode is disabled.
