SHELL := /bin/bash

DOCS = $(wildcard */)
OUTPUT_FOLDER = target
TEMPLATE_HTML_FOLDER = html-template
TEMPLATE_JEKYLL = _templates/jekyll-gazelle-documentation
DOCS := $(filter-out _templates/,$(DOCS))
DOCS := $(filter-out target/,$(DOCS))

# Répertoire source où se trouvent les fichiers Markdown
SOURCE_DIR := $(CURDIR)

# Récupère tous les fichiers Markdown récursivement dans SOURCE_DIR
MARKDOWN_FILES := $(shell find $(SOURCE_DIR) -type f -name "*.md")

# Génère la liste des répertoires uniques où se trouvent les fichiers Markdown
DIRS := $(sort $(dir $(MARKDOWN_FILES)))

# Remplace l'extension .md par .pdf pour les fichiers cibles
PDF_FILES := $(patsubst $(SOURCE_DIR)/%.md,$(OUTPUT_FOLDER)/%.pdf,$(MARKDOWN_FILES))

all: clean jekyll $(DOCS)

$(DOCS): force | $(OUTPUT_FOLDER)
	@echo "Generte doc for" $@ "folder"
	-cd $@ && $(MAKE) && cp target/*.pdf ../$(OUTPUT_FOLDER)/;

force: ;

$(OUTPUT_FOLDER):
	mkdir -p $(OUTPUT_FOLDER)

# launch jekyll serve to preview website with jekyll server
view: jekyll
	cd $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/ && bundle exec jekyll serve

# Create documentation website with jekyll
# Website will be in $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/_site
jekyll: | $(OUTPUT_FOLDER) init_jekyll_folder copy_source_to_jekyll
	cd $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/ && bundle exec jekyll build

prepare_jekyll: | $(OUTPUT_FOLDER) init_jekyll_folder copy_source_to_jekyll

generate_site:
	cd $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/ && bundle exec jekyll build

init_jekyll_folder:
	cp -r $(TEMPLATE_JEKYLL) $(OUTPUT_FOLDER)
	cd $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/ && bundle

setup:
	sudo apt-get install git git-svn texlive-xetex pandoc ruby-dev
	sudo gem install bundler

# copy markdown sources to jekyll foler
copy_docs_to_target:
	@for a in $(DOCS); do \
	        if [ -d $$a ]; then \
	            echo "copy pdf documents from $$a folder to $(OUTPUT_FOLDER)/"; \
	            cp -r $$a/target/*.pdf $(OUTPUT_FOLDER)/; \
	        fi; \
	    done;
	    @echo "Done!"

# copy markdown sources to jekyll foler
copy_source_to_jekyll:
	@for a in $(DOCS); do \
		if [ -d $$a ]; then \
			echo "copy folder $$a to $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/"; \
						cp -r $$a $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/; \
		fi; \
	done;
	cp $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/index-default.html $(OUTPUT_FOLDER)/jekyll-gazelle-documentation/index.html
	@echo "Done!"

.PHONY: clean

clean:
	-rm -rf $(OUTPUT_FOLDER)
	@for a in $(DOCS); do \
					if [ -d $$a ]; then \
							echo "clean folder $$a"; \
							$(MAKE) clean -C $$a; \
					fi; \
			done;
			@echo "All clear!"

# Règle par défaut pour générer tous les PDF. Attention avec les tableaux il peut y avoir des problèmes de rendu voir README
pdf-gen: $(PDF_FILES)

# Règle pour convertir un fichier Markdown en PDF
$(OUTPUT_FOLDER)/%.pdf: $(SOURCE_DIR)/%.md
	@mkdir -p $(@D) # Crée le répertoire cible s'il n'existe pas
	cd $(dir $<) && pandoc  $<  -N -s --toc --pdf-engine=xelatex -o $(SOURCE_DIR)/$@


