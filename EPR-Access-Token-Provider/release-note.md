---
title: Release note
subtitle: EPR Access Token Provider
toolversion: 1.0.3
releasedate: 2020-09-21
author: Wylem BARS
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-EPR_ACCESS_TOKEN_PROVIDER
---

# CH:IUA Access Token Provider 1.0.3
_Release date: 2020-09-21_

__Improvement__
* \[[IUAINFRA-65](https://gazelle.ihe.net/jira/browse/IUAINFRA-65)\] Add new users to ATP

# CH:IUA Access Token Provider 1.0.2
_Release date: 2020-09-15_

__Remarks__
Improvement in IUA Standard Block

__Bug__
* \[[IUAINFRA-63](https://gazelle.ihe.net/jira/browse/IUAINFRA-63)\] [Dummy Authz Server] AudienceSecretRetriever can be null while instantiating Authrization Service

## JWT Standard-Block 1.0.1
_Release date: 2020-09-15_

__Story__
* \[[IUAINFRA-2](https://gazelle.ihe.net/jira/browse/IUAINFRA-2)\] Validate Extended JWT Access Token

# CH:IUA Access Token Provider 1.0.1
_Release date: 2020-09-07_

__Bug__
* \[[IUAINFRA-60](https://gazelle.ihe.net/jira/browse/IUAINFRA-60)\] JNDI Operational Properties property in Access Token Provider is wrong

# CH:IUA Access Token Provider 1.0.0
_Release date: 2020-08-28_

__Story__
* \[[IUAINFRA-31](https://gazelle.ihe.net/jira/browse/IUAINFRA-31)\] Provide dummy JWS Access token for CH:IUA
* \[[IUAINFRA-52](https://gazelle.ihe.net/jira/browse/IUAINFRA-52)\] Expose Dummy Authz Server as Webservice
* \[[IUAINFRA-53](https://gazelle.ihe.net/jira/browse/IUAINFRA-53)\] Add aerne Patient in the Access Token Provider

__Task__
* \[[IUAINFRA-30](https://gazelle.ihe.net/jira/browse/IUAINFRA-30)\] Simple IUA Access Token Provider Model and API
