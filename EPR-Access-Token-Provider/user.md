---
title:  User Manual
subtitle: Access Token Provider
author: Wylem BARS
function: Developer
releasedate: 2020-09-21
toolversion: 1.0.3
version: 0.01
status: DRAFT
reference: KER1-MAN-IHE-ACCESS_TOKEN_PROVIDER
customer: IHE-EUROPE
---


eHealthSuisse Access Token Provider
-----------------------------------------

ehealthSuisse Access Token Provider is a Dummy rest service providing Token for IUA transactions used in EPR context.
This tool is used to generate an IUA token in order to perform FHIR based tests between participants.

### Data Set

#### Available user IDs

| User ID  | User Role |
|----------|-----------|
| aamrein  | PAT       |
| aerne    | PAT       |
| aerne2   | PAT       |
| bovie    | PAT       |
| lavdic   | PAT       |
| magpar   | HCP       |
| rspieler | HCP       |
| aandrews | HCP       |
| ltieche  | ASS       |

Those are the available users for whom token can be generated.

#### Audience

Those are configured in the tool configuration files. Each Audience is represented with an audienceId and a secret value that is used by the tool
to sign issued tokens.

### EndPoint

* Access Token Provider : [https://ehealthsuisse.ihe-europe.net/authorization-server/mock-token](https://ehealthsuisse.ihe-europe.net/authorization-server/mock-token)

### Request examples

Example 1 : using user magpar (role : HCP)

```https://ehealthsuisse.ihe-europe.net/authorization-server/mock-token?userId=magpar&audienceId=audience```

Example 2 : using user aerne (role : patient)

```https://ehealthsuisse.ihe-europe.net/authorization-server/mock-token?userId=aerne&audienceId=audience```

Example 3 : using user ltieche (role : ASS)

```https://ehealthsuisse.ihe-europe.net/authorization-server/mock-token?userId=ltieche&audienceId=audience```

| Parameter Name  | Usage                                                             |
|-------------|-----------------------------------------------------------------------|
| userId      | User for whom the token is generated                                  |
| audienceId  | ID of the audience used to retrieve secret in Gazelle configurations. |

The response body to this request will be the content of the generated token.