---
title:  Installation Manual
subtitle: EPR Access Token Provider
author: Wylem Bars
releasedate: 2020-09-21
toolversion: 1.0.3
function: Engineer
version: 1.01
status: Approved document
reference: KER1-MAN-IHE-EPR_ACCESS_TOKEN_PROVIDER_INSTALL
customer: IHE-EUROPE
---

# Introduction

EPR Access Token Provider a Dummy rest service providing Token for IUA transactions used in EPR context.
This tool is used to generate an IUA token in order to perform FHIR based tests between participants.

# Sources and binaries

* Sources are available on Inria's GitLab: [https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/access-token-provider](https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/access-token-provider)
* A compiled version is available in the Gazelle Nexus repository, download the [WAR of the latest version](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22dummy-authorization-server-service%22).

# Installation

## Database

No database is needed for this tool.

## Wildfly 18.0.0

EPR Access Token Provider runs on a Wildfly 18 server.

If your do not have yet a Wildfly 18 server, you can install one using the [instructions to install Wildfly 18](../General/wildfly18.html) page.

## Deployment

You are now ready to deploy the tool and to start the Jboss server:

1. Copy the app.dummy-authorization-server-service-1.0.2.war file in the standalone/deployments folder of this new wildfly server.
2. Start Wildfly server (sudo service wildfly-{name} start)

# Configuration

## Audience

Audience used by the tool are configured in a __.properties__ file. It needs to be configured in the Wildfly server under JNDI name 
__java:/app/gazelle/audience-retriever/operational-preferences__. The file content shall resemble something like :

```
audience1=secret1
audience2=secret2
```

To be able to access the file from the application you need to :

1. Install the __factories__ module available [here](https://gazelle.ihe.net/wildfly18/factories.jar) in your Wildfly. See 
[Install or update PostgreSQL JDBC driver](../General/wildfly18.html#install-or-update-postgresql-jdbc-driver) section from the Wildfly18 
Gazelle documentation for more information on how to install the module.
2. Add this configuration to your standalone.xml in the __urn:jboss:domain:naming:2.0__ subsystem :

```xml
<subsystem xmlns="urn:jboss:domain:naming:2.0">
    <bindings>
        <object-factory name="java:/app/gazelle/audience-retriever/operational-preferences" module="net.ihe.gazelle.factories" class="net.ihe.gazelle.factories.PropertiesFactory">
            <environment>
                <property name="path" value="{path}/audience.properties"/>
            </environment>
        </object-factory>
    </bindings>
    <remote-naming/>
</subsystem>
```

{path} can be any path you want on your file system.