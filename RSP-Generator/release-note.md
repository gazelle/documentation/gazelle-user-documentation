---
title: Release note
subtitle: RSP Generator
toolversion: 1.1.1
releasedate: 2025-03-06
author: Claude Lusseau
function: Software Engineer
customer: IHE CATALYST
reference: KER1-MAN-IHE_CATALYST-RSP_GENERATOR_USER-1_00
---
# 1.1.1
_Release date: 2025-03-06_

__Bug__
* \[[RSP-14](https://gazelle.ihe.net/jira/browse/RSP-14)\] error 500 with specific DRE

# 1.1.0
_Release date: 2025-02-21_

__Improvement__
* \[[RSP-2](https://gazelle.ihe.net/jira/browse/RSP-2)\] Add ROC V2 in front.
* \[[RSP-3](https://gazelle.ihe.net/jira/browse/RSP-3)\] Add ROC V2 in backend.
* \[[RSP-7](https://gazelle.ihe.net/jira/browse/RSP-7)\] Suggestions for improving the interface.

__Bug__
* \[[RSP-5](https://gazelle.ihe.net/jira/browse/RSP-5)\] ROCV1 & ROCV2 -  Incorrect date format on some dates in the positive service message.
* \[[RSP-6](https://gazelle.ihe.net/jira/browse/RSP-6)\] Successive import of invalid DREs, the last valid DRE import remains displayed.
* \[[RSP-8](https://gazelle.ihe.net/jira/browse/RSP-8)\] Absence of the second transfer wording in the RSPs for private clinics.
* \[[RSP-9](https://gazelle.ihe.net/jira/browse/RSP-9)\] Random anomaly when importing invalid DREs (DRE containing errors).
* \[[RSP-10](https://gazelle.ihe.net/jira/browse/RSP-10)\] ROCV1 & ROCV2 -  The expected format of ARL entities is not correct.
* \[[RSP-11](https://gazelle.ihe.net/jira/browse/RSP-11)\] ROCV1 & ROCV2 -  The LOT reference in entity 080 must be retrieved from the DRE.
* \[[RSP-12](https://gazelle.ihe.net/jira/browse/RSP-12)\] Interface bug in the case of a 908R- Click on the ‘Meaning of codes’ rejects validates the generation of the RSP.
* \[[RSP-13](https://gazelle.ihe.net/jira/browse/RSP-13)\] Error 500 with a DRE passed.

# 1.0.0
_Release date: 2024-11-08_

__Story__
* \[[RSP-1](https://gazelle.ihe.net/jira/browse/RSP-1)\] Générateur RSP/ARL

