---
subtitle:  RSP Generator
title: Installation Manual
author: Romuald DUBOURG
date: 12/11/2024
toolversion: 1.0.0
function: Software Developer
version: 1.0.0
status: Draft
---

## Requirements
* Java virtual machine : JDK 17


## Installation
Since this tools has two parts, the UI and the backend, you have to install both.
For the backend, you have to pull the project from:
https://gitlab.inria.fr/gazelle/private/kereval/rsp-generator

to start the backend use the following command in the project fetched:
```sh
mvn clean install 
```

By default, the backend will start on port 8080.

and the templates used by the project are created in /opt/rsp-generator/mustache/

For the UI, you have to pull the project from:
https://gitlab.inria.fr/gazelle/private/kereval/rsp-generator-ui

There are few environment variables that you have to set in the .env file :
RSP_API_URL=http://localhost:8082/rsp-generator
SCHEMATRON_API_URL=https://acceptance.ihe-catalyst.net/SchematronValidator-ejb/SchematronValidatorService/SchematronValidator?wsdl
SCHEMATRON_VALIDATOR=DRE-ES;
mustache.template.path=/opt/rsp-generator/mustache/


| Name                                                         | Description                         | Value Example                                                                                 |
|--------------------------------------------------------------|-------------------------------------|-----------------------------------------------------------------------------------------------|
| RSP_API_URL                                     | Path to the back end endpoint       | http://localhost:8080/rsp-generator                                                           |
| SCHEMATRON_API_URL                                            | Path to Schemetatron validator api  | https://localhost/SchematronValidator-ejb/SchematronValidatorService/SchematronValidator?wsdl |
| SCHEMATRON_VALIDATOR                                     | Name of the schematron who validate | http://localhost:8080/rsp-generator                                                           |



to start the UI use the following command in the project fetched:
```sh
npm install
npm run dev
```

the UI will start on port 3000.

to access the UI, you have to go to http://localhost:3000/rsp-generator-ui


