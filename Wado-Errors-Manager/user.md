---
subtitle:  Wado Errors Manager
title: User Manual
author: Romuald DUBOURG
date: 08/09/2023
toolversion: 1.0.0
function: Software Developer
version: 0.01
status: Draft
reference:
customer: IHE-EUROPE
---

# Introduction
This simulator has no database and no ui. There is only an endpoint that return an error matching with the request sent.
This endpoint will only return endpoint don't expect any successful response. The goal here is to respond different kind of error to see if they are rightfully handle.

## Usage
There is an imported file wado-errors-manager/wado-errors-manager-service/src/main/docker/imports/Mapping_OID-HTTP_Error.csv who define for a request which kind of error return.
There are 5 kind of error who can be return :
HTTP 403 Forbidden
HTTP 404 Not Found
HTTP 410 Gone
HTTP 502 Bad Gateway
HTTP 504 Gateway Timeout

The endpoint on which you should send you Get request is define as : HOST/dcm4chee-arc/aets/DCM4CHEE/rs/studies/{studies}/series/{series}/instance/{instances}
where studies and series can be anything because only instance is important.

The file Mapping_OID-HTTP_Error.csv will link an instance to an error, here is an example with Mapping_OID-HTTP_Error.csv as :

OID (SeriesInstanceUID),Code d'erreur
1.2.3.4.5.6.7.8.9.1,HTTP 403 Forbidden


If I host my application on my computer I can send a get request with postman to http://localhost:8646/rest/wado/dcm4chee-arc/aets/DCM4CHEE/rs/studies/1.3.6.1.4.1.5962.99.1.3711725607.829148806.1687338938407.3.0/series/1.3.6.1.4.1.5962.99.1.3711725607.829148806.1687338938407.6.0/instance/1.2.3.4.5.6.7.8.9.1

The last part of the url( the instance) match with a line of the imported file ,so it will reply the matching error, here it's a 403 Forbidden error.

![example_request.png](media/example_request.png)

In case of incorrect or non-matching instance the error reply is a bad request error.


