---
subtitle:  Wado Errors Manager
title: Installation Manual
author: Romuald DUBOURG
date: 08/09/2023
toolversion: 1.0.0
function: Software Developer
version: 0.01
status: Draft
reference: 
customer: IHE-EUROPE
---

## Requirements
* Java virtual machine : JDK 17


## Installation
First you should pull the project from:
https://gitlab.inria.fr/gazelle/private/industrialization/docker/wado-errors-manager.git
then you can use quarkus with :
``
mvn clean package
cd wado-errors-manager-service
mvn docker:start
``
Or
you can use the application with :
``
mvn clean install
``



