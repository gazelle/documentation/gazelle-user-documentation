---
title:  Release note
subtitle: XDSMU Simulator
author: Youn Cadoret
function: Developer
date: 02/06/2019
toolversion: 1.2
version: 1.0
status: To be reviewed
reference: KER1-RNO-IHE-XDSMU_SIMULATOR
customer: IHE-EUROPE
---


# 1.2.0 (a00479fc)
_Release date: 2019-07-31_

__Remarks__

* update XDS-MU simulator [EPR 1.9]


# 1.1 (#67337 )
_Release date: 2019-05-31_

__Remarks__

* update XDS-MU simulator [EPR 1.8.1]


# 1.0 (#66480)
_Release date: 2019-04-15_

__Remarks__

* update XDS-MU simulator [EPR 1.7]
