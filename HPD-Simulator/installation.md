---
title:  Installation manual
subtitle: HPD Simulator
author: Malo TOUDIC
function: Develop
date: 07/02/2022
toolversion: 3.x.x
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-HPD_SIMULATOR_INSTALLATION-1_02
customer: IHE-EUROPE
---

# Introduction

The HPD Simulator has been developed in order to help users with testing their implementations of the Healthcare Provider Directory integration profile published by IHE as trial implementation. This page explains how to install the tool and the underlying LDAP directory. For a full description of the architecture of the tool, please refer to the user manual available in the [Gazelle user guides section](../user.html).

As mentioned in the user guide, the LDAP directory has not been fully developed by the team, it makes use of a 3rd party tool named ApacheDS. This server is open source, certified LDAPv3 and supports DSMLv2. It also embeds a Kerberos Server (which can be useful later if we need to provide tools for the EUA profile). We currently use ApacheDS 2.0.0-M14.

The sources of the projects are available on [Inria's gitlab](https://gitlab.inria.fr/gazelle/public/simulation/hpd-simulator.git).

# Pre-requisites

## Install ApacheDS

Before installing the HPDSimulator, you will need to install and configure ApacheDS, below are the steps to follow.

In a terminal, enter the command lines below: 

~~~~
$> wget http://archive.apache.org/dist/directory/apacheds/dist/2.0.0-M14/apacheds-2.0.0-M14-amd64.deb
$> sudo dpkg -i apacheds-2.0.0-M14-amd64.deb
~~~~

When the installation is complete, you can start the server using the command line :

~~~~
$> sudo service apacheds-2.0.0-M14-default start
~~~~

The application can start automatically at start-up, execute

~~~~
$> sudo update-rc.d apacheds-2.0.0-M14-default defaults
~~~~

## Install Apache Directory Studio

The Apache Directory Project offers an Eclipse-based tool that you can use as a human-friendly interface for configuring and managing your LDAP Server remotly. It’s called Apache Directory Studio and it is available at [*http://directory.apache.org/studio/downloads.html*](http://directory.apache.org/studio/downloads.html).

## Configure the connection to the directory

Access the directory from Apache Directory Studio and follow the steps below

1.  Start Apache Directory Studio and switch to the LDAP perspective.

2.  On the bottom-left corner, a window is displayed with a tab “Connections”, go to this tab and click on “new connection..." button, a dialog box appears.

3.  In a first time, you only need to fill out the first tab concerning the network parameters. Note that the default port used by ApacheDS is 10389, there is no encryption needed by default and the Provider to use is “Apache Directory LDAP Client API”.

4.  Hit “OK” button when you have finished with this tab. You may now be connected. If not, double-click on the newly created connection. The DIT (Directory Information Tree) of your LDAP server will be displayed in the LDAP Browser window located on the left-hand side of the tool.

5.  To log in as an admin, default credentials are :

    username : uid=admin,ou=system

    password : secret

## Configure the directory

First you need to import the LDIF files which describes the missing LDAP scheme : hpd (defined by IHE), hc (defined by ISO 21091) and rfc2985. This will allow you to access all the object classes and relative attributes defined by these scheme.

To proceed, first, download the three following files, they are available in the EAR module of the HPDSimulator maven project at src/main/ldif

* hc.ldif
* hpd.ldif
* rfc2985.ldif

Then, import those three files in your LDAP server, process as follows:

1.  In Apache Directory Studio, in the “LDAP browser” window, right-click on ‘Root DSE’ and select Import →  Import LDIF...

2.  Select your file and “Finish”

3.  Do the same for the other files.

Finally, check that the schema has been updated. You shall see three new nodes under ou=schema:  cn=hc, cn=hpd, cn=rfc2985

### Create nodes subordinate to dc=HPD

According to the HPD integration profile, three nodes are subordinate to dc=HPD:

* ou=HCProfessional
* ou=HCRegulatedOrganization
* ou=Relationship

Each of this three nodes will be represented by an LDAP partition on our server. To create such a partition, double-click on the connection you have previously created to open a page entitled “Your connection name - Configuration”. Go to the “Partitions” tab.

On that page, all the existing partitions are listed. To create a new partition, click on the “Add” button. Do not forget to regularly save this configuration file (Ctrl-S) while adding partitions.

### Node HCProfessional

* ID: HCProfessional
* Suffix: ou=HCProfessional,dc=HPD,o=gazelle,c=IHE
* Check the box to have the context entry automatically generated from the DN suffix.

### Node HCRegulatedOrganization

* ID: HCRegulatedOrganization
* Suffix: ou=HCRegulatedOrganization,dc=HPC,o=gazelle,c=IHE
* Check the box to have the context entry automatically generated from the DN suffix.

### Node Relationship

* ID: Relationship
* Suffix: ou=Reliationship,dc=HPD,o=gazelle,c=IHE
* Check the box to have the context entry automatically generated from the DN suffix.

### Additional nodes

We can also add the following nodes (suffix will be built on the same pattern as previous ones):

* HPDProviderCredential
* HPDProviderMembership
* HPDElectronicService

Note that you may have to reset your connection to your server to see the newly created partitions under the RootDSE node. You can now start to add entries into your LDAP server.

# Install the HPD Simulator

## Requirements

The HPD Simulator tool is a Maven 3 project, sources are available on Inria’s gitlab at [*https://gitlab.inria.fr/gazelle/public/simulation/hpd-simulator.git*](https://gitlab.inria.fr/gazelle/public/simulation/hpd-simulator.git).

This application runs under JBoss 7.2.0-GA and uses a postgreSQL 9 database.

## Update the webservice stack (JbossWS) in Jboss7

This step is only required in a Swiss environment.

Download the last jboss 7.2.0 [compatible version](https://developer.jboss.org/docs/DOC-13569) of JbossWS: [https://download.jboss.org/jbossws/jbossws-cxf-4.3.2.Final.zip](https://download.jboss.org/jbossws/jbossws-cxf-4.3.2.Final.zip)

Extract the archive then copy ant.properties.example to ant.properties. In ant.properties set the jboss7 path in *jboss720.home* and make sure *jbossws.integration.target* is set to *jboss720*:

```
# Optional JBoss Home
jboss720.home=/usr/local/jboss7
wildfly800.home=@wildfly800.home@
wildfly810.home=@wildfly810.home@

# The JBoss server under test. This can be [jboss720|wildfly800|wildfly810]
jbossws.integration.target=jboss720

# The JBoss settings
jboss.bind.address=localhost
```

Execute the installation script with:

```bash
ant deploy-jboss720
```

Restart jboss.

## Database creation

Your database must have a user **gazelle**

1.  Connect to your database
```bash
psql -U gazelle
```

1.  Execute the SQL statement to create the database.
```sql
CREATE DATABASE "hpd-simulator" OWNER gazelle ENCODING 'UTF8' ;
```

## Deployment

To deploy HPDSimulator :

1. Download the ear from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22HPDSimulator-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22HPDSimulator-ear%22) (search for **HPDSimulator-X.X.X.ear**)

1.  Paste the archive **HPDSimulator.ear** in the JBoss deployment directory `${JBOSS7_HOME}/standalone/deployments/`

1.  Display JBoss server logs, start JBoss and wait for **ear** deployment.

1.  The application can be browsed at [http://localhost:8080/HPDSimulator](http://localhost:8080/HPDSimulator)
_Port could also be different whether you have modified the JBoss configuration._

1. **WARNING** : From version 2.2.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file HPDSimulator-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : HPDSimulatorDS

Database name : hpd-simulator


## Components

HPD Simulator integrates the three actors defined by the Healthcare Provider Directory integration profile of IHE:

* Provider Information Source
* Provider Information Consumer
* Provider Information Directory

# Configure the HPD Simulator

## Representation of the LDAP schema

In order to help the users with creating the requests to send to Provider Information Directory actor, we need to know the schema of the LDAP server (object classes, attribute types) in the database of the simulator. In this way, we can offer the right list of attributes for a selected object class and make sure that the request will be correct regarding the HPD profile.

## LDAPStructure

To make easier the process of populating the database with the LDAP schema, we have chosen to export some informations contained in the LDAP server to the database using an home-made structure. Actually, Apache Directory Studio allows us to export a node of the Directory Information Tree as a DSML searchResponse. That means that we can save on disk an XML file containing a batchResponse with a unique searchResponse which gathered several searchResultEntry elements (one per leaf of the selected node). As the DSML schema is very basic and that it would not have been very convenient to save the schema using this representation, we have defined a new model. The XML schema is available at the following location: HPDSimulator-ear/src/main/xsl/LDAPStructure.xsd. From this XML schema, we have generated the Java classes and as a consequence, we are able to upload an XML file into the application and saving its content in the database of the tool.

### Generated the XML files to import

This section describes how to generate the XML files which will be then imported into the tool.

You first need to export as DSML response the nodes/leaves listed below. Each node/leaf will be exported separately:

1.  right-click on the node/leaf, select Export → Export DSML...

2.  Click on “Next &gt;”

3.  Select the location where to store the file and give it a name

4.  Check that “DSML Response” is selected

5.  Click on Finnish

#### List of nodes/leaves to export

* ou=schema,cn=hpd
* ou=schema,cn=hc
* ou=schema,cn=rfc2985
* ou=schema,cn=inetorgperson
* ou=schema,cn=core,ou=objectClasses,m-oid=2.5.6.7
* ou=schema,cn=core,ou=objectClasses,m-oid=2.5.6.9
* ou=schema,cn=core,ou=objectClasses,m-oid=2.5.6.6
* ou=schema,cn=system,ou=objectClasses,m-oid=2.5.6.0
* ou=schema,cn=system,ou=objectClasses,m-oid=1.3.6.1.4.1.1466.101.120.111
* ou=schema,cn=system,ou=attributeTypes
* ou=schema,cn=core,ou=attributeTypes
* ou=schema,cn=cosine,ou=attributeTypes

### Generate LDAPStructure files

Transform the generated XML files into other XML files (valid against the LDAPStructure.xsd schema) using the XML stylesheet located here: HPDSimulator-ear/src/main/xsl/dsml2LDAPStructure.xsl.

You can perform the transformation using Oxygen or the following command line in the directory where your XML files are located:

~~~~
$> find . -name '\*.xml' -exec saxon-xslt -o '{}' '{}\_new.xml' XSL\_LOCATION\\;
~~~~

### Upload files in HPDSimulator

1.  Logged on HPDSimulator with admin\_role role, go to Administration → Manage LDAP Server → Upload LDAP Structure.

2.  Select the files to upload, you upload ten files at once.

3.  Click on the “Upload” button.

When it’s done, you will be redirected to the page gathering the list of object classes.

### Browse object classes and attribute types

Logged on the application with admin\_role role, you can browse the object classes and attribute types registered in the database of the simulator:

Administration → Manage LDAP Server → Object classes

Administration → Manage LDAP Server → Attribute types

## Definition of LDAP Partitions

In order to access the LDAP server from the simulator, we need to know its IP address and the port on which it is listening. In addition, we do not want users to access all the partitions defined in our server. They shall only be able to modify and make searches on the nodes dedicated to the HPD profile. As a consequence, we use an entity named LDAPPartition which allows us to get the information on the various partitions defined on the server and how to access them.

As we have seen when we have created the partitions on the ApacheDS server, the suffix DN is based on the same ‘root’ for each subordinate node to dc=HPD. As a consequence, in the simulator, an LDAPPartition object stands for the base DN. That means that we have only one LDAPPartition entry for the LDAP partitions we have previously created; it’s base DN is dc=HPD,o=gazelle,c=IHE. This LDAPPartition entry has subordinate nodes which are HCRegisteredOrganization, HCProfessional and Relationship.

A subordinate node is reprensenting by the LDAPNode object which is composed of a name (HCProfessional for instance) and a list of object classes (eg. HCProfessional, naturalPerson, HPDProviderCredential, and HPDProvider).

### Create a new LDAPNode

1.  Go to Administration → Manage LDAP Server → Manage LDAP Nodes

2.  Click on “Create a new node”

3.  Enter the name of this node, give a brief description and select the object classes

4.  Click on “Save”

### Create a new LDAPPartition

<!-- -->

1.  Go to Administration → Manage LDAP Server → Manage LDAP Partitions

2.  Click on “Create a new partition”

3.  Fill out the form, select the subordinate nodes

4.  Click on “Save” 

# Application preferences

| **Prefence name**                | **Description**                                                                          | **Default value**                                                                                                         |
|----------------------------------|------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| application\_name                | The name of the application                                                              | HPD Simulator                                                                                                             |
| application\_url                 | URL to reach the tool                                                                    | https://gazelle.ihe.net/HPDSimulator                                                                                       |
| assertion\_manager\_url          | Link to the Assertion Manager tool                                                       | https://gazelle.ihe.net/AssertionManagerGui                                                                                |
| dsml\_xsl\_location              | URL of the stylesheet used to display DSMLv2 messages                                    | https://gazelle.ihe.net/xsl/dsmlStylesheet.xsl                                                                             |
| ldap\_password                   | Password used to log onto the LDAP server (if authentication is required)                | N/A (no authentication put in place)                                                                                      |
| ldap\_user                       | Username used to log onto the LDAP server (if authentication is required)                | N/A (no authentication put in place)                                                                                      |
| message\_permanent\_link         | Link to directly access simulator logs                                                   | https://gazelle.ihe.net/HPDSimulator/messages/messageDisplay.seam?id=                                                      |
| NUMBER\_OF\_ITEMS\_PER\_PAGE     | How many lines to display in tables                                                      | 20                                                                                                                        |
| prov\_info\_dir\_wsdl            | URL to contact the Provider Information Directory endpoint (to be displayed to the user) | https://gazelle.ihe.net/HPDSimulator-ejb/ProviderInformationDirectory\_Service/ProviderInformationDirectory\_PortType?wsdl |
| results\_xsl\_location           | URL to access the stylesheet to display HPD validation results                           | https://gazelle.ihe.net/xsl/hl7v3validatorDetailedResult.xsl                                                               |
| SeHE\_mode\_enabled              | Is the application configured for MoH needs                                              | false                                                                                                                     |
| svs\_repository\_url             | Used for the validation of codes in the validation engine                                | https://gazelle.ihe.net                                                                                                    |
| time\_zone                       | To display time in the appropriate time zone                                             | Europe/Paris                                                                                                              |
| xsd\_location                    | URI to access the DSMLv2 schema (used by validation service)                             | /opt/hpd/xsd/IHE/DSMLv2.xsd                                                                                               |

# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

# Configuration of the documentation for HPD Model-based validator

The administrator is able to configure the documentation of HPD constraints and rules

1. First the user shall be logged in as an administrator.
1. Then, go to Administration > Validation tool > Manage HPD constraints file.
1. The user shall then upload the XML file, generated from the model of constraints, into the tool.
1. After the upload is ended, click on the button "Delete and Generate". The tool will delete all the constraints related to packages that are mentionned into the uploaded XML file.

If there are some constraints related to other packages, there will not be deleted. All the information into the XML document are inserted in the database of the tool.
