---
title:  Release note
subtitle: RMU Simulator
author: Youn Cadoret
function: Developer
releasedate: 2019-09-20
toolversion: 1.3.1
version: 1.0
status: To be reviewed
reference: KER1-RNO-IHE-RMU_SIMULATOR
customer: IHE-EUROPE
---

# 1.3.1
_Release date: 2019-09-20_

__Bug__

* \[[RMU-3](https://gazelle.ihe.net/jira/browse/RMU-3)\] RMU Registry Simulator - Wrong message id in WS-Addressing header


# 1.3.0
_Release date: 2019-08-02_

__Task__

* \[[RMU-2](https://gazelle.ihe.net/jira/browse/RMU-2)\] [RMU] Create groovy script to record mock transaction

# 1.2.0 (a00479fc)
_Release date: 2019-07-01_

__Remarks__

* update RMU simulator [EPR 1.9]

__Task__

* \[[RMU-1](https://gazelle.ihe.net/jira/browse/RMU-1)\] RMU material testing


# 1.1.0 
_Release date: 2019-01-03_

__Remarks__

* update RMU simulator [EPR 1.8]



# 1.0 (#63755)
_Release date: 2018-07-20_

__Remarks__

* update RMU simulator [EPR 1.7]ulator [EPR 1.7]