---
title:  Installation Manual
subtitle: RMU Simulator
author: Youn Cadoret
function: Developer
date: 28/06/2019
toolversion: 1.1.0
version: 0.01
status: to be reviewed
reference: KER1-MAN-IHE-RMU_SIMULATOR_INSTALLATION_USER
customer: IHE-EUROPE
---

# EPR RMU MockUp

## Overview

The RMU MockUp is a SoapUI webservice (mock) that provides Cross-Gateway Update Document Set [ITI-X1] Response. This mock requires that the Update Metadata Registry MockUp is started.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net:8095/restricted-metadata-update?wsdl
* default_path: /restricted-metadata-update
* default_port: 8095
* default_mock_name: RespondingGateway_Binding_Soap12 MockService
* default_mock_path: /opt/simulators/epr-restricted-metadata-update-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-restricted-metadata-update.log
* default_init.d: /etc/init.d/rmuRespondingGatewayMock
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR RMU MockUp

### Get the git project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/metadata-update-responders.git $EPR_RMU_MOCK_DIR
```


## Mock as a service

### Prepare the init.d script

Edit the init.d scripts `$EPR_RMU_MOCK_DIR/init.d/rmuRespondingGatewayMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_RMU_MOCK_DIR/init.d/rmuRespondingGatewayMock /etc/init.d/rmuRespondingGatewayMock
sudo chmod u+x /etc/init.d/rmuRespondingGatewayMock
sudo chmod 775 /etc/init.d/rmuRespondingGatewayMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d rmuRespondingGatewayMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/rmuRespondingGatewayMock start
```

To stop the mock

```bash
sudo /etc/init.d/rmuRespondingGatewayMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/rmuRespondingGatewayMock status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```

You might need to resolve errors when starting the mock

```bash
sudo mv /root/.soapuios/ /root/.soapuios_old
```



# EPR ITI-18 Registry Stored Query MockUp

## Overview

The ITI-18 Registry Stored Query MockUp is a SoapUI webservice (mock) that provides Registry Stored Query [ITI-18] Response.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net:8093/registry-stored-query?wsdl
* default_path: /registry-stored-query
* default_port: 8093
* default_mock_name: DocumentRegistry_Binding_Soap11 MockService
* default_mock_path: /opt/simulators/epr-restricted-metadata-update-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-registry-stored-query.log
* default_init.d: /etc/init.d/RegistryStoredQueryMock
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR Assertion Provider MockUp

### Get the git project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/metadata-update-responders.git $EPR_RMU_MOCK_DIR
```


## Mock as a service

### Prepare the init.d script

Edit the init.d scripts `$EPR_RMU_MOCK_DIR/init.d/RegistryStoredQueryMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_RMU_MOCK_DIR/init.d/RegistryStoredQueryMock /etc/init.d/RegistryStoredQueryMock
sudo chmod u+x /etc/init.d/RegistryStoredQueryMock
sudo chmod 775 /etc/init.d/RegistryStoredQueryMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d RegistryStoredQueryMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/RegistryStoredQueryMock start
```

To stop the mock

```bash
sudo /etc/init.d/RegistryStoredQueryMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/RegistryStoredQueryMock status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```

You might need to resolve errors when starting the mock

```bash
sudo mv /root/.soapuios/ /root/.soapuios_old
```



# EPR ITI-57 Update Metadata Set MockUp

## Overview

The ITI-57 Update Metadata Set MockUp is a SoapUI webservice (mock) that provides Update Document Set [ITI-57] Response.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net:8094/update-document-set?wsdl
* default_path: /update-document-set
* default_port: 8094
* default_mock_name: DocumentRegistry_Binding_Soap12 MockService
* default_mock_path: /opt/simulators/epr-restricted-metadata-update-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-update-document-set.log
* default_init.d: /etc/init.d/UpdateMetadataSetMock
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR Assertion Provider MockUp

### Get the Git project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/metadata-update-responders.git $EPR_RMU_MOCK_DIR
```


## Mock as a service

### Prepare the init.d script

Edit the init.d scripts `$EPR_RMU_MOCK_DIR/init.d/rmuRespondingGatewayMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_RMU_MOCK_DIR/init.d/UpdateMetadataSetMock /etc/init.d/UpdateMetadataSetMock
sudo chmod u+x /etc/init.d/UpdateMetadataSetMock
sudo chmod 775 /etc/init.d/UpdateMetadataSetMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d UpdateMetadataSetMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/UpdateMetadataSetMock start
```

To stop the mock

```bash
sudo /etc/init.d/UpdateMetadataSetMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/UpdateMetadataSetMock status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```

You might need to resolve errors when starting the mock

```bash
sudo mv /root/.soapuios/ /root/.soapuios_old
```

