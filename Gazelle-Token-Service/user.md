---
title: User Manual
subtitle: Gazelle Token Service
author: Claude Lusseau
date: 15/09/2022
toolversion: 1.0.0
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_TOKEN_SERVICE-1_00
---
# Introduction

Gazelle Token Service is a Web GUI for create token to access GazelleTM Rest Resources.

The Web Service Description Language (WSDL) needed to contact our Web service with SOAP and XML Schema is linked below.


**An CAS authentication is asked for this server**. 



This STS is used as part of the Connectathons in NA and EU. Our X-Assertion Provider is configured with a Certificate signed by IHE Europe CA. It is available here . It is only configured to help you perform all XUA tests, and hence does not provide token for a different context. As a user of the service you must trust that certificate.

## CAS

**Account connection.**

![img.png](media/user/cas.png)


## User access

the user interface offers three token management panels

**Token generation.**

To create a token, click on "Create Token" : 

![img.png](media/user/create.png)

You can now copy the token to use it in your application.

![img.png](media/user/created.png)

You will find the validity period of this new token above the value to be copied.

If you need deactivate the token, you can click on Deactivate button.

### _Important_

**_Only one token can be generated at a time._**



****Token list****

In the token list panel, you will be able to see the active token, if there is one, as well as all inactive tokens

![img.png](media/user/listToken.png)

**Purge tokens**

You can clean the database of old tokens.
**Only expired/deactivated tokens will be deleted.**

![img.png](media/user/purge.png)
