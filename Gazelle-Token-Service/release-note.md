---
title: Release note
subtitle: Gazelle Token Service
author: Claude Lusseau
date: 15/09/2022
toolversion: 1.0.0
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_TOKEN_SERVICE
---
# GazelleTokenService 1.0.0
_Release date: 2022-28-09_

__Story__
* \[[ANSBP-103](https://gazelle.ihe.net/jira/browse/ANSBP-103)\] Create Token Service

