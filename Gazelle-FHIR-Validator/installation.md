---
title:  Installation Manual
subtitle: Gazelle FHIR Validator
author: Nicolas BAILLIET
releasedate: 2024-11-06
toolversion: 4.1.8
function: Engineer
version: 2.01
status: Approved document
reference: KER1-MAN-IHE-GAZELLE_FHIR_VALIDATOR_INSTALL
customer: IHE-EUROPE
---

# Introduction

Gazelle FHIR Validator provides

* A Graphic User Interface for the administrator to define the FHIR Validators and the user to browse them
* A SOAP webservice to validate FHIR resources and FHIR URLs

# Sources and binaries

* Sources are available on Inria's GitLab: [https://gitlab.inria.fr/gazelle/applications/test-execution/validator/FhirValidator/](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/FhirValidator/)
* A compiled version is available in the Gazelle Nexus repository, download the [EAR of the latest version](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~FhirValidator*~~~).

# Installation

## Database

Create the database to be used by the tool. Execute `createdb -U gazelle -E UTF8 gazelle-fhir-validator`.

## Wildfly 10.0.0

From version 3.0.0, Gazelle Fhir Validator only runs on a Wildfly 10 server.

If your do not have yet a Wildfly server, you can install one using the [instructions to install Wildfly 10](../General/wildfly10.html) page.

## FHIR R4 profile

A new R4 profile has been implemented in the FhirValidator project to manage two differents versions of FHIRValidator on the same machine (R4 and DSTU3).
* The default name of the database for this profile is **gazelle-fhir-validator-r4**
* The name of the application url is **GazelleFhirValidatorR4**



## Deployment

You are now ready to deploy the tool and to start the Jboss server:

1. Copy the FhirValidator.ear file in the standalone/deployments folder of this new jboss server.
2. Start Jboss server (sudo service wildfly-{name} start)
1. **WARNING** : From version 3.0.0, datasource need to be extracted from the **ear** folder (found in /src/main/application/META-INF).
For more informations about how to manage that externalization, please refer to [General > wildfly10](../General/wildfly10.html) part of the documentation. The following parameters have to be used:

YOUR_TOOL_DATASOURCES : FhirValidatorDS

YOUR_TOOL_DB : gazelle-fhir-validator

Since version 4.1.0, Gazelle FHIR Validator uses a new version of the HAPI library. In order to deploy it on a Wildfly server, the following JARs are required in the server file system :

* [jackson-core-2.10.0.jar](https://gazelle.ihe.net/nexus/service/local/repositories/central/content/com/fasterxml/jackson/core/jackson-core/2.10.0/jackson-core-2.10.0.jar)
* [jackson-annotations-2.10.0.jar](https://gazelle.ihe.net/nexus/service/local/repositories/central/content/com/fasterxml/jackson/core/jackson-annotations/2.10.0/jackson-annotations-2.10.0.jar)
* [jackson-databind-2.11.0.wso2v1](https://gazelle.ihe.net/nexus/service/local/repositories/central/content/org/wso2/orbit/com/fasterxml/jackson/core/jackson-databind/2.11.0.wso2v1/jackson-databind-2.11.0.wso2v1.jar)

Please refer to [General > wildfly10](../General/wildfly10.html) part on how to setup PostgreSQL where a module addition is documented.

## Hapi Fhir JPA Server with IG support

A project you can use to deploy a FHIR server using HAPI FHIR JPA validation with the npm implementation support : https://github.com/ahdis/hapi-fhir-jpaserver-starter/tree/ig.

## IHE Structure Definitions

SQL script files available in the project directory imports entries to use the structure definitions and value sets defined by IHE. In order to use them, you can checkout the content of [fhir-structure-definitions](https://gitlab.inria.fr/gazelle/test-content/fhir-conformance-resources) directory from Inria's forge into /opt/fhirvalidator folder. It is the default location used by the tool. This can however be changed by updating the path to each custom structure definition in the database.

## Accesses

The endpoint to be contacted will be located at [YOUR_SERVER_URL]/FhirValidator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl.

The graphic user interface is available at [YOUR_JBOSS_URL]/GazelleFhirValidator.

# Configuration

## Application preferences

Under the Administration menu, you will find a sub-menu entitled "Configuration". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the Gazelle FHIR Validator tool.

| **Variable** |**Description** | **Default value** |
|--------------|------------------------------------------|--------------|
| application\_url |  The URL used by any user to access the tool. The application needs it to build permanent links inside the tool |  https://publicUrlOfJboss/GazelleFhirValidator |
| application_issue_tracker | Link to the issue tracker where users can report issues (used in the footer of the app) | https://gazelle.ihe.net/jira/browse/FHIRVAL |
| application_release_note_url | Link to the release note of the tool (used in the footer of the app) | https://gazelle.ihe.net/jira/browse/FHIRVAL |
| contact_email | The email of the person to contact for any questions regarding the tool (used in the footer of the app) | |
| contact_name | Name of the person to contact for any questions regarding the tool (used in the footer of the app) | |
| contact_title | Function of the person to contact for any questions regarding the tool (used in the footer of the app) | |
| documentation_url | Link to the user manual (used in the footer of the tool) | https://gazelle.ihe.net/gazelle-documentation/Gazelle-FHIR-Validator/user.html |
| NUMBER_OF_ITEMS_PER_PAGE | Default number of entries in tables | 20 |
| structure_definition_stylesheet_location | URL of the XSL used to display the structure definition | https://gazelle.ihe.net/xsl/fhir/structureDefinition.xsl |
| time_zone | Default time zone to be used to display dates and times | Europe/Paris |
| value_set_location | Where the tool accesses the value sets to be loaded | /opt/fhir-structure-definition/ValueSet |
| code_system_location | Where the tool accesses the code systems to be loaded | /opt/fhir-structure-definition/CodeSystem |
| ig_fhir_server_url | URL of the Fhir Server used for validation | http://localhost:8080/hapi-fhir-jpavalidator/fhir|

## SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

## Managing validators

As of version 2.2.0, two types of validators can be defined:

* FHIR Resource Validators. They are based either on :
    * a FHIR structureDefinition files, the entry in the database is then a pointer to the proper resource to be used
    * a profile validated by a remote Fhir server, using the $validate operation. The entry in the database is then a reference to the profile to validate URL. 
* FHIR URL Validators are based on an in-house algorithm which checks the correctness of the provided URL regarding the definition of the URL input by the administrator of the tool

### FHIR Resource validators

#### Structure definitions

Structure definition files shall be loaded directly on the file system. Future versions will allow the administrator to upload all the necessary files.

When structure definitions and/or value sets are added, removed or modified, you need to use the "Reload structure definitions" button. It will destroy the snapshots of the structure definitions stored in memory and load them again.

Since version 4.0.1, R4 version of Resource Validators are mandatory.

#### Remote validation server

Deploy the FHIR server used for validation that is available [here](https://github.com/ahdis/hapi-fhir-jpaserver-starter/tree/ig.). 

You can change which implementation guides are loaded by modifying the hapi.properties file.

Then be sur to correctly set the __ig_fhir_server_url__ property to point to the deployed server. The validators are ready !

### Definition of a validator

Gazelle FHIR Validator makes use of several kind of validation engines:

* FHIR library provided by hapi-fhir for default resources
* Custom structure Definitions
* Use of the FHIR $validate operation on a remote server.
* In-house algorithm for URLs

The configuration of each kind of validations will slightly differs.

### Common attributes

* __OID__: uniquely identified a validation (for easy management in calling applications)
* __Name__: Explicit name of the validator (see [convention](#conventions) section below)
* __Available__: When set to false, validation will not show up in the list of available validators (used by web service)
* __Profile__: Keyword of the IHE profile related to this validator
* __Descriminator__: By convention, we use the keyword of the affinity domain. Used in the web service to filter the list of available validators by context

### Attributes for FHIR Resource validators

* __Execute schema validation__: True will trigger the execution of the schema validation implemented by hapi-fhir as a first step of the validation process
* __Execute schematron validation__: True will trigger the execution of the schematron validation implemented by hapi-fhir
* __Path to structure definition__: Path to the custom structure definition. Use for Structure Definition based validation.
* __Path to structure definition context__: Path to the custom structure definition context. Use for Structure Definition based validation.
* __URL__: URL field from the structure definition. Used for both structure definition and server based validations.
* __Base definition URL__: URL of the base structure definition. Use for Structure Definition based validation.
* __Weight__: Custom structure definition might reference each others, the weight is used to process the structure definition in the correct order to 
correctly generate the snapshot. Validators with the highest weight are processed first. Use for Structure Definition based validation.

### Attributes for the URL validators

For some interactions, like search or read, FHIR uses HTTP operation, in that case the initiator system build a URL. Examples:
* Search: `GET [base]/[type]?name=value&...{&_format=[mime-type]}}`
* Create: `POST [base]/[type] {?_format=[mime-type]}`
* Conditional delete: `DELETE [base]/[type]/?[search parameters]`
* and so on...


Gazelle FHIR Validator allows the test designer to describe this syntax which is then processed by an algorithm to detect errors in the provided URL.

* __Resource name__: The name of the resource the interaction is performed on (example: Patient in a PDQm request), refers to [base] in the URL
* __Operation__: For custom operation to be used in the URL (example: IHE defines $ihe-pix for PIXm)
* __Parameters__: A list of URL parameters to be used in the URL

For each parameter, the following attributes are used:

* __name__: the name of the parameter as used in the URL
* __type__: the type of the parameter, used to verify the conformance of the value
* __prefix__: for some parameters, a prefix is required, it can be set using this attribute
* __optionality__: indicates whether the parameter is required in the URL or only optional
* __regex__: by default the value will be checked against the regex defined for the chosen datatype, if the content of the value for a parameter shall be more specific, the regex can be overriden using this attribute

### Conventions

The name of the validators uses the following pattern: [TRANSACTION-KEYWORD] Message name where:

* TRANSACTION-KEYWORD is the keyword of the transaction which is checked using this validator
* Message name is the name of the message as defined in the technical framework
