---
title: Release note
subtitle: Datamatrix
toolversion: 1.0.1
releasedate: 2023-10-24
author: Nicolas BAILLIET
function: Developer
customer: ANS
reference: KER1-RNO-ANS-DATAMATRIX
---
# 1.0.1
_Release date: 2023-10-24_

__Task__
* \[[DATAMATRIX-1](https://gazelle.ihe.net/jira/browse/DATAMATRIX-1)\] IHM improvement - Size text area, Version number, user manual
* \[[DATAMATRIX-2](https://gazelle.ihe.net/jira/browse/DATAMATRIX-2)\] C40 encoding and broken link