---
title: Release note
subtitle: Digital Covid Certificate validator
toolversion: 1.2.2
releasedate: 2023-11-03
author: Malo Toudic
function: Software developer
customer: IHE Europe
reference: KER1-RNO-IHE-DCC_VALIDATOR
---

# 1.2.2
_Release date: 2023-11-03_

__Story__
* \[[DCC-20](https://gazelle.ihe.net/jira/browse/DCC-20)\] Make the DDVC verify url configurable

__Problem__
* \[[DCC-21](https://gazelle.ihe.net/jira/browse/DCC-21)\] Some DDVC QR CODE cannot be decoded

# 1.2.1
_Release date: 2022-11-29_

__Task__
* \[[DCC-15](https://gazelle.ihe.net/jira/browse/DCC-15)\] Issue in the DCC Validator
* \[[DCC-18](https://gazelle.ihe.net/jira/browse/DCC-18)\] Add HTTPS support for DDCC Verifier inside application

# 1.2.0
_Release date: 2022-11-29_
  
__Task__
* \[[DCC-14](https://gazelle.ihe.net/jira/browse/DCC-14)\] Install DDCCVerifierConnector to manage validation with WHO DDCC Verifier

# 1.1.0
_Release date: 2022-05-11_

__Story__
* \[[DCC-11](https://gazelle.ihe.net/jira/browse/DCC-11)\] API to retrieve json

# 1.0.0
_Release date: 2022-04-05_

__Task__
* \[[DCC-9](https://gazelle.ihe.net/jira/browse/DCC-9)\] Clean prototype
* \[[DCC-10](https://gazelle.ihe.net/jira/browse/DCC-10)\] Validate valueset
