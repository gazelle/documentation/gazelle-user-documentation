---
title:  Installation Manual
subtitle: DCC Validator
author: Malo Toudic
releasedate: 03/11/2023
toolversion: 1.2.x
function: Developer
version: 0.01
status: Draft document
reference: KER1-MAN-IHE-DCC_VALIDATOR_INSTALLATION-0_01
customer: IHE-EUROPE
---

# Digital COVID Certificate Validator

The EU digital-covid-certificate Validator is an open-source Validation Service (under Apache 2 licence) provided by IHE Europe Gazelle Test Bed for verifying conformity of generated certificates to the EU specifications.

## Sources & binaries

The sources are accessible here: [https://gitlab.inria.fr/gazelle/applications/test-execution/validator/eu-digital-covid-certificate-validator](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/eu-digital-covid-certificate-validator)

Bugs and issue tracking are accessible here : [https://gazelle.ihe.net/jira/projects/DCC](https://gazelle.ihe.net/jira/projects/DCC). The name of the latest release, can be obtained in the "Releases" section.

To get official artifact (binaries), search for `dcc-validator-service` in IHE Nexus : [https://gazelle.ihe.net/nexus](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22dcc-validator-service%22) and select the **app.dcc-validator-service.war** artifact for download.

## Installation

### Application server

Patient registry need to be deployed on Widlfly-18. Please read first the documentation on how to install and configure this server for Gazelle
applications : [general considerations for WildFly 18](https://gazelle.ihe.net/gazelle-documentation/General/wildfly18.html)

**Be careful**: Once the /etc/init.d/wildfly18 is installed, the Java option `-Djsse.enableSNIExtension` **MUST BE** set to `true` in order to perform DDCC Verifer Validation.

### Deployment

Copy the **war** artifact app.dcc-validator-service-X.X.X.war in the deployment folder of the wildfly installation under the name **app.dcc-validator-service**. This is important for the path on which DCC Validator's web services will be exposed.

```bash
cp app.dcc-validator-service-X.X.X.war /usr/local/wildfly18/standalone/deployments/app.dcc-validator-service.war
```

Start wildfly. The API can be accessed at (depending on your configured host and port): <http://localhost:8080/dcc-validator/dccValidationService?wsdl>

### Operational Preferences

DCC validator defines mandatory Operational Preferences that you will have to define in your Application server.
To know how to do that, see [General considerations for WildFly 18](https://gazelle.ihe.net/gazelle-documentation/General/wildfly18.html)

Define a resource in your server with name *java:/app/gazelle/dcc-validator/operational-preferences*.

This resource should refer to a deployment.properties file. This file shall contain the following properties :

| Property Name | Description                                                                                           | Example value                     |
| --- |-------------------------------------------------------------------------------------------------------|-----------------------------------|
| svssimulator.url | URL of Simulator                                                                                      | <http://localhost/SVSSimulator>   |
| truststore.path | Path of the truststore containing the trusted certificates that issues Digital covid certificates     | /opt/dcc-validator/truststore.jks |
| truststore.password | Password of the truststore containing the trusted certificates that issues Digital covid certificates | password                          |
| ddvcverifier_url | URL of the WHO DDVC verifier                                                                          | http://localhost/verify           |

#### Configure WildFly 18 for DCC Validator

Install factories module

1. Stop wildfly and go to

   ```bash
   sudo mkdir -p /usr/local/wildfly18/modules/system/layers/base/net/ihe/gazelle/factories/main
   cd /usr/local/wildfly18/modules/system/layers/base/net/ihe/gazelle/factories/main
   ```

2. Download module `factories.jar`

   ```bash
   sudo wget https://gazelle.ihe.net/wildfly18/factories.jar
   ```

3. Create `module.xml` file

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <module xmlns="urn:jboss:module:1.1" name="net.ihe.gazelle.factories">
       <resources>
           <resource-root path="factories.jar"/>
       </resources>
       <dependencies>
           <module name="javax.api"/>
           <module name="javax.transaction.api"/>
           <module name="javax.servlet.api" optional="true"/>
       </dependencies>
   </module>
   ```

4. Add `jboss:jboss-admin` rights

   ```bash
   sudo chown jboss:jboss-admin *
   sudo chmod 775 *
   ```

5. Stop Wildfly and edit standalone.xml in `/usr/local/wildfly18/standalone/configuration` ; add the factories binding in the naming subsystem :

   Replace this property `${DEPLOYMENT_PROPERTIES}` with your own path to the deployment property:

   ```xml
   <subsystem xmlns="urn:jboss:domain:naming:2.0">
      <bindings>
         <object-factory name="java:/app/gazelle/dcc-validator/operational-preferences" module="net.ihe.gazelle.factories" class="net.ihe.gazelle.factories.PropertiesFactory">
            <environment>
               <property name="path" value="${DEPLOYMENT_PROPERTIES}"/>
            </environment>
         </object-factory>
      </bindings>
      <remote-naming/>
   </subsystem>
   ```
