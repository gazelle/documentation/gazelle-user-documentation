---
title:  Installation Manual
subtitle: Gazelle X Validator Rule Editor
author: Maxime ARIF
date: 19/03/2019
toolversion: 1.3.0
function: Engineer
version: 1.3.0
status: For review
reference: KER1-MAN-IHE-DEMOGRAPHIC_DATA_SERVER_INSTALLATION-1_00
customer: IHE-EUROPE
---
# Sources and package

Schematron-based Validator is a maven project. Sources are available on the IHE Subversion server at the following URL:

[*https://svn.ihe-europe.net/gazelle/Maven/GazelleXValidatorRuleEditor*](https://svn.ihe-europe.net/gazelle/Maven/GazelleXValidatorRuleEditor)

If you'd like to checkout the sources on your system you might want to use the following svn command, provided subversion is installed on your system.

```bash
svn checkout svn+ssh://svn.ihe-europe.net/gazelle/Maven/GazelleXValidatorRuleEditor/trunk
```

Note that the latest executable can be extracted from our nexus repository as well  at
[*Nexus Repository Manager*](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22GazelleXValidatorRuleEditor-ear%22).

**Important**, if you download the **ear** from Nexus it will have a name such as `GazelleXValidatorRuleEditor-ear-X.X.X.ear`, then be sure to rename it to `GazelleXValidatorRuleEditor.ear` otherwise the deployment will fail.

# Installation

This tool is to be installed in a Jboss 7.2 application server and runs with a PosgreSQL database.

If you do not have yet a Jboss installed in your environment, proceed as described in [General consideration page](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

## Database creation

Your database must have a user **gazelle**.
1. Download the SQL scripts archive from [*Nexus Repository Manager*](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22GazelleXValidatorRuleEditor-ear%22) (search for **DemographicDataServer-ear-X.X.X-sql.zip**).
2. Unzip the archive into a folder `GazelleXValidatorRuleEditor-sql`
3. Connect to your database
```bash
psql -U gazelle
```
4. Excute the SQL statement to create the database.
```sql
CREATE DATABASE "gazelle-x-validator-rule-editor" OWNER gazelle ENCODING UTF8 ;
```
5. Initialize database by running :
```bash
psql -U gazelle gazelle-x-validator-rule-editor < GazelleXValidatorRuleEditor-sql/init-x.x.x.sql
```

## Deployment
To deploy Gazelle X Validator Rule Editor:
1. Paste the archive **GazelleXValidatorRuleEditor.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`
2. Display JBoss server logs, start JBoss and wait for **ear** deployment.
3. The application can be browsed at http://localhost:8080/GazelleXValidatorRuleEditor. *Port could also be different whether you have modified the JBoss configuration*.
4. **WARNING** : From version 1.3.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file GazelleXValidatorRuleEditor-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : GazelleXValidatorRuleEditorDS

Database name : gazelle-x-validator-rule-editor

# Update from a previous version

1. Download the new **ear** and its associated update SQL script from our [*Nexus Repository Manager*](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22GazelleXValidatorRuleEditor-ear%22) (search for **GazelleXValidatorRuleEditor-X.X.X.ear** and **GazelleXValidatorRuleEditor-X.X.X-sql.zip**). Not each version has an update sql to execute.

1. Unzip the SQL archive

1. Execute SQL statements. Check that the database is correctly updated.

1. Deploy the new **ear**

**Due to the update mechanism of the database, it is important to not skip any version of the application in an overall update. You cannot go directly from 2.0.0 to 2.3.0, you will have to repeat the process from 2.0.0 to 2.0.1, 2.0.2, 2.0.3 and so on.**

# Configuration

| Preference name | Description | Default value |
|-----------------|-----------------------------------------|---------------------|
|application_url         |  URL to access the application  |  https://FQDN/GazelleXValidatorRuleEditor    |
|assertion_manager_url         |  URL to the assertion manager tool instance  |  https://FQDN/AssertionManagerGui   |
|documentation_url         |     Link to the user documentation                                   |  https://gazelle.ihe.net/gazelle-documentation/Gazelle-X-Validator-Rule-Editor/user.html                  |
|release_notes_url         |  Link to the issue reporter (JIRA) |  https://gazelle.ihe.net/jira/browse/XVALEDIT#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel   |
|svs_repository_url         |  URL to the svs repository instance |  -   |
|ut_directory         |  |  /opt/x_validation/ut_directory   |
|x_validation_editor         | Enable the edition of x validators |  true   |
|x_validation_enabled         | Enable the x validation module |  true   |
|x_validation_log_root_oid         | X val log root OID |  1.1.1.1.1.1.1.1.1.1.1.1.2.   |
|x_validation_report_xsl_url         |   |  https://FQDN/GazelleXValidatorRuleEditor/resources/stylesheet/report-mini.xsl   |
|x_validation_validator         | Enable the x validation |  true   |
|xvalidator_directory         |  |  /opt/x_validation   |
|x_validator_root_oid         | X val root OID |  1.1.1.1.1.1.1.1.1.1.1.1.1.   |
|xvalidator_xsl_url         |  |  https://FQDN/GazelleXValidatorRuleEditor/resources/stylesheet/gazelleXValidator.xsl   |

# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).