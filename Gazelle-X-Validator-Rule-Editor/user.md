---
title:  User Manual
subtitle: Gazelle X Validator Rule Editor
author: Anne-Gaëlle BERGE
function: Engineer
date: 07/07/2017
toolversion: 1.0.x
version: 1_02
status: Reviewed
reference: KER1-MAN-IHE-GAZELLE_X_VALIDATOR_RULE_EDITOR_USER-1_02
customer: IHE-EUROPE
---     

# Introduction

The Gazelle X Validator module has been developed in order to be able to check the consistency of a document and/or a message in the context in which it has been produced. That means that we are not going to check the conformance of the message syntax and content against the specifications and underlying standards, conformance validation tools exist for this purpose. In the contrary, the Gazelle X Validator takes at least two files (documents, messages) and checks that they respect a set of rules which have been clearly defined, using one file as a reference to check the correctness of the others.

Checks can be multiple and for instance we are verifying that (non-exhaustive list) :

*   A dispensation is correct regarding the related prescription
*   The XDS meta data correctly reflects the properties of the shared CDA
*   Informations provided in the query are correctly copied in the response
*   The response contains the information the request was designed to ask for

## Purpose of the document

This document is the user manual for the Gazelle X (Cross) Validator module.

## Scope of the document

The scope of this document is limited to the usage of the several tools in which the Gazelle X Validator module has been introduced :

* Gazelle X Validator Rule Editor is the application dedicated to the IHE-Europe team (in a first time) to develop and maintain the various X validator.
* EVS Client is the front-end to the validation services offered by the Gazelle platform; it embeds the validator part of the Gazelle X Validator module (in its basic configuration)
* gazelle-x-validation-service is the command-line version of the Gazelle X Validator.

How to install, configure and maintain the tool is described in the Installation manual.

# Definitions

In this section we introduce the various concepts used in the tool and in the user manual.

## Gazelle X Validator

The purpose of the Gazelle X Validator is to validate two or more files against each other to verify the consistency of the whole corpus. The Gazelle X Validator tool is an engine which processes a set or well-defined rules on a list of inputs. A set of rules which applies on the same corpus of files and the purpose of which is to verify the consistency of the corpus in a given context is also called a Gazelle X Validator.

To distinguish the engine and the set or rules, we will talk about

* Gazelle X Validator tool : the engine
* Gazelle X Validator or simply validator for the set or rules.

## Referenced objects and inputs

The referenced objects and inputs are the files which are being verified by the validator. In other words, the validator is designed to check the consistency between at least two files, each of them is called an input or a referenced object. Currently supported inputs are CDA document and any other XML-based files as well as HL7v2 messages and DICOM objects. In a future release, JSON documents are REST requests might be supported.

## Rule

A rule is the expression of a constraint and is designed to cover one or several assertions. A set of rules defines a validator. In most of the cases, the outcome of a rule is either SUCCESS or FAILURE which indicates if the constraint is respected or not. Sometimes, the execution of the rule can result in the ABORTED status for various reasons; this indicates that the execution exited in a abnormal way.
The level of the rule (ERROR, WARNING, INFO) indicates the degree of inconsistency when the execution of the rule results in a failure. The final status of the validation directly depends on the number of critical rules (level = ERROR) which failed.

## Validator

A validator is a set of rules which are designed to check the consistency of a corpus of files (documents, messages). The inputs and the purposes of the validators are well-known and it allows to validate the inputs in a well-defined context. The outcome of the execution of a validator is PASSED, FAILED or ABORTED. The PASSED status indicates that none of the critical rules failed, but other rules may have failed or been aborted.

## Expression

An expression is the part of the rule which is evaluated. It is based on Xpath expressions and specifically designed operators. Each member of the expression is an Xpath which is evaluated separately and then, the outcome of each Xpath is used as input of the designed operator. Operators can be combined to create complex expressions.

The outcome of an expression is either true or false. In sometimes, it can result on an exception and in those cases, the rule is reported as aborted. Each rule contains exactly one expression.

Below are explained the various operators we have designed for the needs of the cross validation tool.

### Logical operators : AND, OR, NOT and XOR

Logical operators are used to perform logical operations between expressions. The Not operator uses a single expression as input whereas the others use a list of expressions.

**Examples**

-   Not exp returns exp

-   AND {exp1, exp2, exp3} returns exp1 . exp2 . exp3

-   OR {exp1, exp2, exp3} returns exp1 + exp2 + exp3

-   XOR {exp1, exp2, exp3} returns exp1 XOR exp2 XOR exp3

### Basic operators : EQUAL, CONTAINS, IN, SIZE EQUAL, SIZE GREATER OR EQUAL, SIZE LESS OR EQUAL

Basic operators are used to compare the outcome of exactly two Xpath; this outcome can be a list of XML nodes, a boolean or a string but in those three cases, we choose to use a list of XML nodes.

First, that is to be noted that two nodes are equal if and only if:

-   root elements are equals

-   child elements are equals which means that elements, attributes' names and values are equals

Six expressions are part of the set of basic operators:

-   **IN** returns true if and only if all the nodes from the first list are present in the second one. The second list can have more elements.

-   **CONTAINS** returns true if and only if all the nodes from the second list are present in the first one. The first one can have more elements.

-   **EQUAL** returns true if and only if the two list of nodes are equals. That means that the first list is IN the second one and the first list contains the second one. Both list have exactly the same number of nodes.

-   **SIZE EQUAL** returns true if both lists contain the same number of nodes

-   **SIZE GREATER OR EQUAL** returns true if the first list contains the same number as or more nodes than the second one

-   **SIZE LESS OR EQUAL** returns true if the first list contains the same number as or less nodes than the second one.

### True

This expression returns true if the enclosed Xpath returns true.

### Present

This expression returns true if the enclosed Xpath returns at least one node.

### For Each

This expression encloses an Xpath and an expression. For each node returns by the Xpath, the expression is evaluated and their outcomes combined with a logical and. That means that the For Each expression returns true if, for all the selected nodes, the enclosed expression returns true. If the Xpath does not select something, the expression aborts.

### In Value Set

This expression encloses an Xpath, the OID of a value set and optionally a language. The In Value Set expression returns true if the Xpath selects a String and that the content of this string is a code present in the given value set. If the Xpath does not select something or if the value set is not reachable, the expression aborts.

### Math comparison

It is used to compare the outcome of exactly two Xpath, this outcome shall be an integer. The math comparison can be **equals**, **less than**, **less or equal**, **greater than** or **greater or equal**.

# One module, several usages

## Gazelle-x-validation

The gazelle-x-validation is a Java module which inherits of the gazelle-tools module as most of the other Gazelle artefacts. It contains the validation engine, the model of a the validator as well as the model of the report. In addition, it contains all the GUI features specific to this tool.

It is an independent module which can be easily integrated into another Gazelle tool.

The module offers three main features:

-   Development and maintenance of validators

-   Access to the documentation

-   Validation itself

### Sources and binaries

The sources are available on the INRIA GitLab in the Gazelle project : [*https://gitlab.inria.fr/gazelle/library/gazelle-x-validation*](https://gitlab.inria.fr/gazelle/library/gazelle-x-validation)

Released binaries are available in the Gazelle Nexus repository at : [*http://gazelle.ihe.net/nexus/index.html\#nexus-search;gav~~gazelle-x-validation\**](http://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~gazelle-x-validation*).

### Issue tracker

Issues and features requests shall be entered through the issue bug tracking system of the Gazelle platform, at [*http://gazelle.ihe.net/jira/browse/XVAL*](http://gazelle.ihe.net/jira/browse/XVAL).

## EVS Client

The EVS Client is the front-end to the validation services offered by the Gazelle platform. It embeds the gazelle-x-validation module from its version 4.5.0.

### Configurations

The EVS Client includes the gazelle-x-validation module but can be configured in two ways described below.

#### EVS Client as validator

When only the validator part of the gazelle-x-validation module is enabled, creation and edition of validators and rules is not allowed. Administrators shall make use of the import/export validators feature to add and update validators.

#### EVS Client as validator and editor

The EVS Client can also be used as validator and editor. In that case, the administrators and the test editor are allowed to create and manage validators as described in section 7 .

### Sources and binaries

The sources are available on Inria's Gitlab in the Gazelle project : [*https://gitlab.inria.fr/gazelle/applications/test-execution/validator/EVSClient](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/EVSClient)

Released binaries are available in the Gazelle Nexus repository at [*http://gazelle.ihe.net/nexus/index.html\#nexus-search;gav~~EVSClient\**](http://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~EVSClient*).

### Issue tracker

For an issue specific to the gazelle-x-validation module, please use the issue tracker at [*http://gazelle.ihe.net/jira/browse/XVAL*](http://gazelle.ihe.net/jira/browse/XVAL). For an issue specific to the EVS Client, you can use [*http://gazelle.ihe.net/jira/browse/EVSCLT*](http://gazelle.ihe.net/jira/browse/EVSCLT).

### Access the application

The instance of EVS Client maintained by the Gazelle team is available at [*http://gazelle.ihe.net/EVSClient*](http://gazelle.ihe.net/EVSClient).

## Gazelle X Validator Rule Editor

The Gazelle X Validator Rule Editor has been designed to be used, mainly and in a first time, by the Gazelle development team. This tool is dedicated to the design and maintenance of the different validators the team will have to produce. The validator and the documentation sections are still available but the access to the whole tool is restricted to the administrator and test editor users. Our purpose, when developing this tool, was to be able to gather all the validators in a same place and to maintain them whereas they are in used in one or several instances of EVS Client. The Export/Import feature will allow us to share the validators with the several instances of EVS Client and to easily update them.

This application is plugged to the CAS service of EU-CAT and has it own home page and application preferences.

### Sources and binaries

The sources are available on Inria's Forge in the Gazelle project : [*https://svn.ihe-europe.net/gazelle/Maven/GazelleXValidatorRuleEditor*](https://svn.ihe-europe.net/gazelle/Maven/GazelleXValidatorRuleEditor)

### Issue tracker

For an issue specific to the gazelle-x-validation module, please use the issue tracker at [*http://gazelle.ihe.net/jira/browse/XVAL*](http://gazelle.ihe.net/jira/browse/XVAL). For an issue specific to the Gazelle X Validator Rule Editor, you can use [*http://gazelle.ihe.net/jira/browse/XVALEDIT*](http://gazelle.ihe.net/jira/browse/XVALEDIT).

### Access the application

The instance of the Gazelle X Validator Rule maintained by the Gazelle team is available at [*http://gazelle.ihe.net/GazelleXValidatorRuleEditor*](http://gazelle.ihe.net/GazelleXValidatorRuleEditor).

## gazelle-x-validation-service and other command-line utilities

Integrated in a web application, the validator is accessible through a graphical user interface. A web service will also be available in a future release. To allow the use of the Gazelle X Validation in an integration testing process, a ZIP file is produced when the module is packaged. This archive contains three command-line utilities which are documented in the next sections.

The ZIP file (gazelle-x-validation-core-VERSION-bin.zip) is available at [*http://gazelle.ihe.net/jenkins/job/gazelle-x-validation/ws/gazelle-x-validation-core/target*](http://gazelle.ihe.net/jenkins/job/gazelle-x-validation/ws/gazelle-x-validation-core/target).

### gazelle-x-validation-service

This first utility is the validation engine. It validates a set of input based on the validator provided as argument of the command-line. Actually, the validators are exportable as XML files and those ones can be used as input of the command-line utility. No rule is defined in the gazelle-x-validation-service.

#### Usage

Running `$> ./gazelle-x-validation-service` will output the following manual .


Usage:

~~~bash
$> gazelle-x-validation-service -validator yourValidator
\[-files filesToValidate (keyword=path)\]
\[-output reportPath\]
-files <arg> path to the files required for the validation, separated by coma, and formatted as keyword=path
-help print this message
-output <arg> path to the XML file to save the report
-validator <arg> path to the XML file representing the Gazelle Cross
~~~

There are several ways to use this utility; in all the cases, you are required to provide the path to the XML file representing the validator to execute. Note that this file will always be validated against the XSD schema embedded in the tool. This check is necessary to assure that the execution of the rules will not result in an abnormal exit.

If you only provide the path to the validator, the program will parse the validator and ask you for the absolute paths to the expected files.

If you provide the list of input files, before asking you for a file, the program will look in the argument if one file is declared with the expected keyword.

If you do not provide a path to store the XML report, it will be printed out on the standard output.

#### Examples

Only -validator argument is provided :

~~~bash
$> ./gazelle-x-validation-service -validator validateDicomWorklist.xml
~~~

Output:

~~~bash
INFO : validating validateDicomWorklist.xml against XValidator.xsd

INFO : ... OK

INFO : Enter the path to this file : 'a modality worklist query':

MWLQuery.dcm

\# user manually enters the path to the file describe in the validator file

INFO : Enter the path to this file : 'a modality worklist':

dicomWorklist.wl

\# user manually enters the path to the file describe in the validator file

\# below is the output of the validator, the XML report
~~~

~~~xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<GazelleCrossValidationReport>
<ReportSummary>
<GazelleCrossValidator engineVersion="1.0.0-SNAPSHOT">
<CalledValidator affinityDomain="DUMMY" version="1.0.0" name="Dicom Worklist Validator"/>
</GazelleCrossValidator>
<ValidationDate>2014-11-28T16:55:16.108+01:00</ValidationDate>
<ValidationResult>PASSED</ValidationResult>
<ValidationCounters>
<nrOfChecks>1</nrOfChecks>
<NrOfValidationErrors>0</NrOfValidationErrors>
<NrOfValidationWarnings>0</NrOfValidationWarnings>
<NrOfValidationInfos>0</NrOfValidationInfos>
<NrOfValidationReports>0</NrOfValidationReports>
<NrOfValidationUnknown>0</NrOfValidationUnknown>
<NrOfValidationAborted>1</NrOfValidationAborted>
</ValidationCounters>
</ReportSummary>
<Notifications>
<Aborted>
<Test>mwl1</Test>
<ProcessedInput>MWLQuery::/DicomObject/PatientName\[1\]/value\[1\]</ProcessedInput>
<Description>Queried patient name is present in the response</Description>
<Expression>MWLQuery::/DicomObject/PatientName\[1\]/value\[1\] = MWL::/DicomObject/PatientName\[1\]/value\[1\]</Expression>
<reason>This XPath has not returned any element</reason>
</Aborted>
</Notifications>
</GazelleCrossValidationReport>
~~~

For an non-interactive execution of the utility, you can simply run the following command :

```bash
$> ./gazelle-x-validation-service -validator validateDicomWorklist.xml -files MWLQuery=MWLQuery.dcm,MWL=dicomWorklist.wl -output /tmp/myXValidationReport.xml
```

where MWLQUERY is the keyword given to the file with description 'a modality worklist query' and MWL the keyword of the file with description 'a modality worklist'. Those informations are available in the XML file representing the validator under the ValidatedObject elements. The final report will be output in a file stored at /tmp/myXValidationReport.xml.

### Gazelle-x-validation-xmldump

The Gazelle X Validator engine is based on XPath 2.0 which means that the tool manipulates XML files. As previously mentionned, we are able to validate CDA and XML-based files but also HL7v2 messages and DICOM object. This is feasible by using an XML dump of those documents. The most common encoding for HL7v2 messages in ER7, using hapi library, the tool is able to convert them to an XML structure. For the DICOM object, the tool offers two alternatives : the DICOM to XML library from dcm4chee or the one from pixelmed.

To help you with

-   Debugging your files

-   Developing rules

We have exposed the methods used to perform the conversions in a command-line utility named gazelle-x-validation-xmldump.

#### Usage

Running `$> ./gazelle-x-validation-xmldump` will produce the following output

```bash
Usage : gazelle-x-validation-xmldump filepath HL7v2|dcm4che2|pixelmed
```

Two arguments are required : the path to the file to be processed and the type of dump to perform. The XML version of your file will be printed out on the standard output.

### Gazelle-x-validation-expression-converter

You can convert the expressions enclosed in the Gazelle X Validator (as XML structure) into textual version of the expression. Simply run:

```bash
$> ./gazelle-x-validation-expression-converter myValidator.xml
```

# Documentation

In Gazelle X Validator Rule Editor, access the documentation from menu Validation → X Validator Documentation. Only users with administrator and test\_editor roles can access this section of the tool.

In EVS Client, access the documentation from menu Add-ons → Gazelle X Validator → Documentation.

Illustrations are screen captures of the Gazelle X Validator Rule Editor application. The theme might be different if you are using the EVS Client, anyway the features and the text of the buttons and titles remain the same.

## Content

The documentation section of the Gazelle X Validator gives access to the validators which have been marked as “Available for testing”; they are the validators which are also available in the Validator part. That means that they are ready to be used. The validation reports contain links to this section of the tool.

The validation reports contain a lot of informations:

-   About the validator : its version, name, affinity domain (context) and description

-   About the rules: their version, keyword, level and the textual version of the expression which was evaluated at runtime. In addition, the list of assertions covered by each rule is also provided.

Browsing the documentation will not give you more informations but it can be easier to navigate through this GUI than in the report.

## Browsing

### List of validators

When you access the documentation, the list of available validators is first displayed. You can use the Search criteria panel to restrain the list of validators to the one which are of interest for you.

For each validator, you can see when it was modified the last time and by who.

![](./media/image3.png)
*Illustration 1: Gazelle X Validator - Documentation home page*

As you can see in Illustration 1: Gazelle X Validator - Documentation home page above, from this page, and by the "Acces" button, you can :

-   Access the list of rules

-   Access the list of referenced objects

-   Access the details of a given validator (magnifying-glass icon)

Hitting the “Reset criteria” button available in the first panel will clean up the all the criteria and thus display the entire list of validators.

### Validator's details

The validator's details page is accessed by clicking on the magnifying-glass. For each validator, the page is divided into several sections.

The general information panel provides the name of the validator, the affinity domain in which it applies, its version, a brief summary and a permanent link to make a static reference to this page.

![](./media/image4.png)
*Illustration 2: Gazelle X Validator - Validator documentation (general informations)*

Then the list of inputs expected by the validator, that means the files which will be validated are listed in the Inputs section.

![](./media/image5.png)
*Illustration 3: Gazelle X Validator - Validator documentation (inputs)*

Some information about the configuration to use at validation and reporting time are mentionned:

-   The Assertion Manager URL tells the tool to which instance of Assertion Manager the user shall be redirected when he/she wants to access the details about an assertion covered by this validator

-   The declared namespaces is the list of prefix/uri to be used when executed Xpath expressions

-   The DICOM to XML libraries indicates which parser is used to transform the DICOM object into an XML file

-   The SVS Repository indicates to the tool and to the user where to find the value sets if some rules are requested to check the existance of a code in a given value set. If none is specified, the one defined by default in the tool is used. Command-line utility uses, by default, the instance of SVS Simulator hosted by the Gazelle team.

![](./media/image6.png)
*Illustration 4: Gazelle X Validator - Validator documentation (configuration)*

Then, the list of active rules (that means, the one which are effectively executed in this version of the validator) are listed and you can access the details of each of them by hitting the magnifying-glass icon. In the next illustration, we can see that three rules are executed.

![](./media/image7.png)
*Illustration 5: Gazelle X Validator - Validator documentation (executed rules)*

Finally, the latest panel, entitled “Covered assertions” lists all the assertions which are covered by this validator. This list is obtains by retrieving the list of assertions covered by each rule linked to this validator.

### List of rules

The rules listed in the documentation sections are active and owned by validator the status of which is “available for testing”. You can access this list of rules by clicking on the “Access rules” button which is available on the home page of the documentation section (where validators are listed).

This page is divided into three sections. First, the search criteria are displayed and allow the user to restrain his/her search. Then, from the "Acces" button, you can access to the other sections of the documentation : the validators and the referenced objects. Finally, the list of rules, filtered according the above criteria is displayed. For each rule, the magnifying-glass gives you access to the details of the rule.

![](./media/image8.png)
*Illustration 6: Gazelle X Validator - Documentation - All rules*

### Rules' details

The general information panel provides the keyword of the rule (unique through the application), its version, a brief summary, its severity and status and the link to the validator which owns this rule. In addition are mentioned the username of the person who performed the last change and when he/she did it. Finally, the textual version of the expression is displayed to allow you to understand what is tested by this rule.

![](./media/image9.png)
*Illustration 7: Gazelle X Validator - Rule documentation (general information)*

Other sections describe the inputs of the rule and the configuration of the validator. If assertions are linked to this rule, they are also displayed.

![](./media/image10.png)
*Illustration 8: Gazelle X Validator - Rule documentation (inputs)*

For each input, you can acess its details using the magnifying-glass.

![](./media/image11.png)
*Illustration 9: Gazelle X Validator - Rule documentation (Configuration)*

For details about the meaning of the different configuration's parameters, refer to the following section: **Validator's details** .

### List of referenced objects

You will access the list of referenced objects in two ways :

-   From the list of rules

-   From the list of validators

In both cases, you have to hit the “Referenced objects” button under the "Acces" button.

![](./media/image12.png)
*Illustration 10: Gazelle X Validator - Documentation - All referenced objects*

The search criteria panel allows you to search a referenced object by object type. For each referenced objects, you can access its details using the magnifying-glass icon.

### Referenced objects' details

The details panel indicates the keyword of the referenced object as well as the object type and a short description. In some cases, the link to the XML schema which will be used to check the validity of the object is provided.

![](./media/image13.png)
*Illustration 11: Gazelle X Validator - Referenced object documentation*

In addition, this page lists all the validators which make use of this object.

# Validating a set of files

Both the EVSClient and the Gazelle X Validator Rule Editor applications offer a graphical user interface which allows the user to validate a corpus of file with a chosen validator.

In EVSClient, this page is accessible (if the X Validation is enabled) via menu Add-Ons → Gazelle Cross Validator → Validator.

In Gazelle X Validator Rule Editor, this page is accessible by administrators and test editors via menu Validation → Gazelle X Validator.

## Validation

First you are asked to select a validator. For each validator, the affinity domain in which it applies appear first.

![](./media/image14.png)
*Illustration 12: Gazelle X Validator - Select a validator*

Once you have selected a validator, a table is displayed which gathers the list of files requested by the validator.

![](./media/image15.png)
*Illustration 13: Gazelle X Validator - Upload files*

As an example, in the above illustration, you are requested to provide four XML files. The two firt are AuditMessages, the third is a request and the last is a response  (all the four are linked). We will check that the elements from the documents are consistent one to another.

To upload the requested files, you use the buttons which are located in each row.

Once all the files are upload, you can click on the “Validate” button, the report will be displayed at the bottom of the page. If you want to clean up the entire form, use the “reset” button.

## Report

The validation report is made of several sections:

-   A summary with the unique OID of the validation report, a permanent link to this report and so on

-   The validator description: which validator was applied

-   The validator summary: when was the validation performed, what is its outcome

-   The XSD Validation (optional): Are the XML files valid, if not, what are the errors found

-   The Validation details: for each rule, its outcome, severity, textual expression, inputs

### Validation details

In the validation details section, all the rules which have been executed are listed and sorted by outcome. First the rules which resulted in an error (rules with Severity = Error which failed), then the ones which resulted in a warning (rules with Severity = Warning which failed), the Info (rules with Severity = Info which failed) and then the report (all the rules which outcome is PASSED). Finally, if some rules aborted in an abnormal way, they are listed under the Aborted label.

For the Error, Warning, Info and Report case, the files which have been processed by this rule are mentioned, the description of the rule is given and will indicate what was expected. Finally, the expression which was evaluated is displayed.

![](./media/image17.png)
*Illustration 14: Gazelle X Validator - Sample report*

In the case of the aborted rule, the tool gives you more details about the reason of the exception and may also give you the Xpath which caused the failure.

### Actions

From the validation report page you can perform the following actions:

-   Download the processed files

-   Download the XML report

-   Mark result as private: if you are logged in, you can ask the tool to hide your report from other users (administrators and monitors will still be able to access it)

-   Mark result as public: if you have marked your result as private you can turn it back to public

-   Share this result: if your report is private, you can generate a privacy key and share the permanent link to others; only persons knowing the full link will be able to access the report (in addition of yourself, the administrators and the monitors).

## Logs

All the validation perform with the tool are logged in its database for a later retrieval.

In EVSClient, you will access this page from menu Add-ons → Gazelle X Validator → Validation logs.

In Gazelle X Validator Rule Editor, you will access this page from menu Validation → Validation logs.

The list of logs contains

-   if you are administrator or monitor: the whole list of logs

-   if you are not logged in: the list of public logs; that means the logs of validation performed in an anonymous way or marked public by the logged in users

-   if you are logged in: the list of public logs and the logs you have performed yourself

Search criteria are available to help you with filtering the list.

![](./media/image18.png)
*Illustration 15: Gazelle X Validation - Validation logs*

If you want to access the details of a particular log, click on the OID (first column) it will lead you to the permanent page for this log.

# Gazelle X Validator Rule Editor

## Life cycle of validators and rules

Versioning of the testing methods is very important. As a consequence, both the validator and the rule have a version attribute; the one shall be correctly incremented each time it is necessary. Actually, we need to be able to reproduce the result of a validation, that means that we need to know the exact content of a validator at a point of time. Both the validators and the rules have a proper life cycle, the ones are linked.

### Validator statuses

A Validator can have the following statuses:

-   Under development: The validator has been created, rule are being developed and it has never been used for testing (in production)

-   Available for testing: The validator is ready to be used in production, all tests have been performed and we are confident about its correctness. While the validator remains in this state it cannot be edited.

-   Maintenance: The validator is available for testing but some errors or some CPs request changes. While the validator remains in the state, it can be edited but version of rules shall be increased when edited.

-   Deprecated: The validator is no more pertinent and has been removed from our pool of validators.

![](./media/image19.png)   
*Illustration 16: Gazelle X Validator - Validator state diagram*

For some of the transitions, actions are triggered:

-   Under development → available for testing : User is asked to check the version of the validator

-   Available for testing → maintenance: the current version of the validator is back up (see archived validators) and -um is appended to the version number.

-   Maintenance → available for testing : user is asked to provide a new version number for the validator.

If you switch a validator to the deprecated status by mistake, you will be able to revert your changes but it is not the normal work-flow.

### Rule statuses

A rule can have the following statuses:

-   Inactive: the rule is not ready or under maintenance. It will not be executed even if the validator which owns it is called

-   Active: the rule is ready to be executed and will be included in the validator until its status is turned to Inactive or Deprecated.

-   Deprecated: this rule is no more pertinent, we keep it because it might have been referenced in some reports.

![](./media/image20.png)    
*Illustration 17: Gazelle X Validator - Rule state diagram*

For some of the transitions, actions are triggered:

-   Inactive → active : user is asked to check or update the version of the rule

-   active → inactive : the validator may be backup (look at the whole life cycle) and -um is appended at the end of the version number.

### Complete life cycle and triggered actions

**Creation**

At creation time, the status of the validator is set to “Under development”; the status of the rule is set to “Inactive”.

While the validator remains in this state, the user is allowed to edit it (including identification attributes: name, affinity domain, version) and to add, remove, edit the rules it owns.

**Availability**

Changing of a validator from “Under development” to “Available'” is permitted only if the validator owns at least one active rule.

A rule can be marked as “Active” only if it owns a correct expression. An expression is correct if all its members are correctly set.

While a rule is “active” or a validator is “available”, the access to the edit page for this entity is forbidden. In other words, there is no way to bypass the work-flow.

**Maintenance**

If the user wants to update an “Available” validator or an “Active” rule, then some actions are automatically triggered depending the cases.

-   Click on the “update validator” button:

 1. The current version of the validator is archived

 2. The status of the validator is set to “maintenance”

 3. Its version is updated from xxx to xxx-um

-   Click on the “edit rule” button:

  1.  If the validator is “available” then, the previous actions are applied

  2.  In addition, the status of the rule is marked “Inactive”

  3.  Its version is updated from xxx to xxx-um

-   Mark a rule from inactive to active

  1.  User is asked to provide a new version number

  2.  Status is set back to “active”

-   Mark a validator from maintenance to available

  1.  User is asked to provide a new version number

  2.  Status is set back to “available”

It is to be noted that the deletion of a rule (rule is marked as deprecated), triggers the change of status of the validator from available to maintenance.

**Deprecated**

When a validator or a rule is deprecated, it is not possible to edit it any more. However, since the user can mark a validator as deprecated by mistake, a button allow the user to revert its status.

## Validator

### Create a new validator

You can create a new validator from page Manage X Validators. Simply hit the “Create a new validator” button.

If you have the XML version of a validator that you want to add to the tool, please refer to section **Import/Export features**.

First, you are asked to give the pieces of information which describe the validator; that means its name, the affinity domain in which it will apply, is version and a brief description to inform the user about the purpose of the validator. All the fields with a star (\*) are mandatory. If one of them is missing, you will not be able to go further. Once all the fields are populated, click on the “Save” button.

Until you mark the validator available, you are allowed to modify those fields.

![](./media/image21.png)
*Illustration 18: Gazelle X Validator - New validator - Step 1*

After saving, new panels are displayed at the bottom of the page. They will allow you to

-   Indicate if the validator inherits rules from another one

-   detail the configuration of the validator

-   define the inputs expected by the validator

-   define the rules

#### Inheritance

If you want your validator to call the rules from another validator, use the inheritance panel. Each time you select a validator, the tool checks that the inputs defined in the selected validator are declared in the one you are editing; if it is not the case, the missing inputs are added.

![](./media/image22.png)
*Illustration 19: Gazelle X Validator - New validator - Step 2*

Clicking on the red trash beside the name of the validator will delete the link to this validator. Notice that relative inputs are not removed.

![](./media/image23.png)
*Illustration 20: Gazelle X Validator - New validator - remove inheritance*

Validator is automatically saved after every action.

#### Configuration

For the detail of each fields of the Validator configuration panel, please refer to section **Validator's details**.

![](./media/image24.png)
*Illustration 21: Gazelle X Validator - New validator - Step 3*

The default URL of Assertion Manager and the one of the SVS Repository are retrieved from the preferences of the application. You can edit them to override them.

The XML namespaces are mandatory by SAX to correctly execute the Xpath you will define in the rules. The URI which have already been used in the tool are suggested but you can use another one.

If a namespace has been added by mistake or is no more needed, you can delete it by clicking on the red trash.

Once you are happy with the configuration parameters, do not forget to click on the “Save” button.

#### Validator inputs

This panel listed all the files which will be processed by your validator. This table is also shown to the user when he/she wants to perform a validation. As a consequence, you shall be as precise as possible to allow the user to understand which files he/she have to upload.

![](./media/image25.png)
*Illustration 22: Gazelle X Validator - New validator - Step 4*

You can remove an input from the list by hitting the red trash and access the details of the referenced object by clicking on the magnifying-glass icon.

To add an input to this list, click on the “Add an input button”. From this point, you have to choices:

-   The referenced object already exists and you select it from the list

-   The referenced object has not been defined yet and you hit the “Create a new referenced object” button to display a new form which will allow you to describe it.

![](./media/image26.png)
*Illustration 23: Gazelle X Validator - New validator - Step 4 (select existing referenced object)*

The cardinality of the input is required. If the user is allowed to upload as many files as he/she wants, use cardinality max = -1.
It is also possible to make an input optional by setting the minimum cardinality to 0 : the service will not expect a file for this input to launch cross validation.

![](./media/image27.png)
*Illustration 24: Gazelle X Validator - New validator - Step 5 (create a new referenced object)*

In both cases, click on the “Add” button will add the input to the list and automatically saved the changes.

#### Rules

The Rules panel lists all the rules owned by the validator whatever their status is.

![](./media/image28.png)
*Illustration 25: Gazelle X validator - New validator - Step 5*

For each rule, you can view its details (the magnifying-glass icon), edit it or delete it (rule will be marked as deprecated).

To add a new rule, simply hit the “Add a rule” button and follow the instructions given in section **Create a new rule**.

### Edit a validator

You can edit a validator either from the Manage X Validators page.

![](./media/image29.png)
*Illustration 26: Gazelle X Validator - Extract from Manage X Validators page*

The above pictures show you four cases:

-   Validator is available: clicking on the edit button will trigger the maintenance process of the validator and will allow you to perform changes

-   Validator is under maintenance: clicking on the edit button will allow you to perform the changes, attributes relative to the identification of the validator cannot be changed

-   Validator is under development: clicking on the edit button will allow you to perform any kind of changes

-   Validator is deprecated: you are no more allowed to edit it. If you marked it as deprecated by mistake, the reload icon allows you to revert the changes.

## Referenced objects

### Create a referenced object

The creation of referenced object is performed from the edition page of a validator, refer to section **Validator inputs** for details.

### Update a referenced object

From the “Manage referenced objects” page, clicking on the edit icon will display the page to edit a referenced object. Only its description and the location of the XML schema can be modified.

![](./media/image30.png)
*Illustration 27: Gazelle X Validator - Edit referenced object*

## Rule

### Create a new rule

The page to create a new rule is available from the “Edit X Validator” page. You are not allowed to create a rule outside a validator. As a consequence, to create a new rule for a given validator, edit the validator and in the “Rules” panel, hit the “Add a rule” button. This button leads you to a new page, the rule is already linked to the validator (see Gazelle Cross Validator field at the bottom of the panel).

All the fields displayed in the “Rule details” panel are mandatory. Once you have filled them out, click on the “Save” button to continue.

![](./media/image31.png)
*Illustration 28: Gazelle X Validator - New rule - Step 1*

If the validator is under development, you will be able to update those fields until the validator is marked as available for testing.

If the validator is under maintenance, you are only allowed to update the description field. This is done on purpose; actually, the validation report refers to a rule by its keyword, if the keyword is modified, a user will not be able to retrieve the information about a rule run during a previous execution of the validator.

#### Inputs and configuration

For information purpose, the list of inputs declared at the validator level as well as its configuration are displayed.

#### Covered assertions

The purpose of the “covered assertions” panel is to indicate to the tool what are the assertions which are covered by this rule.

The assertions which are already linked are listed. The hyperlink leads to the description of the assertion in Assertion Manager. You can unlink them by clicking on the red trash icon.

To add a new one, fill out the scheme ID field and hit the “add” button. Schemes and assertion Ids which have already been used in the tool are displayed in a drop-down menu so that you can avoid typo.

![](./media/image33.png)                      
*Illustration 29: Gazelle X Validator - New rule - Step 2 (add assertion)*

#### Expression

The expression is the part of the rule which is executed by the tool and which outcome gives indication about the correctness of the processed files.

After saving your rule for the first time, you will notice that a new field appears, entitled Textual expression. This is the place where the human-readable version of the expression is displayed. To create/update the expression, click on the “Edit expression” button; one panel will be displayed under the configuration one.

![](./media/image34.png)    
*Illustration 30: Gazelle X Validator - New rule - Step 3*


The working board panel is where you will be able to create new expression and arrange them together to build your final expression.

![](./media/image35.png)
*Illustration 31: Gazelle X Validator - New rule - Step 4 (working board)*

As shown on the above illustration, this panel is divided into two secondary panel. Above, the expressions you are building, below the list of expression type you can create.

To add an expression to the working board either

-   click on one of the "Create expression" button

-   edit the "Applies on" and "XPath" fields and click on the validate button.

The expressions in the drop-down comes from the other rules owned by the validator. It allows you to reuse some members of an expression in a new one. It will save you time and avoid mistakes and typos.

Depending the type of expression you have selected, you are requested to fill out different fields. For the NOT, AND, OR, XOR and FOR EACH expressions, you need to drag and drop other expressions so that they will be combined.

When the expression and the rule are saved, a couple of checks are performed and you might be informed by the tool that your expression is not correct. Below are the constraints you have to respect:

-   “Applies on” field shall never be empty

-   “Xpath” field shall never be empty

-   variable's name of the For Each expression shall never be empty

-   Not and For Each expressions shall always have exactly one child expression

-   And, Or and Xor expressions shall always have at least two child expressions

-   Value Set OID attribute of the In Value Set expression is mandatory and shall not be empty

Finally, you can delete an expression by clicking on the red cross in its panel's header.

### Edit a rule

You can edit a rule from the validator edition page, from the Manage rules page, or from the documentation page of the rule.

![](./media/image37.png)
*Illustration 32: Gazelle X Validator - Extract from the Manage rules page*

In all cases, a pop-up will be displayed and asked you to confirm your choice. It can appear that the tool notify you that the validator owning the rule will enter in a maintenance process. Refer to section **Life cycle of validators and rules**.

You can notice that you are not allowed to edit a deprecated rule.

### Unit test of rules

As a guidance when you developed rules, a feature allows you to test your rule unitary. That means that only the rule is executed not the whole set owned by the validator.

Basically, you feed the tool with the inputs expected by the rule and the tool executes the enclosed expression on those inputs. As you may want to both correct and error cases, you notify the tool of the outcome to expect. Before returning the status of the test, the tool compare the effective result to the expected one.

The files and the unit tests are stored by the tool so that you define them only once and you run it each time you feel it is necessary.

At the button of the edition page of a rule, you will find a panel entitled “Unit tests”.

For each unit test, the result of its last execution is reported as well as the date/time of this last execution. The timestamp of the last successful run is also displayed as long as the reason for the failure (if last result is failed). In addition, the expected result is shown so that you can evaluate if some unit tests are missing.

#### Create a new unit test

To create a new unit test, push the “Create a new unit test” button, you will be led to a new page.

On that page, the details of the rule are put as a remember. Then, in the “Unit test details” panel, you are asked to provide a keyword for that test as well as the result which is expected. In addition, you can put a comment as a remember of the purpose of this test.

![](./media/image39.png)
*Illustration 33: Gazelle X Validator - New Unit test - Step 1*

When you are done with those details, hit the “Save” button. Finally, upload the requested files from the “Unit test inputs” panel.

To add a file, click on the “Add a file” button, the list of inputs used by the rule are displayed, choose one (upload file icon).

![](./media/image40.png)
*Illustration 34: Gazelle X Validator - New Unit test - Step 2*

Upload a file and eventually put a comment (maybe you want to be reminded of where the error is in this file or something else).

![](./media/image41.png)
*Illustration 35: Gazelle X Validator - New Unit test - Step 3*

The whole unit test is saved each time you add a new input.


#### Run unit test

You can run those unit tests from different places:

-   From the rule edition page, in the “Unit tests” panel, you can run tests individually or all of them

-   From the unit test edition page

-   From the page which lists all the unit tests defined in the tool; on that page you can run tests individually or all the tests which are selected thanks to the search criteria. That means, that for instance, you can run all the tests which previously failed for a given validator.

#### Browse unit tests

The page gathering all the unit tests defined within the tool is reachable from the Manage rule page (Access unit tests button) and, in Gazelle X Validator Rule Editor, from menu Rule Editor → Unit test of rules → Browse unit tests for rules.

A set of filters will help you restrict your search.

“Run all filtered tests” button will run all the unit tests selected thanks to the search criteria.

![](./media/image43.png)
*Illustration 36: Gazelle X Validator - Browse unit tests for rules*

For each tests, you can see whether it is active or not, the rule which is tested and the result of the last run. In addition, it provides information about the version of the rule which have been tested during the last run, the last run date and the timestamp of the last successful run.

From this page, you can edit a test, access its logs, run it or deactivate it.

#### Inspect results

In Gazelle X Validator Rule Editor, from menu Rule Editor → Unit test of rules → Rule unit test logs you access all the runs for all the unit tests.

You can restrict your search using the various filters available at the top of the page.

## Archived versions of validators

All the revisions of validators are stored as XML file on disk and synchronised with our SVN repository. The list of the old versions of validators is available, in Gazelle X Validator Rule Editor, from Rule Editor → Browse archived validators.

You can consult the content of a given version, download the XML file and use it for validate a file. This last feature is interesting if you are asked to reproduce a validation outcome whereas the current version of the validator does not match the one previously used to run the validation.

## Import/Export features

# Annex A : Validator development guidelines

This section gathers the tips and requirements that are to be applied by any user which aims to develop or maintain a Gazelle X Validator. Following this set or rules, we will obtain a consistent set of validators.

## Defining the scope of a validator

## Validator naming conventions

A Gazelle X Validator is uniquely known thanks to two attributes:

-   Its name

-   Its affinity domain

Knowing the name and the affinity domain should be enough to understand the purpose of the validator; the description is available to give more details about the context in which the validator applies.

As most as we can, we will apply the following conventions for naming the Gazelle X Validators

-   Since the affinity domain is provided, there is no need to repeat it in the name of the validator

-   If it applies to a specific integration profile (or use case or standard), the keyword of the profile shall appear first

-   If it applies to a specific transaction, the keyword of the transaction shall appear (first or second)



## Rule naming conventions

The rule is known by its keyword which is unique across the whole application. This keyword must reflect:

-   the context

-   the covered assertion

-   optionally the part of the assertion which is covered

As most as we can, we will apply the following conventions for the naming of the rules:

-   contains the keyword of the standard, profile and/or transaction it applies to (lower case, separated by underscore)

-   contains the id of the covered assertion (if several assertions are covered, the main one is enough)

-   if the assertion is covered by several rules, a short lower case text (no white space, words separated by dashes or capitalized) can give details (to distinguish the different rules covering the same assertion)

-   keyword of the rule shall not depend on its version (version shall not appear in the keyword)

**Examples**

pdqv3\_ASSERT8\_checkCodeCardinality

pdqv3\_ASSERT8\_checkCodeValue

hl7v25\_MSA-2\_checkReturnedValue

## Content of a rule

In a perfect word, the rule is aimed to cover a unique assertion. Several rules might be needed to cover a single assertion. Ideally, when a rule fails, the user shall be able to clearly and rapidly identify its mistake.

### Good practices

Before writing a rule for a specific assertion, ask yourself the following questions:

-   Does the assertion apply in all cases ?

-   Does the assertion have predicates ?

-   How many checks have to be performed ? Which means how many rules shall be used to covered the assertion ?

Then, think about an example which will cause the rule to fail and another one which will result in a success.

### The power of Xpath 2.0

Xpath is a powerful tool. However, SAX requires a namespace to correctly evaluate the Xpath. Namespaces are declared at the validator level, the empty prefix is not allowed. As a consequence, when you enter an Xpath in the tool, double check that the correct prefix is used.

### Conditional

The if/else structure is not available when you right a rule but the XOR operator can play this role. For instance, if your assertion sounds like “If the element/@attribute is present then it shall be valued 'A' or 'B'”. Then, the expression enclosed in your rule shall look like: not (//element/@attribute is present) XOR (//element\[@attribute = 'A' or @attribute = 'B'\] is present).

### The description

In most of the cases, it is enough to copy and paste the content of the covered assertion in the description field. This must not be too verbose but clear and precise enough to understand the goal of the rule. If the testing of an assertion is split between several rules, the text of the assertion must be rephrased to match the purpose of each rules.

### level

The level of the rule gives indication about the severity of the failure. If the validators returns only failure on warning and info rules, the global outcome of the validation is passed. In the contrary, if at least one error rule fails, the validation outcome is set to failed.

In most of the cases, the level of the rule will immediately depends on the wording of the assertion:

-   SHALL, MUST, MANDATORY, REQUIRED → Error

-   SHOULD, COULD, MIGHT → Warning

-   OPTIONALY, MAY → Info

## Tips

### Attributes

/rad:RetrieveImagingDocumentSetRequest/rad:StudyRequest/@studyInstanceUID will return a node of type attribute and named studyInstanceUID

If you rather want to select the value of the studyInstanceUID attribute, use the following Xpath expression: string(/rad:RetrieveImagingDocumentSetRequest/rad:StudyRequest/@studyInstanceUID)
