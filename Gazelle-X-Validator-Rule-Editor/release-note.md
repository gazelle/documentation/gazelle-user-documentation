---
title: Release note
subtitle: Gazelle X Validator Rule Editor
toolversion: 2.0.0
releasedate: 2024-02-02
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_X_VALIDATOR_RULE_EDITOR
---
# 2.0.0
_Release date: 2024-02-02_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[XVALEDIT-73](https://gazelle.ihe.net/jira/browse/XVALEDIT-73)\] Integrate new sso-client-v7
* \[[XVALEDIT-74](https://gazelle.ihe.net/jira/browse/XVALEDIT-74)\] Remove username display in GUI

# 1.3.0
_Release date: 2019-03-19_

__Improvement__

* \[[XVALEDIT-68](https://gazelle.ihe.net/jira/browse/XVALEDIT-68)\] Extract datasource configuration
* \[[XVALEDIT-69](https://gazelle.ihe.net/jira/browse/XVALEDIT-69)\] Update SQL scripts archive

# 1.2.0
_Release date: 2018-10-31_

__Bug__

* \[[XVALEDIT-43](https://gazelle.ihe.net/jira/browse/XVALEDIT-43)\] Cannot access pages where unit tests are displayed
* \[[XVALEDIT-63](https://gazelle.ihe.net/jira/browse/XVALEDIT-63)\] Unit tests table filter not responding 

# 1.1.0
_Release date: 2018-04-26_

__Bug__

* \[[XVALEDIT-35](https://gazelle.ihe.net/jira/browse/XVALEDIT-35)\] Update postgresql driver to version 42.2.1-jre7

__Improvement__

* \[[XVALEDIT-34](https://gazelle.ihe.net/jira/browse/XVALEDIT-34)\] Use new CAS apereo

# 1.0.0
_Release date: 2017-06-12_

__Bug__

* \[[XVALEDIT-21](https://gazelle.ihe.net/jira/browse/XVALEDIT-21)\] Unit test File not uploaded when ut_directory is wrongly defined

__Epic__

* \[[XVALEDIT-10](https://gazelle.ihe.net/jira/browse/XVALEDIT-10)\] Migrate Gazelle X Validator Editor to Jboss 7

__Story__

* \[[XVALEDIT-1](https://gazelle.ihe.net/jira/browse/XVALEDIT-1)\] Creation of the project

__Improvement__

* \[[XVALEDIT-2](https://gazelle.ihe.net/jira/browse/XVALEDIT-2)\] Improve layout
* \[[XVALEDIT-3](https://gazelle.ihe.net/jira/browse/XVALEDIT-3)\] Add security considerations
* \[[XVALEDIT-8](https://gazelle.ihe.net/jira/browse/XVALEDIT-8)\] minifying assets
* \[[XVALEDIT-11](https://gazelle.ihe.net/jira/browse/XVALEDIT-11)\] migrate to Jboss 7
* \[[XVALEDIT-12](https://gazelle.ihe.net/jira/browse/XVALEDIT-12)\] fix GUI issues
