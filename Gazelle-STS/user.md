---
title:  User Manual
subtitle: Gazelle STS
author: Cédric EOCHE-DUVAL
function: Test Engineer
date: 04/07/2017
toolversion: 1.3.1
version: 2.03
status: Approved document
reference: KER1-MAN-IHE-STS_USER-2_03
customer: IHE-EUROPE
---
# Introduction

A Security Token Service (STS) is a Web service that issues security tokens according to the WS-Security protocol. It is a standard component of security network that enables actions like: authentication, identity validation or security token exchange.
The primary use of a STS is to acquire SAML tokens in order to request a service in a different security domain.

A SAML assertion in WS-Trust is the kind of security token that provides our STS.
The Web Service Description Language (WSDL) needed to contact our Web service with SOAP and XML Schema is linked below.

Service URL: [https://gazelle.ihe.net/gazelle-sts?wsdl](https://gazelle.ihe.net/gazelle-sts?wsdl)

**An HTTP authentication is asked for this server**. The password is **connectathon** and the username depends on the kind of assertion you are expecting (see table below). The default username is **valid**.

The animation hereunder explains the different steps when a requestor need information from a service provider using a STS :

![](./media/giphy.gif)

This STS is used as part of the Connectathons in NA and EU. Our X-Assertion Provider is configured with a Certificate signed by IHE Europe CA. It is available here . It is only configured to help you perform all XUA tests, and hence does not provide token for a different context. As a user of the service you must trust that certificate.

## Issue Tracker

**If you encounter any issue with this application, report it to our developer team !**

A development team works each day to improve this application and fix notified issues. It is very easy and fast to report an issue, and this is the best way to get it fixed as soon as possible. You just need to create an account on our Issue Tracker (link below) and to report it with its resolution priority.

Click on that link to report a problem : [https://gazelle.ihe.net/jira/projects/STS](https://gazelle.ihe.net/jira/projects/STS) and create an issue.

## Examples of common queries

**Line break and whitespaces must be stripped out from requests, otherwise the following requests will not work.**
In the software SoapUI, the option `Strip whitespaces = true` must be set *(Project > Test Suite > Test Case > Test Steps > any soap request > properties)*.

Here is a link to download a valid SoapUI 5.3.0 (and 5.4.0) project with all following requests : [Project SoapUI Gazelle-STS - Examples queries](https://svn.ihe-europe.net/gazelle/Maven/gazelle-sts/trunk/gazelle-sts-service/src/test/resources/gazelle-sts-soapui-project.xml)
(Right-click the link, select "Save target as" or "Save link as.")

### Requesting a Security Token

To get a security token, a ws-trust RequestSecurityToken request must be send.If the policy permits, a response will be issued containing the token.

#### HTTP Authentication
**A basic HTTP authentication with a username and a password is required to process the request**. The password is `connectathon` and the username depends of the kind of assertion that you expect. In the following table, you will find all types of assertions Gazelle-STS is able to generate. For a default assertion, use the username : "valid".

| HTTP authentication user               | Description                                                                                                                                                         |
|----------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| valid                                  | Valid assertion                                                                                                                                                     |
| notyetvalid                            | Assertion that will be valid in one day                                                                                                                             |
| expired                                | Assertion expired from one day                                                                                                                                      |
| unsigned                               | Assertion without ds:Signature element                                                                                                                              |
| invalidsignature                       | Assertion modified after the signature                                                                                                                              |
| missingkeyinfo                         | Assertion without ds:Signature/ds:KeyInfo element                                                                                                                   |
| missingkeyvalue                        | Assertion without ds:Signature/ds:KeyInfo/ds:KeyValue element                                                                                                       |
| missingrsakeyvalue                     | Assertion without ds:Signature/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue element                                                                                        |
| missingrsakeymodulus                   | Assertion without ds:Signature/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Modulus element                                                                             |
| missingrsakeyexponent                  | Assertion without ds:Signature/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Exponent element                                                                            |
| invalidversion                         | Assertion with invalid @Version attribute value                                                                                                                     |
| missingversion                         | Assertion without @Version attribute                                                                                                                                |
| invalidid                              | Assertion with invalid @Id attribute value                                                                                                                          |
| missingid                              | Assertion without @Id attribute                                                                                                                                     |
| missingsubjectconfirmation             | Assertion without saml2:Subject/saml2:SubjectConfirmation element                                                                                                   |
| missingsubjectconfirmationmethod       | Assertion without saml2:Subject/saml2:SubjectConfirmation/@Method attribute                                                                                         |
| missingsubject                         | Assertion without saml2:Subject element                                                                                                                             |
| missingsubjectnameid                   | Assertion without saml2:Subject/saml2:NameID element                                                                                                                |
| missingissuer                          | Assertion without saml2:Issuer element                                                                                                                              |
| missingissuerformat                    | Assertion without saml2:Issuer/saml2:NameID/@Format attribute                                                                                                       |
| invalidissueremailformat               | Assertion which has saml2:Issuer/saml2:NameID/@Format equals to emailAddress but saml2:Issuer/saml2:NameID value is not an emailAddress                             |
| invalidissuerx509format                | Assertion which has saml2:Issuer/saml2:NameID/@Format equals to x509SubjectName but saml2:Issuer/saml2:NameID value is not an x509SubjectName                       |
| invalidissuerwindowsdomainformat       | Assertion which has saml2:Issuer/saml2:NameID/@Format equals to WindowsDomainQualifiedName but saml2:Issuer/saml2:NameID value is not an WindowsDomainQualifiedName |
| missingissueinstant                    | Assertion without @IssueInstant attribute                                                                                                                           |
| invalidissueinstant                    | Assertion with @IssueInstant attribute value as String instead of dateTime                                                                                          |
| invalidrsapublickeymodulus             | Assertion with invalid ds:Signature/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Modulus element value                                                                  |
| invalidrsapublickeyexponent            | Assertion with invalid ds:Signature/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Exponent element value                                                                 |
| invalidsubjectnameidformat             | Assertion with invalid saml2:Subject/saml2:NameID/@Format attribute value                                                                                           |
| invalidx509certificate                 | Assertion with invalid saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData/ds:KeyInfo/ds:x509Data element value                                   |
| lateissueinstant                       | Assertion with @IssuerInstant attribute value set to june of year 2117                                                                                              |
| missingsubjectconfdata                 | Assertion without saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData element                                                                     |
| missingsubjectconfirmationkeyinfo      | Assertion without saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData/ds:KeyInfo element                                                          |
| missingsubjectconfrsapublickeyexponent | Assertion without saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Exponent element                   |
| invalidsubjectconfrsapublickeymodulus  | Assertion with invalid saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Modulus element value         |
| invalidsubjectconfrsapublickeyexponent | Assertion with invalid saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData/ds:KeyInfo/ds:KeyValue/ds:RSAKeyValue/ds:Exponent element value        |
| unknownaudience                        | Assertion where saml2:Conditions/saml2:AudienceRestriction/saml2:Audience does not match the AppliesTo element of the request                                       |
| invalidauthncontext                    | Assertion with invalid saml2:AuthnStatement/saml2:AuthnContext/saml2:AuthnContextClassRef element value                                                             |
| secondauthncontext                     | Assertion where saml2:AuthnStatement/saml2:AuthnContext/saml2:AuthnContextClassRef element value is set with lower rights                                           |
| secondrole                             | Assertion where saml2:AttributeStatement/saml2:Attribute\[@Name="urn:oasis:names:tc:xacml:2.0:subject:role"\] is set with lower rights                              |
| secondpurposeofuse                     | Assertion where saml2:AttributeStatement/saml2:Attribute\[@Name="urn:oasis:names:tc:xacml:2.0:subject:purposeofuse"\] is set with lower rights                      |
| withauthzconsent                       | Assertion where attributes statement "urn:ihe:iti:bppc:2007:docid" and "urn:oasis:names:tc:xacml:2.0:resource:resource-id" are present                              |

In this other document, there are usernames used by the old picketlink-sts during previous connectathon (2015-2017). This document is kept for archiving only : [Previous username and assertions for Connectathon](https://docs.google.com/spreadsheets/d/1d3I_j4lShrAske3iIUkEbPmjaT56eGwrPl6rEUdlLSk/edit?usp=sharing)

#### Request sample

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
   <soap:Header>
      <Action xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</Action>
      <MessageID xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">urn:uuid:9901f17a-14c4-4dc0-a639-132b96d5fd0d</MessageID>
      <To xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">https://gazelle.ihe.net/gazelle-sts</To>
      <ReplyTo xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">
         <Address>http://www.w3.org/2005/08/addressing/anonymous</Address>
      </ReplyTo>
   </soap:Header>
   <soap:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://www.w3.org/ns/ws-policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>http://ihe.connectathon.XUA/X-ServiceProvider-IHE-Connectathon</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
      </wst:RequestSecurityToken>
   </soap:Body>
</soap:Envelope>
```

In the response from the web service, if everything worked fine, you should receive an assertion in the tag `<saml:Assertion> ... </saml:Assertion>` (the opening tag may looks like `<saml:Assertion ID="ID_420d4acb-a582-44ab-bba5-52514857c62f" IssueInstant="2015-03-16T16:05:57.406Z" Version="2.0" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">`).
The lifetime of a token is 2 hours.

### Renew a Security Token

If your previous token expired, you can renew it and get the same token with new expiration semantics. An HTTP authentication with an username and a password is needed to proceed the request. Copy/paste your assertion achieved from the response of a requesting security token (Part 1 in this tutorial) in the indicated area on the following code. Don't forget to copy information from the opening tag `<saml:Assertion ... >`.

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
   <soap:Header>
      <Action xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</Action>   
      <MessageID xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">urn:uuid:9901f17a-14c4-4dc0-a639-132b96d5fd0d</MessageID>
      <To xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">https://gazelle.ihe.net/gazelle-sts</To>
      <ReplyTo xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">
         <Address>http://www.w3.org/2005/08/addressing/anonymous</Address>
      </ReplyTo>
   </soap:Header>
   <soap:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Renew</wst:RequestType>        
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:RenewTarget>         
            <saml:Assertion ... >


/!\ INSERT YOUR ASSERTION HERE /!\


            </saml:Assertion>
         </wst:RenewTarget>
      </wst:RequestSecurityToken>
   </soap:Body>
</soap:Envelope>
```
In the response from the web service, if everything worked fine, you should receive a new assertion in the tag `<saml:Assertion> ... </saml:Assertion>` (the opening tag may looks like `<saml:Assertion ID="ID_420d4acb-a582-44ab-bba5-52514857c62f" IssueInstant="2015-03-16T16:05:57.406Z" Version="2.0" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">`).
The lifetime of a new token is 2 hours.

### Cancel a Security Token

Not supported at this time

### Validate a Security Token

When you get a security token from a requestor, you may have to validate the assertion. You can use any authentication in this part, it has no influence.

**Note that Gazelle-STS will verify signature, trusted chain relations and time validity, but will not check specific IHE rules (or other standards business rules). This is the responsability of the service provider.**

Copy/paste the assertion from the request in the indicated area on the following code. Don't forget to copy information from the opening tag `<saml:Assertion ... >`.

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
   <soap:Header>
      <Action xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</Action>
      <MessageID xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">urn:uuid:9901f17a-14c4-4dc0-a639-132b96d5fd0d</MessageID>
      <To xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">https://gazelle.ihe.net/gazelle-sts</To>
      <ReplyTo xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">
         <Address>http://www.w3.org/2005/08/addressing/anonymous</Address>
      </ReplyTo>
   </soap:Header>
   <soap:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Validate</wst:RequestType>        
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>        
         <wst:ValidateTarget>         
            <saml:Assertion ... >


/!\ INSERT YOUR ASSERTION HERE /!\


            </saml:Assertion>
         </wst:ValidateTarget>
      </wst:RequestSecurityToken>
   </soap:Body>
</soap:Envelope>
```

In the response from the web service, you should receive a reply with the approval or the rejection of the request.
