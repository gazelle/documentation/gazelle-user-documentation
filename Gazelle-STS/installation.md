---
title:  Installation Manual
subtitle: Gazelle-STS
author: Cédric EOCHE-DUVAL
function: Engineer
date: 19/04/2017
toolversion: 1.3.1
version: 4.02
status: Approved document
reference: KER1-MAN-IHE-STS_INSTALLATION-4_02
customer: IHE-EUROPE
---

# Introduction

A Security Token Service (STS) is a Web service that issues security tokens according to the WS-Security protocol. It is a standard component of security network that enables actions like: authentication, identity validation or security token exchange.

Gazelle-STS has been developped for __testing purpose only__. It is able to distribute security assertions for different test cases : valid, expired, unknown service provider, etc.

It relies on [Picketlink](http://picketlink.org/) 2.7.0.Final. Gazelle-STS also requires to query [Gazelle Security Suite](/gazelle-documentation/Gazelle-Security-Suite/user.html) (with PKI mode enabled) to validate security tokens.

# Sources & binaries

Gazelle-STS is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via Subversion at [https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/gazelle-sts/](hhttps://gitlab.inria.fr/gazelle/applications/test-execution/simulator/gazelle-sts/).

The latest public packaged release can be downloaded from our Sonatype Nexus repository [gazelle-sts releases](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-sts-service%22) (search for **gazelle-sts-service-X.X.X.war**).

# STS Installation

If you are installing a Gazelle tool for the first time in your environment, make sure to read carefully the [general considerations for JBoss7](/gazelle-documentation/General/jboss7.html).

## Jboss 7 configuration

1. Edit to ${JBOSS_INSTALL_DIR}/standalone/configuration/standalone.xml

1. Add in `<subsystem><security-domains>` the following lines

```xml
<security-domain name="idp" cache-type="default">
  <authentication>
    <login-module code="UsersRoles" flag="required">
      <module-option name="usersProperties" value="users.properties"/>
      <module-option name="rolesProperties" value="roles.properties"/>
    </login-module>
  </authentication>
</security-domain>
<security-domain name="gazelle-sts" cache-type="default">
  <authentication>
      <login-module code="net.ihe.gazelle.sts.login.STSLoginModule" flag="required"/>
  </authentication>                    
</security-domain>
<security-domain name="sp" cache-type="default">
  <authentication>
    <login-module code="org.picketlink.identity.federation.bindings.jboss.auth.SAML2LoginModule" flag="required"/>
  </authentication>
</security-domain>
```

## Database

No database is required for Gazelle-STS

## Deployment

To deploy Gazelle STS :

1.  Paste the archive **gazelle-sts-service.war** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`

1.  Display JBoss server logs, start JBoss and wait for **war** deployment.

1.  The application can be browsed at [http://localhost:8080/gazelle-sts?wsdl](http://localhost:8080/gazelle-sts?wsdl).  
_Port could also be different whether you have modified the JBoss configuration._

## Configuration

By default, the project is configured for working in **gazelle.ihe.net** environment. The keystore used to sign assertions must be located at
`/opt/sts/keystore.jks`, the privateKeyEntry must have the alias `tomcat`, and the certificate validator webservice must be reachable at
[https://gazelle.ihe.net/gazelle-atna-ejb/CertificateValidatorService/CertificateValidator](https://gazelle.ihe.net/gazelle-atna-ejb/CertificateValidatorService/CertificateValidator).

This configuration can be overridden. Download the config archive from our Nexus repository
[gazelle-sts releases](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-sts-service%22) (search for **gazelle-sts-service-X.X.X-config.zip**).
Then unzip the archive, copy the file picketlink-sts.xml under `/opt/sts/` and edit its content.
As well, the zip archive contains 2 property files named 'ihe.assertion.properties' and 'sequoia.assertion.properties' containing properties related
to assertions such as namespaces, roles, organization, etc. These properties files shall also be copied in `/opt/sts/`. The content may also be edited
to adjust values for defined properties.

Note: The property `X509CERTIFICATE` of the `KeyProvider` must stay equals to the `SigningKeyAlias` property, otherwise the public key declared as
issuer will be inconsistant with the private key used to sign the assertion.

## Sequoia configurations
Some special configurations have defined in the source code for the Sequoia project.
In order to use them, you need to use the 'sequoia.assertion.properties' contained in the zip archive described above, as well as modifying the 'picketlink-sts.xml' file to use Sequoia token provider class.

## Update from a previous Version

### From 1.2.0 to 1.3.0

Add the files ihe.assertion.properties and sequoia.assertion.properties in /opt/sts. More information in the "Configuration" section.


## Tests

A small SoapUI project to tests the STS web-service can be found on our Nexus repository [gazelle-sts releases](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22gazelle-sts%22) (search for **gazelle-sts-service-X.X.X-soapui.zip**)

The project custom property `ServiceBaseURL` is used to define the targeted host.
