---
title:  Installation Manual
subtitle: Assertion Manager
author: Jean-Francois LABBE
function: Engineer
date: 06/02/2024
toolversion: 5.X.X
version: 1.02
status:  draft
reference: KER1-MAN-IHE-ASSERTION\_MANAGER\_INSTALLATION-1\_02
customer: IHE-EUROPE
---
# Purpose

Here is a guide to help you to install Assertion Manager.

# Minimal Requirements

* Debian squeeze or ubuntu 12.04 64bits or higher with an Internet access.
* Database : Postgresql 9.1+
* Java virtual machine : JDK 1.7
* Application server : JBoss 7.2.0.
    * It must be installed in: `/usr/local/jboss7`

To install those requirements you can refer to the documentation of installation of JBoss : [*General Requirements JBOSS 7*](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html)

## Sources & binaries

Assertions Manager is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via Gitlab at [https://gitlab.inria.fr/gazelle/public/core/assertion-manager](https://gitlab.inria.fr/gazelle/public/core/assertion-manager).

The latest public packaged release can be downloaded from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22AssertionManagerGui-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22AssertionManagerGui-ear%22).


### Database creation

Your database must have a user **gazelle** :

1. Connect to your database

```bash
psql -U gazelle
```

1. Execute the SQL statement to create the database.

```sql
CREATE DATABASE "assertion-manager-gui" OWNER gazelle ENCODING 'UTF8' ;
```

## Deployment
To deploy Assertion Manager:
1. Paste the archive **AssertionManagerGui.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`
2. Display JBoss server logs, start JBoss and wait for **ear** deployment.
3. The application can be browsed at http://localhost:8080/AssertionManagerGui. *Port could also be different whether you have modified the JBoss configuration*.
4. **WARNING** : From version 4.2.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file AssertionManager-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : AssertionManagerBaseDS

Database name : assertion-manager-gui

### Application configuration

1. Download the SQL scripts archive from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22AssertionManagerGui-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22AssertionManagerGui-ear%22) (search for **AssertionManagerGui-ear-X.X.X-sql.zip**)

1. Unzip the archive

1. Edit the application_url value in init.sql.

1. From the bash, update the application configuration by running :

```bash
psql -U gazelle assertion-manager-gui < schema-X.X.X.sql

psql -U gazelle assertion-manager-gui < init-X.X.X.sql
```
Or

Configuration can be done through the Administration interface or with an sql script.

Each time a configuration is modified, the application needs to be restarted.

To restart the application, there are 2 options:

* Restart jboss: *sudo service jboss restart*
* Touch the ear: *touch /usr/local/jboss7/standalone/deployments/AssertionManagerGui.ear*

![](./media/application_preference_menu.png)

Here is the list of configuration variables that must be defined: 

| Variable| Default value                                                                                                  | Description|
|---------|----------------------------------------------------------------------------------------------------------------|------------|
| application_url                           | http://server_domain:8080/AssertionManagerGui                                                                  | URL to reach the tool|
| upload_max_size                           | 100000000                                                                                                      | Used to limit uploaded files size|
| assertion_manager_rest_path_to_assertion  | /testAssertion/assertion                                                                                       | do not change|
| assertion_manager_rest_url                | https://server_domain:8080/AssertionManagerGui/rest                                                            |  update server_domain:8080 to fit your needs.|
| security-policies                         | true                                                                                                           |  Enable security features|
| X-Frame-Options                           | SAMEORIGIN                                                                                                     |  https://developer.mozilla.org/en-US/docs/Web/HTTP/X-Frame-Options|
| Cache-Control                             | private, no-cache, no-store, must-revalidate, max-age=0                                                        |  https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers#Cache-control|
| Strict-Transport-Security                 | max-age=31536000 ; includeSubDomains                                                                           |  https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers#Cache-control|
| X-Content-Security-Policy                 |                                                                                                                | https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers#Cache-control|
| X-WebKit-CSP                              | Use X-Content-Security-Policy values                                                                           | |
| X-WebKit-CSP-Report-Only                  | Use X-Content-Security-Policy-Report-Only values                                                               | |
| X-Content-Security-Policy-Report-Only     | default-src self \*.ihe-europe.net; script-src self unsafe-eval unsafe-inline ; style-src self unsafe-inline ; |  https://developer.mozilla.org/en-US/docs/Web/Security/CSP/Using_Content_Security_Policy |
| sql_injection_filter_switch               | true                                                                                                           |  Enable sql injection filtering|
| gazelle_test-management-url               | https://server_domain:8080/gazelle/rest/                                                                       | points to gazelle test management rest interface|
| application_documentation                 | https://gazelle.ihe.net/content/assertion-manager                                                              | |
| documentation_url                         | https://gazelle.ihe.net/content/assertion-manager                                                              | |
| application_release_notes_url             | https://gazelle.ihe.net/jira/browse/AS#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel  | |
| application_issue_tracker_url             | https://gazelle.ihe.net/browse/EVSCLT                                                                          | URL of the project in the issue tracking system |

### SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).


# Compile from sources

The following chapters will explain how to build and configure Assertion Manager from sources.

This is useful only to change the database configuration.

Assertion Manager is an open source project under Apache2 licence. The sources are available in the INRIA's Gitlab project :
```bash
sudo apt-get install maven2 git
git clone https://gitlab.inria.fr/gazelle/applications/test-design/AssertionManager.git assertion-manager-gui
cd assertion-manager-gui
mvn clean package -U -Pproduction
```

If maven cannot download the required dependencies, checkout the maven configuration guide: [*https://gazelle.ihe.net/content/development-support-tools*](https://gazelle.ihe.net/content/development-support-tools)

## Database creation and initialization

The name of the databases are defined in the pom.xml file. Use the following commands with the adaptations you want:
```bash
su postgres
psql
postgres=\# CREATE USER gazelle;
postgres=\# ALTER USER gazelle WITH ENCRYPTED PASSWORD 'password';
postgres=\# CREATE DATABASE "assertion-manager-gui" OWNER gazelle ENCODING UTF-8;
postgres=\# \\q
exit
```

## Compile

Before compiling, go into gazelle-tm's directory and edit the file pom.xml. Open this file and adapt the properties of prod profile to your needs :

* jdbc.connection.url : jdbc:postgresql:assertion-manager-gui
* jdbc.connection.user / jdbc.connection.password : credentials for database access

Then, create the EAR archive with the command line:
```bash
cd assertion-manager-gui
mvn clean package -Pproduction
```

The archive (EAR) and the distribution file are created and placed into assertion-manager-gui/AssertionManagerGui-ear/target directory.

## Deployment

Copy the AssertionManagerGui.ear into the "/usr/local/jboss7/standalone/deployments" directory of your JBoss server. Finally, start your server.

When the application is deployed, open a browser and go to http://yourserver/AssertionManagerGui.

If the deployment and the database initializations are successful, you should see the home page.

![](./media/home_page.png)
