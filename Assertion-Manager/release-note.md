---
title: Release note
subtitle: Assertion Manager
toolversion: 5.1.0
releasedate: 2025-01-22
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-ASSERTION_MANAGER
---
# 5.1.0
_Release date : 2025-01-22_

Context : GUM Step 4

__Task__
* \[[AM-299](https://gazelle.ihe.net/jira/browse/AM-299)\] Upgrade sso-client-v7 to 5.0.0

# 5.0.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[AM-295](https://gazelle.ihe.net/jira/browse/AM-295)\] Remove username display in GUI
* \[[AM-296](https://gazelle.ihe.net/jira/browse/AM-296)\] Integrate new sso-client-v7

# 4.2.5
_Release date: 2023-10-19_

__Bug__
* \[[AM-293](https://gazelle.ihe.net/jira/browse/AM-293)\] Editing Assertions lead to an error page when saving

# 4.2.4
_Release date: 2023-04-18_

__Bug__
* \[[AM-292](https://gazelle.ihe.net/jira/browse/AM-292)\] Fix TOS popup

# 4.2.3
_Release date: 2023-01-17_

__Bug__
* \[[AM-289](https://gazelle.ihe.net/jira/browse/AM-289)\] Timeout for assertions export

__Story__
* \[[AM-286](https://gazelle.ihe.net/jira/browse/AM-286)\] Assertionmanager issu: 'Export as excel' functionality

# 4.2.2
_Release date: 2022-02-07_

__Bug__
* \[[AM-288](https://gazelle.ihe.net/jira/browse/AM-288 )\] Regular user can edit or delete assertions

# 4.2.1
_Release date: 2022-01-19_

__Bug__
* \[[AM-283](https://gazelle.ihe.net/jira/browse/AM-283 )\] Assertion manager: creating IdSchemes and assertions with special characters causes error 
* \[[AM-284](https://gazelle.ihe.net/jira/browse/AM-284 )\] Assertion Manager unable to add documents
* \[[AM-285](https://gazelle.ihe.net/jira/browse/AM-285 )\] Add an error message when linking a Document to an empty idScheme
  
__Story__
* \[[AM-287](https://gazelle.ihe.net/jira/browse/AM-287 )\] GDPR Refacto banner in standalone library

# 4.2.0
_Release date: 2020-10-29_

__Bug__
* \[[AM-257](https://gazelle.ihe.net/jira/browse/AM-257)\] NPE while trying to save an assertion in review mode
* \[[AM-272](https://gazelle.ihe.net/jira/browse/AM-272)\] Applies to entities not updated in assertions table when removing links
* \[[AM-273](https://gazelle.ihe.net/jira/browse/AM-273)\] Navigating between pages in both Assertions Menu and review mode doesn't work

__Improvement__
* \[[AM-258](https://gazelle.ihe.net/jira/browse/AM-258)\] Extract datasource configuration
* \[[AM-259](https://gazelle.ihe.net/jira/browse/AM-259)\] Update SQL scripts archive


# 4.1.0
_Release date: 2018-06-06_

__Remarks__
This new version of Assertion Manager is bound to the new Gazelle SSO

__Bug__
* \[[AM-236](https://gazelle.ihe.net/jira/browse/AM-236)\] Impossible to import assertions when an attribute is present and empty
* \[[AM-237](https://gazelle.ihe.net/jira/browse/AM-237)\] Imported assertions do not have a creator attribute
* \[[AM-242](https://gazelle.ihe.net/jira/browse/AM-242)\] Recent PDF are not displayed 

__Improvement__
* \[[AM-252](https://gazelle.ihe.net/jira/browse/AM-252)\] Use new CAS SSO

# 4.0.2
_Release date: 2016-10-04_

__Bug__
* \[[AM-230](https://gazelle.ihe.net/jira/browse/AM-230)\] When creating / editing a PDF document, the PDF is not displayed in the right panel
* \[[AM-233](https://gazelle.ihe.net/jira/browse/AM-233)\] I have edited an assertion in the application in the review mode. Last updated and Last modifier are not updated

__Story__
* \[[AM-234](https://gazelle.ihe.net/jira/browse/AM-234)\] Release 4.0.2

__Improvement__
* \[[AM-228](https://gazelle.ihe.net/jira/browse/AM-228)\] Add the possibility to search in the list linking a document to an idscheme
* \[[AM-229](https://gazelle.ihe.net/jira/browse/AM-229)\] When deleting a document, add a message to warn the user that the idschemes will be unlinked
* \[[AM-231](https://gazelle.ihe.net/jira/browse/AM-231)\] Add informations when linking a document to an idscheme
* \[[AM-232](https://gazelle.ihe.net/jira/browse/AM-232)\] Stay on the same page when user clics the CAS login button

# 4.0.1
_Release date: 2016-10-04_

__Bug__
* \[[AM-106](https://gazelle.ihe.net/jira/browse/AM-106)\] We need a field that describe the content of a scheme
* \[[AM-173](https://gazelle.ihe.net/jira/browse/AM-173)\] Filtering in documents from assertion page seems duplicating the document names
* \[[AM-215](https://gazelle.ihe.net/jira/browse/AM-215)\] When logged by CAS, it's impossible to change the page number in the assertions list page
* \[[AM-220](https://gazelle.ihe.net/jira/browse/AM-220)\] Null pointer exception when trying to get an URI in a specific case
* \[[AM-225](https://gazelle.ihe.net/jira/browse/AM-225)\] Unable to find component with ID scopeFilter in view.

__Improvement__
* \[[AM-30](https://gazelle.ihe.net/jira/browse/AM-30)\] When a test is deprecated in GMM, then the assertion bound should not be covered anymore
* \[[AM-84](https://gazelle.ihe.net/jira/browse/AM-84)\] Better reporting on IdSchemes upload (progress and errors)
* \[[AM-90](https://gazelle.ihe.net/jira/browse/AM-90)\] Add a create assertion button next to missing assertion reported by mbvServices
* \[[AM-97](https://gazelle.ihe.net/jira/browse/AM-97)\] Mark an assertion as not testable
* \[[AM-108](https://gazelle.ihe.net/jira/browse/AM-108)\] Export to TAML is not exporting the fields "comment" and "status".
* \[[AM-112](https://gazelle.ihe.net/jira/browse/AM-112)\] Document have a field revision. This is confusing. Is that a revision date or a revision number. Do we need
* \[[AM-138](https://gazelle.ihe.net/jira/browse/AM-138)\] A "link all selected assertions"  button would be appreciated
* \[[AM-147](https://gazelle.ihe.net/jira/browse/AM-147)\] In the page "Apply To" we need a button "link all" to apply the link to the filtered assertion
* \[[AM-152](https://gazelle.ihe.net/jira/browse/AM-152)\] Add Original language description from the standard as a field
* \[[AM-206](https://gazelle.ihe.net/jira/browse/AM-206)\] When an assertion is covered by a test, display the test name instead of id
* \[[AM-207](https://gazelle.ihe.net/jira/browse/AM-207)\] Add a review filtered assertion feature
* \[[AM-216](https://gazelle.ihe.net/jira/browse/AM-216)\] when adding an assertion to a new idScheme, user should be able to select a document
* \[[AM-223](https://gazelle.ihe.net/jira/browse/AM-223)\] IdScheme page could be managed with CRUD pages instead of popups
* \[[AM-221](https://gazelle.ihe.net/jira/browse/AM-221)\] AM-223 Improve assertion coverage Filtering
* \[[AM-222](https://gazelle.ihe.net/jira/browse/AM-222)\] Some buttons are not in the panel footer
