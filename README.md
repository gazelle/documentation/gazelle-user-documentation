# Gazelle User Documentation

Generate gazelle framework documentation from markdown files.

This project generates static website (using jekyll) and PDF documents (oudated).

## Build and preview website with Docker

It requires Docker and docker-compose installed.

No more local Ruby, Pandoc, tex, ... are required.

To update website and serve it (ONLY FOR DEV):

```bash
docker-compose up --build
```

Document is then available at <http://localhost:1080/gazelle-documentation/>

## Build without Docker

### setup

```bash
sudo apt-get install make git git-svn texlive-xetex pandoc ruby-dev build-essential dh-autoreconf
```

Then, from `gazelle-dev-documentation` folder run to install some more libs and required gems

```bash
make setup
```

### Generate the documents and the website
```bash
make
```


### Add a gem in the bundle
```bash
cd target/jekyll-gazelle-documentation
sudo bundle add webrick
sudo bundle install


```

### Generate all the documents and start Jekyll (do not use in production)
```bash
sudo bundle exec jekyll serve
```

Once it is done you server must have been started on localhost:4000/gazelle-documentation/
## Folder structure

    gazelle-documentation
      |
      +--tool1
      |  |--installation.md
      |  |--Makefile
      |  |--media/
      |  |--user.md
	  |	 |--release-notes/
	  |  |	|--1_0_0.md (one file per release, named with version identifier)
      |  |--target/ (Created at compile time)
      |
      +--tool2
      |  |--installation.md
      |  |--Makefile
      |  |--media/
      |  |--user.md
	  |	 |--release-notes/
	  |  |	|--1_0_0.md (one file per release, named with version identifier)
      |  |--target/ (Created at compile time)
      |
      |
      +--Makefile.generic
      +--Makefile
      +--target/ (Created at compile time)

## Markdown files

The source files use the Markdown language.

To get fancy final documents, you need:
* A YAML header containing all the requested metadata (see YAML section)
* Use Relative paths to pictures (eg. *“./media/mypicture.jpg”*)
* Use Relative paths to point the resources located on the same platform (eg. If you need to reference a page in your tool, start the URL at *“/theTool”*)

The YAML header is used by Jekyll and pandoc to generate menus and front pages, make sure all the metadata are present and up-to-date. The *.md files must start with the YAML header.

The YAML header is used by Jekyll and pandoc to generate menus and front pages, make sure all the metadata are present and up-to-date. The *.md files must start with the YAML header.

## Conversion Markdown to Pdf files

To generate pdf files use the command (might take some times)

```bash
sudo make -i pdf-gen
```

you can use 
```bash
sudo make clean 
```
to remove all generated files

!!WARNING!!
Be careful with table in markdown they may break your pdf if table are too big.

In any case if your pdf is break you might want to use the extension markdown pdf with visual studio code.

With this extension you can generate pdf one by one with command available in the extension.

Table with this extension are correct 

## Links

http://pandoc.org/

[Pandoc installation package](https://github.com/jgm/pandoc/releases/tag/1.17.2)

