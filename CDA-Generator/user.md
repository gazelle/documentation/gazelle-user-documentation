---
title:  User Manual
subtitle: CDA Generator
author: Abderrazek BOUFAHJA
function: Engineer
date: 24/01/2022
toolversion: 2.2.x
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-CDA_GENERATOR_USER
customer: IHE-EUROPE
---

# Introduction

This application has multiple purposes.

* It can be used to generate sample CDA documents that can be used for testing purposes
	* Today it allows to generate a dispensation document based on a prescription in the context of the epSOS project.
	* It will be extended to allow the generation of other kind of CDA documents.
* It hosts the models of the CDA documents and offers the documentation and the edition interface for those models.
* Documentation of the model based contraints
* Generation and Edition of basic CDA R2 documents

Please email questions to the IHE Europe Connectathon Manager (Eric Poiseau) for guidance through the process.

![](./media/image19.png)

# CDA Documentation

CDAGenerator offers a documentation of CDA templates, CDA constraints, and CDA structure.

# CDA Structure

CDAGenerator offers a complete documentation of the structure of the CDA standard. It is the same description that we find in the schema of CDA R2. The documentation is generated from the UML model that describes the CDA standard. To access the documentation, you have to go to menu -&gt; CDA Documentation -&gt; CDA Content Documentation

![](./media/image20.png)

The html page that describes the Root element (ClinicalDocument) looks like this :

![](./media/image21.png)

# CDA Templates Documentation

All the templates described and validated by CDAGenerator are documented by the tool.

To access the documentation you have to go  to : menu -&gt; Documentation -&gt; [*CDA Templates Documentation*](https://gazelle.ihe.net/CDAGenerator/docum/templates/listTemplates.seam), which looks like this :

![](./media/image22.png)

The documentation of each template is generated automatically from the UML model, which describes the template and its constraints.

# CDA constraint documentation

The CDA constraint documentation is a page that contains all the constraints validated by CDAGenerator. This page allows you to perform searches of a constraint by its name, its dependency to a class or a package, or by keyword from the description, etc.

To go to the page of constraint documentation ou have to go to the menu -&gt; Documentation -&gt; [*Constraints Search*](https://gazelle.ihe.net/CDAGenerator/docum/constraints/constraints.seam)

![](./media/image23.png)

# CDA Edition

CDAGenerator offers the possibility to edit and to create basic CDA documents. To do so, you have to go to menu -&gt; CDA Generation -&gt; CDA-Edition

![](./media/image24.png)

You can Generate an empty file, or you can uplaod a CDA file :

![](./media/image25.png)

To use the module of edition, you have to click on the button edit.

A new page will be opened containing the possibility to edit element by element each sub element of the root ClinicalDocument.

![](./media/image26.png)

The same page allows to download the modified CDA document, or to validate it, or to save it.

The editor of CDA document is generated also from the model of description of the structure of CDA document.

# CDA Web service

CDAGenerator includes a web service of validation, that could be used as a standalone service without using EVSClient, to validate a CDA document

![](./media/image27.png)

# Disclaimer

The CDA Generator Application is an experimental system.

IHE-Europe assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or any other characteristic.

We would appreciate acknowledgement if the software is used.
