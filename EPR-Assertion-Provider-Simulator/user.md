---
title:  User Manual
subtitle: Assertion Provider Simulator
author: Youn Cadoret
function: Developer
date: 28/06/2019
toolversion: 1.3.0
version: 0.01
status: to be reviewed
reference: KER1-MAN-IHE-ASSERTION_PROVIDER_SIMULATOR_USER
customer: IHE-EUROPE
---


eHealthSuisse Assertion Provider MockUp
---------------------------------------

eHealthSuisse Assertion Provider MockUp can generate dummy X-User Assertion to use in X-transactions.

To be able to generate an X-User Assertion, the simulator is expecting Ws-Trust RequestSecurityToken requests. Those need to have a list of claimed attributes and an authentication assertion in the header of the soap enveloppe. The exact list of required elements is depending on which person is authenticating.

#### Healthcare Professional (HP)

*   In the authentication assertion
    *   The GLN of the HP MAY be provided as an attribute
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id

#### Assistant

*   In the authentication assertion
    *   The GLN of the assistant must be provided as NameID
    *   The Assistant organization or or group ID MAY be present as an attribute
    *   The Assistant organization or or group Name MAY be present as an attribute
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id
    *   The GLN of the healthcare professional the assistant is acting on behalf of MUST be provided as principal-id
    *   The Name of the healthcare professional the assistant is acting on behalf of MUST be provided as principal-name

#### Technical User

*   In the authentication assertion
    *   The Unique ID of the Technical User must be provided as NameID
    *   The Assistant organization or or group ID MAY be present as an attribute
    *   The Assistant organization or or group Name MAY be present as an attribute
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id
    *   The GLN of the healthcare professional the Technical User is acting on behalf of MUST be provided as principal-id
    *   The Name of the healthcare professional the Technical User is acting on behalf of MUST be provided as principal-name

#### Policy Administrator

*   In the authentication assertion
    *   The Unique ID of the Policy Administrator MUST be provided as NameID
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id

#### Document Administrator

*   In the authentication assertion
    *   The Unique ID of the Document Administrator MUST be provided as NameID
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id

#### Patient

*   In the authentication assertion
    *   The Unique ID of the Patient MUST be provided as NameID
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id
    *   The EPR-SPID of the Patient MAY be provided as principal-id
    *   The Name of the Patient MAY be provided as principal-name

#### Representative

*   In the authentication assertion
    *   The Unique ID of the Representative MUST be provided as NameID
*   In the request claims
    *   The Purpose of Use MUST be provided as purposeofuse
    *   The Role of the subject MUST be provided as role
    *   The EPR-SPID of the targeted patient must be provided as resource-id
    *   The Unique ID of the Representative MAY be provided as principal-id
    *   The Name of the Representative MAY be provided as principal-name

#### Other informations

This Authentication assertion does not need to be perfectly valid (signature is not checked by the dummy simulator)

### End Points

End point with TLS mutual authentication (testing certificate from GSS PKI): [https://ehealthsuisse.ihe-europe.net:10443/STS?wsdl](https://ehealthsuisse.ihe-europe.net:10443/STS?wsdl)

The wsdl can be browsed [here](/STS?wsdl).

### Data to be used

Data to be used can be found in documents [pat-patient-sut.xlsx](https://ehealthsuisse.ihe-europe.net/test_data/pat-patients-sut.xlsx) and [pat-idp-hp-sut.csv](https://ehealthsuisse.ihe-europe.net/test_data/pat-idp-hp-sut.csv).

First document define the patients to use for the query. This include the patient doing the request if it is the case, and the patient queried as a resource. The patient that needs to be used are found in sheet "pat-patients-sut". You need to use the SPID defined in column "Patient Identifier 1". The simulator won't recognize any SPID not in this documents sheet and you would end up with an InvalidRequest error if you use any other SPID.

The second document define the HCP to use with this simulator. HOWEVER, as the Assertion provider needs to resolve the name and ID of the practician's company, please use HCP Ann Andrews or Richard Reynolds. Other practicians are not linked to an organization and you would end un with an error message as well.

For any other role (DADM, PADM, TCU, REP, ASS), any ID will do. The "dummy" simulator will always accept the IDs and will assume that actors acting on behalf of a HCP are authorized to do so.

### Request example for an HP

```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
  <env:Header>
    <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
    <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
      <saml2:Assertion ID="Assertion\_3efbfc7917a1d3ec6e33ec70f410393d655980bb" IssueInstant="2018-03-28T09:01:06.421Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <saml2:Issuer>http://fed.hintest.ch/saml/2.0/epd/</saml2:Issuer>
        <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
          <ds:SignedInfo>
            <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
            <ds:SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
            <ds:Reference URI="#Assertion\_3efbfc7917a1d3ec6e33ec70f410393d655980bb">
              <ds:Transforms>
                <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                  <ec:InclusiveNamespaces PrefixList="xs" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                </ds:Transform>
              </ds:Transforms>
              <ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
              <ds:DigestValue>Uo4LNDD3qibeLswkH/ZWqIqe3Wc=</ds:DigestValue>
            </ds:Reference>
          </ds:SignedInfo>
          <ds:SignatureValue>SIGNATURE AS CREATED BY IDP</ds:SignatureValue>
          <ds:KeyInfo>
            <ds:X509Data>
              <ds:X509Certificate>IDP\_CERTIFICATE</ds:X509Certificate>
            </ds:X509Data>
          </ds:KeyInfo>
        </ds:Signature>
        <saml2:Subject>
          <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent">33165</saml2:NameID>
          <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
            <saml2:SubjectConfirmationData InResponseTo="\_59e6f0ab8fcad259789d70f3a43fd39d6bde4523f3" NotOnOrAfter="2018-03-29T01:41:06.506Z" Recipient="http://aak.hin.test/simplesaml/module.php/saml/sp/saml2-acs.php/fed.hintest.ch\_Post\_aak.local"/>
          </saml2:SubjectConfirmation>
        </saml2:Subject>
        <saml2:Conditions NotBefore="2018-03-28T09:01:06.421Z" NotOnOrAfter="2018-03-29T01:41:06.421Z">
          <saml2:AudienceRestriction>
            <saml2:Audience>http://aak.hin.test/simplesaml/module.php/saml/sp/saml2-acs.php/fed.hintest.ch\_Post\_aak.local</saml2:Audience>
          </saml2:AudienceRestriction>
        </saml2:Conditions>
        <saml2:AuthnStatement AuthnInstant="2018-03-28T09:01:06.421Z" SessionNotOnOrAfter="2018-03-28T14:34:26.421Z">
          <saml2:AuthnContext>
            <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml2:AuthnContextClassRef>
          </saml2:AuthnContext>
        </saml2:AuthnStatement>
        <saml2:AttributeStatement>
          <saml2:Attribute Name="GLN" NameFormat="urn:oasis:names:tc:ebcore:partyid-type:DataUniversalNumberingSystem:0060">
            <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">7601000000000</saml2:AttributeValue>
          </saml2:Attribute>
          <saml2:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
            <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Aaron</saml2:AttributeValue>
          </saml2:Attribute>
          <saml2:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
            <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Akeret</saml2:AttributeValue>
          </saml2:Attribute>
        </saml2:AttributeStatement>
      </saml2:Assertion>
    </wsse:Security>
  </env:Header>
  <env:Body>
    <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
      <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
      <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
        <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
          <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
        </wsa:EndpointReference>
      </wsp:AppliesTo>
      <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
      <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
        <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
          <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">305000^^^&amp;2.16.756.5.30.1.109.6.5.3.1.1&amp;ISO</saml2:AttributeValue>
        </saml2:Attribute>
        <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse">
          <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyType">
            <PurposeOfUse xmlns="urn:hl7-org:v3" code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE"/>
          </saml2:AttributeValue>
        </saml2:Attribute>
	<saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
          <saml2:AttributeValue>
            <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="HCP" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
          </saml2:AttributeValue>
        </saml2:Attribute>
      </wst:Claims>
    </wst:RequestSecurityToken>
  </env:Body>
</env:Envelope>
```

### Request example for an Assistant

```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
   <env:Header>
      <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
      <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         <saml2:Assertion ID="\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6" IssueInstant="2018-03-28T09:02:43.155Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:del="urn:oasis:names:tc:SAML:2.0:conditions:delegation" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <saml2:Issuer>xua.hin.ch</saml2:Issuer>
            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
               <ds:SignedInfo>
                  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                  <ds:Reference URI="#\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6">
                     <ds:Transforms>
                        <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                        <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                           <ec:InclusiveNamespaces PrefixList="del xsd" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </ds:Transform>
                     </ds:Transforms>
                     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                     <ds:DigestValue>UyqzdpLkYUMscBO0bEP6FwnKnlUscVCD70GL3uP6aSY=</ds:DigestValue>
                  </ds:Reference>
               </ds:SignedInfo>
               <ds:SignatureValue>SIGNATURE AS CREATED BY X-SERVICE USER REQUESTOR</ds:SignatureValue>
               <ds:KeyInfo>
                  <ds:X509Data>
                     <ds:X509Certificate>X-SERVICE USER REQUESTOR CERTIFICATE</ds:X509Certificate>
                  </ds:X509Data>
               </ds:KeyInfo>
            </ds:Signature>

            <saml2:Subject>
               <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000000</saml2:NameID>
               <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
                  <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  <saml2:SubjectConfirmationData>
                     <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                        <saml2:AttributeValue xsi:type="xsd:string">Aaron Akeret</saml2:AttributeValue>
                     </saml2:Attribute>
                  </saml2:SubjectConfirmationData>
               </saml2:SubjectConfirmation>
            </saml2:Subject>

            <saml2:Conditions NotBefore="2018-03-28T09:02:43.155Z" NotOnOrAfter="2018-03-28T09:32:43.155Z">
               <saml2:AudienceRestriction>
                  <saml2:Audience>urn:e-health-suisse:token-audience:all-communities</saml2:Audience>
               </saml2:AudienceRestriction>
               <saml2:Condition>
                  <del:Delegate>
                     <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  </del:Delegate>
               </saml2:Condition>
            </saml2:Conditions>

            <saml2:AuthnStatement AuthnInstant="2018-03-28T09:02:43.155Z" SessionNotOnOrAfter="2018-03-28T09:12:43.154Z">
               <saml2:AuthnContext>
                  <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml2:AuthnContextClassRef>
               </saml2:AuthnContext>
            </saml2:AuthnStatement>

            <saml2:AttributeStatement>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">305000^^^&amp;2.16.756.5.30.1.109.6.5.3.1.1&amp;ISO</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <PurposeOfUse code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">Hans Muster</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">USB</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">urn:oid:2.2.2.2</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <Role code="HCP" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
            </saml2:AttributeStatement>

         </saml2:Assertion>
      </wsse:Security>
   </env:Header>
   <env:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse">
	     <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyType">
	       <PurposeOfUse xmlns="urn:hl7-org:v3" code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE"/>
	     </saml2:AttributeValue>
	   </saml2:Attribute>
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
             <saml2:AttributeValue>
               <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="ASS" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
             </saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
             <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:e-health-suisse:principal-id">
             <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">7601002469191</saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:e-health-suisse:principal-name">
	     <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Ann Andrews</saml2:AttributeValue>
	   </saml2:Attribute>
         </wst:Claims>
      </wst:RequestSecurityToken>
   </env:Body>
</env:Envelope>
```

### Request example for a Technical User

```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
   <env:Header>
      <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
      <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         <saml2:Assertion ID="\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6" IssueInstant="2018-03-28T09:02:43.155Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:del="urn:oasis:names:tc:SAML:2.0:conditions:delegation" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <saml2:Issuer>xua.hin.ch</saml2:Issuer>
            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
               <ds:SignedInfo>
                  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                  <ds:Reference URI="#\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6">
                     <ds:Transforms>
                        <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                        <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                           <ec:InclusiveNamespaces PrefixList="del xsd" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </ds:Transform>
                     </ds:Transforms>
                     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                     <ds:DigestValue>UyqzdpLkYUMscBO0bEP6FwnKnlUscVCD70GL3uP6aSY=</ds:DigestValue>
                  </ds:Reference>
               </ds:SignedInfo>
               <ds:SignatureValue>SIGNATURE AS CREATED BY X-SERVICE USER REQUESTOR</ds:SignatureValue>
               <ds:KeyInfo>
                  <ds:X509Data>
                     <ds:X509Certificate>X-SERVICE USER REQUESTOR CERTIFICATE</ds:X509Certificate>
                  </ds:X509Data>
               </ds:KeyInfo>
            </ds:Signature>

            <saml2:Subject>
               <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000000</saml2:NameID>
               <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
                  <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  <saml2:SubjectConfirmationData>
                     <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                        <saml2:AttributeValue xsi:type="xsd:string">Aaron Akeret</saml2:AttributeValue>
                     </saml2:Attribute>
                  </saml2:SubjectConfirmationData>
               </saml2:SubjectConfirmation>
            </saml2:Subject>

            <saml2:Conditions NotBefore="2018-03-28T09:02:43.155Z" NotOnOrAfter="2018-03-28T09:32:43.155Z">
               <saml2:AudienceRestriction>
                  <saml2:Audience>urn:e-health-suisse:token-audience:all-communities</saml2:Audience>
               </saml2:AudienceRestriction>
               <saml2:Condition>
                  <del:Delegate>
                     <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  </del:Delegate>
               </saml2:Condition>
            </saml2:Conditions>

            <saml2:AuthnStatement AuthnInstant="2018-03-28T09:02:43.155Z" SessionNotOnOrAfter="2018-03-28T09:12:43.154Z">
               <saml2:AuthnContext>
                  <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml2:AuthnContextClassRef>
               </saml2:AuthnContext>
            </saml2:AuthnStatement>

            <saml2:AttributeStatement>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">305000^^^&amp;2.16.756.5.30.1.109.6.5.3.1.1&amp;ISO</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <PurposeOfUse code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">Hans Muster</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">USB</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">urn:oid:2.2.2.2</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <Role code="HCP" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
            </saml2:AttributeStatement>

         </saml2:Assertion>
      </wsse:Security>
   </env:Header>
   <env:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse">
	     <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyType">
	       <PurposeOfUse xmlns="urn:hl7-org:v3" code="AUTO" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE"/>
	     </saml2:AttributeValue>
	   </saml2:Attribute>
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
             <saml2:AttributeValue>
               <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="TCU" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
             </saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
             <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:e-health-suisse:principal-id">
             <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">7601002469191</saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:e-health-suisse:principal-name">
	     <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Ann Andrews</saml2:AttributeValue>
	   </saml2:Attribute>
         </wst:Claims>
      </wst:RequestSecurityToken>
   </env:Body>
</env:Envelope>
```

### Request example for a Policy Administrator

```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
   <env:Header>
      <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
      <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         <saml2:Assertion ID="\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6" IssueInstant="2018-03-28T09:02:43.155Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:del="urn:oasis:names:tc:SAML:2.0:conditions:delegation" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <saml2:Issuer>xua.hin.ch</saml2:Issuer>
            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
               <ds:SignedInfo>
                  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                  <ds:Reference URI="#\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6">
                     <ds:Transforms>
                        <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                        <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                           <ec:InclusiveNamespaces PrefixList="del xsd" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </ds:Transform>
                     </ds:Transforms>
                     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                     <ds:DigestValue>UyqzdpLkYUMscBO0bEP6FwnKnlUscVCD70GL3uP6aSY=</ds:DigestValue>
                  </ds:Reference>
               </ds:SignedInfo>
               <ds:SignatureValue>SIGNATURE AS CREATED BY X-SERVICE USER REQUESTOR</ds:SignatureValue>
               <ds:KeyInfo>
                  <ds:X509Data>
                     <ds:X509Certificate>X-SERVICE USER REQUESTOR CERTIFICATE</ds:X509Certificate>
                  </ds:X509Data>
               </ds:KeyInfo>
            </ds:Signature>

            <saml2:Subject>
               <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000000</saml2:NameID>
               <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
                  <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  <saml2:SubjectConfirmationData>
                     <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                        <saml2:AttributeValue xsi:type="xsd:string">Aaron Akeret</saml2:AttributeValue>
                     </saml2:Attribute>
                  </saml2:SubjectConfirmationData>
               </saml2:SubjectConfirmation>
            </saml2:Subject>
            
            <saml2:Conditions NotBefore="2018-03-28T09:02:43.155Z" NotOnOrAfter="2018-03-28T09:32:43.155Z">
               <saml2:AudienceRestriction>
                  <saml2:Audience>urn:e-health-suisse:token-audience:all-communities</saml2:Audience>
               </saml2:AudienceRestriction>
               <saml2:Condition>
                  <del:Delegate>
                     <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  </del:Delegate>
               </saml2:Condition>
            </saml2:Conditions>

            <saml2:AuthnStatement AuthnInstant="2018-03-28T09:02:43.155Z" SessionNotOnOrAfter="2018-03-28T09:12:43.154Z">
               <saml2:AuthnContext>
                  <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml2:AuthnContextClassRef>
               </saml2:AuthnContext>
            </saml2:AuthnStatement>

            <saml2:AttributeStatement>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">305000^^^&amp;2.16.756.5.30.1.109.6.5.3.1.1&amp;ISO</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <PurposeOfUse code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">Hans Muster</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">USB</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">urn:oid:2.2.2.2</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <Role code="HCP" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
            </saml2:AttributeStatement>

         </saml2:Assertion>
      </wsse:Security>
   </env:Header>
   <env:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse">
	     <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyType">
	       <PurposeOfUse xmlns="urn:hl7-org:v3" code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE"/>
	     </saml2:AttributeValue>
	   </saml2:Attribute>
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
             <saml2:AttributeValue>
               <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="PADM" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
             </saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
             <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</saml2:AttributeValue>
           </saml2:Attribute>
         </wst:Claims>
      </wst:RequestSecurityToken>
   </env:Body>
</env:Envelope>
```

### Request example for a Document Administrator

```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
   <env:Header>
      <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
      <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         <saml2:Assertion ID="\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6" IssueInstant="2018-03-28T09:02:43.155Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:del="urn:oasis:names:tc:SAML:2.0:conditions:delegation" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <saml2:Issuer>xua.hin.ch</saml2:Issuer>
            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
               <ds:SignedInfo>
                  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                  <ds:Reference URI="#\_2cfcc382-7e60-44e0-99b5-18e3f718cbc6">
                     <ds:Transforms>
                        <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                        <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                           <ec:InclusiveNamespaces PrefixList="del xsd" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </ds:Transform>
                     </ds:Transforms>
                     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                     <ds:DigestValue>UyqzdpLkYUMscBO0bEP6FwnKnlUscVCD70GL3uP6aSY=</ds:DigestValue>
                  </ds:Reference>
               </ds:SignedInfo>
               <ds:SignatureValue>SIGNATURE AS CREATED BY X-SERVICE USER REQUESTOR</ds:SignatureValue>
               <ds:KeyInfo>
                  <ds:X509Data>
                     <ds:X509Certificate>X-SERVICE USER REQUESTOR CERTIFICATE</ds:X509Certificate>
                  </ds:X509Data>
               </ds:KeyInfo>
            </ds:Signature>

            <saml2:Subject>
               <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000000</saml2:NameID>
               <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
                  <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  <saml2:SubjectConfirmationData>
                     <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                        <saml2:AttributeValue xsi:type="xsd:string">Aaron Akeret</saml2:AttributeValue>
                     </saml2:Attribute>
                  </saml2:SubjectConfirmationData>
               </saml2:SubjectConfirmation>
            </saml2:Subject>

            <saml2:Conditions NotBefore="2018-03-28T09:02:43.155Z" NotOnOrAfter="2018-03-28T09:32:43.155Z">
               <saml2:AudienceRestriction>
                  <saml2:Audience>urn:e-health-suisse:token-audience:all-communities</saml2:Audience>
               </saml2:AudienceRestriction>
               <saml2:Condition>
                  <del:Delegate>
                     <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent" NameQualifier="urn:gs1:gln">7601000000011</saml2:NameID>
                  </del:Delegate>
               </saml2:Condition>
            </saml2:Conditions>

            <saml2:AuthnStatement AuthnInstant="2018-03-28T09:02:43.155Z" SessionNotOnOrAfter="2018-03-28T09:12:43.154Z">
               <saml2:AuthnContext>
                  <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml2:AuthnContextClassRef>
               </saml2:AuthnContext>
            </saml2:AuthnStatement>

            <saml2:AttributeStatement>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">305000^^^&amp;2.16.756.5.30.1.109.6.5.3.1.1&amp;ISO</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <PurposeOfUse code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">Hans Muster</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">USB</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
        <saml2:AttributeValue xsi:type="xsd:string">urn:oid:2.2.2.2</saml2:AttributeValue>
               </saml2:Attribute>
        <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                  <saml2:AttributeValue>
        <Role code="HCP" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
                  </saml2:AttributeValue>
               </saml2:Attribute>
            </saml2:AttributeStatement>

         </saml2:Assertion>
      </wsse:Security>
   </env:Header>
   <env:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse">
	     <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyType">
	       <PurposeOfUse xmlns="urn:hl7-org:v3" code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.5" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE"/>
	     </saml2:AttributeValue>
	   </saml2:Attribute>
	   <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
             <saml2:AttributeValue>
               <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="DADM" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
             </saml2:AttributeValue>
           </saml2:Attribute>
           <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
             <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</saml2:AttributeValue>
           </saml2:Attribute>
         </wst:Claims>
      </wst:RequestSecurityToken>
   </env:Body>
</env:Envelope>
```

### Request example for Patient

```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
   <env:Header>
      <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
      <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
	<saml2:Assertion ID="\_ca894d9e028d5d6cb2317415d8d7c4b4" IssueInstant="2018-09-12T16:56:06.545Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <saml2:Issuer>https://idp.ihe-europe.net/idp/shibboleth</saml2:Issuer>
            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
               <ds:SignedInfo>
                  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha512"/>
                  <ds:Reference URI="#\_ca894d9e028d5d6cb2317415d8d7c4b4">
                     <ds:Transforms>
                        <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                        <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                           <ec:InclusiveNamespaces PrefixList="xsd" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </ds:Transform>
                     </ds:Transforms>
                     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/>
                     <ds:DigestValue>30zjFYMfxh0UAruBbLeO8cxqDi8F4Orp1HtIL3KEV/luNh1HwOxoIEMpFvfeoa91x7ejabUdQoeh
gB6320GPGg==</ds:DigestValue>
                  </ds:Reference>
               </ds:SignedInfo>
               <ds:SignatureValue>SIGNATURE AS CREATED BY IDP</ds:SignatureValue>
               <ds:KeyInfo>
                  <ds:X509Data>
                     <ds:X509Certificate>IDP\_CERTIFICATE</ds:X509Certificate>
                  </ds:X509Data>
               </ds:KeyInfo>
            </ds:Signature>
            <saml2:Subject>
               <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient" NameQualifier="https://idp.ihe-europe.net/idp/shibboleth" SPNameQualifier="https://sp-clone.ihe-europe.net/shibboleth">AAdzZWNyZXQxbmFe4bvDlqy5W/SSqP0Tckee3DX4qhfEG6yVsHykjCu/XSee1lfEJZ81T0LzUL2uNmiKVRVEBt5B/S33R9ypQ9qz58BhE5t1GgAvaUdLNMT6mOzcnC5528Ww4OlMaKEMh/Ljr/FtjVgOcN4qEDQx</saml2:NameID>
               <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
                  <saml2:SubjectConfirmationData Address="95.128.151.250" InResponseTo="\_20180912165606447" NotOnOrAfter="2018-09-12T17:01:06.550Z" Recipient="https://sp-clone.ihe-europe.net/Shibboleth.sso/SAML2/ECP"/>
               </saml2:SubjectConfirmation>
            </saml2:Subject>
            <saml2:Conditions NotBefore="2018-09-12T16:56:06.545Z" NotOnOrAfter="2018-09-12T17:01:06.545Z">
               <saml2:AudienceRestriction>
                  <saml2:Audience>https://sp-clone.ihe-europe.net/shibboleth</saml2:Audience>
               </saml2:AudienceRestriction>
            </saml2:Conditions>
            <saml2:AuthnStatement AuthnInstant="2018-09-12T16:56:06.541Z" SessionIndex="\_60ad35760f745f98d8c017d8e3471e1c">
               <saml2:SubjectLocality Address="95.128.151.250"/>
               <saml2:AuthnContext>
                  <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</saml2:AuthnContextClassRef>
               </saml2:AuthnContext>
            </saml2:AuthnStatement>
            <saml2:AttributeStatement>
               <saml2:Attribute FriendlyName="uid" Name="urn:oid:0.9.2342.19200300.100.1.1" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">sebibi</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="gender" Name="urn:oid:1.3.6.1.5.5.7.9.3" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">F</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="surname" Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Ebibi-Limani</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="displayName" Name="urn:oid:2.16.840.1.113730.3.1.241" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Salome Anja Ebibi-Limani</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="givenName" Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Salome Anja</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="dateOfBirth" Name="urn:oid:1.3.6.1.5.5.7.9.1" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">19760816140541.344Z</saml2:AttributeValue>
               </saml2:Attribute>
            </saml2:AttributeStatement>
         </saml2:Assertion>
      </wsse:Security>
   </env:Header>
   <env:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
            <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">
               <saml2:AttributeValue xsi:type="xs:anyType" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <PurposeOfUse code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
               </saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
              <saml2:AttributeValue>
        		 <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="PAT" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
              </saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
          	<saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</saml2:AttributeValue>
            </saml2:Attribute>
	    <saml2:Attribute Name="urn:e-health-suisse:principal-id" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">
               <saml2:AttributeValue xsi:type="xs:string" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">761337610455909127^^^SPID&amp;amp;2.16.756.5.30.1.127.3.10.3&amp;amp;ISO</saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute Name="urn:e-health-suisse:principal-name" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">
               <saml2:AttributeValue xsi:type="xs:string" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Salome Anja Ebibi-Limani</saml2:AttributeValue>
            </saml2:Attribute>
         </wst:Claims>
      </wst:RequestSecurityToken>
   </env:Body>
</env:Envelope>
```

### Request example for Representative


```xml
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
   <env:Header>
      <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
      <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:d888b36e-625f-4e25-a166-b27815be357f</wsa:MessageID>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
	<saml2:Assertion ID="\_ca894d9e028d5d6cb2317415d8d7c4b4" IssueInstant="2018-09-12T16:56:06.545Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <saml2:Issuer>https://idp.ihe-europe.net/idp/shibboleth</saml2:Issuer>
            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
               <ds:SignedInfo>
                  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha512"/>
                  <ds:Reference URI="#\_ca894d9e028d5d6cb2317415d8d7c4b4">
                     <ds:Transforms>
                        <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                        <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                           <ec:InclusiveNamespaces PrefixList="xsd" xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </ds:Transform>
                     </ds:Transforms>
                     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/>
                     <ds:DigestValue>30zjFYMfxh0UAruBbLeO8cxqDi8F4Orp1HtIL3KEV/luNh1HwOxoIEMpFvfeoa91x7ejabUdQoeh
gB6320GPGg==</ds:DigestValue>
                  </ds:Reference>
               </ds:SignedInfo>
               <ds:SignatureValue>SIGNATURE AS CREATED BY IDP</ds:SignatureValue>
               <ds:KeyInfo>
                  <ds:X509Data>
                     <ds:X509Certificate>IDP\_CERTIFICATE</ds:X509Certificate>
                  </ds:X509Data>
               </ds:KeyInfo>
            </ds:Signature>
            <saml2:Subject>
               <saml2:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient" NameQualifier="https://idp.ihe-europe.net/idp/shibboleth" SPNameQualifier="https://sp-clone.ihe-europe.net/shibboleth">AAdzZWNyZXQxbmFe4bvDlqy5W/SSqP0Tckee3DX4qhfEG6yVsHykjCu/XSee1lfEJZ81T0LzUL2uNmiKVRVEBt5B/S33R9ypQ9qz58BhE5t1GgAvaUdLNMT6mOzcnC5528Ww4OlMaKEMh/Ljr/FtjVgOcN4qEDQx</saml2:NameID>
               <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
                  <saml2:SubjectConfirmationData Address="95.128.151.250" InResponseTo="\_20180912165606447" NotOnOrAfter="2018-09-12T17:01:06.550Z" Recipient="https://sp-clone.ihe-europe.net/Shibboleth.sso/SAML2/ECP"/>
               </saml2:SubjectConfirmation>
            </saml2:Subject>
            <saml2:Conditions NotBefore="2018-09-12T16:56:06.545Z" NotOnOrAfter="2018-09-12T17:01:06.545Z">
               <saml2:AudienceRestriction>
                  <saml2:Audience>https://sp-clone.ihe-europe.net/shibboleth</saml2:Audience>
               </saml2:AudienceRestriction>
            </saml2:Conditions>
            <saml2:AuthnStatement AuthnInstant="2018-09-12T16:56:06.541Z" SessionIndex="\_60ad35760f745f98d8c017d8e3471e1c">
               <saml2:SubjectLocality Address="95.128.151.250"/>
               <saml2:AuthnContext>
                  <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</saml2:AuthnContextClassRef>
               </saml2:AuthnContext>
            </saml2:AuthnStatement>
            <saml2:AttributeStatement>
               <saml2:Attribute FriendlyName="uid" Name="urn:oid:0.9.2342.19200300.100.1.1" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">sebibi</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="gender" Name="urn:oid:1.3.6.1.5.5.7.9.3" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">F</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="surname" Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Ebibi-Limani</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="displayName" Name="urn:oid:2.16.840.1.113730.3.1.241" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Salome Anja Ebibi-Limani</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="givenName" Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Salome Anja</saml2:AttributeValue>
               </saml2:Attribute>
               <saml2:Attribute FriendlyName="dateOfBirth" Name="urn:oid:1.3.6.1.5.5.7.9.1" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml2:AttributeValue xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">19760816140541.344Z</saml2:AttributeValue>
               </saml2:Attribute>
            </saml2:AttributeStatement>
         </saml2:Assertion>
      </wsse:Security>
   </env:Header>
   <env:Body>
      <wst:RequestSecurityToken xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
         <wst:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</wst:RequestType>
         <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <wsa:EndpointReference xmlns:wsa="http://www.w3.org/2005/08/addressing">
               <wsa:Address>https://localhost:17001/services/iti18</wsa:Address>
            </wsa:EndpointReference>
         </wsp:AppliesTo>
         <wst:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</wst:TokenType>
         <wst:Claims Dialect="http://bag.admin.ch/epr/2017/annex/5/addendum/2">
            <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">
               <saml2:AttributeValue xsi:type="xs:anyType" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <PurposeOfUse code="NORM" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse Verwendungszweck" displayName="Normalzugriff" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
               </saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
              <saml2:AttributeValue>
        		 <Role xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="REP" codeSystem="2.16.756.5.30.1.127.3.10.6" codeSystemName="eHealth Suisse EPR Akteure" displayName="Behandelnde(r)" xsi:type="CE" xmlns="urn:hl7-org:v3"/>
              </saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
          	<saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</saml2:AttributeValue>
            </saml2:Attribute>
         </wst:Claims>
      </wst:RequestSecurityToken>
   </env:Body>
</env:Envelope>
```

## Mock messages on GWT

Messages sent to the simulator can be found in Mock Messages feature of Gazelle Webservice Tester.
This feature is documented at : [https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html).

Messages exchanged with EPR-Assertion-Provider-Simulator can be found by filtering with the actor X-ASSERTION_PROVIDER. 

