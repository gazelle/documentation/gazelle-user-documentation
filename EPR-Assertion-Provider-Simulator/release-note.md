---
title: Release note
subtitle: Assertion Provider Simulator
toolversion: 1.3.0
releasedate: 2023-06-08
author: Malo TOUDIC
function: Software developer
customer: IHE-Europe
reference: KER1-RNO-IHE-ASSERTION_PROVIDER_SIMULATOR
---
# 1.3.0
_Release date: 2023-06-08_

__Story__
* \[[APS-17](https://gazelle.ihe.net/jira/browse/APS-17)\] CH New ordinance EPDBEP-89 Purpose of use DICOM_AUTO

# 1.2.0 
_Release date: 2021-09-14_

## Story
* \[[APS-16](https://gazelle.ihe.net/jira/browse/APS-16)\] Generate CH Assertion with homeCommunityID

# 1.1.9
_Release date: 2020-10-08_

__Bug__
* \[[APS-15](https://gazelle.ihe.net/jira/browse/APS-15)\] Update the location of the faultcode in the XUA mockup



# 1.1.8
_Release date: 2020-09-24_

__Bug__
* \[[APS-14](https://gazelle.ihe.net/jira/browse/APS-14)\] AppliesTo is not a mandatory element in the SAML assertion



# 1.1.7
_Release date: 2020-03-05_

__Bug__

* \[[APS-13](https://gazelle.ihe.net/jira/browse/APS-13)\] One namespace used in Assertion Provider is not correct


# 1.1.6
_Release date: 2020-02-11_

__Bug__

* \[[APS-12](https://gazelle.ihe.net/jira/browse/APS-12)\] The assertion provider doesn't return an assertion for certain HCP



# 1.1.5
_Release date: 2020-01-14_

__Bug__

* \[[APS-10](https://gazelle.ihe.net/jira/browse/APS-10)\] The patient id syntax is too restritive in the Assertion Provider Simulator
* \[[APS-11](https://gazelle.ihe.net/jira/browse/APS-11)\] Assertion Provider Mock should not verify the urn:e-health-suisse:principal-id


# 1.1.4
_Release date: 2019-10-14_

__Bug__

* \[[APS-9](https://gazelle.ihe.net/jira/browse/APS-9)\] Regression in version 1.1.3 of the tool

# 1.1.3
_Release date: 2019-10-10_

__Improvement__

* \[[APS-8](https://gazelle.ihe.net/jira/browse/APS-8)\] Purpose Of Use "AUTO" for TCU

# 1.1.2
_Release date: 2019-09-03_

__Bug__

* \[[APS-6](https://gazelle.ihe.net/jira/browse/APS-6)\] Assertion Provider XUA request fails at validation : wrong PurposeOfUse codesystem

# 1.1.1
_Release date: 2019-08-22_

__Improvement__

* \[[APS-3](https://gazelle.ihe.net/jira/browse/APS-3)\] Implement EPR 1.9

# 1.1.0
_Release date: 2019-08-02_

__Task__

* \[[APS-2](https://gazelle.ihe.net/jira/browse/APS-2)\] [XUA] Create groovy script to record mock transaction

# 66723
_Release date: 2019-03-12_

__Improvement__
* \[[APS-1](https://gazelle.ihe.net/jira/browse/APS-1)\] Implement EPR 1.7 & 1.8
