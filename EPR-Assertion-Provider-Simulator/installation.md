---
title:  Installation Manual
subtitle: Assertion Provider Simulator
author: Youn Cadoret
function: Developer
date: 28/06/2019
toolversion: 1.3.0
version: 0.01
status: to be reviewed
reference: KER1-MAN-IHE-ASSERTION_PROVIDER_SIMULATOR_INSTALLATION_USER
customer: IHE-EUROPE
---

# EPR Assertion Provider MockUp

## Overview

The EPR Assertion Provider MockUp is a SoapUI webservice (mock) that provides SAML Assertion when requested.
The Assertion Provider is based on a backend Java library net.ihe.gazelle.XUAGenerator that need to be deployed on the running SoapUI.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net:8090/STS?wsdl
* default_path: /STS
* default_port: 8090
* default_mock_name: STSBindingMockService
* default_mock_path: /opt/simulators/epr-assertion-provider-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-assertion-provider.log
* default_init.d: /etc/init.d/mock-eCH-XUAGenerator
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR SAML Generator project

### Get the Subversion project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/assertion-provider.git $SAMLGENERATOR_DIR
```

### Deploy EPR SAML Generator in SoapUI

Build the projet

```bash
cd $SAMLGENERATOR_DIR
mvn clean package
```

Copy the jar into the external SoapUI intallation dir

```bash
cp target/epr-saml-generator-X.X.X-jar-with-dependencies.jar $SOAPUI_INSTALL_DIR/bin/ext/.
```

## Install EPR Assertion Provider MockUp

### Get the Subversion project

```bash
svn checkout https://svn.ihe-europe.net/gazelle/EPD/trunk/epr-assertion-provider-mockup/ $EPR_AP_MOCK_DIR
```

### Install libraries required by SoapUI

Copy the external jars (esapi, velocity and postgresql)

```bash
cp $EPR_AP_MOCK_DIR/external_jar/esapi-2.1.0.1.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_AP_MOCK_DIR/external_jar/velocity-1.7.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_AP_MOCK_DIR/external_jar/postgresql-9.3-1102.jdbc4.jar $SOAPUI_INSTALL_DIR/lib/
```

### Prepare the database

Database is not mandatory, but if not configured, it will raise log errors for each recieving request.

```bash
psql -U gazelle postgres
> CREATE DATABASE "assertion-provider" OWNER gazelle ;
> \q
psql -U gazelle assertion-provider < $EPR_AP_MOCK_DIR/sql/schema.sql
```

## Mock as a service

### Prepare the init.d script

Edit the init.d script `$EPR_AP_MOCK_DIR/init.d/assertionProviderMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_AP_MOCK_DIR/init.d/assertionProviderMock /etc/init.d/assertionProviderMock
sudo chmod u+x /etc/init.d/assertionProviderMock
sudo chmod 775 /etc/init.d/assertionProviderMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d assertionProviderMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/assertionProviderMock start
```

To stop the mock

```bash
sudo /etc/init.d/assertionProviderMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/assertionProviderMock status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```

You might need to resolve errors when starting the mock

```bash
sudo mv /root/.soapuios/ /root/.soapuios_old
```
