---
title: User Manual
subtitle: ELM Fhir Simulator
author: Alexandre POCINHO
releasedate: 2024-08-06
toolversion: 1.0.0
function: Engineer
version: 0.1
status: draft
---

# ELM Fhir Simulator

## Description

The ELM FHIR Simulator acts like a server to return queried resources.
It have a validation process to check if the Body is compliant regarding the IHE specification.


## Prerequisite

Before deploying the project the system MUST have:

- a deployed Gazelle Datahouse application: <https://gitlab.inria.fr/gazelle/private/kereval/gazelle-datahouse>
- a `index_configs.json` file in `/opt/datahouse` with the following content

```json
[
    {
    	"name": "type",
    	"itemType": "FHIR_DOCUMENT_REFERENCE",
    	"fieldType": "STRING",
    	"fullName": "type",
    	"value": "{\"type\": 1}"
    },
    {
    	"name": "date",
    	"itemType": "FHIR_DOCUMENT_REFERENCE",
    	"fieldType": "DATE",
    	"fullName": "date",
    	"value": "{\"date\": 1}"
    },
    {
    	"name": "last_updated",
    	"itemType": "FHIR_DOCUMENT_REFERENCE",
    	"fieldType": "DATE",
    	"fullName": "content.meta.lastUpdated",
    	"value": "{\"meta.lastUpdated\": 1}"
    },
    {
    	"name": "identifier_system",
    	"itemType": "FHIR_DOCUMENT_REFERENCE",
    	"fieldType": "STRING",
    	"fullName": "content.identifier.system",
    	"value": "{\"identifier.system\": 1}"
    },
    {
    	"name": "identifier_value",
    	"itemType": "FHIR_DOCUMENT_REFERENCE",
    	"fieldType": "STRING",
    	"fullName": "content.identifier.value",
    	"value": "{\"identifier.value\": 1}"
    },
    {
    	"name": "elm_status_value",
    	"itemType": "FHIR_DOCUMENT_REFERENCE",
    	"fieldType": "STRING",
    	"fullName": "content.extension.extension.valueCode",
    	"value": "{\"extension.extension.valueCode\": 1}"
    }
]
```

- a deployed Matchbox application: <https://github.com/ahdis/matchbox> with CH-ELM IG imported and a set up EVSClient application to manage Matchbox validation for ch-elm


- having this parameter in system environment (or in .env in case of Docker deployment)

```properties
elm.server.base.url=https://www.example.com/elm-simulator/fhir
elm.done.elapse.time.ms=10000
datahouse.page.limit=1000
elm.server.evs.endpoint=https://example.com/evs/rest/validations
elm.server.validation.enabled=true
fhir.validation.name=MatchboxV3
elm.server.validation.profile.publish.documentReference=http://fhir.ch/ig/ch-elm/StructureDefinition/PublishDocumentReference|1.3.1
```

| Property | Value | Remarks |
|:-----------:|:-----------:|:-----------:|
|elm.server.base.url|https://www.example.com/elm-simulator/fhir|The URL of the deployed application|
|elm.done.elapse.time.ms|10000|To simulate the processing in step 4, a timer in millisecond is set. After this specific delay Document elm-status will be automatically updated to "complete" status|
|datahouse.page.limit|10000|The page limit of datahouse, SHOULD always be superior to elm pagination|
|elm.server.evs.endpoint|https://example.com/evs/rest/validations| The URL of Gazelle EVS Client to send validation request|
|elm.server.validation.enabled|true| Activate the validation process, should always be true in production|
|fhir.validation.name|MatchboxV3| Name of the validation service set in evs|
|elm.server.validation.profile.publish.documentReference|http://fhir.ch/ig/ch-elm/StructureDefinition/PublishDocumentReference\|1.3.1|The canoncial URL of Validation Profile for CH:ELM publish docuementReference|




## CH-ELM API

- [Specification](http://fhir.ch/ig/ch-elm/api.html)

## Overview

Here is a quick overview of the available functionality from MCSD Fhir Simulator

| Operation | HTTP Methods | URL to call | Entry parameter | Returned value | Remarks|
|:-----------:|:-----------:|:-----------:|:-----------:|:-----------:|:-----------:|
|Publish A DocumentReference|POST|https://example.com/elm-simulator/fhir/ch/DocumentReference|X|- If validation passed: The given DocumentReference with a 201 Created HTTP Code and elm-status=in-progress<br/>- If validation failed : a link to the validation report with an failed status|X|
|Get DocumentReference|GET|https://example.com/elm-simulator/fhir/ch/DocumentReference/{id}|id = id of the resource|The queried resource|X|
|Search DocumentReference|GET|https://example.com/elm-simulator/fhir/ch/DocumentReference?param1=value1[&paramN=valueN..]|elm-status = {completed \| in-progress \| failed} <br/><br/> date = {dateTime format} <br/> <br/> _lastUpdated = {dateTime format} <br/> <br/> identifier = {system\|value}|The queried resource with the given parameters|the params can also be used to sort the result with "_sort=", pagination is also supported with "_count=" parameter|



Capability statement of the application can be found with : <https://example.com/elm-simulator/fhir/ch/metadata>








