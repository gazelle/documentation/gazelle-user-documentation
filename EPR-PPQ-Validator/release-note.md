---
title:  Release note
subtitle: PPQ Validator
author: Youn Cadoret
function: Developer
date: 02/06/2019
toolversion: R2
version: 1.0
status: To be reviewed
reference: KER1-RNO-IHE-PPQ_VALIDATOR
customer: IHE-EUROPE
---

# R2
_Release date: 2018-02-14_

__Remarks__

__Improvement__

* \[[CHPPQ-13](https://gazelle.ihe.net/jira/browse/CHPPQ-13)\] Update PPQ Schematrons



# R1
_Release date: 2017-06-09_

__Remarks__

__Task__

* \[[CHPPQ-3](https://gazelle.ihe.net/jira/browse/CHPPQ-3)\] Create schematrons for PPQ messages
* \[[CHPPQ-4](https://gazelle.ihe.net/jira/browse/CHPPQ-4)\] Create sample data for testing PPQ schematrons
* \[[CHPPQ-9](https://gazelle.ihe.net/jira/browse/CHPPQ-9)\] Create junit tests for PPQ schematrons


