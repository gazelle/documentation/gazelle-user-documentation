---
title:  User Manual
subtitle: XCPDRESPSimulator
author: Abderrazek BOUFAHJA
function: Engineer
date: 31/01/2017
toolversion: 0.2-GA
version: 1.00
status: Approved document
reference: KER1-MAN-IHE-CDA_XCPDRESP_USER
customer: IHE-EUROPE
---

The XCPD profile involves two actors, the [*XCPD Initiating Gateway*](https://gazelle.ehdsi.eu/XDStarClient/epsos2/epsos2xcpd.seam) and the [*XCPD Reponding Gateway*](https://gazelle.ehdsi.eu/XCPDRESPSimulator).

There is a simulator available for both actors.

The XCPD Responding Gateway has the following configuration:

* Unsecured Endpoint: [*https://gazelle.ehdsi.eu/XCPDRESPSimulator-XCPDRESPSimulator/RespondingGatewayPortTypeImpl?wsdl*](http://gazelle.ehdsi.eu/XCPDRESPSimulator-XCPDRESPSimulator/RespondingGatewayPortTypeImpl?wsdl)

* Secured endpoint: [*https://gazelle.ehdsi.eu:1443/xcpd*](https://gazelle.ehdsi.eu:1443/xcpd)

* Home Community ID: 2.16.17.710.812.1000.990.1

The XCPD Initiating gateway is integrated in [*XDStarClient*](https://gazelle.ehdsi.eu/XDStarClient) tool.

You have to go to the menu Simulator --&gt; epSOS-2 --&gt; Patient Identification and Authentication Service
