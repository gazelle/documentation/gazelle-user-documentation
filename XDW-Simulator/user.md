---
title:  User manual
subtitle: XDW Simulator
author: Abderrazek BOUFAHJA
date: 30/12/2016
toolversion: 1.1.x
function: Engineer
version: 1.02
status: Approved
reference: KER1-MAN-IHE-XDW_SIMULATOR_USER-1_02
customer: IHE-EUROPE
---

# Introduction

This application has multiple purposes.

* It hosts the models of the XDW documents and offers the documentation and the edition interface for those models.
* Documentation of the model based contraints
* Generation and Edition of basic XDW documents

Please email questions to the IHE Europe Connectathon Manager (Eric Poiseau) for guidance through the process.

![](./media/xdw1.png)

# XDW Documentation

XDWSimulator offer a documentation of XDW templates, XDW constraints, and XDW structure.

## XDW Structure

XDWSimulator offers a complete documentation of the structure of XDW standard. It is the same description that we find in the schema of XDW. The documentation is generated from the model UML that describes the XDW standard. To go to documentation you have to go to menu -&gt; XDW Documentation -&gt; XDW - Document Structure

![](./media/xdw2.png)

The html page that describe the Root element (XDW.WorkflowDocument) looks like this :

![](./media/xdw3.png)

## XDW Templates Documentation

All the templates described and validated by XDWSimulator are documented by the tool.

To go to the documentation you have to go  to : menu -&gt; XDW Documentation -&gt; [*XDW Templates Documentation*](https://gazelle.ihe.net/XDWSimulator/docum/templates/listTemplates.seam), which looks like this :

![](./media/xdw4.png)

The documentation of the templates is generated automatically from the model UML, which describes the template and its constraints.

## XDW constraints documentation

The XDW constraint documentation is a page that contains all the constraint validated by XDWSimulator. This page allows to do search of a constraint by its name, its dependency to a class or a package, or by keyword from the description, etc.

To go to the page of constraint documentation ou have to go to the menu -&gt; XDW Documentation -&gt; [*XDW Constraints Search*](https://gazelle.ihe.net/XDWSimulator/docum/constraints/constraints.seam)

![](./media/xdw5.png)

# XDW Edition

XDWSimulator offers the possibility to edit and to create basic XDW documents. To do so, you have to go to menu -&gt; Tools -&gt; XDW Edition

![](./media/xdw6.png)

You can Generate an empty file, or you can uplaod a XDW file :

![](./media/xdw7.png)

![](./media/xdw8.png)

To use the module of edition, you have to click on the button edit.

A new page will be opened containg the possibility to edit element by element each sub element of the root XDW.WorkflowDocument .

![](./media/xdw9.png)

The same page allows to download the modified XDW document, or to validate it, or to save it.

The editor of XDW document is generated also from the model of description of the structure of XDW document.

# View XDW Document

XDWSimulator provides a document viewer. You need to go to menu -&gt; Tools -&gt; View XDW-doc

![](./media/xdw11.png)

You need to upload a document and then click on the button View.

We have two presentation : HTML presentation and diagram presentation

![](./media/xdw12.png)

![](./media/xdw13.png)

# XDW Web service

XDWSimulator include a web service of validation, that could be used as a standalone without using EVSClient, to validate an XDW document

![](./media/xdw10.png)

[Link to the Webservice](https://gazelle.ihe.net/XDWSimulator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl)

# Disclaimer

The XDWSimulator Application is an experimental system.

IHE-Europe assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or any other characteristic.

We would appreciate acknowledgement if the software is used.
